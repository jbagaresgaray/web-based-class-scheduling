<?php
session_start();
include('lib.php');

header("Content-Type: application/json");

switch ($_POST['command']) {

	case 'InsertSchedule':
		InsertSchedule($_POST['data']);
		break;
	case 'UpdateSchedule':
		UpdateSchedule($_POST['SchedID'],$_POST['data']);
		break;
	case 'DeleteSchedule':
		DeleteSchedule($_POST['SchedID']);
		break;
	case 'getScheduleInformation':
		getScheduleInformation($_POST['SchedID']);
		break;
	case 'GetAllSchedule':
		GetAllSchedule($_POST['TermID'],$_POST['page']);
		break;
	case 'FillClassSection':
		FillClassSection($_POST['ProgramID'],$_POST['TermID']);
		break;
	case 'FillSubjectOffering':
		FillSubjectOffering($_POST['SectionID'],$_POST['TermID'],$_POST['ProgramID']);
		break;
	case 'select_ScheduleAYTerm':
		select_ScheduleAYTerm();
		break;
	case 'select_ScheduleAcadPrograms':
		select_ScheduleAcadPrograms();
		break;
	case 'select_RoomEvents':
		select_RoomEvents();
		break;
	case 'select_Faculty':
		select_Faculty();
		break;
	case 'select_Building':
		select_Building();
		break;
	case 'select_BuildingRooms':
		select_BuildingRooms($_POST['BldgID']);
		break;
	case 'select_ScheduleYearLevel':
		select_ScheduleYearLevel();
		break;
	case 'select_ScheduleYearLevelTerm':
		select_ScheduleYearLevelTerm($_POST['YearLevelID']);
		break;
	case 'RoomInUse':
		RoomInUse($_POST['TermID'],$_POST['Room'],$_POST['Day'],$_POST['From'],$_POST['sTo']);
		break;
	case 'FacultyInUse':
		FacultyInUse($_POST['TermID'],$_POST['Faculty'],$_POST['Day'],$_POST['From'],$_POST['sTo']);
		break;
	case 'SectionInUse':
		SectionInUse($_POST['ScheduleID'],$_POST['TermID'],$_POST['SectionID'],$_POST['Day'],$_POST['From'],$_POST['sTo']);
		break;
	case 'AddSchedule1':
		AddSchedule1($_POST['data']);
		break;
	case 'AddSchedule2':
		AddSchedule2($_POST['data']);
		break;
	case 'AddSchedule3':
		AddSchedule3($_POST['data']);
		break;
	case 'GetRoomScheduleConflicts':
		GetRoomScheduleConflicts($_POST['TermID'],$_POST['RoomID'],$_POST['Day'],$_POST['TimeStart'],$_POST['TimeEnd']);
		break;
	case 'GetFacultyScheduleConflicts':
		GetFacultyScheduleConflicts($_POST['TermID'],$_POST['FacultyID'],$_POST['Day'],$_POST['TimeStart'],$_POST['TimeEnd']);
		break;
	case 'GetClassScheduleConflicts':
		GetClassScheduleConflicts($_POST['ScheduleID'],$_POST['TermID'],$_POST['SectionID'],$_POST['Day'],$_POST['TimeStart'],$_POST['TimeEnd']);
		break;
	case 'RemoveSchedule':
		RemoveSchedule($_POST['SubjectOfferingID']);
		break;
	case 'SubjectOfferingSelfConflicts':
		SubjectOfferingSelfConflicts($_POST['TermID'],$_POST['ScheduleID'],$_POST['Day'],$_POST['From'],$_POST['sTo'],$_POST['CurrentSched']);
		break;

	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}

exit();


function select_ScheduleAYTerm(){
	$result = query("SELECT * FROM tblayterm;");
	print json_encode($result);
}

function select_ScheduleYearLevel(){
	$result = query("SELECT * FROM tblyearlevel;");
	print json_encode($result);
}

function select_ScheduleYearLevelTerm($YearLevelID){
	$result = query("SELECT * FROM tblyearlevelterm WHERE YearLevelID='%s';",$YearLevelID);
	print json_encode($result);
}


function select_ScheduleAcadPrograms(){
	$result = query("SELECT * FROM tblprograms;");
	print json_encode($result);
}

function FillClassSection($ProgramID,$TermID){
	$result = query("SELECT SectionTitle,SectionID,fnYearLevel(YearLevelID) AS YearLevel,YearLevelID FROM tblsection
			WHERE ProgramID='%s' AND TermID='%s'",$ProgramID,$TermID);
	print json_encode($result);
}

function select_Building(){
	$result = query("SELECT BldgName,BldgID FROM tblbuilding ORDER BY BldgID");
	print json_encode($result);
}

function select_BuildingRooms($BldgID){
	$result = query("SELECT CONCAT(RoomNo,' - ',RoomName) AS RoomName,RoomID FROM tblroom WHERE BldgID='%s' ORDER BY RoomNo",$BldgID);
	print json_encode($result);
}

function select_Faculty(){
	$result = query("SELECT fnEmployeeName(EmployeeID) AS TeacherName,TeacherID FROM tblteacher ORDER BY TeacherID;");
	print json_encode($result);
}

function select_RoomEvents(){
	$result = query("SELECT RoomType,RoomTypeID FROM tblroomtypes;");
	print json_encode($result);
}

function InsertSchedule($data){
    $result = query("INSERT INTO tblclassschedule VALUES (null,'%s','%s','%s','%s','%s','%s');",
    	$data[0]['TermID'],$data[0]['SubjectID'],$data[0]['SectionID'],$data[0]['Limit'],$data[0]['IsSpecialClasses'],
    	$data[0]['SchedTimeStart'],$data[0]['SchedTimeEnd'],$data[0]['TeacherID'],$data[0]['RoomID'],$data[0]['Days'],$data[0]['EventID1'],$data[0]['Sched1'],
    	$data[0]['SchedTimeStart2'],$data[0]['SchedTimeEnd2'],$data[0]['TeacherID2'],$data[0]['RoomID2'],$data[0]['Days2'],$data[0]['EventID2'],$data[0]['Sched2'],
    	$data[0]['SchedTimeStart3'],$data[0]['SchedTimeEnd3'],$data[0]['TeacherID3'],$data[0]['RoomID3'],$data[0]['Days3'],$data[0]['EventID3'],$data[0]['Sched3'],
    	$data[0]['SchedTimeStart4'],$data[0]['SchedTimeEnd4'],$data[0]['TeacherID4'],$data[0]['RoomID4'],$data[0]['Days4'],$data[0]['EventID4'],$data[0]['Sched4'],
    	$data[0]['SchedTimeStart5'],$data[0]['SchedTimeEnd5'],$data[0]['TeacherID5'],$data[0]['RoomID5'],$data[0]['Days5'],$data[0]['EventID5'],$data[0]['Sched5'],
    	$data[0]['OverRideConflict'],$data[0]['IsDissolved'],$data[0]['PostedBy'],$data[0]['DatePosted'],$data[0]['RoomPostedBy'],
    	$data[0]['RoomDatePosted'],$data[0]['FacultyDatePosted'],$data[0]['FacultyPostedBy'],$data[0]['CreatedBy'],$data[0]['CreationDate'],
    	$data[0]['ModifiedDate'],$data[0]['ModifiedBy']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Schedule went failed');
	}
}

function UpdateSchedule($SchedID,$data){
    $result = query("UPDATE tblclassschedule
    	SET TermID='%s',SubjectID='%s',SectionID='%s',Limit='%s',IsSpecialClasses='%s',
    	SchedTimeStart='%s',SchedTimeEnd='%s',TeacherID='%s',RoomID='%s',Days='%s',EventID1='%s',Sched1='%s',
    	SchedTimeStart2='%s',SchedTimeEnd2='%s',TeacherID2='%s',RoomID2='%s',Days2='%s',EventID2='%s',Sched2='%s',
    	SchedTimeStart3='%s',SchedTimeEnd3='%s',TeacherID3='%s',RoomID3='%s',Days3='%s',EventID3='%s',Sched3='%s',
    	SchedTimeStart4='%s',SchedTimeEnd4='%s',TeacherID4='%s',RoomID4='%s',Days4='%s',EventID4='%s',Sched4='%s',
    	SchedTimeStart5='%s',SchedTimeEnd5='%s',TeacherID5='%s',RoomID5='%s',Days5='%s',EventID5='%s',Sched5='%s',
    	OverRideConflict='%s',IsDissolved='%s',PostedBy='%s',DatePosted='%s',RoomPostedBy='%s',
    	RoomDatePosted='%s',FacultyDatePosted='%s',FacultyPostedBy='%s',ModifiedDate='%s',ModifiedBy='%s',
    	WHERE SubjectOfferingID ='%s'",
        $data[0]['TermID'],$data[0]['SubjectID'],$data[0]['SectionID'],$data[0]['Limit'],$data[0]['IsSpecialClasses'],
    	$data[0]['SchedTimeStart'],$data[0]['SchedTimeEnd'],$data[0]['TeacherID'],$data[0]['RoomID'],$data[0]['Days'],$data[0]['EventID1'],$data[0]['Sched1'],
    	$data[0]['SchedTimeStart2'],$data[0]['SchedTimeEnd2'],$data[0]['TeacherID2'],$data[0]['RoomID2'],$data[0]['Days2'],$data[0]['EventID2'],$data[0]['Sched2'],
    	$data[0]['SchedTimeStart3'],$data[0]['SchedTimeEnd3'],$data[0]['TeacherID3'],$data[0]['RoomID3'],$data[0]['Days3'],$data[0]['EventID3'],$data[0]['Sched3'],
    	$data[0]['SchedTimeStart4'],$data[0]['SchedTimeEnd4'],$data[0]['TeacherID4'],$data[0]['RoomID4'],$data[0]['Days4'],$data[0]['EventID4'],$data[0]['Sched4'],
    	$data[0]['SchedTimeStart5'],$data[0]['SchedTimeEnd5'],$data[0]['TeacherID5'],$data[0]['RoomID5'],$data[0]['Days5'],$data[0]['EventID5'],$data[0]['Sched5'],
    	$data[0]['OverRideConflict'],$data[0]['IsDissolved'],$data[0]['PostedBy'],$data[0]['DatePosted'],$data[0]['RoomPostedBy'],
    	$data[0]['RoomDatePosted'],$data[0]['FacultyDatePosted'],$data[0]['FacultyPostedBy'],$data[0]['ModifiedDate'],$data[0]['ModifiedBy'],$SchedID);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating Schedule went failed');
	}
}

function DeleteSchedule($SchedID){
    $result = query("DELETE FROM tblclassschedule WHERE SubjectOfferingID ='%s'",$SchedID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting Schedule went failed');
	}
}

function RemoveSchedule($SubjectOfferingID){
	$result = query("UPDATE tblclassschedule SET
		SchedTimeStart=null,SchedTimeEnd=null,TeacherID=null,RoomID=null,Days=null,EventID1=null,Sched1=null,
		SchedTimeStart2=null,SchedTimeEnd2=null,TeacherID2=null,RoomID2=null,Days2=null,EventID2=null,Sched2=null,
		SchedTimeStart3=null,SchedTimeEnd3=null,TeacherID3=null,RoomID3=null,Days3=null,EventID3=null,Sched3=null,
		OverRideConflict=0 WHERE SubjectOfferingID ='%s'",$SubjectOfferingID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Schedule successfully deleted!'));
	} else {
		errorJson('Deleting Schedule went failed');
	}
}

function getScheduleInformation($SchedID){
	$result = query("SELECT * FROM tblclassschedule WHERE SubjectOfferingID='$SchedID' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Schedule Information found!');
	}
}


function GetAllSchedule($TermID,$page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT * FROM tblclassschedule WHERE TermID='$TermID';");
	$result1 = query("SELECT * FROM tblclassschedule WHERE TermID='$TermID' LIMIT $start, $limit;");
	$rows = count($result['result']);

	$paging = pagination($limit,$adjacent,$rows,$page);

	print json_encode(array('result' =>$result,'pagination'=>$paging));

}

function FillSubjectOffering($SectionID, $TermID, $ProgramID){
	$result = query("SELECT S.SectionTitle,SUB.SubjectID,SUB.SubjectCode,SUB.SubjectTitle,SUB.CreditUnits,SUB.LabUnits,SUB.Units,CD.SubjectOfferingID,CD.`Limit`,S.ProgramID,S.SectionID,CD.IsSpecialClasses,CD.IsDissolved,CD.OverrideConflict,S.TermID,CONCAT(IFNULL(CD.Sched1,''),' ',IFNULL(CD.Sched2,''),' ',IFNULL(CD.Sched3,'')) AS Schedule
		FROM tblsection AS S INNER JOIN tblclassschedule AS CD ON S.SectionID = CD.SectionID  INNER JOIN tblsubject AS SUB ON SUB.SubjectID = CD.SubjectID
		WHERE S.SectionID LIKE '%s' AND S.TermID='%s' AND S.ProgramID='%s';",$SectionID, $TermID, $ProgramID);
	print json_encode($result);
}

function SubjectOfferingSelfConflicts($sTermID,$ScheduleID,$Day,$From,$sTo,$CurrentSched){
	$result = query("CALL GetSubjectOfferingSelfConflicts('%s','%s','%%%s%%','%s','%s','%s')",$sTermID,$ScheduleID,$Day,$From,$sTo,$CurrentSched);
	if (count($result['result'])>0) {
		print json_encode(array('result' =>$result,'success'=>true));
	} else {
		print json_encode(array('success'=>false));
	}
}

function RoomInUse($sTermID,$Room,$Day,$From,$sTo){
	$result = query("CALL GetRoomScheduleConflicts('%s','%s','%%%s%%','%s','%s')",$sTermID,$Room,$Day,$From,$sTo);
	if (count($result['result'])>0) {
		print json_encode(array('result' =>$result,'success'=>true));
	} else {
		print json_encode(array('success'=>false));
	}
}

function FacultyInUse($sTermID,$Faculty,$Day,$From,$sTo){
	$result = query("CALL GetFacultyScheduleConflicts('%s','%s','%%%s%%','%s','%s')",$sTermID,$Faculty,$Day,$From,$sTo);
	if (count($result['result'])>0) {
		print json_encode(array('result' =>$result,'success'=>true));
	} else {
		print json_encode(array('success'=>false));
	}
}

function SectionInUse($ScheduleID,$sTermID,$SectionID,$Day,$From,$sTo){
	$result = query("CALL GetClassScheduleConflicts('%s','%s','%s','%%%s%%','%s','%s')",$ScheduleID,$sTermID,$SectionID,$Day,$From,$sTo);
	if (count($result['result'])>0) {
		print json_encode(array('result' =>$result,'success'=>true));
	} else {
		print json_encode(array('success'=>false));
	}
}

function GetRoomScheduleConflicts($TermID,$RoomID,$sDay,$TimeStart,$TimeEnd){
	$result = query("CALL GetRoomScheduleConflicts('%s','%s','%%%s%%','%s','%s')",$sTermID,$Room,$Day,$From,$sTo);

	print json_encode($result);
}

function GetClassScheduleConflicts($ScheduleID,$TermID,$SectionID,$sDay,$TimeStart,$TimeEnd){
	$result = query("CALL GetClassScheduleConflicts('%s','%s','%s','%%%s%%','%s','%s')",$ScheduleID,$TermID,$SectionID,$sDay,$TimeStart,$TimeEnd);
	print json_encode($result);
}

function GetFacultyScheduleConflicts($TermID,$FacultyID,$sDay,$TimeStart,$TimeEnd){
	$result = query("CALL GetFacultyScheduleConflicts('%s','%s','%%%s%%','%s','%s')",$sTermID,$Faculty,$Day,$From,$sTo);
	print json_encode($result);
}


function AddSchedule1($data){
	$result = query("UPDATE tblclassschedule SET SchedTimeStart='%s',SchedTimeEnd='%s',Days='%s',Sched1='%s',EventID1='%s',TeacherID='%s',RoomID='%s',OverRideConflict='%s',IsDissolved='%s'
		WHERE SubjectOfferingID ='%s'",
		$data[0]['SchedTimeStart'],$data[0]['SchedTimeEnd'],$data[0]['Days'],$data[0]['Sched1'],$data[0]['EventID1'],$data[0]['TeacherID'],$data[0]['RoomID'],$data[0]['OverrideConflict'],$data[0]['IsDissolved'],$data[0]['SubjectOfferingID']);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'SCHEDULE [1]: Class Schedule successfully saved!'));
	} else {
		errorJson('SCHEDULE [1]: Adding Class Schedule went failed');
	}
}

function AddSchedule2($data){
	$result = query("UPDATE tblclassschedule SET SchedTimeStart2='%s',SchedTimeEnd2='%s',Days2='%s',Sched2='%s',EventID2='%s',TeacherID2='%s',RoomID2='%s',OverRideConflict='%s',IsDissolved='%s'
		WHERE SubjectOfferingID ='%s'",
		$data[0]['SchedTimeStart'],$data[0]['SchedTimeEnd'],$data[0]['Days'],$data[0]['Sched1'],$data[0]['EventID1'],$data[0]['TeacherID'],$data[0]['RoomID'],$data[0]['OverrideConflict'],$data[0]['IsDissolved'],$data[0]['SubjectOfferingID']);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'SCHEDULE [2]: Class Schedule successfully saved!'));
	} else {
		errorJson('SCHEDULE [2]: Adding Class Schedule went failed');
	}
}

function AddSchedule3($data){
	$result = query("UPDATE tblclassschedule SET SchedTimeStart3='%s',SchedTimeEnd3='%s',Days3='%s',Sched3='%s',EventID3='%s',TeacherID3='%s',RoomID3='%s',OverRideConflict='%s',IsDissolved='%s'
		WHERE SubjectOfferingID ='%s'",
		$data[0]['SchedTimeStart'],$data[0]['SchedTimeEnd'],$data[0]['Days'],$data[0]['Sched1'],$data[0]['EventID1'],$data[0]['TeacherID'],$data[0]['RoomID'],$data[0]['OverrideConflict'],$data[0]['IsDissolved'],$data[0]['SubjectOfferingID']);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'SCHEDULE [3]: Class Schedule successfully saved!'));
	} else {
		errorJson('SCHEDULE [3]: Adding Class Schedule went failed');
	}
}


?>
