<?php

function InsertSubject($data){
	$result = query("INSERT INTO tblsubject VALUES (null,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
    	$data[0]['SubjectCode'],$data[0]['SubjectTitle'],$data[0]['Description'],
    	$data[0]['InActive'],$data[0]['Units'],$data[0]['CreditUnits'],
    	$data[0]['LecHrs'],$data[0]['LabUnits'],$data[0]['LabHrs'],$data[0]['SubjectMode'],$data[0]['IsNonAcademic']
    	,$data[0]['CreatedBy'],$data[0]['CreationDate'],$data[0]['ModifiedBy'],$data[0]['ModifiedDate']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Subject went failed');
	}
}

function UpdateSubject($data,$SubjectID){
	$result = query("UPDATE tblsubject set SubjectCode = '%s',SubjectTitle = '%s',Description = '%s',InActive = '%s',
				Units = '%s',CreditUnits = '%s',LecHrs = '%s',LabUnits = '%s',LabHrs = '%s',
				SubjectMode = '%s',IsNonAcademic = '%s',ModifiedBy = '%s',ModifiedDate = '%s' Where SubjectID = '%s'",
				$data[0]['SubjectCode'],$data[0]['SubjectTitle'],$data[0]['Description'],$data[0]['InActive'],$data[0]['Units'],$data[0]['CreditUnits'],
				$data[0]['LecHrs'],$data[0]['LabUnits'],$data[0]['LabHrs'],$data[0]['SubjectMode'],$data[0]['IsNonAcademic'],$data[0]['ModifiedBy'],
				$data[0]['ModifiedDate'],$SubjectID);
	if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Updating Subject went failed');
	}
}

function InsertSubjectMode($data){
	$result = query("INSERT INTO tblsubjectmode VALUES (null,'%s','%s');",
    	$data[0]['Description'],$data[0]['ShortName']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Subject Mode went failed');
	}
}

function UpdateSubjectMode($data,$SubjectMode){
	$result = query("UPDATE tblsubjectmode set Description = '%s',ShortName = '%s' Where SubjectMode = '%s'",
				$data[0]['Description'],$data[0]['ShortName'],$SubjectMode);
	if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Updating Subject Mode went failed');
	}
}

function DeleteSubject($SubjectID){
	$result = query("DELETE FROM tblsubject WHERE SubjectID ='%s'",$SubjectID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting an Subject went failed');
	}
}

function DeleteSubjectMode($SubjectMode){
	$result = query("DELETE FROM tblsubjectmode WHERE SubjectMode ='%s'",$SubjectMode);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting an Subject Mode went failed');
	}
}

function getSubject($ID){
	$result = query("SELECT * FROM tblsubject Where SubjectID = $ID;");
	if (count($result['result'])>0) {


		print json_encode($result);
	} else {
		errorJson('No Subject Record found!');
	}
}

function fecthSubjectMode(){
	$result = query("SELECT * FROM tblsubjectmode;");
	if (count($result['result'])>0) {


		print json_encode($result);
	} else {
		errorJson('No Subject Mode Record found!');
	}
}

function getSubjectMode($SubjectID){
	$result = query("SELECT * FROM tblsubjectmode Where SubjectMode = '%s';",$SubjectID);
	if (count($result['result'])>0) {

		print json_encode($result);
	} else {
		errorJson('No Subject Mode Record found!');
	}
}

function fecthSubject($page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT S.* FROM tblsubject S LEFT JOIN tblsubjectmode B ON S.SubjectMode = B.SubjectMode WHERE InActive <> 1;");
	$result1 = query("SELECT S.* , IFNULL(B.Description,'') AS Description,IF(S.IsNonAcademic,'YES','NO') AS IsNonAcademicSubj
	 FROM tblsubject S LEFT JOIN tblsubjectmode B ON S.SubjectMode = B.SubjectMode WHERE InActive <> 1 LIMIT $start, $limit;");
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('result' =>$result1,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching Subject failed');
	}
}

function SearchSubject($value){
	$result = query("SELECT * FROM tblsubject WHERE SubjectCode = '$value';");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Subject Building found!');
	}
}

?>
