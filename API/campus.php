<?php

function InsertCampus($data){
	$newimage = $data[0]['CampusLogo'];
	switch ($data[0]['image_type']) {
		case 'image/jpg':
			$newimage = str_replace('data:image/jpg;base64,','', $newimage);
			break;
		case 'image/jpeg':
			$newimage = str_replace('data:image/jpeg;base64,','', $newimage);
			break;
		case 'image/png':
			$newimage = str_replace('data:image/png;base64,','', $newimage);
			break;
		case 'image/gif':
			$newimage = str_replace('data:image/gif;base64,','', $newimage);
			break;
		default:
			print json_encode(array('success'=>false,'msg'=>'Invalid file type'));
			return;
			break;
	}
	$newimage = base64_decode($newimage);

    $result = query("INSERT INTO tblcampus VALUES (null,0,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
    	$data[0]['CampusName'],$data[0]['Acronym'],$data[0]['ShortName'],
    	$data[0]['CampusHead'],$data[0]['Barangay'],$data[0]['TownCity'],
    	$data[0]['District'],$data[0]['Province'],$data[0]['ZipCode'],
    	$data[0]['MailingAddress'],$data[0]['Email'],$data[0]['TelNo'],
    	$data[0]['FaxNo'],$data[0]['SchoolWebsite'],$data[0]['CampusCode'],$newimage);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Campus Information went failed');
	}
}

function UpdateCampus($data){

	$newimage = $data[0]['CampusLogo'];
	switch ($data[0]['image_type']) {
		case 'image/jpg':
			$newimage = str_replace('data:image/jpg;base64,','', $newimage);
			break;
		case 'image/jpeg':
			$newimage = str_replace('data:image/jpeg;base64,','', $newimage);
			break;
		case 'image/png':
			$newimage = str_replace('data:image/png;base64,','', $newimage);
			break;
		case 'image/gif':
			$newimage = str_replace('data:image/gif;base64,','', $newimage);
			break;
		case '  ';
			$newimage = '';
			break;
		default:
			print json_encode(array('success'=>false,'msg'=>'Invalid file type'));
			return;
			break;
	}
	$newimage = base64_decode($newimage);

    $result = query("UPDATE tblcampus SET CampusName='%s',Acronym='%s',ShortName='%s',
    	CampusHead='%s',Barangay='%s',TownCity='%s',
    	District='%s',Province='%s',ZipCode='%s',
    	MailingAddress='%s',Email='%s',TelNo='%s',
    	FaxNo='%s',SchoolWebsite='%s',CampusCode='%s',
    	CampusLogo='%s'",
        $data[0]['CampusName'],$data[0]['Acronym'],$data[0]['ShortName'],
    	$data[0]['CampusHead'],$data[0]['Barangay'],$data[0]['TownCity'],
    	$data[0]['District'],$data[0]['Province'],$data[0]['ZipCode'],
    	$data[0]['MailingAddress'],$data[0]['Email'],$data[0]['TelNo'],
    	$data[0]['FaxNo'],$data[0]['SchoolWebsite'],$data[0]['CampusCode'],$newimage);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating Campus Information went failed');
	}
}

function DeleteCampus(){
    $result = query("DELETE FROM tblcampus;");
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting Campus Information went failed');
	}
}

function getCampusInformation(){
	$result = query("SELECT * FROM tblcampus LIMIT 1;");
	if (count($result['result'])>0) {


		print json_encode($result);
	} else {
		errorJson('No Campus Record found!');
	}
}

?>
