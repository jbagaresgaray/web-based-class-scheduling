<?php
session_start();
include('lib.php');


header("Content-Type: application/json");

switch ($_POST['command']) {

	/* --------------------- TABLE AY TERM ------------------------*/
	case 'InsertUserGroup':
		InsertUserGroup($_POST['mug_name'],$_POST['data']);
		break;
	case 'UpdateUserGroup':
		UpdateUserGroup($_POST['mug_id'],$_POST['mug_name'],$_POST['data']);
		break;
	case 'DeleteUserGroup':
		DeleteUserGroup($_POST['mug_id']);
		break;
	case 'GetAllUserGroup':
		GetAllUserGroup($_POST['page']);
		break;
	case 'GetUserGroupInfo':
		GetUserGroupInfo($_POST['mug_id']);
		break;
	case 'get_module_menu_items':
		get_module_menu_items($_POST['mug_id']);
		break;
	case 'get_modules_user_group_id':
		get_modules_user_group_id($_POST['mug_id'],$_POST['mmg_link'],$_POST['mmi_link']);
		break;
	case 'select2_usergroup':
		select2_usergroup();
		break;
	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}

exit();

function select2_usergroup(){
	$result = query("SELECT * FROM module_user_groups;");
	print json_encode($result);
}

function InsertUserGroup($mug_name,$data){
    $result = query("INSERT INTO module_user_groups VALUES (null,'%s');",$mug_name);
    if ($result) {
    	foreach ($data as $key) {
			query("INSERT INTO module_access_rights VALUES (null,'%s','%s');",$result['LAST_INSERT_ID'],$key['mmi_id']);
		}
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting User Group went failed');
	}
}


function UpdateUserGroup($mug_id,$mug_name,$data){
    $result = query("UPDATE module_user_groups SET mug_name='%s' WHERE mug_id ='%s'",$mug_name,$mug_id);
    if ($result) {

    	query("DELETE FROM module_access_rights WHERE mug_id='%s';",$mug_id);

    	foreach ($data as $key) {
			query("INSERT INTO module_access_rights VALUES (null,'%s','%s');",$mug_id,$key['mmi_id']);
		}
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating User Group went failed');
	}
}

function DeleteUserGroup($mug_id){
    $result = query("DELETE FROM module_user_groups WHERE mug_id ='%s'",$mug_id);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting User Group went failed');
	}
}

function GetUserGroupInfo($mug_id){
	$result = query("SELECT * FROM module_user_groups WHERE mug_id='$mug_id' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode(array('success'=>true,'result'=>$result));
	} else {
		errorJson('No User Group found!');
	}
}


function GetAllUserGroup($page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT * FROM module_user_groups;");
	$result1 = query("SELECT * FROM module_user_groups LIMIT $start, $limit;");
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);
		print json_encode(array('result' =>$result1,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching User Group failed');
	}
}

function get_module_menu_items($mug_id){
	$sql = 'SELECT mmi.*,mmg.mmg_id, mmg.mmg_name, mmg.mmg_order,';
	if($mug_id == '') {
		$sql .= 'FALSE AS checked';
	} else {
		$sql .= "
			CASE
				WHEN (SELECT COUNT(*) FROM module_access_rights WHERE mmi_id = mmi.mmi_id AND mug_id ='$mug_id') > 0 THEN TRUE
				ELSE FALSE
			END AS checked
		";
	}
	$sql .=' FROM module_menu_items mmi LEFT JOIN module_menu_groups mmg ON mmi.mmg_id = mmg.mmg_id ORDER BY mmg.mmg_order,mmi.mmi_order ASC;';
	$result = query($sql);



	$sql1 = 'SELECT DISTINCT mmg.*,';
	if($mug_id == '') {
		$sql1 .= 'FALSE AS checked';
	} else {
		$sql1 .= "
			CASE
				WHEN (SELECT COUNT(*) FROM module_access_rights WHERE mmi_id = mmi.mmi_id AND mug_id ='$mug_id') > 0 THEN TRUE
				ELSE FALSE
			END AS checked
		";
	}
	$sql1 .='FROM module_menu_groups  mmg INNER JOIN module_menu_items mmi ON mmi.mmg_id = mmg.mmg_id  ORDER BY mmg.mmg_order,mmi.mmi_order ASC;';
	$result1 = query($sql1);

	print json_encode(array('module_menu_groups' =>$result1,'module_menu_items'=>$result));
}


function get_modules_user_group_id($mug_id, $mmg_link='', $mmi_link=''){

	$sql = "SELECT mmi.mmi_id,mmi.mmi_name,mmi.mmi_link,mmi.mmi_description,mmg.mmg_id,mmg.mmg_name,mmg.mmg_link,mug.mug_id,mug.mug_name
		FROM module_access_rights mar
		LEFT JOIN module_user_groups mug ON mar.mug_id = mug.mug_id
		LEFT JOIN module_menu_items mmi ON  mar.mmi_id = mmi.mmi_id
		LEFT JOIN module_menu_groups mmg ON mmi.mmg_id = mmg.mmg_id
		WHERE mar.mug_id = '%s'";
	if($mmg_link != '') {
		$sql.=" AND mmg.mmg_link ='%s' ";
	}
	if($mmi_link != '') {
		$sql.=" AND mmi.mmi_link ='%s' ";
	}
	$sql.="ORDER BY mmg.mmg_order, mmi.mmi_order ASC;";

	$result = query($sql,$mug_id, $mmg_link, $mmi_link);
	print json_encode($result);
}


?>
