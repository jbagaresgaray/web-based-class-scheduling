<?php
session_start();
include('lib.php');

header("Content-Type: application/json");

switch ($_POST['command']) {
	case 'select_ScheduleAYTerm':
		select_ScheduleAYTerm();
		break;
	case 'GetFacultySchedule':
		GetFacultySchedule($_POST['TermID'],$_POST['FacultyID']);
		break;
	case 'GetFacultySchedule2':
		GetFacultySchedule2($_POST['TermID']);
		break;
	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}
exit();


function select_ScheduleAYTerm(){
	$result = query("SELECT * FROM tblayterm;");
	print json_encode($result);
}


function GetFacultySchedule($TermID,$FacultyID){
	$result = query("CALL GetFacultySchedule('$TermID','$FacultyID')");
	print json_encode($result);
}

function GetFacultySchedule2($TermID){
	$result = query("SELECT SubjectOfferingID AS `SID`, SubjectCode, SubjectTitle,
					IFNULL(Sched1,'') AS Sched1, IFNULL(R1.RoomNo,'') AS `Room1`,
					IFNULL(Sched2,'') AS Sched2, IFNULL(R2.RoomNo,'') AS `Room2`,
					IFNULL(Sched3,'') AS Sched3, IFNULL(R3.RoomNo,'') AS `Room3`,
					fnFacultyName(CS.TeacherID) AS `Faculty`,
					Units, LabUnits, LecHrs, LabHrs,
				    Days AS DAYS1,
				    SchedTimeStart AS `Time_Start1`,
				    SchedTimeEnd AS `Time_End1`,
				    Days2 AS DAYS2,
				    SchedTimeStart2 AS `Time_Start2`,
				    SchedTimeEnd2 AS `Time_End2`,
				    Days3 AS DAYS3,
				    SchedTimeStart3 AS `Time_Start3`,
				    SchedTimeEnd3 AS `Time_End3`,
				    CS.IsSpecialClasses, CS.OverRideConflict,CS.IsDissolved,
					fnAcademicYearTerm(CS.TermID) AS AcademicYear,CSec.SectionTitle
				FROM tblclassschedule AS CS
					LEFT JOIN tblsubject AS S ON S.SubjectID = CS.SubjectID
					LEFT JOIN tblsection AS CSec ON CS.SectionID = CSec.SectionID
					LEFT JOIN tblroom AS R1 ON CS.RoomID = R1.RoomID
					LEFT JOIN tblroom AS R2 ON CS.RoomID2 = R2.RoomID
					LEFT JOIN tblroom AS R3 on CS.RoomID3 = R3.RoomID
				WHERE CS.TermID ='$TermID' And CS.Isdissolved = '0' ORDER BY S.SubjectCode;");
	print json_encode($result);
}
