<?php

function InsertTerm($data){
    $result = query("INSERT INTO tblayterm VALUES (null,'%s','%s','%s','%s','%s','%s');",
    	$data[0]['SchoolYear'],$data[0]['Locked'],$data[0]['SchoolTerm'],
    	$data[0]['StartofSY'],$data[0]['EndofSY'],$data[0]['tHidden']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting an Academic Year went failed');
	}
}

function UpdateTerm($TermID,$data){
    $result = query("UPDATE tblayterm SET SchoolYear='%s',Locked='%s',SchoolTerm='%s',StartofSY='%s',EndofSY='%s',Hidden='%s' WHERE TermID ='%s'",
                    $data[0]['SchoolYear'],$data[0]['Locked'],$data[0]['SchoolTerm'],
                    $data[0]['StartofSY'],$data[0]['EndofSY'],$data[0]['tHidden'],$TermID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating an Academic Year went failed');
	}
}

function DeleteTerm($TermID){
    $result = query("DELETE FROM tblayterm WHERE TermID ='%s'",$TermID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting an Academic Year went failed');
	}
}

function getTerm($TermID){
	$result = query("SELECT * FROM tblayterm WHERE TermID='$TermID' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Academic Term found!');
	}
}

function searchTerm($value){
	$result = query("SELECT * FROM tblayterm WHERE TermID = '$value' OR SchoolYear = '$value';");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Term found!');
	}
}


function GetAllTerm($page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT * FROM tblayterm;");
	$result1 = query("SELECT * FROM tblayterm LIMIT $start, $limit;");
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('result' =>$result1,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching Academic Term failed');
	}
}

?>
