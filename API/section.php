<?php
session_start();
include('lib.php');

header("Content-Type: application/json");

switch ($_POST['command']) {

	case 'SearchSubjectForSection':
		SearchSubjectForSection($_POST['value']);
		break;
	case 'GetAllSection':
		GetAllSection($_POST['TermID'],$_POST['page']);
		break;
	case 'getSectionDetails':
		getSectionDetails($_POST['SectionID']);
		break;
	case 'InsertSubjectOffering':
		InsertSubjectOffering($_POST['data']);
		break;
    case 'DeleteSectionOffering':
		DeleteSectionOffering($_POST['SectionID'],$_POST['SubjectID'],$_POST['TermID']);
		break;
	case 'checkHasOffering':
		checkHasOffering($_POST['SectionID'],$_POST['TermID']);
		break;
	case 'checkHasOfferingHasSchedule':
		checkHasOfferingPosted($_POST['SectionID'],$_POST['SubjectID'],$_POST['TermID']);
		break;
	case 'getSubjectOffered':
		getSubjectOffered($_POST['SectionID'],$_POST['TermID'],$_POST['ProgramID']);
		break;
	case 'InsertSection':
		InsertSection($_POST['data']);
		break;
	case 'UpdateSection':
		UpdateSection($_POST['SectionID'],$_POST['data']);
		break;
	case 'DeleteSection':
		DeleteSection($_POST['SectionID']);
		break;
	case 'SectionHasSchedule':
		SectionHasSchedule($_POST['SectionID'],$_POST['TermID']);
		break;
	case 'getSectionDetails':
		getSectionDetails($_POST['SectionID']);
		break;
	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}

exit();

function InsertSection($data){
    $result = query("INSERT INTO tblsection VALUES (null,'%s','%s','%s',null,'%s',null,'%s',null,'%s',null,'%s',NOW(),'%s',null,null);",
    	$data[0]['SectionTitle'],$data[0]['YearLevelID'],$data[0]['TermID'],
    	$data[0]['ProgramID'],$data[0]['IsBlock'],$data[0]['Limit'],$data[0]['IsDissolved'],$data[0]['CreatedBy']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Class Section went failed');
	}
}

function InsertSubjectOffering($data){
	foreach ($data as $key) {
        $result1 = query("SELECT * FROM tblclassschedule WHERE SectionID='%s' AND SubjectID='%s' AND TermID='%s';",$key['SectionID'],$key['SubjectID'],$key['TermID']);
        if (count($result1['result'])>0) {
            $result=query("UPDATE tblclassschedule SET `Limit`='%s',IsSpecialClasses='%s',IsDissolved='%s',OverRideConflict='%s' WHERE SubjectID='%s' AND SectionID='%s' AND TermID='%s'",
	    	$key['Limit'],$key['IsSpecialClasses'],$key['IsDissolved'],$key['OverRideConflict'],$key['SubjectID'],$key['SectionID'],$key['TermID']);
        } else {
            $result=query("INSERT INTO tblclassschedule(SubjectID,SectionID,TermID,`Limit`,CreationDate,CreatedBy,IsSpecialClasses,IsDissolved,OverRideConflict)
            	VALUES ('%s','%s','%s','%s',NOW(),'%s','%s','%s','%s');",
    		$key['SubjectID'],$key['SectionID'],$key['TermID'],$key['Limit'],$key['CreatedBy'],$key['IsSpecialClasses'],$key['IsDissolved'],$key['OverrideConflict']);
        }
	}
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting Subject Offering went failed');
	}
}

function UpdateSection($SectionID,$data){
    $result = query("UPDATE tblsection SET SectionTitle='%s',YearLevelID='%s',TermID='%s',ProgramID='%s',IsBlock='%s',`Limit`='%s',IsDissolved='%s',ModifiedBy='%s',ModifiedOn=NOW() WHERE SectionID ='%s'",
                    $data[0]['SectionTitle'],$data[0]['YearLevelID'],$data[0]['TermID'],$data[0]['ProgramID'],$data[0]['IsBlock'],
    				$data[0]['Limit'],$data[0]['IsDissolved'],$data[0]['CreatedBy'],$SectionID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Recodrd successfully updated!'));
	} else {
		errorJson('Updating Class Section went failed');
	}
}

function DeleteSubjectOffering($SectionID,$SubjectID,$TermID){
    $result = query("DELETE FROM tblclassschedule WHERE SectionID='%s' AND SubjectID='%s' AND TermID='%s'",$SectionID,$SubjectID,$TermID);
    if ($result) {
		print json_encode(array('success' =>true));
	} else {
		errorJson('Deleting Subject Offering went failed');
	}
}


function DeleteSection($SectionID){
    $result = query("DELETE FROM tblsection WHERE SectionID ='%s'",$SectionID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting an Academic Year went failed');
	}
}

function SectionHasSchedule($SectionID,$TermID){
	$result = query("SELECT * FROM tblclassschedule WHERE SectionID='%s' AND TermID='%s';",$SectionID,$TermID);
	if (count($result['result'])>0) {
		print json_encode(array('success'=>true));
	} else {
		print json_encode(array('success'=>false));
	}
}


function getSectionDetails($SectionID){
	$result = query("SELECT * FROM tblsection WHERE SectionID='$SectionID' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode(array('result' =>$result,'success'=>true));
	} else {
		errorJson('No Class Section found!');
	}
}

function SearchSubjectForSection($value){
	$result = query("SELECT * FROM tblsubject WHERE SubjectCode LIKE '%%%s%%' OR SubjectTitle LIKE '%%%s%%' LIMIT 10;",$value,$value);
	print json_encode($result);
}

function checkHasOffering($SectionID,$TermID){
	$result = query("SELECT * FROM tblclassschedule WHERE SectionID='%s' AND TermID='%s';",$SectionID,$TermID);
	if (count($result['result'])>0) {
		print json_encode(array('success' =>true));
	} else {
		print json_encode(array('success' =>false));
	}
}

function checkHasOfferingHasSchedule($SectionID,$SubjectID,$TermID){
	$result = query("SELECT * FROM tblclassschedule WHERE SectionID='%s' AND SubjectID='%s' AND TermID='%s';",$SectionID,$SubjectID,$TermID);
	if (count($result['result'])>0) {
		print json_encode(array('success' =>true));
	} else {
		print json_encode(array('success' =>false));
	}
}


function getSubjectOffered($SectionID,$sTermID,$sProgramID){
	$result = query("SELECT S.SectionTitle,SUB.SubjectID,SUB.SubjectCode,SUB.SubjectTitle,SUB.CreditUnits,SUB.LabUnits,SUB.Units,CD.SubjectOfferingID,CD.`Limit`,S.ProgramID,S.SectionID,CD.IsSpecialClasses,CD.IsDissolved,CD.OverrideConflict,S.TermID
		FROM tblsection AS S INNER JOIN tblclassschedule AS CD ON S.SectionID = CD.SectionID  INNER JOIN tblsubject AS SUB ON SUB.SubjectID = CD.SubjectID
		WHERE S.SectionID='%s' AND S.TermID='%s' AND S.ProgramID='%s';",$SectionID,$sTermID,$sProgramID);
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Subject Offered on this section!');
	}
}

function GetAllSection($TermID,$page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT * FROM tblsection WHERE TermID='%s';",$TermID);
	$result1 = query("SELECT SectionTitle,SectionID,ProgramID,fnYearLevelByLevelTerm(YearLevelID) YearLvlTerm,IF(IsBlock,'YES','NO') AS IsBlock,
	IF(IsDissolved,'YES','NO') AS Dissolved,`Limit`,CreationDate,CreatedBy FROM tblsection WHERE TermID='%s' LIMIT $start, $limit;",$TermID);
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('result' =>$result1,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching Class Section failed');
	}
}







?>
