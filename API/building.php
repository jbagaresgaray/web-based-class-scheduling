<?php

function InsertBuilding($data){
    $result = query("INSERT INTO tblbuilding VALUES (null,'%s','%s','%s','%s','%s',null,'%s','%s','%s');",
    	$data[0]['CampusID'],$data[0]['BldgName'],$data[0]['BldgOtherName'],
    	$data[0]['Acronym'],$data[0]['FloorsCount'],$data[0]['IsLANReady'],
    	$data[0]['Elevator'],$data[0]['Escalator']);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting an Campus Building went failed');
	}
}

function UpdateBuilding($data,$BuildingID){
    $result = query("UPDATE tblbuilding SET CampusID='%s',BldgName='%s',
    	BldgOtherName='%s',Acronym='%s',FloorsCount='%s',IsLanReady='%s',
    	Elevator='%s',Escalator='%s' WHERE BldgID ='%s'",
        $data[0]['CampusID'],$data[0]['BldgName'],$data[0]['BldgOtherName'],
    	$data[0]['Acronym'],$data[0]['FloorsCount'],$data[0]['IsLANReady'],
    	$data[0]['Elevator'],$data[0]['Escalator'],$BuildingID);

    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating an Campus Building went failed');
	}
}

function DeleteBuilding($BuildingID){
    $result = query("DELETE FROM tblbuilding WHERE BldgID ='%s'",$BuildingID);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting an Campus Building went failed');
	}
}

function getBuilding($BuildingID){
	$result = query("SELECT * FROM tblbuilding WHERE BldgID='$BuildingID' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Campus Building found!');
	}
}


function GetAllBuilding($page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}

	$result = query("SELECT * FROM tblbuilding tb JOIN tblcampus tc ON tc.CampusID = tb.CampusID;");
	$result1 = query("SELECT * FROM tblbuilding tb JOIN tblcampus tc ON tc.CampusID = tb.CampusID LIMIT $start, $limit;");
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('result' =>$result,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching Campus Building failed');
	}
}

function searchBuilding($value){
	$result = query("SELECT * FROM tblbuilding WHERE BldgName = '$value';");
	if (count($result['result'])>0) {
		print json_encode($result);
	} else {
		errorJson('No Campus Building found!');
	}
}


?>
