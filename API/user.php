<?php
session_start();
include('lib.php');


header("Content-Type: application/json");

switch ($_POST['command']) {

	case 'login':
		main_login($_POST['username'],$_POST['password']);
		break;
	case 'InsertUser':
		InsertUser($_POST['data']);
		break;
	case 'UpdateUser':
		UpdateUser($_POST['u_id'],$_POST['data']);
		break;
	case 'DeleteUser':
		DeleteUser($_POST['u_id']);
		break;
	case 'GetAllUsers':
		GetAllUsers($_POST['page']);
		break;
	case 'GetUserInfo':
		GetUserInfo($_POST['u_id']);
		break;
	case 'select_employeeUser':
		select_employeeUser();
		break;

	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}

exit();

function main_login($username,$password){
	$result = query("SELECT U.*,fnFacultyName(U.e_id) AS Fullname,mug.mug_id, mug.mug_name
		FROM tblusers U
		LEFT JOIN module_user_groups mug ON U.mug_id = mug.mug_id
		WHERE u_username ='%s' AND e_password='%s'",$username,$password);
	if (count($result['result'])>0) {
		print json_encode(array('success' =>true,'result'=>$result));
	} else {
		errorJson('Username and Password does not match');
	}
}


function select_employeeUser(){
	$result = query("SELECT TeacherID,EmployeeID,fnFacultyName(TeacherID) AS `EmployeeName` FROM tblteacher;");
	print json_encode($result);
}

function InsertUser($data){
    $result = query("INSERT INTO tblusers VALUES (null,'%s','%s','%s','%s');",
    	$data[0]['u_username'],$data[0]['e_password'],$data[0]['mug_id'],$data[0]['e_id']);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully saved!'));
	} else {
		errorJson('Inserting User Group went failed');
	}
}

function UpdateUser($u_id,$data){
    $result = query("UPDATE tblusers SET u_username='%s',e_password='%s',mug_id='%s' WHERE u_id ='%s'",
    	$data[0]['u_username'],$data[0]['e_password'],$data[0]['mug_id'],$u_id);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully updated!'));
	} else {
		errorJson('Updating User Group went failed');
	}
}

function DeleteUser($u_id){
    $result = query("DELETE FROM tblusers WHERE u_id ='%s'",$u_id);
    if ($result) {
		print json_encode(array('success' =>true,'msg'=>'Record successfully deleted!'));
	} else {
		errorJson('Deleting User Group went failed');
	}
}

function GetUserInfo($u_id){
	$result = query("SELECT * FROM tblusers WHERE u_id='$u_id' LIMIT 1;");
	if (count($result['result'])>0) {
		print json_encode(array('success'=>true,'result'=>$result));
	} else {
		errorJson('No User Information found!');
	}
}


function GetAllUsers($page){
	$limit = 10;
	$adjacent = 3;

	if($page==1){
	   $start = 0;
	}else{
	  $start = ($page-1)*$limit;
	}
	$result1 = query("SELECT U.*,fnFacultyName(U.e_id) AS UserFullname,(SELECT M.mug_name FROM module_user_groups M WHERE M.mug_id = U.mug_id LIMIT 1) AS UserGroup FROM tblusers U WHERE U.e_id IS NOT NULL LIMIT $start, $limit;");
	$result = query("SELECT * FROM tblusers WHERE e_id IS NOT NULL;");
	$rows = count($result['result']);

	if ($rows >0) {
		//authorized
		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('result' =>$result,'pagination'=>$paging));
	} else {
		//not authorized
		errorJson('fetching User List failed');
	}
}


?>
