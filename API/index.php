<?php
session_start();
include('lib.php');
include('building.php');
include('subject.php');
include('AcadProg.php');
include('campus.php');
include('ayterm.php');



header("Content-Type: application/json");

switch ($_POST['command']) {

	/* --------------------- TABLE AY TERM ------------------------*/
	case 'InsertTerm':
		InsertTerm($_POST['data']);
		break;
	case 'UpdateTerm':
		UpdateTerm($_POST['TermID'],$_POST['data']);
		break;
	case 'DeleteTerm':
		DeleteTerm($_POST['TermID']);
		break;
	case 'GetAllTerm':
		GetAllTerm($_POST['page']);
		break;
	case 'searchTerm':
		searchTerm($_POST['value']);
		break;
	case 'getTerm':
		getTerm($_POST['TermID']);
		break;

	/* --------------------- TABLE CAMPUS ------------------------*/
	case 'InsertCampus':
		InsertCampus($_POST['data']);
		break;
	case 'UpdateCampus':
		UpdateCampus($_POST['data']);
		break;
	case 'DeleteCampus':
		DeleteCampus();
		break;
	case 'getCampusInformation':
		getCampusInformation();
		break;


	/* --------------------- TABLE BUILDING ------------------------*/

	case 'InsertBuilding':
		InsertBuilding($_POST['data']);
		break;
	case 'UpdateBuilding':
		UpdateBuilding($_POST['data'],$_POST['BuildingID']);
		break;
	case 'DeleteBuilding':
		DeleteBuilding($_POST['BuildingID']);
		break;
	case 'getBuilding':
		getBuilding($_POST['BuildingID']);
		break;
	case 'GetAllBuilding':
		GetAllBuilding($_POST['page']);
		break;
	case 'searchBuilding':
		searchBuilding($_POST['value']);
		break;

	/* --------------------- TABLE SECTION ------------------------*/
	case 'SearchSubjectForSection':
		SearchSubjectForSection($_POST['value']);
		break;
	case 'GetAllSection':
		GetAllSection($_POST['TermID'],$_POST['page']);
		break;
	case 'getSectionDetails':
		getSectionDetails($_POST['SectionID']);
		break;
	case 'InsertSectionOffering':
		InsertSectionOffering($_POST['data']);
		break;
	case 'checkHasOffering':
		checkHasOffering($_POST['SectionID']);
		break;
	case 'getSubjectOffered':
		getSubjectOffered($_POST['SectionID']);
		break;

	/* --------------------- TABLE SUBJECT ------------------------*/
	case 'fecthSubject':
		fecthSubject($_POST['page']);
		break;
	case 'InsertSubject':
		InsertSubject($_POST['data']);
		break;
	case 'InsertSubjectMode':
		InsertSubjectMode($_POST['data']);
		break;
	case 'UpdateSubject':
		UpdateSubject($_POST['data'],$_POST['SubjectID']);
		break;
	case 'UpdateSubjectMode':
		UpdateSubjectMode($_POST['data'],$_POST['SubjectMode']);
		break;
	case 'fecthSubjectMode':
		fecthSubjectMode();
		break;
	case 'getSubject':
		getSubject($_POST['ID']);
		break;
	case 'DeleteSubject':
		DeleteSubject($_POST['SubjectID']);
		break;
	case 'DeleteSubjectMode':
		DeleteSubjectMode($_POST['SubjectMode']);
		break;
	case 'getSubjectMode':
		getSubjectMode($_POST['SubjectID']);
		break;
	case 'SearchSubject':
		SearchSubject($_POST['value']);
		break;

	/* --------------------- TABLE ACAD PROGRAMS ------------------------*/

	case 'InsertProgramClass':
		InsertProgramClass($_POST['data']);
		break;
	case 'UpdateProgramClass':
		UpdateProgramClass($_POST['data'],$_POST['ClassCode']);
		break;

	default:
		echo 'CLASS SCHEDULING SYSTEM API v.0.1 developed by: DesignR (Programmer: Philip Cesar Garay and Neil Ragadio)';
		break;
}

exit();
?>
