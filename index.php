<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Faculty Scheduling System</title>

  <link href="app/scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="app/scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <link href="app/styles/loader.css" rel="stylesheet">
  <link href="app/styles/signin.css" rel="stylesheet">
</head>

<body>

  <div class="container">

    <form class="form-signin" role="form">
      <h2 class="form-signin-heading">
                <!-- <img src="app/styles/img/logo.png" alt="Google" id="hplogo" title="Google" style="height: 50px;"> -->
            </h2>
      <input type="email" class="form-control" placeholder="Username" name="username" id="username" required autofocus>
      <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
      <input type="text" class="form-control" placeholder="IP Server" name="server" id="server" required>
      <br>
      <a href="javascript:login();" class="btn btn-lg btn-primary btn-block">Sign in</a>
    </form>
    <center>
      <div class="contener_general hidden" id="loader">
        <div class="contener_mixte">
          <div class="ballcolor ball_1">&nbsp;</div>
        </div>
        <div class="contener_mixte">
          <div class="ballcolor ball_2">&nbsp;</div>
        </div>
        <div class="contener_mixte">
          <div class="ballcolor ball_3">&nbsp;</div>
        </div>
        <div class="contener_mixte">
          <div class="ballcolor ball_4">&nbsp;</div>
        </div>
      </div>
    </center>
  </div>
  <!-- /container -->


  <script src="app/scripts/lib/jquery/jquery-2.0.0.min.js"></script>
  <script src="app/scripts/lib/bootstrap/js/bootstrap.min.js"></script>

  <script src="app/scripts/public/login.js"></script>
</body>

</html>
