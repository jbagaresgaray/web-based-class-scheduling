$(document).ready(function(){
	fecthSubject('1');
	fetchSubjectMode();
	fecthSubjectModeDisplay();
});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validateSubject(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    if($('#txtSubjectCode').val() == ''){
    	$('#txtSubjectCode').next('span').text('Subject Code is required');
    	empty = false;
    }

    if($('#txtSubjectTitle').val() == ''){
    	$('#txtSubjectTitle').next('span').text('Subject Title is required');
    	empty = false;
    }

    if($('#txtDescription').val() == ''){
    	$('#txtDescription').next('span').text('Description is required');
    	empty = false;
    }

    if($('#txtUnits').val() == ''){
    	$('#txtUnits').next('span').text('Units is required');
    	empty = false;
    }

    if($('#txtCreditUnits').val() == ''){
    	$('#txtCreditUnits').next('span').text('Credit Units is required');
    	empty = false;
    }

    if($('#txtLecHrs').val() == ''){
    	$('#txtLecHrs').next('span').text('Hours per Lecture is required');
    	empty = false;
    }

    if($('#txtLabUnits').val() == ''){
    	$('#txtLabUnits').next('span').text('Laboratory Units is required');
    	empty = false;
    }

    if($('#txtLabHrs').val() == ''){
    	$('#txtLabHrs').next('span').text('Laboratory Hours is required');
    	empty = false;
    }

    if($('#txtSubjectMode').val() == ''){
    	$('#txtSubjectMode').next('span').text('Subject Mode is required');
    	empty = false;
    }

    if($('#txtIsNonAcademic').val() == ''){
    	$('#txtIsNonAcademic').next('span').text('Non Academic is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function validateSubjectMode(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    if($('#txtDescription').val() == ''){
    	$('#txtDescription').next('span').text('Description is required');
    	empty = false;
    }

    if($('#txtShortName').val() == ''){
    	$('#txtShortName').next('span').text('Short Name is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function saveSubject(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#SubjectuniqueID').val() == ''){
		if(validateSubject() == true){

			var data = new Array();

			Subject = new Object();

			Subject.SubjectCode = $('#txtSubjCode').val();
			Subject.SubjectTitle = $('#txtSubjTitle').val();
			Subject.Description = $('#txtDesc').val();

			if($('#chckinActive').prop("checked") == true){
				Subject.InActive = '1';
			}else{
				Subject.InActive = '0';
			}

			Subject.Units = $('#txtUnits').val();
			Subject.CreditUnits = $('#txtCredperUnit').val();
			Subject.LecHrs = $('#txtLecPerHour').val();
			Subject.LabUnits = $('#txtLabUnits').val();
			Subject.LabHrs = $('#txtLabHrs').val();
			Subject.SubjectMode = $('#chckSubjectMode').val();

			if($('#chckIsnonAcad').prop("checked") == true){
				Subject.IsNonAcademic = '1';
			}else{
				Subject.IsNonAcademic = '0';
			}

			var currentdate = new Date();
			var datetime = "" + currentdate.getFullYear() + "-"
            + (currentdate.getMonth()+1)  + "-"
            + currentdate.getDate() + " "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

			Subject.CreatedBy = 'administrator';
			Subject.CreationDate = datetime;
			Subject.ModifiedBy = 'administrator';
			Subject.ModifiedDate = datetime;

			data.push(Subject);

			console.log(data);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertSubject',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "subjects.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateSubject() == true){

			var data = new Array();

			Subject = new Object();

			Subject.SubjectCode = $('#txtSubjCode').val();
			Subject.SubjectTitle = $('#txtSubjTitle').val();
			Subject.Description = $('#txtDesc').val();

			if($('#chckinActive').prop("checked") == true){
				Subject.InActive = '1';
			}else{
				Subject.InActive = '0';
			}

			Subject.Units = $('#txtUnits').val();
			Subject.CreditUnits = $('#txtCredperUnit').val();
			Subject.LecHrs = $('#txtLecPerHour').val();
			Subject.LabUnits = $('#txtLabUnits').val();
			Subject.LabHrs = $('#txtLabHrs').val();
			Subject.SubjectMode = $('#chckSubjectMode').val();

			if($('#chckIsnonAcad').prop("checked") == true){
				Subject.IsNonAcademic = '1';
			}else{
				Subject.IsNonAcademic = '0';
			}

			var currentdate = new Date();
			var datetime = "" + currentdate.getFullYear() + "-"
            + (currentdate.getMonth()+1)  + "-"
            + currentdate.getDate() + " "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

			Subject.CreatedBy = 'administrator';
			Subject.CreationDate = datetime;
			Subject.ModifiedBy = 'administrator';
			Subject.ModifiedDate = datetime;

			data.push(Subject);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateSubject',
		            data : data,
		            SubjectID: $('#SubjectuniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "subjects.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fecthSubject(page);
});


function saveSubjectMode(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");


	if($('#SubjectModeuniqueID').val() == ''){
		if(validateSubjectMode() == true){

			var data = new Array();

			SubjectMode = new Object();

			SubjectMode.Description = $('#txtDescription').val();
			SubjectMode.ShortName = $('#txtShortName').val();

			data.push(SubjectMode);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertSubjectMode',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;
		        	fecthSubjectModeDisplay();

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        // window.location.href = "subjects.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateSubjectMode() == true){

			var data = new Array();

			SubjectMode = new Object();

			SubjectMode.Description = $('#txtDescription').val();
			SubjectMode.ShortName = $('#txtShortName').val();

			data.push(SubjectMode);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateSubjectMode',
		            data : data,
		            SubjectMode: $('#SubjectModeuniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;
		        	fecthSubjectModeDisplay();

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        // window.location.href = "subjects.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function fecthSubject(page){
	$('#processing-modal').modal('show');
	$('#tbl_subject tbody > tr').remove();
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthSubject',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.result.length > 0){
	    			for(var i = 0; i < decode.result.result.length; i++){
	    				var row = decode.result.result;

	    				var html = '<tr>\
	    							<td>' +row[i].SubjectCode+ '</td>\
	    							<td>' +row[i].SubjectTitle+ '</td>\
	    							<td>' +row[i].Description+ '</td>\
	    							<td>' +row[i].Units+ '</td>\
	    							<td>' +row[i].CreditUnits+ '</td>\
	    							<td>' +row[i].LecHrs+ '</td>\
	    							<td>' +row[i].LabUnits+ '</td>\
	    							<td>' +row[i].LabHrs + '</td>\
	    							<td>' +row[i].Description + '</td>\
	    							<td>' +row[i].IsNonAcademicSubj + '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-subject-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalSubjectModeDisplay" data-id="' + row[i].SubjectMode + '">\
                                                <i class="fa fa-search"></i>\
                                            </a>\
                                            <a class="edit-subject-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].SubjectID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-subject-icon btn btn-danger btn-xs" data-id="' + row[i].SubjectID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_subject tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    	$('#processing-modal').modal('hide');
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
	});
}

function fetchSubjectMode(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthSubjectMode'
	    },
	    success: function(response){
	    	var decode = response;
            $('#chckSubjectMode').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;

                var html = '<option id="' + row[i].SubjectMode + '" value="' + row[i].SubjectMode + '">' + row[i].Description + '</option>';
                $("#chckSubjectMode").append(html);
            }
        },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

function searchSubject(){
	$('#processing-modal').modal('show');
	$('#tbl_subject tbody > tr').remove();
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'SearchSubject',
	        value: $('#txtvalue').val()
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;

	    				var html = '<tr>\
	    							<td>' +row[i].SubjectCode+ '</td>\
	    							<td>' +row[i].SubjectTitle+ '</td>\
	    							<td>' +row[i].Description+ '</td>\
	    							<td>' +row[i].InActive+ '</td>\
	    							<td>' +row[i].Units+ '</td>\
	    							<td>' +row[i].CreditUnits+ '</td>\
	    							<td>' +row[i].LecHrs+ '</td>\
	    							<td>' +row[i].LabUnits+ '</td>\
	    							<td>' +row[i].LabHrs+ '</td>\
	    							<td>' +row[i].SubjectMode+ '</td>\
	    							<td>' +row[i].IsNonAcademic+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-subject-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalSubjectModeDisplay" data-id="' + row[i].SubjectMode + '">\
                                                <i class="fa fa-search"></i>\
                                            </a>\
                                            <a class="edit-subject-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].SubjectID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-subject-icon btn btn-danger btn-xs" data-id="' + row[i].SubjectID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_subject tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    	$('#processing-modal').modal('hide');
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
	});
}

function fecthSubjectModeDisplay(){
	$('#tbl_subjectModeDisplay tbody > tr').remove();
	 $.ajax({
        url: 'http://localhost/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'fecthSubjectMode'
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;

	            var html = '<tr>\
							<td>' +row[i].Description+ '</td>\
							<td>' +row[i].ShortName+ '</td>\
							<td>\
								<div class="text-right">\
		                            <a class="edit-subjectmodedisplay-icon btn btn-primary btn-xs" data-modeid="' + row[i].SubjectMode + '">\
		                                <i class="fa fa-pencil"></i>\
		                            </a>\
		                            <a class="remove-subjectmodedisplay-icon btn btn-danger btn-xs" data-modeid="' + row[i].SubjectMode + '">\
		                                <i class="fa fa-trash"></i>\
		                            </a>\
		                        </div>\
							</td>\
						</tr>';

				$("#tbl_subjectModeDisplay tbody").append(html);
	        }
	    }
    });
}

//Show SubjectMode
$(document).on("click", ".Add-subject-icon", function(){
	var SubjectID = $(this).data('id');
  var ipaddress = sessionStorage.getItem("ipaddress");

	$('#tbl_subjectMode tbody > tr').remove();
	 $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getSubjectMode',
            SubjectID: SubjectID
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;

	            var html = '<tr>\
							<td>' +row[i].Description+ '</td>\
							<td>' +row[i].ShortName+ '</td>\
							<td>\
								<div class="text-right">\
		                            <a class="edit-subjectmode-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalSubjectMode" data-modeid="' + row[i].SubjectMode + '">\
		                                <i class="fa fa-pencil"></i>\
		                            </a>\
		                            <a class="remove-subjectmode-icon btn btn-danger btn-xs" data-modeid="' + row[i].SubjectMode + '">\
		                                <i class="fa fa-trash"></i>\
		                            </a>\
		                        </div>\
							</td>\
						</tr>';

				$("#tbl_subjectMode tbody").append(html);

				$('#myModalSubjectModeDisplay').modal('show')
	        }
	    }
    });
});

//Edit Subject
$(document).on("click", ".edit-subject-icon", function() {

    var SubjectID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getSubject',
            ID: SubjectID
        },
        success: function(response) {
            var decode = response;

                $('#SubjectuniqueID').val(decode.result[0].SubjectID);
				$('#txtSubjCode').val(decode.result[0].SubjectCode);
			    $('#txtSubjTitle').val(decode.result[0].SubjectTitle);
			    $('#txtDesc').val(decode.result[0].Description);

			    if(decode.result[0].InActive == 1){
			    	$('#chckinActive').prop("checked", true);
			    }else{
			    	$('#chckinActive').prop("checked", false);
			    }

			    $('#txtUnits').val(decode.result[0].Units);
			    $('#txtCredperUnit').val(decode.result[0].CreditUnits);
			    $('#txtLecPerHour').val(decode.result[0].LecHrs);
			    $('#txtLabUnits').val(decode.result[0].LabUnits);
			    $('#txtLabHrs').val(decode.result[0].LabHrs);
			    $('#chckSubjectMode').val(decode.result[0].SubjectMode);
			    $('#chckIsnonAcad').val(decode.result[0].IsNonAcademic);


                $('#myModalSubject').modal('show')
            }
    });
});


//Edit SubjectMode
$(document).on("click", ".edit-subjectmode-icon", function() {

    var SubjectMode = $(this).data('modeid');
    $('#myModalSubjectModeDisplay').modal('hide');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getSubjectMode',
            SubjectID: SubjectMode
        },
        success: function(response) {
            var decode = response;
                $('#SubjectModeuniqueID').val(decode.result[0].SubjectMode);
				$('#txtDescription').val(decode.result[0].Description);
			    $('#txtShortName').val(decode.result[0].ShortName);


                $('#myModalSubjectMode').modal('show');

        }
    });
});

//Edit SubjectMode
$(document).on("click", ".edit-subjectmodedisplay-icon", function() {

    var SubjectMode = $(this).data('modeid');
    $.ajax({
        url: 'http://localhost/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getSubjectMode',
            SubjectID: SubjectMode
        },
        success: function(response) {
            var decode = response;
                $('#SubjectModeuniqueID').val(decode.result[0].SubjectMode);
				$('#txtDescription').val(decode.result[0].Description);
			    $('#txtShortName').val(decode.result[0].ShortName);
        }
    });
});

// on delete icon click Subject
$(document).on("click", ".remove-subject-icon", function() {
    if (confirm('Are you sure to delete this Subject?')) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        var SubjectID = $(this).data('id');

        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteSubject',
                SubjectID: SubjectID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "subjects.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// on delete icon click SubjectMode
$(document).on("click", ".remove-subjectmode-icon", function() {
    if (confirm('Are you sure to delete this Subject Mode?')) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        var SubjectMode = $(this).data('modeid');
        console.log(SubjectMode);
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteSubjectMode',
                SubjectMode: SubjectMode
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "subjects.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

$(document).on("click", ".remove-subjectmodedisplay-icon", function() {
    if (confirm('Are you sure to delete this Subject Mode?')) {

        var SubjectMode = $(this).data('modeid');
        console.log(SubjectMode);
        $.ajax({
            url: 'http://localhost/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteSubjectMode',
                SubjectMode: SubjectMode
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "subjects.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

function clearSubjectFields(){
	$('#txtSubjCode').val('');
    $('#txtSubjTitle').val('');
    $('#txtDesc').val('');
    $('#txtUnits').val('');
    $('#txtCredperUnit').val('');
    $('#txtLecPerHour').val('');
    $('#txtLabUnits').val('');
    $('#txtLabHrs').val('');
    $('#txtSubjectMode').val('');
    $('#txtIsNonAcademic').val('');
    $('#chckinActive').prop("checked",false);
    $('#chckIsnonAcad').prop("checked",false);
}

function clearSubjectModeFields(){
	$('#txtDescription').val('');
	$('#txtShortName').val('');
}
