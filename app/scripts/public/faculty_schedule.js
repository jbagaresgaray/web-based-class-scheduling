$(document).ready(function() {
    fetch_AYTerm();
});

function fetch_AYTerm() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboAYTerm').empty();
    $.post('http://'+ipaddress+'/scheduling/API/scheduling.php', {
        command: 'select_ScheduleAYTerm',
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + (row[i].SchoolYear + ' - ' + row[i].SchoolTerm) + '</option>';
            $("#cboAYTerm").append(html);
        }
    });
}

function refresh() {
    GetFacultySchedule2($('#cboAYTerm').val(), '1');
}

$("#cboAYTerm").change(function() {
    console.log('cboAYTerm onChange');
    GetFacultySchedule2($('#cboAYTerm').val(), '1');
});

/*$("#cboAYTerm").click(function() {
    console.log('cboAYTerm onChange');
    GetFacultySchedule2($('#cboAYTerm').val(), '1');
});
*/
function GetFacultySchedule(TermID, FacultyID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    var data = {
        command: 'GetFacultySchedule2',
        TermID: TermID,
        FacultyID: FacultyID
    };

    $('#tblSchedule tbody > tr').remove();
    $.post('http://'+ipaddress+'/scheduling/API/faculty_schedule.php', data, function(response) {
        var decode = response;
        if (decode.result.length > 0) {
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                var html = '<tr class="odd">\
                            <td>' + row[i].SubjectCode + '</td>\
                            <td>' + row[i].SubjectTitle + '</td>\
                            <td>' + row[i].SectionTitle + '</td>\
                            <td>' + row[i].Sched1 + '</td>\
                            <td>' + row[i].Room1 + '</td>\
                            <td>' + row[i].Sched2 + '</td>\
                            <td>' + row[i].Room2 + '</td>\
                            <td>' + row[i].Sched3 + '</td>\
                            <td>' + row[i].Room3 + '</td>\
                            <input id="authenticity_token" name="authenticity_token" type="hidden" value="' + row[i].SID + '">\
                            <input id="Units" name="Units" type="hidden" value="' + row[i].Units + '">\
                            <input id="LabUnits" name="LabUnits" type="hidden" value="' + row[i].LabUnits + '">\
                            <input id="LecHrs" name="LecHrs" type="hidden" value="' + row[i].LecHrs + '">\
                            <input id="LabHrs" name="LabHrs" type="hidden" value="' + row[i].LabHrs + '">\
                        </tr>';
                $("#tblSchedule tbody").append(html);
            }
        }
    });
}

function GetFacultySchedule2(TermID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    var data = {
        command: 'GetFacultySchedule2',
        TermID: TermID
    };

    $('#tblSchedule tbody > tr').remove();
    $.post('http://'+ipaddress+'/scheduling/API/faculty_schedule.php', data, function(response) {
        var decode = response;
        if (decode.result.length > 0) {
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                var html = '<tr class="odd">\
                            <td>' + row[i].SubjectCode + '</td>\
                            <td>' + row[i].SubjectTitle + '</td>\
                            <td>' + row[i].SectionTitle + '</td>\
                            <td>' + row[i].Sched1 + '</td>\
                            <td>' + row[i].Room1 + '</td>\
                            <td>' + row[i].Sched2 + '</td>\
                            <td>' + row[i].Room2 + '</td>\
                            <td>' + row[i].Sched3 + '</td>\
                            <td>' + row[i].Room3 + '</td>\
                            <input id="authenticity_token" name="authenticity_token" type="hidden" value="' + row[i].SID + '">\
                            <input id="Units" name="Units" type="hidden" value="' + row[i].Units + '">\
                            <input id="LabUnits" name="LabUnits" type="hidden" value="' + row[i].LabUnits + '">\
                            <input id="LecHrs" name="LecHrs" type="hidden" value="' + row[i].LecHrs + '">\
                            <input id="LabHrs" name="LabHrs" type="hidden" value="' + row[i].LabHrs + '">\
                        </tr>';
                $("#tblSchedule tbody").append(html);
            }
        }
    });
}

$('#tblSchedule').on('click', 'tr', function(e) {
    var row = $(this).closest('tr').children('td');

    var SubjectCode = row.eq(0).text();
    var SubjectTitle = row.eq(1).text();

    var SID = $(this).closest('tr').find('input[name="authenticity_token"]').val();
    var LectureUnits = $(this).closest('tr').find('input[name="Units"]').val();
    var LabUnits = $(this).closest('tr').find('input[name="LabUnits"]').val();
    var LecHrs = $(this).closest('tr').find('input[name="LecHrs"]').val();
    var LabHrs = $(this).closest('tr').find('input[name="LabHrs"]').val();

    $('#lbl_SubjectCode').text(SubjectCode);
    $('#lbl_SubjectTitle').text(SubjectTitle);
    $('#lbl_LecUnits').text(LectureUnits);
    $('#lbl_LabUnits').text(LabUnits);
    $('#lbl_LecHrs').text(LecHrs);
    $('#lbl_LabHrs').text(LabHrs);
});
