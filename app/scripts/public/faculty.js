$(document).ready(function(){

});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validateFaculty(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    if($('#txtEmployeeID').val == null){
    	$('#txtEmployeeID').next('span').text('Employee ID is required');
    	empty = false;
    }

    if($('#txtLastName').val == null){
    	$('#txtLastName').next('span').text('Last Name is required');
    	empty = false;
    }

    if($('#txtFirstName').val == null){
    	$('#txtFirstName').next('span').text('First Name is required');
    	empty = false;
    }

    if($('#txtMiddleName').val == null){
    	$('#txtMiddleName').next('span').text('Middle Name is required');
    	empty = false;
    }

    if($('#txtExtName').val == null){
    	$('#txtExtName').next('span').text('Extension Name is required');
    	empty = false;
    }

    if($('#txtPosnTitleID').val == null){
    	$('#txtPosnTitleID').next('span').text('Position Title is required');
    	empty = false;
    }

    if($('#txtCampusID').val == null){
    	$('#txtCampusID').next('span').text('Campus ID is required');
    	empty = false;
    }

    if($('#txtCampusID').val == null){
    	$('#txtCampusID').next('span').text('Campus ID is required');
    	empty = false;
    }

    if($('#txtPrefix').val == null){
    	$('#txtPrefix').next('span').text('Date Of Birth is required');
    	empty = false;
    }

    if($('#txtDateOfBirth').val == null){
    	$('#txtDateOfBirth').next('span').text('Prefix is required');
    	empty = false;
    }

    if($('#txtDateOfBirth').val == null){
    	$('#txtDateOfBirth').next('span').text('Prefix is required');
    	empty = false;
    }

    if($('#txtPlaceOfBirth').val == null){
    	$('#txtPlaceOfBirth').next('span').text('Place of Birth is required');
    	empty = false;
    }

    if($('#txtGender').val == null){
    	$('#txtGender').next('span').text('Gender is required');
    	empty = false;
    }

    if($('#txtTelNo').val == null){
    	$('#txtTelNo').next('span').text('Telephone No. is required');
    	empty = false;
    }

    if($('#txtMobileNo').val == null){
    	$('#txtMobileNo').next('span').text('Mobile No. is required');
    	empty = false;
    }

    if($('#txtEmail').val == null){
    	$('#txtEmail').next('span').text('Email is required');
    	empty = false;
    }

    if($('#txtEmail').val == null){
    	$('#txtEmail').next('span').text('Email is required');
    	empty = false;
    }

    if($('#txtAddressNo').val == null){
    	$('#txtAddressNo').next('span').text('Address No. is required');
    	empty = false;
    }

 	if($('#txtStreet').val == null){
    	$('#txtStreet').next('span').text('Street is required');
    	empty = false;
    }

    if($('#txtBarangay').val == null){
    	$('#txtBarangay').next('span').text('Barangay is required');
    	empty = false;
    }

	if($('#txtTownCity').val == null){
    	$('#txtTownCity').next('span').text('Town/City is required');
    	empty = false;
    }

    if($('#txtProvince').val == null){
    	$('#txtProvince').next('span').text('Province is required');
    	empty = false;
    }
	if($('#txtZipCode').val == null){
		$('#txtZipCode').next('span').text('Province is required');
		empty = false;
	}

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function validateFacultyPosition(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    if($('#txtPositionCode').val == true){
    	$('#txtPositionCode').next('span').text('Position Code is required');
    	empty = false;
    }

    if($('#txtPositionDesc').val == true){
    	$('#txtPositionDesc').next('span').text('Position Description is required');
    	empty = false;
    }

    if($('#txtShortName').val == true){
    	$('#txtShortName').next('span').text('Short Name is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function saveFaculty(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#FacultytuniqueID').val() == ''){
		if(validateFaculty() == true){

			var data = new Array();

			Faculty = new Object();

			Faculty.EmployeeID = $('#txtEmployeeID').val();
			Faculty.LastName = $('#txtLastName').val();
			Faculty.FirstName = $('#txtFirstName').val();
			Faculty.MiddleName = $('#txtMiddleName').val();
			Faculty.MiddleInitial = '';
			Faculty.ExtName = $('#txtExtName').val();
			Faculty.Prefix = $('#txtPrefix').val();
			Faculty.PosnTitleID = $('#txtPosnTitleID').val();
			Faculty.CampusID = $('#txtCampusID').val();
			Faculty.DateOfBirth = $('#txtDateOfBirth').val();
			Faculty.PlaceOfBirth = $('#txtPlaceOfBirth').val();
			Faculty.Gender = $('#txtGender').val();
			Faculty.TelNo = $('#txtTelNo').val();
			Faculty.MobileNo = $('#txtMobileNo').val();
			Faculty.Email = $('#txtEmail').val();
			Faculty.AddressNo = $('#txtAddressNo').val();
			Faculty.Street = $('#txtStreet').val();
			Faculty.Barangay = $('#txtBarangay').val();
			Faculty.TownCity = $('#txtTownCity').val();
			Facultyl.Province = $('#txtProvince').val();
			Faculty.ZipCode = $('#txtZipCode').val();
			Faculty.Photo = '';
			Faculty.Signature = '';

			if($('#chckInActive').prop("checked") == true){
				Faculty.Inactive = '1';
			}else{
				Faculty.Inactive = '0';
			}

			data.push(Faculty);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertFaculty',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "faculty.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateSubject() == true){

			var data = new Array();

			Faculty = new Object();

			Faculty.EmployeeID = $('#txtEmployeeID').val();
			Faculty.LastName = $('#txtLastName').val();
			Faculty.FirstName = $('#txtFirstName').val();
			Faculty.MiddleName = $('#txtMiddleName').val();
			Faculty.MiddleInitial = '';
			Faculty.ExtName = $('#txtExtName').val();
			Faculty.Prefix = $('#txtPrefix').val();
			Faculty.PosnTitleID = $('#txtPosnTitleID').val();
			Faculty.CampusID = $('#txtCampusID').val();
			Faculty.DateOfBirth = $('#txtDateOfBirth').val();
			Faculty.PlaceOfBirth = $('#txtPlaceOfBirth').val();
			Faculty.Gender = $('#txtGender').val();
			Faculty.TelNo = $('#txtTelNo').val();
			Faculty.MobileNo = $('#txtMobileNo').val();
			Faculty.Email = $('#txtEmail').val();
			Faculty.AddressNo = $('#txtAddressNo').val();
			Faculty.Street = $('#txtStreet').val();
			Faculty.Barangay = $('#txtBarangay').val();
			Faculty.TownCity = $('#txtTownCity').val();
			Facultyl.Province = $('#txtProvince').val();
			Faculty.ZipCode = $('#txtZipCode').val();
			Faculty.Photo = '';
			Faculty.Signature = '';

			if($('#chckInActive').prop("checked") == true){
				Faculty.Inactive = '1';
			}else{
				Faculty.Inactive = '0';
			}

			data.push(Faculty);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateFaculty',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "faculty.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function saveFacultyPosition(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#FacultytuniqueID').val() == ''){
		if(validateFacultyPosition() == true){

			var data = new Array();

			FacultyPosition = new Object();

			FacultyPosition.PositionCode = $('#txtPositionCode').val();
			FacultyPosition.PositionDesc = $('#txtPositionDesc').val();
			FacultyPosition.ShortName = $('#txtShortName').val();

			data.push(FacultyPosition);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertFaculty',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "faculty.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateFacultyPosition() == true){

			var data = new Array();

			FacultyPosition = new Object();

			FacultyPosition.PositionCode = $('#txtPositionCode').val();
			FacultyPosition.PositionDesc = $('#txtPositionDesc').val();
			FacultyPosition.ShortName = $('#txtShortName').val();

			data.push(FacultyPosition);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateFaculty',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "faculty.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function fecthFaculty(){
	$('#tbl_faculty tbody > tr').remove();
  var ipaddress = sessionStorage.getItem("ipaddress");

	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthFaculty',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;


	    				var html = '<tr>\
	    							<td>' +row[i].EmployeeID+ '</td>\
	    							<td>' +row[i].FullName+ '</td>\
	    							<td>' +row[i].ExtName+ '</td>\
	    							<td>' +row[i].Prefix+ '</td>\
	    							<td>' +row[i].Position+ '</td>\
	    							<td>' +row[i].Campus+ '</td>\
	    							<td>' +row[i].DateOfBirth+ '</td>\
	    							<td>' +row[i].PlaceOfBirth+ '</td>\
	    							<td>' +row[i].Gender+ '</td>\
	    							<td>' +row[i].TelNo+ '</td>\
	    							<td>' +row[i].MobileNo+ '</td>\
	    							<td>' +row[i].Email+ '</td>\
	    							<td>' +row[i].AddressNo+ '</td>\
	    							<td>' +row[i].Street+ '</td>\
	    							<td>' +row[i].Barangay+ '</td>\
	    							<td>' +row[i].TownCity+ '</td>\
	    							<td>' +row[i].Province+ '</td>\
	    							<td>' +row[i].ZipCode+ '</td>\
	    							<td>' +row[i].Photo+ '</td>\
	    							<td>' +row[i].Signature+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-faculty-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal">\
                                                <i class="fa fa-plus"></i>\
                                            </a>\
                                            <a class="edit-faculty-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].TeacherID + '">\
                                                <i class="fa fa-Pencil"></i>\
                                            </a>\
                                            <a class="remove-faculty-icon btn btn-danger btn-xs" data-id="' + row[i].TeacherID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_faculty tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
}

function fetchFacultyPosition(){
	$('#tbl_facultyPosition tbody > tr').remove();
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthFaculty',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;


	    				var html = '<tr>\
	    							<td>' +row[i].PositionCode+ '</td>\
	    							<td>' +row[i].PositionDesc+ '</td>\
	    							<td>' +row[i].ShortName+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-facultyposition-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal">\
                                                <i class="fa fa-plus"></i>\
                                            </a>\
                                            <a class="edit-facultyposition-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].PosnTitleID + '">\
                                                <i class="fa fa-Pencil"></i>\
                                            </a>\
                                            <a class="remove-facultyposition-icon btn btn-danger btn-xs" data-id="' + row[i].PosnTitleID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_facultyPosition tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
}

function fetchCampus(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
         url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'selectCampus'
        },
        success: function(response) {
            var decode = response;
            $('#chckCampus').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;


                var html = '<option id="' + row[i].ProgID + '" value="' + row[i].ProgID + '">' + row[i].ProgName + '</option>';
                $("#chckCampus").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function fetchPosition(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
         url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'selectPosition'
        },
        success: function(response) {
            var decode = response;
            $('#chckCampus').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;


                var html = '<option id="' + row[i].PosnTitleID + '" value="' + row[i].PosnTitleID + '">' + row[i].PositionCode + '</option>';
                $("#chckCampus").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

//Edit Faculty
$(document).on("click", ".edit-faculty-icon", function() {

    var TeacherID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getFaculty',
            TeacherID: TeacherID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
            	$('#FacultytuniqueID').val(decode.result.TeacherID);
                $('#txtEmployeeID').val(decode.result.EmployeeID);
        				$('#txtLastName').val(decode.result.LastName);
        				$('#txtFirstName').val(decode.result.FirstName);
        				$('#txtMiddleName').val(decode.result.MiddleName);
        				$('#txtExtName').val(decode.result.ExtName);
        				$('#txtPrefix').val(decode.result.Prefix);
        				$('#txtPosnTitleID').val(decode.result.PosnTitleID);
        				$('#txtCampusID').val(decode.result.CampusID);
        				$('#txtDateOfBirth').val(decode.result.DateOfBirth);
        				$('#txtPlaceOfBirth').val(decode.result.PlaceOfBirth);
        				$('#txtGender').val(decode.result.Gender);
        				$('#txtTelNo').val(decode.result.TelNo);
        				$('#txtMobileNo').val(decode.result.MobileNo);
        				$('#txtEmail').val(decode.result.Email);
        				$('#txtAddressNo').val(decode.result.AddressNo);
        				$('#txtStreet').val(decode.result.Street);
        				$('#txtBarangay').val(decode.result.Barangay);
        				$('#txtTownCity').val(decode.result.TownCity);
        				$('#txtProvince').val(decode.result.Province);
        				$('#txtZipCode').val(decode.result.ZipCode);

                $('#myModalSubjectMode').modal('show')
            } else if (decode.success === false) {
                $('#myModalSubjectMode').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

//Edit FacultyPosition
$(document).on("click", ".edit-facultyposition-icon", function() {

    var PosnTitleID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getFacultyPosition',
            PosnTitleID: PosnTitleID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#FacultyPositiontuniqueID').val(decode.result.PosnTitleID);
				$('#txtPositionCode').val(decode.result.PositionCode);
				$('#txtPositionDesc').val(decode.result.PositionDesc);
				$('#txtShortName').val(decode.result.ShortName);


                $('#myModalFacultyPosition').modal('show')
            } else if (decode.success === false) {
                $('#myModalFacultyPosition').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

// on delete icon click Faculty
$(document).on("click", ".remove-faculty-icon", function() {
    if (confirm('Are you sure to delete this Faculty?')) {

        var TeacherID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteFaculty',
                TeacherID: TeacherID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "faculty.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// on delete icon click Faculty Position
$(document).on("click", ".remove-facultyposition-icon", function() {
    if (confirm('Are you sure to delete this Faculty Position?')) {

        var PosnTitleID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteFacultyPosition',
                PosnTitleID: PosnTitleID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "faculty.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});
