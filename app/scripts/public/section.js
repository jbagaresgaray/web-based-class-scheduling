var subjectoffering = new Array();

$(document).ready(function() {
    fetch_college();
    fetch_yearlevel();
    fetch_AcadProgram();

    window.localStorage.removeItem("subjectoffering");
});

$('#subjectofferingModal').on('hidden.bs.modal', function(e) {
    console.log('clear subjectofferingModal');
    $('#tblSearchSubject tbody > tr').remove();
    $('#tblOfferedSubject tbody > tr').remove();

    $('span.help-inline').each(function() {
        $(this).text('');
    });
    subjectoffering.splice(0,subjectoffering.length);
    window.localStorage.removeItem("subjectoffering");
});

$('#blockSectionModal').on('hidden.bs.modal', function(e) {
    console.log('clear blockSectionModal');
    $('#txtSectionName').val('');
    $('#txtLimit').val('');

    $('#txtSectionName').next('span').text('');
    $('#txtLimit').next('span').text('');
    $('#cboSectionAYTerm').next('span').text('');
    $('#cboYearLevel').next('span').text('');
    $('#cboYearLevelTerm').next('span').text('');
    $('#cboProgram').next('span').text('');

});

function refresh() {
    fillClassSection($('#cboAYTerm').val(), '1');
}

function fetch_college() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboAYTerm').empty();
    $('#cboSectionAYTerm').empty();
    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/scheduling.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_ScheduleAYTerm'
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + (row[i].SchoolYear + ' - ' + row[i].SchoolTerm) + '</option>';
                $("#cboAYTerm").append(html);
                $("#cboSectionAYTerm").append(html);
            }

            fillClassSection($('#cboAYTerm').val(), '1');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function fetch_yearlevel() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboYearLevel').empty();
    $.post('http://'+ ipaddress +'/scheduling/API/scheduling.php', {
        command: 'select_ScheduleYearLevel'
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].YearLevelID + '" value="' + row[i].YearLevelID + '">' + row[i].YearLevelTitle + '</option>';
            $("#cboYearLevel").append(html);
        }
    });
}


function fetch_AcadProgram() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/scheduling.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_ScheduleAcadPrograms'
        },
        success: function(response) {
            var decode = response;
            $('#cboProgram').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                var html = '<option id="' + row[i].ProgID + '" value="' + row[i].ProgID + '">' + row[i].ProgName + '</option>';
                $("#cboProgram").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function search_subject() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value = $('#searchValue').val();

    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/section.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'SearchSubjectForSection',
            value: value
        },
        success: function(response) {
            var decode = response;
            $('#tblSearchSubject tbody > tr').remove();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;

                var html = '<tr>\
                            <td>' + row[i].SubjectCode + '</td>\
                            <td>' + row[i].SubjectTitle + '</td>\
                            <td>' + row[i].Units + '</td>\
                            <td>' + row[i].LabUnits + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="add-subject-icon btn btn-primary btn-xs" data-id="' + row[i].SubjectID + '">\
                                  <i class="fa fa-plus"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tblSearchSubject tbody").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

$("#cboAYTerm").change(function() {
    console.log('cboAYTerm onChange');
    fillClassSection($('#cboAYTerm').val(), '1');
});

$(document).on("click", "#cboProgram", function() {
    console.log('cboAYTerm onclick');
    fillClassSection($('#cboAYTerm').val(), '1');
});

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fillClassSection($('#cboAYTerm').val(), page);
});

function fillClassSection(TermID, page) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#tblSection tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/section.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'GetAllSection',
            TermID: TermID,
            page: page
        },
        success: function(response) {
            var decode = response;

            for (var i = 0; i < decode.result.result.length; i++) {
                var row = decode.result.result;

                var html = '<tr>\
                            <td>' + row[i].SectionTitle + '</td>\
                            <td>' + row[i].YearLvlTerm + '</td>\
                            <td>' + row[i].IsBlock + '</td>\
                            <td>' + row[i].Dissolved + '</td>\
                            <td>' + row[i].Limit + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="detail-section-icon btn btn-success btn-xs" data-programid='+ row[i].ProgramID +' data-id="' + row[i].SectionID + '">\
                                  <i class="fa fa-search"></i>\
                                </a>\
                                <a class="edit-section-icon btn btn-primary btn-xs" data-programid='+ row[i].ProgramID +' data-id="' + row[i].SectionID + '">\
                                  <i class="fa fa-pencil"></i>\
                                </a>\
                                <a class="delete-section-icon btn btn-danger btn-xs" data-programid='+ row[i].ProgramID +' data-id="' + row[i].SectionID + '">\
                                  <i class="fa fa-trash"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tblSection tbody").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function getSubjectOffering(SectionID,TermID,ProgramID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/section.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'checkHasOffering',
            SectionID: SectionID,
            TermID:TermID
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                $('#tblOfferedSubject tbody > tr').remove();
                $.ajax({
                    url: 'http://'+ipaddress+'/scheduling/API/section.php',
                    async: false,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    data: {
                        command: 'getSubjectOffered',
                        SectionID: SectionID,
                        TermID:TermID,
                        ProgramID:ProgramID
                    },
                    success: function(response) {
                        var decode = response;
                        window.localStorage['subjectoffering'] = JSON.stringify(decode.result);
                        for (var i = 0; i < decode.result.length; i++) {
                            var subj = decode.result;

                            var subject = new Object();
                            subject.SubjectID = subj[i].SubjectID;
                            subject.SubjectCode = subj[i].SubjectCode;
                            subject.SubjectTitle = subj[i].SubjectTitle;
                            subject.Units = subj[i].Units;
                            subject.LabUnits = subj[i].LabUnits;
                            subject.Limit = subj[i].Limit;
                            subject.SubjectOfferingID = subj[i].SubjectOfferingID;
                            subject.SectionTitle = subj[i].SectionTitle;
                            subject.SectionID = subj[i].SectionID;
                            subject.TermID = subj[i].TermID;
                            subject.IsSpecialClasses =  subj[i].IsSpecialClasses;
                            subject.IsDissolved =  subj[i].IsDissolved;
                            subject.OverRideConflict =  subj[i].OverRideConflict;
                            subject.ProgramID = subj[i].ProgramID;

                            subjectoffering.push(subject);

                            var html = '<tr>\
                                            <td>' + subj[i].SubjectCode + ' <input type="hidden" name="txtSubjectID" value="' + subj[i].SubjectID + '"></td>\
                                            <td>' + subj[i].SubjectTitle + '</td>\
                                            <td>' + subj[i].Units + '</td>\
                                            <td>' + subj[i].LabUnits + '</td>\
                                            <td><input type="text" class="form-control" name="txtlimit" placeholder="Limit" value="' + subj[i].Limit + '"></td>\
                                            <td>\
                                              <div class="text-right">\
                                                <a class="remove-subject-icon btn btn-danger btn-xs" data-programid='+ subj[i].ProgramID +' data-id="' + subj[i].SubjectID + '">\
                                                  <i class="fa fa-times"></i>\
                                                </a>\
                                              </div>\
                                            </td>\
                                        </tr>';
                            $("#tblOfferedSubject tbody ").append(html);
                        }
                    }
                });
            }
        }
    });
}

$(document).on("click", ".detail-section-icon", function() {

    var $row = $(this).closest('tr').children('td');
    var SectionID = $(this).data('id');
    var ProgramID = $(this).data('programid');

    $('#classSectionName').html($row.eq(0).text());
    $('#hiddenSectionID').val(SectionID);
    $('#hiddenProgramID').val(ProgramID);

    getSubjectOffering(SectionID,$('#cboAYTerm').val(),ProgramID);
    $('#subjectofferingModal').modal('show');
});

$(document).on("click", ".edit-section-icon", function() {
    var SectionID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.post('http://'+ipaddress+'/scheduling/API/section.php', {
        command: 'getSectionDetails',
        SectionID: SectionID
    }, function(response) {
        var decode = response;
        if (decode.success == true) {
            $('#authenticity_token').val(decode.result.result[0].SectionID);
            $('#txtLimit').val(decode.result.result[0].Limit);
            $('#txtSectionName').val(decode.result.result[0].SectionTitle);

            $('#cboProgram').val(decode.result.result[0].ProgramID);
            $('#cboSectionAYTerm').val(decode.result.result[0].TermID);
            $('#cboYearLevel').val(decode.result.result[0].YearLevelID);

            if (decode.result.result[0].IsBlock == 1) {
                $('#chkBlockSection').prop("checked", true);
            } else {
                $('#chkBlockSection').prop("checked", false);
            }

            if (decode.result.result[0].IsDissolved == 1) {
                $('#chkDissolved').prop("checked", true);
            } else {
                $('#chkDissolved').prop("checked", false);
            }

            $('#blockSectionModal').modal('show');

        } else if (decode.success === false) {
            alert(decode.msg);
            return;
        }
    });

});

function SectionHasSchedule(TermID, SectionID) {
    var returnval = false;
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.post('http://'+ipaddress+'/scheduling/API/section.php', {
        command: 'SectionHasSchedule',
        TermID: TermID,
        SectionID: SectionID
    }, function(response) {
        var decode = response;
        console.log(decode);
        if (decode.success == true) {
            returnval = true;
        } else if (decode.success === false) {
            returnval = false;
        }
    });
    return returnval;
}

$(document).on("click", ".delete-section-icon", function() {
    if (confirm("Are you sure to Delete this Class Section?")) {
        var SectionID = $(this).data('id');
        var TermID = $('#cboAYTerm').val();

        if (SectionHasSchedule(TermID, SectionID)) {
            alert('WARNING: Unable to delete Class Section... This section contains subjects with Class Schedules');
            return;
        } else {
          var ipaddress = sessionStorage.getItem("ipaddress");
            $.post('http://'+ipaddress+'/scheduling/API/section.php', {
                command: 'DeleteSection',
                SectionID: SectionID
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    refresh();
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    }
});


$(document).on("click", ".add-subject-icon", function() {
    var subj = JSON.parse(window.localStorage['subjectoffering'] || '{}');
    var subjectid = $(this).data('id');

    if ($('#subjectLimit').val() == '') {
        $('#subjectLimit').next('span').text('***');
        alert('Subject Limit is required.');
        return;
    }

    if (parseInt($('#subjectLimit').val()) < 1) {
        $('#subjectLimit').next('span').text('***');
        alert('Subject Limit must be greater than 0 (zero).');
        return;
    }

    var $row = $(this).closest('tr').children('td');

    var subject = new Object();
    subject.SubjectID = subjectid;
    subject.SubjectCode = $row.eq(0).text();
    subject.SubjectTitle = $row.eq(1).text();
    subject.Units = $row.eq(2).text();
    subject.LabUnits = $row.eq(3).text();
    subject.Limit = $('#subjectLimit').val();
    subject.SubjectOfferingID = '';
    subject.SectionTitle =  $('#classSectionName').text();
    subject.SectionID = $('#hiddenSectionID').val();
    subject.ProgramID = $('#hiddenProgramID').val();
    subject.TermID = $('#cboAYTerm').val();
    subject.IsSpecialClasses = 0;
    subject.IsDissolved = 0;
    subject.OverRideConflict = 0;

    for (var i = 0; i < subj.length; i++) {
        if (subj[i].SubjectID === subjectid) {
            alert('Selected subject already on the list');
            return;
        }
    }

    subjectoffering.push(subject);
    window.localStorage.removeItem("subjectoffering");
    window.localStorage['subjectoffering'] = JSON.stringify(subjectoffering);

    getSubjectOffered();
});

$(document).on("click", ".remove-subject-icon", function() {
    console.log('remove-subject-icon');

    var subjectid = $(this).data('id');
    var ProgramID = $(this).data('programid');

    var TermID = $('#cboAYTerm').val();
    var SectionID = $('#hiddenSectionID').val();

    if (confirm("Are you sure to save this Subject Offering?")) {
        if (checkHasOfferingHasSchedule(subjectid, TermID, SectionID) === true) {
            alert('WARNING: Unable to delete Class Section Subject... It has class Schedule already!');
            return;
        } else {
            var subj = JSON.parse(window.localStorage['subjectoffering'] || '{}');

            if (subj.length > 0) {
                console.log('(subj.length > 0)');
                for (var i = 0; i < subj.length; i++) {
                    console.log('(subj) ' + subj[i].SubjectID);
                    if (subj[i].SubjectID == subjectid) {
                        subj.splice(i, 1);
                        window.localStorage['subjectoffering'] = JSON.stringify(subj);
                    }
                }

                for (var i = 0; i < subjectoffering.length; i++) {
                    if (subjectoffering[i].SubjectID == subjectid) {
                        subjectoffering.splice(i, 1);
                        console.log('subjectoffering.splice(i, 1)');
                    }
                }

                DeleteSectionOffering(subjectid, TermID, SectionID);
            } else {
                console.log('removeItem');
                window.localStorage.removeItem("subjectoffering");
            }
        }
    }
    getSubjectOffering(SectionID,$('#cboAYTerm').val(),ProgramID);
    getSubjectOffered();
});

function checkHasOfferingHasSchedule(SubjectID, TermID, SectionID) {
    var returnval = false;
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.post('http://'+ipaddress+'/scheduling/API/section.php', {
        command: 'checkHasOfferingHasSchedule',
        SubjectID: SubjectID,
        TermID: TermID,
        SectionID: SectionID
    }, function(response) {
        var decode = response;
        console.log(decode);
        if (decode.success == true) {
            returnval = true;
        } else if (decode.success === false) {
            returnval = false;
        }
    });
    return returnval;
}


function DeleteSectionOffering(SubjectID, TermID, SectionID) {
    var returnval = false;
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.post('http://'+ipaddress+'/scheduling/API/section.php', {
        command: 'DeleteSectionOffering',
        SubjectID: SubjectID,
        TermID: TermID,
        SectionID: SectionID
    }, function(response) {
        var decode = response;
        console.log(decode);
        if (decode.success == true) {
            returnval = true;
        } else if (decode.success === false) {
            returnval = false;
        }
    });
    return returnval;
}

function getSubjectOffered() {
    var subj = JSON.parse(window.localStorage['subjectoffering'] || '{}');
    console.log('getSubjectOffered');
    $('#tblOfferedSubject tbody > tr').remove();
    if (subj.length > 0) {
        for (var i = 0; i < subj.length; i++) {
            var html = '<tr>\
                            <td>' + subj[i].SubjectCode + ' <input type="hidden" name="txtSubjectID" value="' + subj[i].SubjectID + '"></td>\
                            <td>' + subj[i].SubjectTitle + '</td>\
                            <td>' + subj[i].Units + '</td>\
                            <td>' + subj[i].LabUnits + '</td>\
                            <td><input type="text" class="form-control" name="txtlimit" placeholder="Limit" value="' + subj[i].Limit + '"></td>\
                            <td>\
                              <div class="text-right">\
                                <a class="remove-subject-icon btn btn-danger btn-xs" data-id="' + subj[i].SubjectID + '">\
                                  <i class="fa fa-times"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
            $("#tblOfferedSubject tbody").append(html);
        }
    }
}





function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtSectionName').val() == '') {
        $('#txtSectionName').next('span').text('Class Section Name is required.');
        empty = true;
    }

    if ($('#txtLimit').val() == '') {
        $('#txtLimit').next('span').text('Class Section Limit is required.');
        empty = true;
    }

    if (parseInt($('#txtLimit').val()) < 1) {
        $('#txtLimit').next('span').text('Class Section Limit must be greater than 0.');
        empty = true;
    }

    if ($('#cboSectionAYTerm').val() == '') {
        $('#cboSectionAYTerm').next('span').text('Academic Term is required.');
        empty = true;
    }

    if ($('#cboYearLevel').val() == '') {
        $('#cboYearLevel').next('span').text('Academic Year Level is required.');
        empty = true;
    }

    if ($('#cboProgram').val() == '') {
        $('#cboProgram').next('span').text('Academic Program is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save_class_Section() {
    var SectionID = $('#authenticity_token').val();
    var ipaddress = sessionStorage.getItem("ipaddress");

    var data = new Array();
    var section = new Object();

    section.SectionTitle = $('#txtSectionName').val();
    section.YearLevelID = $('#cboYearLevel').val();
    section.TermID = $('#cboSectionAYTerm').val();
    section.ProgramID = $('#cboProgram').val();
    section.Limit = $('#txtLimit').val();
    section.CreatedBy = 'admin';
    if ($('#chkBlockSection').prop("checked") == true) {
        section.IsBlock = '1';
    } else {
        section.IsBlock = '0';
    }

    if ($('#chkDissolved').prop("checked") == true) {
        section.IsDissolved = '1';
    } else {
        section.IsDissolved = '0';
    }

    data.push(section);

    if (SectionID == '') {
        if (validate() == true) {
            $.post('http://'+ipaddress+'/scheduling/API/section.php', {
                command: 'InsertSection',
                data: data
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    $('#blockSectionModal').modal('hide');
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    } else {
        if (validate() == true) {
            $.post('http://'+ipaddress+'/scheduling/API/section.php', {
                command: 'UpdateSection',
                SectionID: SectionID,
                data: data
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    $('#blockSectionModal').modal('hide');
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    }
}



function save_subject_offering() {
    resetHelpInLine();

    if ($('#subjectLimit').val() == '') {
        $('#subjectLimit').next('span').text('***');
        alert('Subject Limit is required.');
        return;
    }

    if (parseInt($('#subjectLimit').val()) < 1) {
        $('#subjectLimit').next('span').text('***');
        alert('Subject Limit must be greater than 0 (zero).');
        return;
    }

    if (confirm("Are you sure to save this Subject Offering?")) {

        var exist_row = false;
        var offering = new Array();

        var subj = JSON.parse(window.localStorage['subjectoffering'] || '{}');
        if (subj.length > 0) {
            for (var i = 0; i < subj.length; i++) {

                $('#tblOfferedSubject tbody tr').each(function() {
                    if ($(this).find('input[name="txtlimit"]').val() == '') {
                        exist_row = true;
                    } else if (parseInt($(this).find('input[name="txtlimit"]').val()) < 1) {
                        exist_row = true;
                    }
                });

                var subject = new Object();
                subject.SubjectID = subj[i].SubjectID;
                subject.SectionID = subj[i].SectionID;
                subject.TermID = subj[i].TermID;
                subject.Limit = subj[i].Limit;
                subject.IsSpecialClasses =subj[i].IsSpecialClasses;
                subject.IsDissolved = subj[i].IsDissolved;
                subject.OverRideConflict = subj[i].OverRideConflict;
                subject.CreatedBy = 'admin';

                offering.push(subject);
            }
        } else {
            alert('Unable to save Subject Offering when its empty');
            return;
        }

        if (exist_row) {
            alert('Please enter number of student limit per subject');
            return;
        } else {
            var ipaddress = sessionStorage.getItem("ipaddress");
            $.post('http://'+ ipaddress +'/scheduling/API/section.php', {
                command: 'InsertSubjectOffering',
                data: offering
            }, function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    alert(decode.msg);
                    $('#subjectofferingModal').modal('hide');
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    }
}
