$(document).ready(function(){

});

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}


function validateYearLevel() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtYearLevelTitle').val() == '') {
        $('#txtYearLevelTitle').next('span').text('Year level title is required.');
        empty = true;
    }

    if ($('#txtYearLevelDesc').val() == '') {
        $('#txtYearLevelDesc').next('span').text('Year level Description is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function saveYearLevel(){
	$('#btn-save').button('loding');

	if($('#TermuniqueID').val() == ''){
		if(validate() == true){
			
			var data = new Array();

			yearlevel = new Object();

			yearlevel.YearLevelTitle = $('#txtYearLevelTitle').val();
			yearlevel.YearLevelDesc = $('#txtYearLevelDesc').val();

			data.push(yearlevel);

			$.ajax({
				url: 'http://localhost/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertYearLevel',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "yearlevel.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});

		}
	}else{
		if(validate() == true){
			
			var data = new Array();

			yearlevel = new Object();

			yearlevel.YearLevelTitle = $('#txtYearLevelTitle').val();
			yearlevel.YearLevelDesc = $('#txtYearLevelDesc').val();

			data.push(yearlevel);

			$.ajax({
				url: 'http://localhost/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateYearLevel',
		            data : data,
		            YearLevelID: $('#TermuniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "yearlevel.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});

		}
	}
}