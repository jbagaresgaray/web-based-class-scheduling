$(document).ready(function(){
	fecthTerm('1');
});

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}


function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtSchoolYear').val() == '') {
        $('#txtSchoolYear').next('span').text('School Year is required.');
        empty = true;
    }

    if ($('#txtSchoolTerm').val() == '') {
        $('#txtSchoolTerm').next('span').text('School Term is required.');
        empty = true;
    }

    if ($('#txtStartofSY').val() == '') {
        $('#txtStartofSY').next('span').text('Start of School Year is required.');
        empty = true;
    }

    if ($('#txtEndofSY').val() == '') {
        $('#txtEndofSY').next('span').text('End of School Year is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save(){
	$('#btn-save').button('loding');


	if($('#TermuniqueID').val() == ''){
		if(validate() == true){
			
			var data = new Array();

			term = new Object();

			term.SchoolYear = $('#txtSchoolYear').val();
			if($('#chckLocked').prop("checked") == true){
				term.Locked = '1';
			}else{
				term.Locked = '0';
			}

			term.SchoolTerm = $('#txtSchoolTerm').val();

			if($('#chckSchoolTerm').val() == 1){
				term.SchoolTerm = 'School Year';
			}else if($('#chckSchoolTerm').val() == 2){
				term.SchoolTerm = '1st Semester';
			}else if($('#chckSchoolTerm').val() == 3){
				term.SchoolTerm = '2nd Semester';
			}else if($('#chckSchoolTerm').val() == 4){
				term.SchoolTerm = 'Summer';
			}else if($('#chckSchoolTerm').val() == 5){
				term.SchoolTerm = '1st Trimester';
			}else if($('#chckSchoolTerm').val() == 6){
				term.SchoolTerm = '2nd Trimester';
			}else if($('#chckSchoolTerm').val() == 7){
				term.SchoolTerm = '3rd Trimester';
			}else if($('#chckSchoolTerm').val() == 8){
				term.SchoolTerm = '1st Quarter';
			}else if($('#chckSchoolTerm').val() == 9){
				term.SchoolTerm = '2nd Quarter';
			}else if($('#chckSchoolTerm').val() == 10){
				term.SchoolTerm = '3rd Quarter';
			}else if($('#chckSchoolTerm').val() == 11){
				term.SchoolTerm = '4th Quarter';
			}else if($('#chckSchoolTerm').val() == 12){
				term.SchoolTerm = '6 Months [January-June]';
			}else if($('#chckSchoolTerm').val() == 13){
				term.SchoolTerm = '6 Months [July-December]';
			}

			term.StartofSY = $('#txtStartofSY').val();
			term.EndofSY = $('#txtEndofSY').val();

			if($('#chckHidden').prop("checked") == true){
				term.tHidden = '1';
			}else{
				term.tHidden = '0';
			}

			data.push(term);

			$.ajax({
				url: 'http://localhost/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertTerm',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "ayterm.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});

		}
	}else{
		if(validate() == true){
			
			var data = new Array();

			term = new Object();

			term.SchoolYear = $('#txtSchoolYear').val();
			if($('#chckLocked').prop("checked") == true){
				term.Locked = '1';
			}else{
				term.Locked = '0';
			}

			if($('#chckSchoolTerm').val() == 1){
				term.SchoolTerm = 'School Year';
			}else if($('#chckSchoolTerm').val() == 2){
				term.SchoolTerm = '1st Semester';
			}else if($('#chckSchoolTerm').val() == 3){
				term.SchoolTerm = '2nd Semester';
			}else if($('#chckSchoolTerm').val() == 4){
				term.SchoolTerm = 'Summer';
			}else if($('#chckSchoolTerm').val() == 5){
				term.SchoolTerm = '1st Trimester';
			}else if($('#chckSchoolTerm').val() == 6){
				term.SchoolTerm = '2nd Trimester';
			}else if($('#chckSchoolTerm').val() == 7){
				term.SchoolTerm = '3rd Trimester';
			}else if($('#chckSchoolTerm').val() == 8){
				term.SchoolTerm = '1st Quarter';
			}else if($('#chckSchoolTerm').val() == 9){
				term.SchoolTerm = '2nd Quarter';
			}else if($('#chckSchoolTerm').val() == 10){
				term.SchoolTerm = '3rd Quarter';
			}else if($('#chckSchoolTerm').val() == 11){
				term.SchoolTerm = '4th Quarter';
			}else if($('#chckSchoolTerm').val() == 12){
				term.SchoolTerm = '6 Months [January-June]';
			}else if($('#chckSchoolTerm').val() == 13){
				term.SchoolTerm = '6 Months [July-December]';
			}

			term.StartofSY = $('#txtStartofSY').val();
			term.EndofSY = $('#txtEndofSY').val();

			if($('#chckHidden').prop("checked") == true){
				term.tHidden = '1';
			}else{
				term.tHidden = '0';
			}

			data.push(term);

			$.ajax({
				url: 'http://localhost/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateTerm',
		            data : data,
		            TermID : $('#TermuniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "ayterm.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});

		}
	}
}

function fecthTerm(page){
	$('#tbl_Term tbody > tr').remove();

	$.ajax({
		url: 'http://localhost/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'GetAllTerm',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;
	    	if(decode){
	    		if(decode.result.result.length > 0){
	    			for(var i = 0; i < decode.result.result.length; i++){
	    				var row = decode.result.result;
	    				var html = '<tr>\
	    							<td>' +row[i].SchoolYear+ '</td>\
	    							<td>' +row[i].SchoolTerm+ '</td>\
	    							<td>' +row[i].StartofSY+ '</td>\
	    							<td>' +row[i].EndofSY+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="edit-term-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].TermID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-term-icon btn btn-danger btn-xs" data-id="' + row[i].TermID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_Term tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

function searchTerm(){
	$('#tbl_Term tbody > tr').remove();

	$.ajax({
		url: 'http://localhost/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'searchTerm',
	        value: $('#txtValue').val()
	    },
	    success: function(response){
	    	var decode = response;
	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;
	    				var html = '<tr>\
	    							<td>' +row[i].SchoolYear+ '</td>\
	    							<td>' +row[i].SchoolTerm+ '</td>\
	    							<td>' +row[i].StartofSY+ '</td>\
	    							<td>' +row[i].EndofSY+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="edit-term-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].TermID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-term-icon btn btn-danger btn-xs" data-id="' + row[i].TermID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_Term tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

$(document).on("click", ".edit-term-icon", function() {

	console.log('open for edit ...');

    var TermID = $(this).data('id');

    $.ajax({
        url: 'http://localhost/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getTerm',
            TermID: TermID
        },
        success: function(response) {
            var decode = response;

            console.log(decode.result[0].TermID);

            
            	$('#TermuniqueID').val(decode.result[0].TermID);
                $('#txtSchoolYear').val(decode.result[0].SchoolYear);
                $('#txtSchoolTerm').val(decode.result[0].SchoolTerm);

                if(decode.result[0].SchoolTerm == 'School Year'){
					$('#chckSchoolTerm').val('1');
				}else if(decode.result[0].SchoolTerm == '1st Semester'){
					$('#chckSchoolTerm').val('2');
				}else if(decode.result[0].SchoolTerm == '2nd Semester'){
					$('#chckSchoolTerm').val('3');
				}else if(decode.result[0].SchoolTerm == 'Summer'){
					$('#chckSchoolTerm').val('4');
				}else if(decode.result[0].SchoolTerm == '1st Trimester'){
					$('#chckSchoolTerm').val('5');
				}else if(decode.result[0].SchoolTerm == '2nd Trimester'){
					$('#chckSchoolTerm').val('6');
				}else if(decode.result[0].SchoolTerm == '3rd Trimester'){
					$('#chckSchoolTerm').val('7');
				}else if(decode.result[0].SchoolTerm == '1st Quarter'){
					$('#chckSchoolTerm').val('8');
				}else if(decode.result[0].SchoolTerm == '2nd Quarter'){
					$('#chckSchoolTerm').val('9');
				}else if(decode.result[0].SchoolTerm == '3rd Quarter'){
					$('#chckSchoolTerm').val('10');
				}else if(decode.result[0].SchoolTerm == '4th Quarters'){
					$('#chckSchoolTerm').val('11');
				}else if(decode.result[0].SchoolTerm == '6 Months [January-June]'){
					$('#chckSchoolTerm').val('12');
				}else if(decode.result[0].SchoolTerm == '6 Months [July-December]'){
					$('#chckSchoolTerm').val('13');
				}

                var dataToSplit = decode.result[0].StartofSY;
                if (dataToSplit != null) {
                    var i = dataToSplit.slice(0, 10).split('-');
                    var date = i[0] + "-" + i[1] + "-" + i[2];
                    $('#txtStartofSY').val(date);
                }

                var dataToSplit1 = decode.result[0].EndofSY;
                if (dataToSplit1 != null) {
                    var i1 = dataToSplit1.slice(0, 10).split('-');
                    var date1 = i1[0] + "-" + i1[1] + "-" + i1[2];
                    $('#txtEndofSY').val(date1);
                }

				if(decode.result[0].Locked == 1){
					$('#chckLocked').prop("checked", true);
				}else{
					$('#chckLocked').prop("checked", false);
				}

				if(decode.result[0].Hidden == 1){
					$('#chckHidden').prop("checked", true);
				}else{
					$('#chckHidden').prop("checked", false);
				}

                $('#myModalTerm').modal('show');
        }
    });
});

$(document).on("click", ".remove-term-icon", function() {
    if (confirm('Are you sure to delete this Term?')) {

        var TermID = $(this).data('id');

        $.ajax({
            url: 'http://localhost/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteTerm',
                TermID: TermID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "ayterm.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

function clearTermFields(){
	$('#TermuniqueID').val('');
    $('#txtSchoolYear').val('');
    $('#txtSchoolTerm').val('');
    $('#txtStartofSY').val('');
	$('#txtEndofSY').val('');
	$('#chckLocked').prop("checked", false);
	$('#chckHidden').prop("checked", false);
}