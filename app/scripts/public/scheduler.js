var FormScheduleState;
var subjectoffering = new Object();
var SubjectOfferingID;

$.fn.modal.Constructor.prototype.enforceFocus = function() {};

$(document).ready(function() {
    fetch_college();
    fetch_AcadProgram();

    fetch_building();
    fetch_roomEvents();
    fetch_faculty();

    $('#timepicker1').datetimepicker({
        pickDate: false
    });
    $('#timepicker2').datetimepicker({
        pickDate: false
    });

    $('#timepicker3').datetimepicker({
        pickDate: false
    });
    $('#timepicker4').datetimepicker({
        pickDate: false
    });

    $('#timepicker5').datetimepicker({
        pickDate: false
    });
    $('#timepicker6').datetimepicker({
        pickDate: false
    });


    $("#cboProgram").select2({
        placeholder: "Select a Academic Program"
    });

    $("#cboFaculty1").select2({
        placeholder: "Select a Faculty"
    });
    $("#cboFaculty2").select2({
        placeholder: "Select a Faculty"
    });
    $("#cboFaculty2").select2({
        placeholder: "Select a Faculty"
    });

    $("#cboRoom1").select2({
        placeholder: "Select a Room"
    });
    $("#cboRoom2").select2({
        placeholder: "Select a Room"
    });
    $("#cboRoom3").select2({
        placeholder: "Select a Room"
    });
});


$('#scheduleModal').on('hidden.bs.modal', function(e) {
    $('#cboBuilding1').val('');
    $('#cboBuilding2').val('');
    $('#cboBuilding3').val('');

    $('#cboRoom1').val('');
    $('#cboRoom2').val('');
    $('#cboRoom3').val('');

    $('#cboFaculty1').val('');
    $('#cboFaculty2').val('');
    $('#cboFaculty3').val('');


    $('#cboEvent1').val('');
    $('#cboEvent2').val('');
    $('#cboEvent3').val('');

    $('#timepicker1').data("DateTimePicker").hide();
    $('#timepicker2').data("DateTimePicker").hide();
    $('#timepicker3').data("DateTimePicker").hide();
    $('#timepicker4').data("DateTimePicker").hide();
    $('#timepicker5').data("DateTimePicker").hide();
    $('#timepicker6').data("DateTimePicker").hide();

    $('#chkOverride1').prop("checked", false);
    $('#chkOverride2').prop("checked", false);
    $('#chkOverride3').prop("checked", false);

    $("#schedule1 input:checked").each(function() {
        $(this).prop("checked", false);
    });
    $("#schedule2 input:checked").each(function() {
        $(this).prop("checked", false);
    });
    $("#schedule3 input:checked").each(function() {
        $(this).prop("checked", false);
    });
})


function refreshSubjectOffering() {
    console.log('refreshSubjectOffering');
    var SectionID = $('#classSectionID').val();
    FillSubjectOffering(SectionID, $('#cboAYTerm').val(), $('#cboProgram').val());
}


$(document).on("click", ".add-schedule-icon", function() {
    var $row = $(this).closest('tr').children('td');
    var SubjectOfferingID = $(this).data('id');

    $('#scheduleSubjectName').html($row.eq(0).text() + ' - ' + $row.eq(1).text());
    $('#hiddenSectionID').val($(this).data('sectionid'));
    $('#hiddenSubjectID').val($(this).data('subjectid'));
    $('#hiddenSubjectOfferingID').val(SubjectOfferingID);

    $('#scheduleModal').modal('show');
});

$(document).on("click", ".remove-schedule-icon", function() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var $row = $(this).closest('tr').children('td');
    var SubjectOfferingID = $(this).data('id');

    if (confirm("Are you sure to remove Schedule for this Subject?")) {
        $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
            command: 'RemoveSchedule',
            SubjectOfferingID: SubjectOfferingID
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                refreshSubjectOffering();
            } else if (decode.success == false) {
                alert(decode.msg);
                return;
            }
        });
    }
});


$("#cboBuilding1").change(function() {
    console.log('cboBuilding1 change');
    fetch_RoomByBuilding($('#cboBuilding1').val());
});
$("#cboBuilding1").click(function() {
    console.log('cboBuilding1 change');
    fetch_RoomByBuilding($('#cboBuilding1').val());
});

$("#cboBuilding2").change(function() {
    console.log('cboBuilding2 change');
    fetch_RoomByBuilding($('#cboBuilding2').val());
});
$("#cboBuilding2").click(function() {
    console.log('cboBuilding2 change');
    fetch_RoomByBuilding($('#cboBuilding2').val());
});

$("#cboBuilding3").change(function() {
    console.log('cboBuilding3 change');
    fetch_RoomByBuilding($('#cboBuilding3').val());
});
$("#cboBuilding3").click(function() {
    console.log('cboBuilding3 change');
    fetch_RoomByBuilding($('#cboBuilding3').val());
});

$("#cboAYTerm").change(function() {
    console.log('cboAYTerm change');
    fetch_SectionByTermAndProgram($('#cboAYTerm').val(), $('#cboProgram').val());
});

$("#cboProgram").change(function() {
    console.log('cboProgram change');
    fetch_SectionByTermAndProgram($('#cboAYTerm').val(), $('#cboProgram').val());
});

$(document).on("click", ".detail-section-icon", function() {

    var $row = $(this).closest('tr').children('td');
    var SectionID = $(this).data('id');

    $('#classSectionName').html($row.eq(1).text());
    $('#classSectionID').val(SectionID);

    FillSubjectOffering(SectionID, $('#cboAYTerm').val(), $('#cboProgram').val());
});

$('#chkOverride1').change(function(event) {
    if ($('#chkOverride1').prop("checked") == true) {
        $('#chkOverride1').prop("checked", true);
        $('#chkOverride2').prop("checked", true);
        $('#chkOverride3').prop("checked", true);
    } else {
        $('#chkOverride1').prop("checked", false);
        $('#chkOverride2').prop("checked", false);
        $('#chkOverride3').prop("checked", false);
    }
});
$('#chkOverride2').change(function(event) {
    if ($('#chkOverride2').prop("checked") == true) {
        $('#chkOverride1').prop("checked", true);
        $('#chkOverride2').prop("checked", true);
        $('#chkOverride3').prop("checked", true);
    } else {
        $('#chkOverride1').prop("checked", false);
        $('#chkOverride2').prop("checked", false);
        $('#chkOverride3').prop("checked", false);
    }
});
$('#chkOverride3').change(function(event) {
    if ($('#chkOverride3').prop("checked") == true) {
        $('#chkOverride1').prop("checked", true);
        $('#chkOverride2').prop("checked", true);
        $('#chkOverride3').prop("checked", true);
    } else {
        $('#chkOverride1').prop("checked", false);
        $('#chkOverride2').prop("checked", false);
        $('#chkOverride3').prop("checked", false);
    }
});








function xSched1(From, sTo, Days, DaysChar) {

    var success = true;

    var curSchedTimeEnd = 0;
    var curSchedTimeStart = 0;

    var ScheduleArray = new Array();

    var FromHr = From.split(':');
    var sToHr = sTo.split(':');

    console.log(FromHr);
    console.log(sToHr);

    var FromMin = FromHr[2].split(' ');
    var sToMin = sToHr[2].split(' ');

    curSchedTimeStart = toMilitaryTime(FromMin[1].toLowerCase(), FromHr[0], FromHr[1], FromMin[0]);
    curSchedTimeEnd = toMilitaryTime(sToMin[1].toLowerCase(), sToHr[0], sToHr[1], sToMin[0]);

    console.log('curSchedTimeStart ' + curSchedTimeStart);
    console.log('curSchedTimeEnd ' + curSchedTimeEnd);


    if (parseInt(curSchedTimeStart) < 60000) {
        alert('SCHEDULE [1] Error: Unable to continue scheduling...Start Time must not be lower than 6:00 AM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) > 235900) {
        alert('SCHEDULE [1] Error: Unable to continue scheduling...End Time must not be greater than 11:59:00 PM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) < parseInt(curSchedTimeStart)) {
        alert('SCHEDULE [1] Sorry, you have entered an Invalid Time Range...The Start Time should be earlier than that of the End Time');
        success = false;
        return success;
    }

    if ($('#chkOverride1').prop("checked") != true) {
        console.log('No Override Schedule');

        //TODO: Check for ROOM IN USE and FACULTY IN USE AND SECTION IN USE
        if (SectionInUse(curSchedTimeStart, curSchedTimeEnd, $('#hiddenSectionID').val(), Days, $('#cboAYTerm').val(), $('#hiddenSubjectOfferingID').val()) === true) {
            ShowConflicts('Subject', $('#cboAYTerm').val(), $('#hiddenSectionID').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [1] WARNING: Conflict found on the Section!!!");
            success = false;
            return success;
        }

        if (RoomInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboRoom1').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Room', $('#cboAYTerm').val(), $('#cboRoom1').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [1] WARNING: Conflict found on selected Room!!!");
            success = false;
            return success;
        }

        if (FacultyInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboFaculty1').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Faculty', $('#cboAYTerm').val(), $('#cboFaculty1').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [1] WARNING: Conflict found on selected Faculty!!!");
            success = false;
            return success;
        }

    }

    subjectoffering.Days = Days;
    subjectoffering.SchedTimeStart = curSchedTimeStart;
    subjectoffering.SchedTimeEnd = curSchedTimeEnd;
    subjectoffering.OverrideConflict = BooleanToInt($('#chkOverride1').prop("checked"));
    subjectoffering.EventID1 = $('#cboEvent1').val();
    subjectoffering.Sched1 = DaysChar + ' ' + From + '-' + sTo;
    subjectoffering.RoomID = $('#cboRoom1').val();
    subjectoffering.TeacherID = $('#cboFaculty1').val();
    subjectoffering.IsDissolved = 0;
    subjectoffering.SubjectOfferingID = $('#hiddenSubjectOfferingID').val();


    ScheduleArray.push(subjectoffering);

    if (success === true) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
            command: 'AddSchedule1',
            data: ScheduleArray
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                success = true;
                return success;
            } else if (decode.success == false) {
                alert(decode.msg);
                success = false;
                return success;
            }
        });
    }
    return success;
}

function xSched2(From, sTo, Days, DaysChar) {
    var success = true;

    var curSchedTimeEnd = 0;
    var curSchedTimeStart = 0;

    var ScheduleArray = new Array();

    var FromHr = From.split(':');
    var sToHr = sTo.split(':');

    var FromMin = FromHr[2].split(' ');
    var sToMin = sToHr[2].split(' ');

    curSchedTimeStart = toMilitaryTime(FromMin[1].toLowerCase(), FromHr[0], FromHr[1], FromMin[0]);
    curSchedTimeEnd = toMilitaryTime(sToMin[1].toLowerCase(), sToHr[0], sToHr[1], sToMin[0]);

    console.log('curSchedTimeStart ' + curSchedTimeStart);
    console.log('curSchedTimeEnd ' + curSchedTimeEnd);


    if (parseInt(curSchedTimeStart) < 60000) {
        alert('SCHEDULE [2] Error: Unable to continue scheduling...Start Time must not be lower than 6:00 AM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) > 235900) {
        alert('SCHEDULE [2] Error: Unable to continue scheduling...End Time must not be greater than 11:59:00 PM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) < parseInt(curSchedTimeStart)) {
        alert('SCHEDULE [2] Sorry, you have entered an Invalid Time Range...The Start Time should be earlier than that of the End Time');
        success = false;
        return success;
    }

    if ($('#chkOverride2').prop("checked") != true) {
        console.log('No Override Schedule');

        //TODO: Check for ROOM IN USE and FACULTY IN USE AND SECTION IN USE
        if (SectionInUse(curSchedTimeStart, curSchedTimeEnd, $('#hiddenSectionID').val(), Days, $('#cboAYTerm').val(), $('#hiddenSubjectOfferingID').val()) === true) {
            ShowConflicts('Subject', $('#cboAYTerm').val(), $('#hiddenSectionID').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [2] WARNING: Conflict found on the Section!!!");
            success = false;
            return success;
        }

        if (RoomInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboRoom2').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Room', $('#cboAYTerm').val(), $('#cboRoom2').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [2] WARNING: Conflict found on selected Room!!!");
            success = false;
            return success;
        }

        if (FacultyInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboFaculty2').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Faculty', $('#cboAYTerm').val(), $('#cboFaculty2').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [2] WARNING: Conflict found on selected Faculty!!!");
            success = false;
            return success;
        }

    }

    subjectoffering.Days = Days;
    subjectoffering.SchedTimeStart = curSchedTimeStart;
    subjectoffering.SchedTimeEnd = curSchedTimeEnd;
    subjectoffering.OverrideConflict = BooleanToInt($('#chkOverride2').prop("checked"));
    subjectoffering.EventID1 = $('#cboEvent2').val();
    subjectoffering.Sched1 = DaysChar + ' ' + From + '-' + sTo;
    subjectoffering.RoomID = $('#cboRoom2').val();
    subjectoffering.TeacherID = $('#cboFaculty2').val();
    subjectoffering.IsDissolved = 0;
    subjectoffering.SubjectOfferingID = $('#hiddenSubjectOfferingID').val();

    ScheduleArray.push(subjectoffering);

    if (success === true) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
            command: 'AddSchedule2',
            data: ScheduleArray
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                success = true;
                return success;
            } else if (decode.success == false) {
                alert(decode.msg);
                success = false;
                return success;
            }
        });
    }
    return success;
}

function xSched3(From, sTo, Days, DaysChar) {
    var success = true;

    var curSchedTimeEnd = 0;
    var curSchedTimeStart = 0;

    var ScheduleArray = new Array();

    var FromHr = From.split(':');
    var sToHr = sTo.split(':');

    var FromMin = FromHr[2].split(' ');
    var sToMin = sToHr[2].split(' ');

    curSchedTimeStart = toMilitaryTime(FromMin[1].toLowerCase(), FromHr[0], FromHr[1], FromMin[0]);
    curSchedTimeEnd = toMilitaryTime(sToMin[1].toLowerCase(), sToHr[0], sToHr[1], sToMin[0]);

    console.log('curSchedTimeStart ' + curSchedTimeStart);
    console.log('curSchedTimeEnd ' + curSchedTimeEnd);


    if (parseInt(curSchedTimeStart) < 60000) {
        alert('SCHEDULE [3] Error: Unable to continue scheduling...Start Time must not be lower than 6:00 AM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) > 235900) {
        alert('SCHEDULE [3] Error: Unable to continue scheduling...End Time must not be greater than 11:59:00 PM');
        success = false;
        return success;
    }

    if (parseInt(curSchedTimeEnd) < parseInt(curSchedTimeStart)) {
        alert('SCHEDULE [3] Sorry, you have entered an Invalid Time Range...The Start Time should be earlier than that of the End Time');
        success = false;
        return success;
    }

    if ($('#chkOverride3').prop("checked") != true) {
        console.log('No Override Schedule');

        //TODO: Check for ROOM IN USE and FACULTY IN USE AND SECTION IN USE
        if (SectionInUse(curSchedTimeStart, curSchedTimeEnd, $('#hiddenSectionID').val(), Days, $('#cboAYTerm').val(), $('#hiddenSubjectOfferingID').val()) === true) {
            ShowConflicts('Subject', $('#cboAYTerm').val(), $('#hiddenSectionID').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [3] WARNING: Conflict found on the Section!!!");
            success = false;
            return success;
        }

        if (RoomInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboRoom3').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Room', $('#cboAYTerm').val(), $('#cboRoom3').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [3] WARNING: Conflict found on selected Room!!!");
            success = false;
            return success;
        }

        if (FacultyInUse(curSchedTimeStart, curSchedTimeEnd, $('#cboFaculty3').val(), Days, $('#cboAYTerm').val()) === true) {
            ShowConflicts('Faculty', $('#cboAYTerm').val(), $('#cboFaculty3').val(), Days, curSchedTimeStart, curSchedTimeEnd, $('#hiddenSubjectOfferingID').val());
            alert("SCHEDULE [3] WARNING: Conflict found on selected Faculty!!!");
            success = false;
            return success;
        }

    }

    subjectoffering.Days = Days;
    subjectoffering.SchedTimeStart = curSchedTimeStart;
    subjectoffering.SchedTimeEnd = curSchedTimeEnd;
    subjectoffering.OverrideConflict = BooleanToInt($('#chkOverride3').prop("checked"));
    subjectoffering.EventID1 = $('#cboEvent3').val();
    subjectoffering.Sched1 = DaysChar + ' ' + From + '-' + sTo;
    subjectoffering.RoomID = $('#cboRoom3').val();
    subjectoffering.TeacherID = $('#cboFaculty3').val();
    subjectoffering.IsDissolved = 0;
    subjectoffering.SubjectOfferingID = $('#hiddenSubjectOfferingID').val();

    ScheduleArray.push(subjectoffering);

    if (success === true) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
            command: 'AddSchedule3',
            data: ScheduleArray
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                success = true;
                return success;
            } else if (decode.success == false) {
                alert(decode.msg);
                success = false;
                return success;
            }
        });
    }

    return success;
}






function saveSchedule() {

    var chkArray1 = [];
    var chkArray2 = [];
    var chkArray3 = [];

    var SubjectOfferingID = '';

    $("#schedule1 input:checked").each(function() {
        chkArray1.push($(this).val());
    });
    $("#schedule2 input:checked").each(function() {
        chkArray2.push($(this).val());
    });
    $("#schedule3 input:checked").each(function() {
        chkArray3.push($(this).val());
    });

    var sDay1 = dayLettersToInteger(chkArray1);
    var sDayChar1 = dayLettersToChar(chkArray1);

    var sDay2 = dayLettersToInteger(chkArray2);
    var sDayChar2 = dayLettersToChar(chkArray2);

    var sDay3 = dayLettersToInteger(chkArray3);
    var sDayChar3 = dayLettersToChar(chkArray3);

    if (chkArray1.length > 0 && chkArray2.length < 1 && chkArray3.length < 1) {
        console.log('ScheduleState.OneScheduleState');
        FormScheduleState = ScheduleState.OneScheduleState;
    } else if (chkArray2.length > 0 && chkArray1.length > 0 && chkArray3.length < 1) {
        console.log('ScheduleState.TwoScheduleState');
        FormScheduleState = ScheduleState.TwoScheduleState;
    } else if (chkArray3.length > 0 && chkArray1.length > 0 && chkArray2.length > 0) {
        console.log('ScheduleState.ThreeScheduleState');
        FormScheduleState = ScheduleState.ThreeScheduleState;
    }


    switch (FormScheduleState) {
        case ScheduleState.OneScheduleState:

            if (sDay1.length < 1) {
                alert('SCHEDULE [1]: Choose at least one day of the week.');
                return;
            }

            if ($('#cboRoom1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Classroom');
                return;
            }

            if ($('#cboFaculty1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Faculty');
                return;
            }

            if (typeof $('#timepicker1').data("date") === 'undefined') {
                alert('SCHEDULE [1]: Please enter Start Time for ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            } else if ((typeof $('#timepicker2').data("date") === 'undefined')) {
                alert('SCHEDULE [1]: Please enter End Time ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            }

            if (xSched1($('#timepicker1').data("date"), $('#timepicker2').data("date"), sDay1, sDayChar1) === true) {
                refreshSubjectOffering();

                $('#scheduleModal').modal('hide');
            }
            break;
        case ScheduleState.TwoScheduleState:
            if (sDay1.length < 1) {
                alert('SCHEDULE [1]: Choose at least one day of the week.');
                return;
            }

            if ($('#cboRoom1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Classroom');
                return;
            }

            if ($('#cboFaculty1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Faculty');
                return;
            }

            if (typeof $('#timepicker1').data("date") === 'undefined') {
                alert('SCHEDULE [1]: Please enter Start Time for ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            } else if ((typeof $('#timepicker2').data("date") === 'undefined')) {
                alert('SCHEDULE [1]: Please enter End Time ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            }

            if (sDay2.length < 1) {
                alert('SCHEDULE [2]: Choose at least one day of the week.');
                return;
            }

            if ($('#cboRoom2').val() == '') {
                alert('SCHEDULE [2]: ERROR: Unable to continue scheduling... Please select Classroom');
                return;
            }

            if ($('#cboFaculty2').val() == '') {
                alert('SCHEDULE [2]: ERROR: Unable to continue scheduling... Please select Faculty');
                return;
            }

            if (typeof $('#timepicker3').data("date") === 'undefined') {
                alert('SCHEDULE [2]: Please enter Start Time for ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            } else if ((typeof $('#timepicker4').data("date") === 'undefined')) {
                alert('SCHEDULE [2]: Please enter End Time ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            }


            if (xSched1($('#timepicker1').data("date"), $('#timepicker2').data("date"), sDay1, sDayChar1) === true) {
                if (xSched2($('#timepicker3').data("date"), $('#timepicker4').data("date"), sDay2, sDayChar2) === true) {
                    refreshSubjectOffering();

                    $('#scheduleModal').modal('hide');
                }
            }
            break;
        case ScheduleState.ThreeScheduleState:
            if (sDay1.length < 1) {
                alert('SCHEDULE [1]: Choose at least one day of the week.');
                return;
            }

            if ($('#cboRoom1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Classroom');
                return;
            }

            if ($('#cboFaculty1').val() == '') {
                alert('SCHEDULE [1]: ERROR: Unable to continue scheduling... Please select Faculty');
                return;
            }

            if (typeof $('#timepicker1').data("date") === 'undefined') {
                alert('SCHEDULE [1]: Please enter Start Time for ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            } else if ((typeof $('#timepicker2').data("date") === 'undefined')) {
                alert('SCHEDULE [1]: Please enter End Time ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            }

            if (sDay2.length < 1) {
                alert('SCHEDULE [2]: Choose at least one day of the week.');
                return;
            }

            if ($('#cboRoom2').val() == '') {
                alert('SCHEDULE [2]: ERROR: Unable to continue scheduling... Please select Classroom');
                return;
            }

            if ($('#cboFaculty2').val() == '') {
                alert('SCHEDULE [2]: ERROR: Unable to continue scheduling... Please select Faculty');
                return;
            }

            if (typeof $('#timepicker3').data("date") === 'undefined') {
                alert('SCHEDULE [2]: Please enter Start Time for ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            } else if ((typeof $('#timepicker3').data("date") === 'undefined')) {
                alert('SCHEDULE [2]: Please enter End Time ' + $('#scheduleSubjectName').text() + ' Click [OK] to continue.....');
                return;
            }

            /*xSched1($('#timepicker1').data("date"), $('#timepicker2').data("date"), sDay1, sDayChar1);
            xSched2($('#timepicker3').data("date"), $('#timepicker4').data("date"), sDay2, sDayChar2);
            xSched3($('#timepicker5').data("date"), $('#timepicker6').data("date"), sDay3, sDayChar3);
            */

            if (xSched1($('#timepicker1').data("date"), $('#timepicker2').data("date"), sDay1, sDayChar1) === true) {
                if (xSched2($('#timepicker3').data("date"), $('#timepicker4').data("date"), sDay2, sDayChar2) === true) {
                    if (xSched3($('#timepicker5').data("date"), $('#timepicker6').data("date"), sDay3, sDayChar3) === true) {
                        refreshSubjectOffering();

                        $('#scheduleModal').modal('hide');
                    }
                }
            }

            refreshSubjectOffering();

            $('#scheduleModal').modal('hide');
            break;
    }

}
