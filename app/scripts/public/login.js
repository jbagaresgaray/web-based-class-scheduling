$(document).ready(function() {
    $('#server').val(sessionStorage.getItem("ipaddress"));
});


function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#username').val() == '') {
        alert('Username is required.');
        empty = true;
        return false;
    }

    if ($('#password').val() == '') {
        alert('Password is required.');
        empty = true;
        return false;
    }

    if ($('#server').val() == '') {
        alert('IP Address is required.');
        empty = true;
        return false;
    }

    sessionStorage.setItem("ipaddress", $('#server').val());
    console.log('return true');
    return true;
}

function login() {
    if (validate() == true) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/scheduling/API/user.php',
            async: false,
            type: 'POST',
            data: {
                command: 'login',
                username: $('#username').val(),
                password: $('#password').val()
            },
            success: function(response) {
                var decode = response;
                $('#loader').removeClass('hidden');

                if (decode.success == true) {
                    window.localStorage['user'] = JSON.stringify(decode.result.result);

                    $.ajax({
                        url: 'http://' + ipaddress + '/scheduling/API/user_groups.php',
                        async: false,
                        type: 'POST',
                        data: {
                            command: 'get_module_menu_items',
                            mug_id: decode.result.result[0].mug_id
                        },
                        success: function(response) {
                            var decode = response;
                            window.localStorage['menu_groups'] = JSON.stringify(decode.module_menu_groups.result);
                        }
                    });

                    window.location.href = "app/views/";
                } else {
                    $('#loader').addClass('hidden');
                    alert(decode.error);
                    return;
                }
            }
        });

    }
}
