$(document).ready(function(){

});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validateRoom(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtBldgID').val == null){
		$('#txtBldgID').next('span').text('Buildings ID is required');
		empty = false;
	}

	if($('#txtRoomName').val == null){
		$('#txtRoomName').next('span').text('Room Name is required');
		empty = false;
	}

	if($('#txtCapacity').val == null){
		$('#txtCapacity').next('span').text('Capacity is required');
		empty = false;
	}

	if($('#txtRoomNo').val == null){
		$('#txtRoomNo').next('span').text('Room No. is required');
		empty = false;
	}

	if($('#txtRoomTypeID').val == null){
		$('#txtRoomTypeID').next('span').text('Room Type is required');
		empty = false;
	}

	if($('#txtIsAirConditioned').val == null){
		$('#txtIsAirConditioned').next('span').text('Air Conditioned is required');
		empty = false;
	}

	if($('#txtIsUsable').val == null){
		$('#txtIsUsable').next('span').text('Usable is required');
		empty = false;
	}

	if($('#txtIsLANReady').val == null){
		$('#txtIsLANReady').next('span').text('Lan Ready is required');
		empty = false;
	}

	if($('#txtAllowNightClass').val == null){
		$('#txtAllowNightClass').next('span').text('Allow Night Class is required');
		empty = false;
	}

	if($('#txtShared').val == null){
		$('#txtShared').next('span').text('Shared is required');
		empty = false;
	}

	if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function validateRoomTypes(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtRoomType').val == null){
		$('#txtRoomType').next('span').text('Room Type is required');
		empty = false;
	}

	if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

}

function saveRoom(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#RoomuniqueID').val() == ''){
		if(validateRoom() == true){

			var data = new Array();

			Room = new Object();

			Room.BldgID = $('#txtBldgID').val();
			Room.RoomName = $('#txtRoomName').val();
			Room.Capacity = $('#txtCapacity').val();
			Room.RoomNo = $('#txtRoomNo').val();
			Room.RoomTypeID = $('#txtRoomTypeID').val();

			if($('#chckIsAirConditioned').prop("checked") == true){
				Room.IsAirConditioned = '1';
			}else{
				Room.IsAirConditioned = '0';
			}

			if($('#chckIsUsable').prop("checked") == true){
				Room.IsUsable = '1';
			}else{
				Room.IsUsable = '0';
			}

			if($('#chckIsLANReady').prop("checked") == true){
				Room.IsLANReady = '1';
			}else{
				Room.IsLANReady = '0';
			}

			if($('#chckAllowNightClass').prop("checked") == true){
				Room.AllowNightClass = '1';
			}else{
				Room.AllowNightClass = '0';
			}

			if($('#chckShared').prop("checked") == true){
				Room.Shared = '1';
			}else{
				Room.Shared = '0';
			}

			data.pus(Room);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertRoom',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "buildings.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateRoom() == true){

			var data = new Array();

			Room = new Object();

			Room.BldgID = $('#txtBldgID').val();
			Room.RoomName = $('#txtRoomName').val();
			Room.Capacity = $('#txtCapacity').val();
			Room.RoomNo = $('#txtRoomNo').val();
			Room.RoomTypeID = $('#txtRoomTypeID').val();

			if($('#chckIsAirConditioned').prop("checked") == true){
				Room.IsAirConditioned = '1';
			}else{
				Room.IsAirConditioned = '0';
			}

			if($('#chckIsUsable').prop("checked") == true){
				Room.IsUsable = '1';
			}else{
				Room.IsUsable = '0';
			}

			if($('#chckIsLANReady').prop("checked") == true){
				Room.IsLANReady = '1';
			}else{
				Room.IsLANReady = '0';
			}

			if($('#chckAllowNightClass').prop("checked") == true){
				Room.AllowNightClass = '1';
			}else{
				Room.AllowNightClass = '0';
			}

			if($('#chckShared').prop("checked") == true){
				Room.Shared = '1';
			}else{
				Room.Shared = '0';
			}

			data.pus(Room);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateRoom',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "buildings.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function saveRoomType(){
$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#RoomTypeuniqueID').val() == ''){
		if(validateRoomTypes() == true){

				var data = new Array();

				Room = new Object();

				RoomType.RoomType = $('#txtRoomType').val();

				data.push(RoomType);

				$>ajax({
					url: 'http://'+ipaddress+'/scheduling/API/index.php',
					async: true,
					async: true,
			        type: 'POST',
			        crossDomain: true,
			        dataType: 'json',
			        data: {
			            command: 'InsertRoomType',
			            data : data
			        },
			        success: function(response){
			        	var decode = response;

			        	 if (decode.success == true) {
	                        $('#btn-save').button('reset');
	                        window.location.href = "buildings.html";
	                    } else if (decode.success === false) {
	                        $('#btn-save').button('reset');
	                        alert(decode.msg);
	                        return;
	                    }
			        },
			        error: function(error) {
	                    $('#btn-save').button('reset');
	                    console.log("Error:");
	                    console.log(error.responseText);
	                    console.log(error.message);
	                    return;
	                }
				});
		}
	}else{
		if(validateRoomTypes() == true){

				var data = new Array();

				Room = new Object();

				RoomType.RoomType = $('#txtRoomType').val();

				data.push(RoomType);

				$>ajax({
					url: 'http://'+ipaddress+'/scheduling/API/index.php',
					async: true,
					async: true,
			        type: 'POST',
			        crossDomain: true,
			        dataType: 'json',
			        data: {
			            command: 'UpdateRoomType',
			            data : data
			        },
			        success: function(response){
			        	var decode = response;

			        	 if (decode.success == true) {
	                        $('#btn-save').button('reset');
	                        window.location.href = "buildingss.html";
	                    } else if (decode.success === false) {
	                        $('#btn-save').button('reset');
	                        alert(decode.msg);
	                        return;
	                    }
			        },
			        error: function(error) {
	                    $('#btn-save').button('reset');
	                    console.log("Error:");
	                    console.log(error.responseText);
	                    console.log(error.message);
	                    return;
	                }
				});
		}
	}
}

function fetchRoom(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$('#tbl_room tbody > tr').remove();

	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthRoom',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;


	    				var html = '<tr>\
	    							<td>' +row[i].BldgName+ '</td>\
	    							<td>' +row[i].RoomName+ '</td>\
	    							<td>' +row[i].Capacity+ '</td>\
	    							<td>' +row[i].RoomNo+ '</td>\
	    							<td>' +row[i].RoomTypeID+ '</td>\
	    							<td>' +row[i].IsAirConditioned+ '</td>\
	    							<td>' +row[i].IsUsable+ '</td>\
	    							<td>' +row[i].IsLANReady+ '</td>\
	    							<td>' +row[i].AllowNightClass+ '</td>\
	    							<td>' +row[i].Shared+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-room-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal">\
                                                <i class="fa fa-plus"></i>\
                                            </a>\
                                            <a class="edit-room-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].RoomID + '">\
                                                <i class="fa fa-Pencil"></i>\
                                            </a>\
                                            <a class="remove-room-icon btn btn-danger btn-xs" data-id="' + row[i].RoomID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_room tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}


function fetchBuilding(){
  var ipaddress = sessionStorage.getItem("ipaddress");
  $('#chckBuilding').empty();
	$.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'selectBuilding'
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;


                var html = '<option id="' + row[i].BldgID + '" value="' + row[i].BldgID + '">' + row[i].BldgName + '</option>';
                $("#chckBuilding").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

//Edit Room
$(document).on("click", ".edit-room-icon", function() {

    var RoomID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getRoom',
            RoomID: RoomID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {

            	$('#RoomTypeuniqueID').val(decode.result.RoomID);
              $('#txtBldgID').val(decode.result.BldgID);
        				$('#txtRoomName').val(decode.result.RoomName);
        				$('#txtCapacity').val(decode.result.Capacity);
        				$('#txtRoomNo').val(decode.result.RoomNo);
        				$('#txtRoomTypeID').val(decode.result.RoomTypeID);

        				if(decode.result.IsAirConditione == 1){
        					$('#chckIsAirConditioned').prop("checked", true);
        				}else{
        					$('#chckIsAirConditioned').prop("checked", false);
        				}

        				if(decode.result.IsUsable == 1){
        					$('#chckIsUsable').prop("checked", true);
        				}else{
        					$('#chckIsUsable').prop("checked", false);
        				}

        				if(decode.result.IsLANReady == 1){
        					$('#chckIsLANReady').prop("checked", true);
        				}else{
        					$('#chckIsLANReady').prop("checked", false);
        				}


        				if(decode.result.AllowNightClass == 1){
        					$('#chckAllowNightClass').prop("checked", true);
        				}else{
        					$('#chckAllowNightClass').prop("checked", false);
        				}

        				if(decode.result.Shared == 1){
        					$('#chckShared').prop("checked", true);
        				}else{
        					$('#chckShared').prop("checked", false);
        				}

                $('#myModalRoom').modal('show')
            } else if (decode.success === false) {
                $('#myModalRoom').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

//Edit RoomType
$(document).on("click", ".edit-roomtype-icon", function() {

    var RoomTypeID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getRoomType',
            RoomTypeID: RoomTypeID
        },
        success: function(response) {
            var decode = response;

            	if (decode.success == true) {
            		$('#RoomTypeuniqueID').val(decode.result.RoomTypeID);
            		$('#txtRoomType').val(decode.result.RoomType);

            		$('#myModalRoomType').modal('show')
	            } else if (decode.success === false) {
	                $('#myModalRoomType').modal('hide')
	                alert(decode.msg);
	                return;
	            }

            }
    });
}

// on delete icon click Room
$(document).on("click", ".remove-room-icon", function() {
    if (confirm('Are you sure to delete this Room?')) {

        var RoomID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'Deleterooms',
                RoomID: RoomID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "buildings.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// on delete icon click RoomType
$(document).on("click", ".remove-room-icon", function() {
    if (confirm('Are you sure to delete this Room?')) {

        var RoomID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteroomType',
                RoomID: RoomID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "buildings.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});
