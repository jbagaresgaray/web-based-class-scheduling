$(document).ready(function(){
    fetchCampus();
});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validate(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtcampusName').val == null){
    	$('#txtcampusName').next('span').text('Campus Name is required');
    	empty = false;
    }

    if($('#txtcampusCode').val == null){
    	$('#txtcampusCode').next('span').text('Campus Code is required');
    	empty = false;
    }

    if($('#txtacronym').val == null){
    	$('#txtacronym').next('span').text('Acronym is required');
    	empty = false;
    }

    if($('#txtshortName').val == null){
    	$('#txtshortName').next('span').text('Short Name is required');
    	empty = false;
    }

    if($('#txtcampusHead').val == null){
    	$('#txtcampusHead').next('span').text('Campus Head is required');
    	empty = false;
    }

    if($('#txtbarangay').val == null){
    	$('#txtbarangay').next('span').text('Barangay is required');
    	empty = false;
    }

    if($('#txtcitytown').val == null){
    	$('#txtcitytown').next('span').text('City / Town is required');
    	empty = false;
    }

    if($('#txtdistrict').val == null){
    	$('#txtdistrict').next('span').text('District is required');
    	empty = false;
    }

    if($('#txtprovince').val == null){
    	$('#txtprovince').next('span').text('Province is required');
    	empty = false;
    }

    if($('#txtzipcode').val == null){
    	$('#txtzipcode').next('span').text('Zip Code is required');
    	empty = false;
    }

    if($('#txtmailaddress').val == null){
    	$('#txtmailaddress').next('span').text('Mailing Address is required');
    	empty = false;
    }

    if($('#txtemail').val == null){
    	$('#txtemail').next('span').text('Email is required');
    	empty = false;
    }

    if($('#txttelno').val == null){
    	$('#txttelno').next('span').text('Tel. No. is required');
    	empty = false;
    }

    if($('#txtfaxno').val == null){
    	$('#txtfaxno').next('span').text('Fax No. is required');
    	empty = false;
    }

    if($('#txtwebsite').val == null){
    	$('#txtwebsite').next('span').text('Website is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function save(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#uniqueID').val() == ''){
		if(validate() == true){

			var data = new Array();

			CampusData = new Object();
			CampusData.CampusCode = $('#txtcampusCode').val();
			CampusData.CampusName = $('#txtcampusName').val();
			CampusData.Acronym = $('#txtacronym').val();
			CampusData.ShortName = $('#txtshortName').val();
			CampusData.CampusHead = $('#txtcampusHead').val();
			CampusData.Barangay = $('#txtbarangay').val();
			CampusData.TownCity = $('#txtcitytown').val();
			CampusData.District = $('#txtdistrict').val();
			CampusData.Province = $('#txtprovince').val();
			CampusData.ZipCode = $('#txtzipcode').val();
			CampusData.MailingAddress = $('#txtmailaddress').val();
			CampusData.Email = $('#txtemail').val();
			CampusData.TelNo = $('#txttelno').val();
			CampusData.FaxNo = $('#txtfaxno').val();
			CampusData.SchoolWebsite = $('#txtwebsite').val();

			if ($("#reg_userfile").get(0).files[0]) {
                CampusData.CampusLogo = $("#reg_userfile").get(0).files[0].result;
                CampusData.image_type = $("#reg_userfile").get(0).files[0].type;
            } else {
                CampusData.CampusLogo = '';
                CampusData.image_type = '';
            }

			data.push(CampusData);

			console.log(data);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertCampus',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "campus.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{

            console.log('Update...');
        if(validate() == true){

            var data = new Array();

            CampusData = new Object();
            CampusData.CampusCode = $('#txtcampusCode').val();
            CampusData.CampusName = $('#txtcampusName').val();
            CampusData.Acronym = $('#txtacronym').val();
            CampusData.ShortName = $('#txtshortName').val();
            CampusData.CampusHead = $('#txtcampusHead').val();
            CampusData.Barangay = $('#txtbarangay').val();
            CampusData.TownCity = $('#txtcitytown').val();
            CampusData.District = $('#txtdistrict').val();
            CampusData.Province = $('#txtprovince').val();
            CampusData.ZipCode = $('#txtzipcode').val();
            CampusData.MailingAddress = $('#txtmailaddress').val();
            CampusData.Email = $('#txtemail').val();
            CampusData.TelNo = $('#txttelno').val();
            CampusData.FaxNo = $('#txtfaxno').val();
            CampusData.SchoolWebsite = $('#txtwebsite').val();

            if ($("#reg_userfile").get(0).files[0]) {
                CampusData.CampusLogo = $("#reg_userfile").get(0).files[0].result;
                CampusData.image_type = $("#reg_userfile").get(0).files[0].type;
            } else {
                CampusData.CampusLogo = '';
                CampusData.image_type = '';
            }

            data.push(CampusData);

            console.log(data);

            $.ajax({
                url: 'http://'+ipaddress+'/scheduling/API/index.php',
                async: true,
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'UpdateCampus',
                    data : data
                },
                success: function(response){
                    var decode = response;

                     if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "campus.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        }
    }
}

function fetchCampus(){
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({

        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCampusInformation'
        },
        success: function(response) {
            var decode = response;
            var img = decode.result[0].CampusLogo;
            var new_img;
            if (img === 'W0JJTkFSWV0=' || img === '' || img === null) {
                new_img = '../../styles/img/photo.jpg';
            } else {
                new_img = 'data:image/png;base64,' + img;
            }

            $('#upimage').attr('src', new_img);

           $('#uniqueID').val(decode.result[0].CampusID);
           $('#txtcampusName').val(decode.result[0].CampusName);
           $('#txtcampusCode').val(decode.result[0].CampusCode);
           $('#txtacronym').val(decode.result[0].Acronym);
           $('#txtshortName').val(decode.result[0].ShortName);
           $('#txtcampusHead').val(decode.result[0].CampusHead);
           $('#txtbarangay').val(decode.result[0].Barangay);
           $('#txtcitytown').val(decode.result[0].TownCity);
           $('#txtdistrict').val(decode.result[0].District);
           $('#txtprovince').val(decode.result[0].Province);
           $('#txtzipcode').val(decode.result[0].ZipCode);
           $('#txtmailaddress').val(decode.result[0].MailingAddress);
           $('#txtemail').val(decode.result[0].Email);
           $('#txttelno').val(decode.result[0].TelNo);
           $('#txtfaxno').val(decode.result[0].FaxNo);
           $('#txtwebsite').val(decode.result[0].SchoolWebsite);

        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function clear(){
	$('#txtcampusName').val('');
	$('#txtcampusCode').val('');
    $('#txtacronym').val('');
	$('#txtshortName').val ('');
	$('#txtcampusHead').val('');
	$('#txtbarangay').val('');
	$('#txtcitytown').val('');
	$('#txtdistrict').val('');
	$('#txtprovince').val('');
	$('#txtzipcode').val('');
	$('#txtmailaddress').val('');
	$('#txtemail').val('');
	$('#txttelno').val('');
	$('#txtfaxno').val('');
    $('#txtwebsite').val('');
}
