$(document).ready(function(){
	fecthBuilding('1');
	fetchCampus();
});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validateFaculty(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtCampusID').val == null){
		$('#txtCampusID').next('span').text('Campus ID is required');
		empty = false;
	}

	if($('#txtBldgName').val == null){
		$('#txtBldgName').next('span').text('Building Name is required');
		empty = false;
	}

	if($('#txtBldgOtherName').val == null){
		$('#txtBldgOtherName').next('span').text('Other Building Name is required');
		empty = false;
	}

	if($('#txtFloorsCount').val == null){
		$('#txtFloorsCount').next('span').text('Floors Count is required');
		empty = false;
	}

	if($('#txtAcronym').val == null){
		$('#txtAcronym').next('span').text('Acronym is required');
		empty = false;
	}

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function saveBuilding(){
	$('#btn-save').button('loding');
	var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#BuildinguniqueID').val() == ''){
		if(validateFaculty() == true){

			var data = new Array();

			Building = new Object();

			Building.CampusID = $('#chckCampus').val();
			Building.BldgName= $('#txtBldgName').val();
			Building.BldgOtherName = $('#txtBldgOtherName').val();
			Building.Acronym = $('#txtAcronym').val();
			Building.FloorsCount = $('#txtFloorsCount').val();
			Building.BldgPic = '';

			if($('#chckIsLANReady').prop("checked") == true){
				Building.IsLANReady = '1';
			}else{
				Building.IsLANReady = '0';
			}

			if($('#chckElevator').prop("checked") == true){
				Building.Elevator = '1';
			}else{
				Building.Elevator = '0';
			}

			if($('#chckEscalator').prop("checked") == true){
				Building.Escalator = '1';
			}else{
				Building.Escalator = '0';
			}

			data.push(Building);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertBuilding',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "buildings.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateFaculty() == true){

			var data = new Array();

			Building = new Object();

			Building.CampusID = $('#chckCampus').val();
			Building.BldgName= $('#txtBldgName').val();
			Building.BldgOtherName = $('#txtBldgOtherName').val();
			Building.Acronym = $('#txtAcronym').val();
			Building.FloorsCount = $('#txtFloorsCount').val();
			Building.BldgPic = '';

			if($('#chckIsLANReady').prop("checked") == true){
				Building.IsLANReady = '1';
			}else{
				Building.IsLANReady = '0';
			}

			if($('#chckElevator').prop("checked") == true){
				Building.Elevator = '1';
			}else{
				Building.Elevator = '0';
			}

			if($('#chckEscalator').prop("checked") == true){
				Building.Escalator = '1';
			}else{
				Building.Escalator = '0';
			}

			data.push(Building);
			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateBuilding',
		            data : data,
		            BuildingID: $('#BuildinguniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "buildings.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function fecthBuilding(page){
	$('#tbl_building tbody > tr').remove();
	var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'GetAllBuilding',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;
	    	if(decode){
	    		if(decode.result.result.length > 0){
	    			for(var i = 0; i < decode.result.result.length; i++){
	    				var row = decode.result.result;
	    				var html = '<tr>\
	    							<td>' +row[i].BldgName+ '</td>\
	    							<td>' +row[i].BldgOtherName+ '</td>\
	    							<td>' +row[i].Acronym+ '</td>\
	    							<td>' +row[i].CampusName+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="edit-building-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].BldgID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-building-icon btn btn-danger btn-xs" data-id="' + row[i].BldgID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_building tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}


function fetchCampus(){
	var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({

        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCampusInformation'
        },
        success: function(response) {
            var decode = response;
            $('#chckCampus').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                console.log(decode.result);

                var html = '<option id="' + row[i].CampusID + '" value="' + row[i].CampusID + '">' + row[i].CampusName + '</option>';
                $("#chckCampus").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function searchBuilding(){
	console.log('suoldo');
	var ipaddress = sessionStorage.getItem("ipaddress");
	$('#tbl_building tbody > tr').remove();

	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'searchBuilding',
	        value: $('#txtValue').val()
	    },
	    success: function(response){
	    	var decode = response;
	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;
	    				var html = '<tr>\
	    							<td>' +row[i].BldgName+ '</td>\
	    							<td>' +row[i].BldgOtherName+ '</td>\
	    							<td>' +row[i].Acronym+ '</td>\
	    							<td>' +row[i].CampusName+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="edit-building-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].BldgID + '">\
                                                <i class="fa fa-pencil"></i>\
                                            </a>\
                                            <a class="remove-building-icon btn btn-danger btn-xs" data-id="' + row[i].BldgID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_building tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

//Edit Building
$(document).on("click", ".edit-building-icon", function() {

	console.log('open for edit ...');

    var BldgID = $(this).data('id');
		var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getBuilding',
            BuildingID: BldgID
        },
        success: function(response) {
            var decode = response;

            console.log(decode.result[0].BldgID);


            	$('#BuildinguniqueID').val(decode.result[0].BldgID);
                $('#chckCampus').val(decode.result[0].CampusID);
				$('#txtBldgName').val(decode.result[0].BldgName);
				$('#txtBldgOtherName').val(decode.result[0].BldgOtherName);
				$('#txtAcronym').val(decode.result[0].Acronym);
				$('#txtFloorsCount').val(decode.result[0].FloorsCount);
				// Building.BldgPic = '';

				if(decode.result[0].IsLANReady == 1){
					$('#chckIsLANReady').prop("checked", true);
				}else{
					$('#chckIsLANReady').prop("checked", false);
				}

				if(decode.result[0].Elevator == 1){
					$('#chckElevator').prop("checked", true);
				}else{
					$('#chckElevator').prop("checked", false);
				}

				if(decode.result[0].Escalator == 1){
					$('#chckEscalator').prop("checked", true);
				}else{
					($('#chckEscalator').prop("checked", false));
				}

                $('#myModalBuilding').modal('show');


        }
    });
});

// on delete icon click Building
$(document).on("click", ".remove-building-icon", function() {
    if (confirm('Are you sure to delete this Building?')) {

        var BldgID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteBuilding',
                BuildingID: BldgID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "buildings.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});
