$(document).ready(function(){

	// fetchProgram();
	fecthCampus();

});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validateProgram(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtProgCode').val() == ''){
    	$('#txtProgCode').next('span').text('Program Code is required');
    	empty = false;
    }

    if($('#txtProgName').val() == ''){
    	$('#txtProgName').next('span').text('Program Name is required');
    	empty = false;
    }

    if($('#txtShortName').val() == ''){
    	$('#txtShortName').next('span').text('Short Name is required');
    	empty = false;
    }

    if($('#txtProgYears').val() == ''){
    	$('#txtProgYears').next('span').text('Years of Program is required');
    	empty = false;
    }

    if($('#txtProgSems').val() == ''){
    	$('#txtProgSems').next('span').text('Program Semester is required');
    	empty = false;
    }

    if($('#txtMaxResidency').val() == ''){
    	$('#txtMaxResidency').next('span').text('Max Residency is required');
    	empty = false;
    }

    if($('#txtTotalAcadSubject').val() == ''){
    	$('#txtTotalAcadSubject').next('span').text('Total Academic Subject is required');
    	empty = false;
    }

    if($('#txtTotalAcadCreditUnits').val() == ''){
    	$('#txtTotalAcadCreditUnits').next('span').text('Total Academic Credit Units is required');
    	empty = false;
    }

    if($('#txtTotalNormalLoad').val() == ''){
    	$('#txtTotalNormalLoad').next('span').text('Total Normal Load is required');
    	empty = false;
    }

    if($('#txtTotalGenEdUnits').val() == ''){
    	$('#txtTotalGenEdUnits').next('span').text('Total General Education Units is required');
    	empty = false;
    }

    if($('#txtTotalMajorUnits').val() == ''){
    	$('#txtTotalMajorUnits').next('span').text('Total Major Units is required');
    	empty = false;
    }

    if($('#txtTotalElecUnit').val() == ''){
    	$('#txtTotalElecUnit').next('span').text('Total Elective Units is required');
    	empty = false;
    }

    if($('#txtTotalLectUnit').val() == ''){
    	$('#txtTotalLectUnit').next('span').text('Total Lecture Units is required');
    	empty = false;
    }

    if($('#txtTotalNonLectUnit').val() == ''){
    	$('#txtTotalNonLectUnit').next('span').text('Total Non-Lecture Units is required');
    	empty = false;
    }

    if($('#txtProgClass').val() == ''){
    	$('#txtProgClass').next('span').text('Total Program Class is required');
    	empty = false;
    }

    if($('#txtRemarks').val() == ''){
    	$('#txtRemarks').next('span').text('Remarks is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}

function validateProgramClass(){
	//Rest Help-inline
	resetHelpInLine();

	//Trim
	$('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

	// Check if required field is empty
    var empty = false;

    if($('#txtClassDesc').val() == ''){
    	$('#txtClassDesc').next('span').text('Class Description is required');
    	empty = false;
    }

    if($('#txtShortName').val() == ''){
    	$('#txtShortName').next('span').text('Short Name  is required');
    	empty = false;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

     return true;
}


//save Program Data
function saveProgram(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#PRogramuniqueID').val() == ''){
		if(validateProgram() == true){

			var data = new Array();

			programs = new Object();

			programs.ProgCode = $('#txtProgCode').val();
			programs.ProgName = $('#txtProgName').val();
			programs.ProgShortName = $('#txtShortName').val();
			programs.ProgYears = $('#txtProgYears').val();
			programs.ProgSems = $('#txtProgSems').val();
			programs.MaxResidency = $('#txtMaxResidency').val();
			programs.TotalAcadSubject = $('#txtTotalAcadSubject').val();
			programs.TotalAcadCreditUnits = $('#txtTotalAcadCreditUnits').val();
			programs.TotalNormalLoad = $('#txtTotalNormalLoad').val();
			programs.TotalGenEdUnits = $('#txtTotalGenEdUnits').val();
			programs.TotalMajorUnits = $('#txtTotalMajorUnits').val();
			programs.TotalElectiveUnits = $('#txtTotalElecUnit').val();
			programs.TotalLectureUnits = $('#txtTotalLectUnit').val();
			programs.TotalNonLectUnits = $('#txtTotalNonLectUnit').val();
			programs.ProgClass = $('#txtProgClass').val();
			programs.Remarks = $('#txtRemarks').val();

			if($('#Active').props == true){
				programs.Active = '1';
			}else{
				programs.Active = '0';
			}

			data.push(programs);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdatePrograms',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "programs.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});

		}
	}else{

		if(validateProgram() == true){

			var data = new Array();

			programs = new Object();

			programs.ProgCode = $('#txtProgCode').val();
			programs.ProgName = $('#txtProgName').val();
			programs.ProgShortName = $('#txtShortName').val();
			programs.ProgYears = $('#txtProgYears').val();
			programs.ProgSems = $('#txtProgSems').val();
			programs.MaxResidency = $('#txtMaxResidency').val();
			programs.TotalAcadSubject = $('#txtTotalAcadSubject').val();
			programs.TotalAcadCreditUnits = $('#txtTotalAcadCreditUnits').val();
			programs.TotalNormalLoad = $('#txtTotalNormalLoad').val();
			programs.TotalGenEdUnits = $('#txtTotalGenEdUnits').val();
			programs.TotalMajorUnits = $('#txtTotalMajorUnits').val();
			programs.TotalElectiveUnits = $('#txtTotalElecUnit').val();
			programs.TotalLectureUnits = $('#txtTotalLectUnit').val();
			programs.TotalNonLectUnits = $('#txtTotalNonLectUnit').val();
			programs.ProgClass = $('#txtProgClass').val();
			programs.Remarks = $('#txtRemarks').val();

			if($('#Active').props == true){
				programs.Active = '1';
			}else{
				programs.Active = '0';
			}

			data.push(programs);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdatePrograms',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "programs.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
	}
	}
}

function saveProgramClass(){
	$('#btn-save').button('loding');
  var ipaddress = sessionStorage.getItem("ipaddress");

	if($('#ProgramClassuniqueID').val() == ''){
		if(validateProgramClass() == true){

			var data = new Array();

			programClass = new Object();

			programClass.ClassDesc = $('#txtClassDesc').val();
			programClass.ShortName = $('#txtShortName').val();

			data.push(programClass);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'InsertProgramClass',
		            data : data
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "programs.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}else{
		if(validateProgramClass() == true){

			var data = new Array();

			programClass = new Object();

			programClass.ClassDesc = $('#txtClassDesc').val();
			programClass.ShortName = $('#txtShortName').val();

			data.push(programClass);

			$.ajax({
				url: 'http://'+ipaddress+'/scheduling/API/index.php',
				async: true,
				async: true,
		        type: 'POST',
		        crossDomain: true,
		        dataType: 'json',
		        data: {
		            command: 'UpdateProgramClass',
		            data : data,
		            ClassCode: $('#ProgramClassuniqueID').val()
		        },
		        success: function(response){
		        	var decode = response;

		        	 if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "programs.html";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
		        },
		        error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
			});
		}
	}
}

function fetchProgram(page){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$('#tbl_programs tbody > tr').remove();
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthPrograms',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;


	    				var html = '<tr>\
	    							<td>' +row[i].ProgCode+ '</td>\
	    							<td>' +row[i].ProgName+ '</td>\
	    							<td>' +row[i].ProgShortName+ '</td>\
	    							<td>' +row[i].ProgYears+ '</td>\
	    							<td>' +row[i].ProgSems+ '</td>\
	    							<td>' +row[i].MaxResidency+ '</td>\
	    							<td>' +row[i].TotalAcadSubject+ '</td>\
	    							<td>' +row[i].TotalAcadCreditUnits+ '</td>\
	    							<td>' +row[i].TotalNormalLoad+ '</td>\
	    							<td>' +row[i].TotalGenEdUnits+ '</td>\
	    							<td>' +row[i].TotalMajorUnits+ '</td>\
	    							<td>' +row[i].TotalElectiveUnits+ '</td>\
	    							<td>' +row[i].TotalLectureUnits+ '</td>\
	    							<td>' +row[i].TotalNonLectUnits+ '</td>\
	    							<td>' +row[i].ProgClass+ '</td>\
	    							<td>' +row[i].Remarks+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-programs-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal">\
                                                <i class="fa fa-plus"></i>\
                                            </a>\
                                            <a class="edit-programs-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].ProgID + '">\
                                                <i class="fa fa-Pencil"></i>\
                                            </a>\
                                            <a class="remove-programs-icon btn btn-danger btn-xs" data-id="' + row[i].ProgID + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_programs tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

function fetchProgramClass(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$('#tbl_programClass tbody > tr').remove();
	$.ajax({
		url: 'http://'+ipaddress+'/scheduling/API/index.php',
		async: true,
		async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        command: 'fecthProgramClass',
	        page: page
	    },
	    success: function(response){
	    	var decode = response;

	    	if(decode){
	    		if(decode.result.length > 0){
	    			for(var i = 0; i < decode.result.length; i++){
	    				var row = decode.result;


	    				var html = '<tr>\
	    							<td>' +row[i].ClassDesc+ '</td>\
	    							<td>' +row[i].ShortName+ '</td>\
	    							<td>\
	    								<div class="text-right">\
                                            <a class="Add-programClass-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal">\
                                                <i class="fa fa-plus"></i>\
                                            </a>\
                                            <a class="edit-programClass-icon btn btn-primary btn-xs" data-toggle="modal" data-target="#scheduleModal" data-id="' + row[i].ClassCode + '">\
                                                <i class="fa fa-Pencil"></i>\
                                            </a>\
                                            <a class="remove-programClass-icon btn btn-danger btn-xs" data-id="' + row[i].ClassCode + '">\
                                                <i class="fa fa-trash"></i>\
                                            </a>\
                                        </div>\
	    							</td>\
	    						</tr>';

	    				$("#tbl_programClass tbody").append(html);
	    			}
	    				$('#pagination').html(decode.pagination);
	    		}
	    	}
	    },
	    error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
	});
}

//Edit Program Data
$(document).on("click", ".edit-programs-icon", function() {

    var Prog_ID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getProgram',
            ProgID: Prog_ID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#PRogramuniqueID').val(decode.result.ProgID);
                $('#txtProgCode').val(decode.result.ProgCode);
        				$('#txtProgName').val(decode.result.ProgName);
        				$('#txtShortName').val(decode.result.ProgShortName);
        				$('#txtProgYears').val(decode.result.ProgYears);
        				$('#txtProgSems').val(decode.result.ProgSems);
        				$('#txtMaxResidency').val(decode.result.MaxResidency);
        				$('#txtTotalAcadSubject').val(decode.result.TotalAcadSubject);
        				$('#txtTotalAcadCreditUnits').val(decode.result.TotalAcadCreditUnits);
        				$('#txtTotalNormalLoad').val(decode.result.TotalNormalLoad);
        				$('#txtTotalGenEdUnits').val(decode.result.TotalGenEdUnits);
        				$('#txtTotalMajorUnits').val(decode.result.TotalMajorUnits);
        				$('#txtTotalElecUnit').val(decode.result.TotalElectiveUnits);
        				$('#txtTotalLectUnit').val(decode.result.TotalLectureUnits);
        				$('#txtTotalNonLectUnit').val(decode.result.TotalNonLectUnits);
        				$('#txtProgClass').val(decode.result.ProgClass);
        				$('#txtRemarks').val(decode.result.Remarks);

                $('#myModalProgram').modal('show')
            } else if (decode.success === false) {
                $('#myModalProgram').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

//Edit Program Data
$(document).on("click", ".edit-programClass-icon", function() {

    var Prog_ID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'getProgramClass',
            ProgID: Prog_ID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#ProgramClassuniqueID').val(decode.result.ClassCode);
                $('#txtClassDesc').val(decode.result.ClassDesc);
				$('#txtShortName').val(decode.result.ShortName);

                $('#myModalProgramClass').modal('show')
            } else if (decode.success === false) {
                $('#myModalProgramClass').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});


// on delete icon click
$(document).on("click", ".remove-programs-icon", function() {
    if (confirm('Are you sure to delete this Academic Program?')) {

        var Prog_ID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteProgram',
                ProgID: Prog_ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "programs.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// on delete icon click
$(document).on("click", ".remove-programClass-icon", function() {
    if (confirm('Are you sure to delete this Program Class?')) {

        var Prog_ID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://'+ipaddress+'/scheduling/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'DeleteProgramClass',
                ProgID: Prog_ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "programs.html";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

function fecthCampus(){
  var ipaddress = sessionStorage.getItem("ipaddress");
	$.ajax({
         url: 'http://'+ipaddress+'/scheduling/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCampusInformation'
        },
        success: function(response) {
            var decode = response;
            $('#chckCampus').empty();
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;


                var html = '<option id="' + row[i].CampusID + '" value="' + row[i].CampusID + '">' + row[i].CampusName + '</option>';
                $("#chckCampus").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}
