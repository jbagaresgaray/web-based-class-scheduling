$(document).ready(function() {

    var user = JSON.parse(window.localStorage['user'] || '{}');
    var ipaddress = sessionStorage.getItem("ipaddress");

    if (Object.keys(user).length < 1) {
        // console.log('redirect to main');
        window.location.href = "/scheduling";
    }else if (ipaddress.length < 1){
        console.log('redirect to main');
        localStorage.clear();
        window.location.href = "/scheduling";
    }

    // console.log(user);
    $('#current_user').html(user[0].Fullname);

    populate_menu_groups();

    var mmg_link = window.localStorage['mmg_link'];

    populate_menu_items(user[0].mug_id, mmg_link);
    populate_menu_items2(user[0].mug_id, mmg_link);
});


function populate_menu_groups() {
    var menu_groups = JSON.parse(window.localStorage['menu_groups'] || '{}');
    for (var i = 0; i < menu_groups.length; i++) {
        if (menu_groups[i].checked == '1') {
            var html = '<li data-link="' + menu_groups[i].mmg_link + '"><a href="../' + menu_groups[i].mmg_link + '"> ' + menu_groups[i].mmg_name + '</a></li>';
            // var html = '<li data-link="' + menu_groups[i].mmg_link + '"><a href="#"> ' + menu_groups[i].mmg_name + '</a></li>';
            $('.side-nav').append(html);
        }
    }
}


function populate_menu_items(mug_id, mmg_link) {
    $('.user-groups').empty();
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/user_groups.php',
        async: false,
        type: 'POST',
        data: {
            command: 'get_modules_user_group_id',
            mug_id: mug_id,
            mmg_link: mmg_link,
            mmi_link: ''
        },
        success: function(response) {
            var decode = response;
            window.localStorage['user_groups'] = JSON.stringify(decode.result);

            var user_groups = JSON.parse(window.localStorage['user_groups'] || '{}');

            for (var i = 0; i < user_groups.length; i++) {
                var html = '';
                if ((i == 0) || ((i % 3) == 0)) {
                    html += '<div class="row" style="margin-bottom: 20px;" id="' + i + '"></div>';

                    $('.user-groups').append(html);

                    for (var a = 0; a < user_groups.length; a++) {
                        // console.log(user_groups[a].mmi_name);
                        var html2 = '<div class="col-lg-4" style="margin-bottom: 20px;">\
                                            <i class="fa fa-chevron-right lead"></i>\
                                            <a href="' + user_groups[a].mmi_link + '.php" class="lead text-info">' + user_groups[a].mmi_name + '</a>\
                                            <br>\
                                            <em>' + user_groups[a].mmi_description + '</em>\
                                        </div>';

                        if ((i == 0) && ((i % 3) == 0)) {
                            $("#" + i + "").append(html2);
                        }
                    }
                }
            }

        }
    });
}


function populate_menu_items2(mug_id, mmg_link) {

    var user_groups = JSON.parse(window.localStorage['user_groups'] || '{}');

    $('#list-group-items').empty();

    if (Object.keys(user_groups).length < 1) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/scheduling/API/user_groups.php',
            async: false,
            type: 'POST',
            data: {
                command: 'get_modules_user_group_id',
                mug_id: mug_id,
                mmg_link: mmg_link,
                mmi_link: ''
            },
            success: function(response) {
                var decode = response;
                window.localStorage['user_groups'] = JSON.stringify(decode.result);
                var user_groups = JSON.parse(window.localStorage['user_groups'] || '{}');
                for (var i = 0; i < user_groups.length; i++) {
                    var html = '<a href="' + user_groups[i].mmi_link + '.php" class="list-group-item">' + user_groups[i].mmi_name + '</a>';
                    $('#list-group-items').append(html);
                }
            }
        });
    } else {
        for (var i = 0; i < user_groups.length; i++) {
            var html = '<a href="' + user_groups[i].mmi_link + '.php" class="list-group-item">' + user_groups[i].mmi_name + '</a>';
            $('#list-group-items').append(html);
        }
    }
}

$(document).on("click", "#menu_groups li", function() {
    // $('#menu_groups li').removeClass('active');
    // $(this).addClass("active");

    window.localStorage['mmg_link'] = $(this).data("link");
});


function logout() {
    window.localStorage.clear();
    window.location.href = "/scheduling";
}
