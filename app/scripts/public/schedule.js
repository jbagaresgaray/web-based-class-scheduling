var ScheduleState = {
    OneScheduleState: 1,
    TwoScheduleState: 2,
    ThreeScheduleState: 3
};

var FormState = {
    addFormState: 1,
    updateFormState: 2
};


var m_daysOfWeek = [
    false, false, false, false, false, false, false
];

function dayLettersToString() {
    var result = [];
    var dayLetter = [
        "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
    ];
    for (var i = 0; i < dayLetters.length; i++) {
        if (dayLetters[i] == "Mon") {
            m_daysOfWeek[0] = true;
        } else if (dayLetters[i] == "Tue") {
            m_daysOfWeek[1] = true;
        } else if (dayLetters[i] == "Wed") {
            m_daysOfWeek[2] = true;
        } else if (dayLetters[i] == "Thu") {
            m_daysOfWeek[3] = true;
        } else if (dayLetters[i] == "Fri") {
            m_daysOfWeek[4] = true;
        } else if (dayLetters[i] == "Sat") {
            m_daysOfWeek[5] = true;
        } else if (dayLetters[i] == "Sun") {
            m_daysOfWeek[6] = true;
        }
    }
    for (var a = 0; a < m_daysOfWeek.length; a++) {
        if (m_daysOfWeek[a]) {
            result.push(dayLetter[a]);
        }
    }
    return result.join('');
}

function dayLettersToChar(dayLetters) {
    var result = [];
    var dayLetter = [
        "M", "T", "W", "Th", "F", "S", "Su"
    ];
    for (var i = 0; i < dayLetters.length; i++) {
        if (dayLetters[i] == "Mon") {
            m_daysOfWeek[0] = true;
        } else if (dayLetters[i] == "Tue") {
            m_daysOfWeek[1] = true;
        } else if (dayLetters[i] == "Wed") {
            m_daysOfWeek[2] = true;
        } else if (dayLetters[i] == "Thu") {
            m_daysOfWeek[3] = true;
        } else if (dayLetters[i] == "Fri") {
            m_daysOfWeek[4] = true;
        } else if (dayLetters[i] == "Sat") {
            m_daysOfWeek[5] = true;
        } else if (dayLetters[i] == "Sun") {
            m_daysOfWeek[6] = true;
        }
    }
    for (var a = 0; a < m_daysOfWeek.length; a++) {
        if (m_daysOfWeek[a]) {
            result.push(dayLetter[a]);
        }
    }
    return result.join('');
}

function dayLettersToInteger(dayLetters) {
    var result = [];
    var dayLetter = [
        "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
    ];
    for (var i = 0; i < dayLetters.length; i++) {
        if (dayLetters[i] == "Mon") {
            m_daysOfWeek[0] = true;
        } else if (dayLetters[i] == "Tue") {
            m_daysOfWeek[1] = true;
        } else if (dayLetters[i] == "Wed") {
            m_daysOfWeek[2] = true;
        } else if (dayLetters[i] == "Thu") {
            m_daysOfWeek[3] = true;
        } else if (dayLetters[i] == "Fri") {
            m_daysOfWeek[4] = true;
        } else if (dayLetters[i] == "Sat") {
            m_daysOfWeek[5] = true;
        } else if (dayLetters[i] == "Sun") {
            m_daysOfWeek[6] = true;
        }
    }

    for (var a = 0; a < m_daysOfWeek.length; a++) {
        if (m_daysOfWeek[a]) {
            var sDay = a + 1;
            result.push(sDay);
        }
    }
    return result.join('');
}

function toDaysOfWeek(num) {
    var result = '';
    switch (num) {
        case 1:
            result = "Mon";
            break;
        case 2:
            result = "Tue";
            break;
        case 3:
            result = "Wed";
            break;
        case 4:
            result = "Thu";
            break;
        case 5:
            result = "Fri"
            break;
        case 6:
            result = "Sat";
            break;
        case 7:
            result = "Sun";
            break;
    }
    return result;
}



function toMilitaryTime(ampm, hours, minutes, secs) {
    console.log(ampm + ' ' + hours + ' ' + minutes + ' ' + secs);

    var militaryHours;
    if (ampm == "am") {
        militaryHours = hours;
        // check for special case: midnight
        if (militaryHours == "12") {
            militaryHours = "00";
        }
    } else {
        if (ampm == "pm" || am == "p.m.") {
            // get the interger value of hours, then add
            tempHours = parseInt(hours) + 2;
            // adding the numbers as strings converts to strings
            if (tempHours < 10) tempHours = "1" + tempHours;
            else tempHours = "2" + (tempHours - 10);
            // check for special case: noon
            if (tempHours == "24") {
                tempHours = "12";
            }
            militaryHours = tempHours;
        }
    }
    return militaryHours + minutes + secs;
}

function milToStandard(value) {
    if (value !== null && value !== undefined) {
        if (value.indexOf('AM') > -1 || value.indexOf('PM') > -1) {
            return value;
        } else {
            if (value.length == 8) {
                var hour = value.substring(0, 2);
                var minutes = value.substring(3, 5);
                var identifier = 'AM';

                if (hour == 12) { //If hour is 12 then should set AM PM identifier to PM
                    identifier = 'PM';
                }
                if (hour == 0) { //If hour is 0 then set to 12 for standard time 12 AM
                    hour = 12;
                }
                if (hour > 12) { //If hour is greater than 12 then convert to standard 12 hour format and set the AM PM identifier to PM
                    hour = hour - 12;
                    identifier = 'PM';
                }
                return hour + ':' + minutes + ' ' + identifier; //Return the constructed standard time
            } else { //If value is not the expected length than just return the value as is
                return value;
            }
        }
    }
};

function TimeToInt(cTimeIn) {
    switch (true) {
        case (cTimeIn > 60000 && cTimeIn < 62900): //6:00:00 AM - 6:29:00 AM
            return 1;
            break;
        case (cTimeIn > 63000 && cTimeIn < 65900): //6:30:00 AM - 6:59:00 AM
            return 2;
            break;
        case (cTimeIn > 70000 && cTimeIn < 72900): //7:00:00 AM - 6:29:00 AM
            return 3;
            break;
        case (cTimeIn > 73000 && cTimeIn < 75900): //7:30:00 AM - 6:29:00 AM
            return 4;
            break;
        case (cTimeIn > 80000 && cTimeIn < 82900): //8:00:00 AM - 6:29:00 AM
            return 5;
            break;
        case (cTimeIn > 83000 && cTimeIn < 85900): //8:30:00 AM - 6:29:00 AM
            return 6;
            break;
        case (cTimeIn > 90000 && cTimeIn < 92900): //9:00:00 AM - 6:29:00 AM
            return 7;
            break;
        case (cTimeIn > 93000 && cTimeIn < 95900): //9:30:00 AM - 6:29:00 AM
            return 8;
            break;
        case (cTimeIn > 100000 && cTimeIn < 102900): //10:00:00 AM - 6:29:00 AM
            return 9;
            break;
        case (cTimeIn > 103000 && cTimeIn < 105900): //10:30:00 AM - 6:29:00 AM
            return 10;
            break;
        case (cTimeIn > 110000 && cTimeIn < 112900): //11:00:00 AM - 6:29:00 AM
            return 11;
            break;
        case (cTimeIn > 113000 && cTimeIn < 115900): //11:30:00 AM - 6:29:00 AM
            return 12;
            break;
        case (cTimeIn > 120000 && cTimeIn < 122900): //12:00:00 PM - 12:29:00 PM
            return 13;
            break;
        case (cTimeIn > 123000 && cTimeIn < 125900): //12:30:00 PM - 12:59:00 PM
            return 14;
            break;
        case (cTimeIn > 130000 && cTimeIn < 132900): //13:00:00 PM - 13:29:00 PM
            return 15;
            break;
        case (cTimeIn > 133000 && cTimeIn < 135900): //13:30:00 PM - 13:59:00 PM
            return 16;
            break;
        case (cTimeIn > 140000 && cTimeIn < 142900): //14:30:00 PM - 6:29:00 PM
            return 17;
            break;
        case (cTimeIn > 143000 && cTimeIn < 145900): //15:00:00 PM - 6:29:00 PM
            return 18;
            break;
        case (cTimeIn > 150000 && cTimeIn < 145900): //15:30:00 PM - 6:29:00 PM
            return 19;
            break;
        case (cTimeIn > 153000 && cTimeIn < 152900): //16:00:00 PM - 6:29:00 PM
            return 20;
            break;
        case (cTimeIn > 160000 && cTimeIn < 162900): //16:30:00 PM - 6:29:00 PM
            return 21;
            break;
        case (cTimeIn > 163000 && cTimeIn < 165900): //17:00:00 PM - 6:29:00 PM
            return 22;
            break;
        case (cTimeIn > 170000 && cTimeIn < 172900): //17:30:00 PM - 6:29:00 PM
            return 23;
            break;
        case (cTimeIn > 173000 && cTimeIn < 175900): //18:00:00 PM - 6:29:00 PM
            return 24;
            break;
        case (cTimeIn > 180000 && cTimeIn < 182900): //18:30:00 PM - 6:29:00 PM
            return 25;
            break;
        case (cTimeIn > 183000 && cTimeIn < 185900): //6:00:00 PM - 6:29:00 PM
            return 26;
            break;
        case (cTimeIn > 190000 && cTimeIn < 192900): //6:00:00 PM - 6:29:00 PM
            return 27;
            break;
        case (cTimeIn > 193000 && cTimeIn < 195900): //6:00:00 PM - 6:29:00 PM
            return 28;
            break;
        case (cTimeIn > 200000 && cTimeIn < 202900): //6:00:00 PM - 6:29:00 PM
            return 29;
            break;
        case (cTimeIn > 203000 && cTimeIn < 205900): //6:00:00 PM - 6:29:00 PM
            return 30;
            break;
        case (cTimeIn > 210000 && cTimeIn < 212900): //6:00:00 PM - 6:29:00 PM
            return 31;
            break;
        case (cTimeIn > 213000 && cTimeIn < 215900): //6:00:00 PM - 6:29:00 PM
            return 32;
            break;
        case (cTimeIn > 220000 && cTimeIn < 222900): //6:00:00 PM - 6:29:00 PM
            return 33;
            break;
        case (cTimeIn > 223000 && cTimeIn < 225900): //6:00:00 PM - 6:29:00 PM
            return 34;
            break;
        case (cTimeIn > 230000 && cTimeIn < 232900): //23:00:00 PM - 23:29:00 PM
            return 35;
            break;
        case (cTimeIn > 233000 && cTimeIn < 235900): //23:30:00 PM - 23:59:00 PM
            return 36;
            break;
    }
}

function BooleanToInt(value) {
    switch (value) {
        case true:
            return 1;
        case false:
            return 0;
    }
}

function IntToBoolean(value) {
    switch (value) {
        case 1:
            return true;
        case 0:
            return false;
    }
}


function fetch_building() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboBuilding1').empty();
    $('#cboBuilding2').empty();
    $('#cboBuilding3').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_Building'
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].BldgID + '" value="' + row[i].BldgID + '">' + row[i].BldgName + '</option>';
            $("#cboBuilding1").append(html);
            $("#cboBuilding2").append(html);
            $("#cboBuilding3").append(html);
        }
    });
}

function fetch_RoomByBuilding(BuildingID) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboRoom1').empty();
    $('#cboRoom2').empty();
    $('#cboRoom3').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_BuildingRooms',
        BldgID: BuildingID
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].RoomID + '" value="' + row[i].RoomID + '">' + row[i].RoomName + '</option>';
            $("#cboRoom1").append(html);
            $("#cboRoom2").append(html);
            $("#cboRoom3").append(html);
        }
    });
}

function fetch_roomEvents() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboEvent1').empty();
    $('#cboEvent2').empty();
    $('#cboEvent3').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_RoomEvents',
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].RoomTypeID + '" value="' + row[i].RoomTypeID + '">' + row[i].RoomType + '</option>';
            $("#cboEvent1").append(html);
            $("#cboEvent2").append(html);
            $("#cboEvent3").append(html);
        }
    });
}

function fetch_faculty() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboFaculty1').empty();
    $('#cboFaculty2').empty();
    $('#cboFaculty3').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_Faculty',
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].TeacherID + '" value="' + row[i].TeacherID + '">' + row[i].TeacherName + '</option>';
            $("#cboFaculty1").append(html);
            $("#cboFaculty2").append(html);
            $("#cboFaculty3").append(html);
        }
    });
}


function fetch_college() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboAYTerm').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_ScheduleAYTerm',
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + (row[i].SchoolYear + ' - ' + row[i].SchoolTerm) + '</option>';
            $("#cboAYTerm").append(html);
        }
    });
}

function fetch_AcadProgram() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboProgram').empty();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', {
        command: 'select_ScheduleAcadPrograms',
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].ProgID + '" value="' + row[i].ProgID + '">' + row[i].ProgName + '</option>';
            $("#cboProgram").append(html);
        }
    });
}

function fetch_SectionByTermAndProgram(TermID, ProgramID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    var data = {
        command: 'FillClassSection',
        TermID: TermID,
        ProgramID: ProgramID
    };

    $('#tblSection tbody > tr').remove();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', data, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<tr class="odd">\
                            <td>' + row[i].YearLevel + '</td>\
                            <td>' + row[i].SectionTitle + '</td>\
                            <td class=" ">\
                              <div class="text-right">\
                                <a class="detail-section-icon btn btn-success btn-xs" data-id="' + row[i].SectionID + '">\
                                  <i class="fa fa-search"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
            $("#tblSection tbody").append(html);
        }
    });
}

function FillSubjectOffering(sSectionID, sTermID, sProgramID) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#tblSubjectOffering tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/scheduling.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'FillSubjectOffering',
            TermID: sTermID,
            ProgramID: sProgramID,
            SectionID: sSectionID
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;

                var html = '<tr>\
                            <td>' + row[i].SubjectCode + '</td>\
                            <td>' + row[i].SubjectTitle + '</td>\
                            <td>' + row[i].Schedule + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="add-schedule-icon btn btn-success btn-xs" data-sectionid="' + row[i].SectionID + '" data-subjectid="' + row[i].SubjectID + '" data-id="' + row[i].SubjectOfferingID + '">\
                                  <i class="fa fa-plus"></i>\
                                </a>\
                                <a class="remove-schedule-icon btn btn-danger btn-xs" data-sectionid="' + row[i].SectionID + '" data-subjectid="' + row[i].SubjectID + '"  data-id="' + row[i].SubjectOfferingID + '">\
                                  <i class="fa fa-trash"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tblSubjectOffering tbody").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}


function ShowConflicts(ConflictOccur, TermID, sValueID, Day, TimeStart, TimeEnd, ScheduleID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    if (ConflictOccur == 'Room') {
        var data = {
            command: 'GetRoomScheduleConflicts',
            TermID: TermID,
            RoomID: sValueID,
            Day: Day,
            TimeStart: parseInt(TimeStart),
            TimeEnd: parseInt(TimeEnd)
        };
    } else if (ConflictOccur == 'Faculty') {
        var data = {
            command: 'GetFacultyScheduleConflicts',
            TermID: TermID,
            FacultyID: sValueID,
            Day: Day,
            TimeStart: parseInt(TimeStart),
            TimeEnd: parseInt(TimeEnd)
        };
    } else if (ConflictOccur == 'Subject') {
        var data = {
            command: 'GetClassScheduleConflicts',
            TermID: TermID,
            SectionID: sValueID,
            Day: Day,
            TimeStart: parseInt(TimeStart),
            TimeEnd: parseInt(TimeEnd),
            ScheduleID: ScheduleID
        };
    }

    $('#tblconflict tbody > tr').remove();
    $.post('http://' + ipaddress + '/scheduling/API/scheduling.php', data, function(response) {
        var decode = response;
        console.log(decode);
        if (decode.result) {
            for (var i = 0; i < decode.result.length; i++) {
                var row = decode.result;
                console.log(row[i].SectionTitle);
                var html = '<tr>\
                            <td>' + row[i].SectionTitle + '</td>\
                            <td>' + row[i].Faculty + '</td>\
                            <td>' + (row[i].SubjectCode + ' - ' + row[i].SubjectTitle) + '</td>\
                            <td>' + (row[i].Sched1 + ' ' + row[i].Sched2 + ' ' + row[i].Sched3) + '</td>\
                        </tr>';
                $("#tblconflict tbody").append(html);
            }
        }
    });
}


function SubjectOfferingSelfConflicts(From, sTo, ScheduleID, Day, sTermID) {
    console.log("TimeStart: " + From + " TimeEnd: " + sTo + " ScheduleID: " + ScheduleID + " Day: " + Day + " TermID: " + sTermID);
    var ipaddress = sessionStorage.getItem("ipaddress");
    var i = 1;
    var InUse1 = false;
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/scheduling.php',
        async: false,
        type: 'POST',
        data: {
            command: 'ScheduleID',
            TermID: sTermID,
            ScheduleID: ScheduleID,
            Day: Day,
            From: parseInt(From),
            sTo: parseInt(sTo)
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                InUse1 = true;
            } else if (decode.success == false) {
                InUse1 = false;
            }
        }
    });
    return InUse1;
}

function FacultyInUse(From, sTo, Faculty, Day, sTermID) {
    console.log("TimeStart: " + From + " TimeEnd: " + sTo + " FacultyID1: " + Faculty + " Day: " + Day + " TermID: " + sTermID);
    var ipaddress = sessionStorage.getItem("ipaddress");
    var i = 1;
    var InUse1 = false;
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/scheduling.php',
        async: false,
        type: 'POST',
        data: {
            command: 'FacultyInUse',
            TermID: sTermID,
            Faculty: Faculty,
            Day: Day,
            From: parseInt(From),
            sTo: parseInt(sTo)
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                /*for (var a = 0; a < decode.result.result.length; a++) {
                    var row = decode.result.result;
                    for (var i = 0; i < row[a].Days.length; i++) {

                        var str = row[a].Days.substr(i, 1);
                        console.log('str: ' + str);

                        var match = new RegExp(str, "i");
                        console.log('match: ' + match);
                        if (Day.match(match)) {
                            InUse1 = true;
                            console.log('FacultyInUse');
                            break;
                        } else {
                            console.log('FacultyNotInUse');
                            InUse1 = false;
                        }
                    }
                }*/
                InUse1 = true;
            } else if (decode.success == false) {
                InUse1 = false;
            }
        }
    });
    return InUse1;
}

function RoomInUse(From, sTo, Room, Day, sTermID) {
    console.log("TimeStart: " + From + " TimeEnd: " + sTo + " Room: " + Room + " Day: " + Day + " TermID: " + sTermID);
    var ipaddress = sessionStorage.getItem("ipaddress");
    var i = 1;
    var InUse1 = false;
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/scheduling.php',
        async: false,
        type: 'POST',
        data: {
            command: 'RoomInUse',
            TermID: sTermID,
            Room: Room,
            Day: Day,
            From: parseInt(From),
            sTo: parseInt(sTo)
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                /*for (var a = 0; a < decode.result.result.length; a++) {
                    var row = decode.result.result;
                    for (var i = 0; i < row[a].Days.length; i++) {

                        var str = row[a].Days.substr(i, 1);
                        console.log('str: ' + str);

                        var match = new RegExp(str, "i");
                        console.log('match: ' + match);
                        if (Day.match(match)) {
                            InUse1 = true;
                            console.log('RoomInUse');
                            break;
                        } else {
                            console.log('RoomNotInUse');
                            InUse1 = false;
                        }
                    }
                }*/
                InUse1 = true;
            } else if (decode.success == false) {
                InUse1 = false;
            }
        }
    });
    return InUse1;
}

function SectionInUse(From, sTo, sSectionID, Day, sTermID, ScheduleID) {
    console.log("TimeStart: " + From + " TimeEnd: " + sTo + " sSectionID: " + sSectionID + " Day: " + Day + " TermID: " + sTermID);
    var ipaddress = sessionStorage.getItem("ipaddress");
    var i = 1;
    var InUse1 = false;
    $.ajax({
        url: 'http://' + ipaddress + '/scheduling/API/scheduling.php',
        async: false,
        type: 'POST',
        data: {
            command: 'SectionInUse',
            TermID: sTermID,
            SectionID: sSectionID,
            Day: Day,
            From: parseInt(From),
            sTo: parseInt(sTo),
            ScheduleID: ScheduleID
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                /*for (var a = 0; a < decode.result.result.length; a++) {
                    var row = decode.result.result;
                    for (var i = 0; i < row[a].Days.length; i++) {

                        var str = row[a].Days.substr(i, 1);
                        console.log('str: ' + str);

                        var match = new RegExp(str, "i");
                        console.log('match: ' + match);
                        if (Day.match(match)) {
                            InUse1 = true;
                            console.log('SectionInUse');
                            break;
                        } else {
                            console.log('SectionNotInUse');
                            InUse1 = false;
                        }
                    }
                }*/
                InUse1 = true;
            } else if (decode.success == false) {
                InUse1 = false;
            }
        }
    });
    return InUse1;
}
