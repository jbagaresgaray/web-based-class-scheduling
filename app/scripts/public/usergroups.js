$(document).ready(function() {
    get_module_user_groups('1');
});

$('#myModal').on('show.bs.modal', function(e) {
    var UserGroupID = $('#authenticity_token').val();
    if (UserGroupID == '') {
        get_module_menu_items('');
    } else {
        get_module_menu_items(UserGroupID);
    }

});

$('#myModal').on('hide.bs.modal', function(e) {
    console.log(';asdasdsad');
    $('#authenticity_token').val('');
    $('#panel_menu_items').empty();
    $('#usergroupname').val('');

    resetHelpInLine();
});

function refresh() {
    get_module_user_groups('1');
}

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    get_module_user_groups(page);
});

function get_module_user_groups(page) {
    console.log('get_module_user_groups');
    $('#module_menu_items tbody > tr').remove();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
        command: 'GetAllUserGroup',
        page: page
    }, function(response) {
        var decode = response;
        console.log(decode);
        for (var i = 0; i < decode.result.result.length; i++) {
            var row = decode.result.result;

            var html = '<tr>\
                            <td>' + row[i].mug_name + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="edit-group-icon btn btn-primary btn-xs" data-id="' + row[i].mug_id + '">\
                                    <i class="fa fa-pencil"></i>\
                                </a>\
                                <a class="remove-group-icon btn btn-danger btn-xs" data-id="' + row[i].mug_id + '">\
                                    <i class="fa fa-trash"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
            $("#module_menu_items tbody").append(html);
        }
        $("#pagination").append(decode.result.pagination);
    });
}

$(document).on("click", ".edit-group-icon", function() {

    var UserGroupID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
        command: 'GetUserGroupInfo',
        mug_id: UserGroupID
    }, function(response) {
        var decode = response;
        if (decode.success == true) {

            $('#authenticity_token').val(decode.result.result[0].mug_id);
            $('#usergroupname').val(decode.result.result[0].mug_name);


            $('#myModal').modal('show');
        } else if (decode.success === false) {
            alert(decode.msg);
            return;
        }
    });
});

$(document).on("click", ".remove-group-icon", function() {
    if (confirm("Are you sure to Delete this User Group?")) {

        var UserGroupID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");

        $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
            command: 'DeleteUserGroup',
            mug_id: UserGroupID
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                refresh();
            } else if (decode.success === false) {
                alert(decode.msg);
                return;
            }
        });
    }
});

function get_module_menu_items(mug_id) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#panel_menu_items').empty();
    $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
        command: 'get_module_menu_items',
        mug_id: mug_id
    }, function(response) {
        var decode = response;
        console.log(decode);
        if (decode.module_menu_groups.result.length > 0) {

            for (var i = 0; i < decode.module_menu_groups.result.length; i++) {

                var row = decode.module_menu_groups.result;
                var row_id = row[i].mmg_id;

                if (row[i].mmg_id !== '1') {
                    var html = ' <div class="panel panel-default panel_menu">\
                                    <div class="panel-heading">' + row[i].mmg_name + '</div>\
                                    <ul class="list-group" id="' + row[i].mmg_id + '"></ul>\
                                </div>';

                    $("#panel_menu_items").append(html);
                }

                if (decode.module_menu_items.result.length > 0) {
                    for (var a = 0; a < decode.module_menu_items.result.length; a++) {
                        var rows = decode.module_menu_items.result;
                        if (rows[a].mmg_id === row_id) {

                            if (rows[a].checked === '1') {
                                var html = '<li class="list-group-item">\
                                            <div class="checkbox">\
                                                <label><input type="checkbox" class="chkMenuItems" value="' + rows[a].mmi_id + '" checked="true">' + rows[a].mmi_name + '</label>\
                                            </div>\
                                        </li>';
                            } else {
                                var html = '<li class="list-group-item">\
                                            <div class="checkbox">\
                                                <label><input type="checkbox" class="chkMenuItems" data-id="' + rows[a].mar_id + '" value="' + rows[a].mmi_id + '">' + rows[a].mmi_name + '</label>\
                                            </div>\
                                        </li>';
                            }

                            $("#" + rows[a].mmg_id + "").append(html);
                        }
                    }
                }

            }
        }


    });
}




function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validate() {

    resetHelpInLine();

    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    var empty = false;

    if ($('#usergroupname').val() == '') {
        $('#usergroupname').next('span').text('User Group Name is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save_user_groups() {
    var UserGroupID = $('#authenticity_token').val();
    var module_menu_items = new Array();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('.chkMenuItems').each(function() {
        if ($(this).is(':checked')) {
            var mmi = new Object();
            mmi.mmi_id = $(this).val();
            module_menu_items.push(mmi);
        }
    });

    if (UserGroupID == '') {
        if (validate() == true) {

            if (module_menu_items.length < 1) {
                alert('Please select and checked atlist 1 (one) Module Items');
                return;
            }

            $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
                command: 'InsertUserGroup',
                mug_name: $('#usergroupname').val(),
                data: module_menu_items
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    } else {

        if (module_menu_items.length < 1) {
            alert('Please select and checked atlist 1 (one) Module Items');
            return;
        }

        $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
            command: 'UpdateUserGroup',
            mug_name: $('#usergroupname').val(),
            mug_id: UserGroupID,
            data: module_menu_items
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                $('#blockSectionModal').modal('hide');
                window.location.reload();
            } else if (decode.success == false) {
                alert(decode.msg);
                return;
            }
        });
    }
}
