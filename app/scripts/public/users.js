$(document).ready(function() {
    get_users('1');

    fetch_Employee();
    fetch_userGroup();

    $("#cboFaculty").select2({
        placeholder: "Select a Faculty"
    });
    $("#cboUserGroup").select2({
        placeholder: "Select User Group"
    });

});

$.fn.modal.Constructor.prototype.enforceFocus = function() {};

function refresh() {
    get_users('1');

    fetch_Employee();
    fetch_userGroup();
}

$('#myModal').on('hide.bs.modal', function(e) {
    $("#cboFaculty").select2('enable');
    $("#cboFaculty").select2("val", "");
    $("#cboUserGroup").select2("val", "");
});


$('#myModal').on('show.bs.modal', function(e) {
    fetch_Employee();
    fetch_userGroup();
});


function refresh() {
    get_module_user_groups('1');
}

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    get_users(page);
});

function get_users(page) {
    console.log('get_module_user_groups');
    $('#tblusers tbody > tr').remove();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.post('http://'+ipaddress+'/scheduling/API/user.php', {
        command: 'GetAllUsers',
        page: page
    }, function(response) {
        var decode = response;
        console.log(decode);
        for (var i = 0; i < decode.result.result.length; i++) {
            var row = decode.result.result;

            var html = '<tr>\
                            <td>' + row[i].UserFullname + '</td>\
                            <td>' + row[i].u_username + '</td>\
                            <td>' + row[i].UserGroup + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="edit-user-icon btn btn-primary btn-xs" data-id="' + row[i].u_id + '">\
                                    <i class="fa fa-pencil"></i>\
                                </a>\
                                <a class="remove-user-icon btn btn-danger btn-xs" data-id="' + row[i].u_id + '">\
                                    <i class="fa fa-trash"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
            $("#tblusers tbody").append(html);
        }
        $("#pagination").append(decode.result.pagination);
    });
}


function fetch_Employee() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboFaculty').empty();
    $.post('http://'+ipaddress+'/scheduling/API/user.php', {
        command: 'select_employeeUser'
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].TeacherID + '" value="' + row[i].TeacherID + '">' + row[i].EmployeeName + '</option>';
            $("#cboFaculty").append(html);
        }
    });
}


function fetch_userGroup() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $('#cboUserGroup').empty();
    $.post('http://'+ipaddress+'/scheduling/API/user_groups.php', {
        command: 'select2_usergroup'
    }, function(response) {
        var decode = response;
        for (var i = 0; i < decode.result.length; i++) {
            var row = decode.result;
            var html = '<option id="' + row[i].mug_id + '" value="' + row[i].mug_id + '">' + row[i].mug_name + '</option>';
            $("#cboUserGroup").append(html);
        }
    });
}

$(document).on("click", ".edit-user-icon", function() {

    var UserID = $(this).data('id');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.post('http://'+ipaddress+'/scheduling/API/user.php', {
        command: 'GetUserInfo',
        u_id: UserID
    }, function(response) {
        var decode = response;
        if (decode.success == true) {

            $('#myModal').modal('show');

            $("#cboFaculty").select2("disable");

            $('#authenticity_token').val(decode.result.result[0].mug_id);
            $('#username').val(decode.result.result[0].u_username);
            $('#password').val(decode.result.result[0].e_password);

            $("#cboFaculty").select2().select2("val", decode.result.result[0].e_id);
            $("#cboUserGroup").select2().select2("val", decode.result.result[0].mug_id);

        } else if (decode.success === false) {
            alert(decode.msg);
            return;
        }
    });
});

$(document).on("click", ".remove-user-icon", function() {
    if (confirm("Are you sure to Delete this User Account?")) {

        var UserID = $(this).data('id');
        var ipaddress = sessionStorage.getItem("ipaddress");

        $.post('http://'+ipaddress+'/scheduling/API/user.php', {
            command: 'DeleteUser',
            u_id: UserID
        }, function(response) {
            var decode = response;
            if (decode.success == true) {
                alert(decode.msg);
                refresh();
            } else if (decode.success === false) {
                alert(decode.msg);
                return;
            }
        });
    }
});

function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function validate() {

    resetHelpInLine();

    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    var empty = false;

    if ($('#cboFaculty').val() == '') {
        $('#cboFaculty').next('span').text('Please select Employee, this is required.');
        empty = true;
    }

    if ($('#cboUserGroup').val() == '') {
        $('#cboUserGroup').next('span').text('User Group is required.');
        empty = true;
    }

    if ($('#username').val() == '') {
        $('#username').next('span').text('User Name is required.');
        empty = true;
    }

    if ($('#password').val() == '') {
        $('#password').next('span').text('User Password is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save_user() {
    var UserID = $('#authenticity_token').val();
    var userArray = new Array();
    var ipaddress = sessionStorage.getItem("ipaddress");

    if (UserID == '') {
        if (validate() == true) {

            var user = new Object();
            user.u_username = $('#username').val();
            user.e_password = $('#password').val();
            user.mug_id = $('#cboUserGroup').val();
            user.e_id = $('#cboFaculty').val();
            userArray.push(user);

            $.post('http://'+ipaddress+'/scheduling/API/user.php', {
                command: 'InsertUser',
                data: userArray
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    } else {
        if (validate() == true) {

            var user = new Object();
            user.u_username = $('#username').val();
            user.e_password = $('#password').val();
            user.mug_id = $('#cboUserGroup').val();
            user.e_id = $('#cboFaculty').val();
            userArray.push(user);

            $.post('http://'+ipaddress+'/scheduling/API/user.php', {
                command: 'UpdateUser',
                data: userArray,
                u_id: UserID
            }, function(response) {
                var decode = response;
                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.reload();
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            });
        }
    }
}
