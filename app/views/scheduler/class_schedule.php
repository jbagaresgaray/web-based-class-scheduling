<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Class Schedule</div>
                        <div class="panel-body">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#blockSectionModal"><i class="fa fa-plus"></i> Add Class Section</button>
                            <a href="javascript:refresh();" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Refresh</a>

                            <form class="form-inline pull-right" role="form">
                                <div class="form-group">
                                    <label for="cboAYTerm">Academic Year</label>
                                    <select class="form-control valid" placeholder="Academic Year" data-rule-required="true" id="cboAYTerm" name="cboAYTerm">
                                    </select>
                                </div>
                            </form>
                        </div>

                        <!-- Table -->
                        <table class="table table-hover" id="tblSection">
                            <thead>
                                <tr>
                                    <th>Section</th>
                                    <th>Year Level</th>
                                    <th>Block</th>
                                    <th>Dissolve</th>
                                    <th>Class Limit</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <div id="pagination" cellspacing="0"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="blockSectionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add Class Section</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input id="authenticity_token" name="authenticity_token" type="hidden">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Section Name</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="txtSectionName" name="txtSectionName" placeholder="Section Name">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Section Limit</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="txtLimit" name="txtLimit" placeholder="Section Limit">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cboSectionAYTerm" class="col-sm-2 control-label">Academic Term</label>
                            <div class="col-sm-10">
                                <select class="form-control valid" data-rule-required="true" id="cboSectionAYTerm" name="cboSectionAYTerm">
                                </select>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cboYearLevel" class="col-sm-2 control-label">Year Level</label>
                            <div class="col-sm-10">
                                <select class="form-control valid" data-rule-required="true" id="cboYearLevel" name="cboYearLevel">
                                </select>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cboProgram" class="col-sm-2 control-label">Academic Program</label>
                            <div class="col-sm-10">
                                <select class="form-control valid" data-rule-required="true" id="cboProgram" name="cboProgram">
                                </select>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chkBlockSection">Block Section
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chkDissolved">Dissolved
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="save_class_Section()" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal -->
    <div class="modal fade" id="subjectofferingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Class Section:
                        <span id="classSectionName"></span>
                        <input type="hidden" id="hiddenSectionID" name="hiddenSectionID">
                        <input type="hidden" id="hiddenProgramID" name="hiddenProgramID">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Offered Subject</div>
                                <div class="panel-body">
                                    <!-- Table -->
                                    <table class="table table-hover" id="tblOfferedSubject">
                                        <thead>
                                            <tr>
                                                <th>Subj. Code</th>
                                                <th>Description</th>
                                                <th>Lect. Units</th>
                                                <th>Lab. Units</th>
                                                <th>Limit</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Search Subject</div>
                                <div class="panel-body">
                                    <form class="form-inline pull-left" role="form">
                                        <div class="form-group">
                                            <input type="text" id="searchValue" name="searchValue" class="form-control col-lg-4" placeholder="Search">
                                        </div>
                                        <a href="javascript:search_subject();" class="btn btn-primary"><i class="fa fa-search"></i> Search</a>
                                    </form>
                                    <form class="form-inline pull-right" role="form">
                                        <div class="form-group">
                                            <input type="text" id="subjectLimit" name="subjectLimit" class="form-control col-lg-4" placeholder="Subject Limit">
                                            <span class="help-inline"></span>
                                        </div>
                                    </form>
                                    <br>
                                    <!-- Table -->
                                    <table class="table table-hover" id="tblSearchSubject">
                                        <thead>
                                            <tr>
                                                <th>Subj. Code</th>
                                                <th>Description</th>
                                                <th>Lect. Units</th>
                                                <th>Lab. Units</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="javascript:save_subject_offering();" type="button" class="btn btn-primary">Save changes</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../scripts/public/main.js"></script>
</body>

</html>
