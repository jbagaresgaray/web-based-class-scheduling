<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../scripts/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="../../scripts/lib/select2/select2.css" rel="stylesheet">
    <link href="../../scripts/lib/select2/select2-bootstrap.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.html"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../scheduler"><i class="fa-fa-edit"></i> Scheduler</a>
                        </li>
                        <li class="active"><i class="fa-fa-edit"></i> Scheduler</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Basic Information</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="cboAYTerm">Academic Year</label>
                                        <select class="form-control valid" data-rule-required="true" id="cboAYTerm" name="cboAYTerm">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cboProgram">Academic Program</label>
                                        <select class="form-control valid" data-rule-required="true" id="cboProgram" name="cboProgram">
                                        </select>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">Class Section</div>
                        <div class="panel-body">
                            <table class="table table-hover" id="tblSection">
                                <thead>
                                    <tr>
                                        <th>Year Lvl</th>
                                        <th>Section</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Subject Offering for :
                            <span id="classSectionName"></span>
                            <input type="hidden" id="classSectionID">
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="tblSubjectOffering">
                                <thead>
                                    <tr>
                                        <th>Subject Code</th>
                                        <th>Description</th>
                                        <th>Schedule</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="scheduleModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="scheduleModal">Add Schedule for
                        <span id="scheduleSubjectName"></span>
                        <input type="hidden" id="hiddenSectionID" name="hiddenSectionID">
                        <input type="hidden" id="hiddenSubjectID" name="hiddenSubjectID">
                        <input type="hidden" id="hiddenSubjectOfferingID" name="hiddenSubjectOfferingID">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab1primary" data-toggle="tab">Sched 1</a>
                                        </li>
                                        <li><a href="#tab2primary" data-toggle="tab">Sched 2</a>
                                        </li>
                                        <li><a href="#tab3primary" data-toggle="tab">Sched 3</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <ul class="list-group list-group-item-sched" id="schedule1">
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Mon">Monday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Tue">Tuesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Wed">Wednesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Thu">Thursday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Fri">Friday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="chkSched1" name="chkSched1" value="Sat">Saturday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched1" value="Sun">Sunday
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <form class="form" role="form">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" id="chkOverride1">Override Conflict
                                                            </label>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Time IN</label>
                                                            <div class='input-group date' id='timepicker1'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A" />
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Time OUT</label>
                                                            <div class='input-group date' id='timepicker2'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A" />
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="cboBuilding1">Building</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboBuilding1" name="cboBuilding1">
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="cboRoom1">Room</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboRoom1" name="cboRoom1">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboEvent1">Event</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboEvent1" name="cboEvent1">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboFaculty1">Faculty</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboFaculty1" name="cboFaculty1">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab2primary">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <ul class="list-group list-group-item-sched" id="schedule2">
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Mon">Monday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Tue">Tuesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Wed">Wednesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Thu">Thursday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Fri">Friday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Sat">Saturday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched2" value="Sun">Sunday
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <form class="form" role="form">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="chkOverride2" id="chkOverride2">Override Conflict
                                                            </label>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Time IN</label>
                                                            <div class='input-group date' id='timepicker3'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Time OUT</label>
                                                            <div class='input-group date' id='timepicker4'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="cboBuilding2">Building</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboBuilding2" name="cboBuilding2">
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="cboRoom2">Room</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboRoom2" name="cboRoom2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboEvent2">Event</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboEvent2" name="cboEvent2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboFaculty2">Faculty</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboFaculty2" name="cboFaculty2">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3primary">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <ul class="list-group list-group-item-sched">
                                                        <li class="list-group-item " id="schedule3">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Mon">Monday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Tue">Tuesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Wed">Wednesday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Thu">Thursday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Fri">Friday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Sat">Saturday
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="chkSched3" value="Sun">Sunday
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <form class="form" role="form">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="chkOverride3" id="chkOverride3">Override Conflict
                                                            </label>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Time IN</label>
                                                            <div class='input-group date' id='timepicker5'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Time OUT</label>
                                                            <div class='input-group date' id='timepicker6'>
                                                                <input type='text' class="form-control" data-date-format="hh:mm:00 A"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="cboBuilding3">Building</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboBuilding3" name="cboBuilding3">
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="cboRoom3">Room</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboRoom3" name="cboRoom3">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboEvent3">Event</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboEvent3" name="cboEvent3">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-10">
                                                            <label for="cboFaculty3">Faculty</label>
                                                            <select class="form-control valid" data-rule-required="true" id="cboFaculty3" name="cboFaculty3">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            <div class="panel panel-danger">
                                <div class="panel-heading">Schedule Conflict</div>
                                <div class="panel-body">
                                    <table class="table table-hover" id="tblconflict">
                                        <thead>
                                            <tr>
                                                <th>Section</th>
                                                <th>Faculty</th>
                                                <th>Subject</th>
                                                <th>Schedule</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="saveSchedule()" class="btn btn-primary">Save Schedule</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../scripts/lib/moment.js"></script>
    <script src="../../scripts/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../../scripts/lib/select2/select2.min.js"></script>

    <script src="../../scripts/public/main.js"></script>
    <script src="../../scripts/public/schedule.js"></script>
    <script src="../../scripts/public/scheduler.js"></script>

</body>

</html>
