<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>
<body>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="../../styles/img/logo.png" style="height: 30px;">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:window.print()"><i class="fa fa-print"></i> Print</a>
                </li>
                <li><a href="schedule.php"><i class="fa fa-times"></i> Close</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h3>
                        <img src="../../styles/img/logo.png" style="height: 30px;">
                        Name of the School
                        <br>
                        <small>Address of the School</small>
                    </h3>
                </div>
                <br>
                <div class="invoice-title">
                    <h3 class="text-center">
                        Faculty Schedule
                    </h3>
                    <h4 class="pull-right">
                        <span id="date_now"></span>
                    </h4>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-6">
                        <address>
                            <strong>Faculty Name:</strong>
                            <span id="announcement-voters-heading"></span>
                        </address>
                        <address>
                            <strong>School Year and Semester:</strong>
                            <span id="announcement-voted-heading"></span>
                        </address>
                        <address>
                            <strong>Contact No:</strong>
                            <span id="announcement-voteds-heading"></span>
                        </address>
                    </div>
                </div>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div id="print_content"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-xs-offset-2">
                <div class="col-xs-3">

                    <hr>
                    <center>
                        <strong>Faculty</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
                <div class="col-xs-3">
                    <hr>
                    <center>
                        <strong>Institue Dean</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
                <div class="col-xs-3">
                    <hr>
                    <center>
                        <strong>APPROVED: President</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /#page-wrapper -->


    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/ranking.js"></script>
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
</body>

</html>
