<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
    <style type="text/css">
    .table-hover tr:hover {
        cursor: pointer;
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Faculty Center
                        <small>Module</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Dashboard</li>
                        <li><a href="../faculty"><i class="fa-fa-edit"></i> Faculty</a>
                        </li>
                        <li class="active"><i class="fa-fa-edit"></i> Class Schedule</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Academic Year and Terms</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="cboAYTerm">Academic Year</label>
                                        <select class="form-control valid" data-rule-required="true" id="cboAYTerm" name="cboAYTerm">
                                        </select>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Subject Information :
                            <span id="classSectionName"></span>
                            <input type="hidden" id="classSectionID">
                        </div>
                        <div class="panel-body">
                            <dl class="dl-horizontal">
                                <dt>SUBJECT CODE:</dt>
                                <dd>
                                    <span id="lbl_SubjectCode"></span>
                                </dd>
                                <dt>SUBJECT TITLE:</dt>
                                <dd>
                                    <span id="lbl_SubjectTitle"></span>
                                </dd>
                                <dt>LECTURE UNITS:</dt>
                                <dd>
                                    <span id="lbl_LecUnits"></span>
                                </dd>
                                <dt>LECTURE HOURS:</dt>
                                <dd>
                                    <span id="lbl_LecHrs"></span>
                                </dd>
                                <dt>LAB UNITS:</dt>
                                <dd>
                                    <span id="lbl_LabUnits"></span>
                                </dd>
                                <dt>LAB HOURS:</dt>
                                <dd>
                                    <span id="lbl_LabHrs"></span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Academic Year & Terms</div>
                        <div class="panel-body">
                            <a href="print.php" type="button" class="btn btn-success"><i class="fa fa-print"></i> Print</a>
                            <button onclick="refresh()" type="button" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</button>
                            <!-- Table -->
                        </div>
                        <table class="table table-hover" id="tblSchedule">
                            <thead>
                                <tr>
                                    <th>Subject Code</th>
                                    <th>Subject Title</th>
                                    <th>Section</th>
                                    <th>Schedule 1</th>
                                    <th>Room 1</th>
                                    <th>Schedule 2</th>
                                    <th>Room 2</th>
                                    <th>Schedule 3</th>
                                    <th>Room 3</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <div id="pagination">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../scripts/public/main.js"></script>
    <script src="../../scripts/public/faculty_schedule.js"></script>
</body>

</html>
