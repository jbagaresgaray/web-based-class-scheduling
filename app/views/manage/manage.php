<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.html">
                <img src="../../styles/img/logo.png" style="height: 30px;">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active"><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li><a href="manage.html"><i class="fa fa-edit"></i> Manage</a>
                </li>
                <li><a href="#/inventory"><i class="fa fa-table"></i> Inventory</a>
                </li>
                <li><a href="#/settings"><i class="fa fa-wrench"></i> Settings</a>
                </li>
                <li><a href="#/reports"><i class="fa fa-file"></i> Reports</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right navbar-user">
                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Manage
                        <small>Module</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Dashboard</li>
                        <li class="active"><i class="fa-fa-edit"></i> Manage</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12" style="margin-top: 40px;">
                    <div class="col-lg-4">
                        <i class="fa fa-chevron-right lead"></i>
                        <a href="category.html" class="lead text-info">Categories</a>
                        <br>
                        <em>Manage Thesis Category.</em>
                    </div>
                    <div class="col-lg-4">
                        <i class="fa fa-chevron-right lead"></i>
                        <a href="#" class="lead text-info">Academic Program</a>
                        <br>
                        <em>Manage Academic Program Thesis</em>
                    </div>
                    <div class="col-lg-4">
                        <i class="fa fa-chevron-right lead"></i>
                        <a href="#" class="lead text-info">Year Level</a>
                        <br>
                        <em>Manage Academic Year Level.</em>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="margin-top: 40px;">
                    <div class="col-lg-4">
                        <i class="fa fa-chevron-right lead"></i>
                        <a href="#category" class="lead text-info">Documents</a>
                        <br>
                        <em>Manage Thesis Documents Information.</em>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
