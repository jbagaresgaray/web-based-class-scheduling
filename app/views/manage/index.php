<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><span class="text-info">Manage</span>
                            <small>Module</small>
                        </h1>
                    </div>
                </div>

                <!-- /.row -->
                <div class="row" style="margin-top: 40px;">
                    <div class="col-lg-12 user-groups">
                    </div>
                </div>
        </div>
    </div>
    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../scripts/public/main.js"></script>
</body>

</html>
