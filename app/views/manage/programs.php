<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Academic Programs - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Academic Programs</div>
                        <div class="panel-body">
                            

                            <div class="form-inline pull-right" role="form">
                                <!-- <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Show Deleted
                                        </label>
                                    </div> -->
                                <div class="form-group">
                                    <input id="txtvalue" type="text" class="form-control" placeholder="Search">
                                </div>
                                <button onclick="javascript:searchProgram()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalProgram"><i class="fa fa-plus"></i> Add</button>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalProgramClass"><i class="fa fa-th-list"></i> Program Class</button>
                            </div>
                        </div>

                        <!-- Table -->
                        <div class="scrollable-area">
                            <table id="tbl_Program" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Campus</th>
                                        <th>Program Code</th>
                                        <th>Program Name</th>
                                        <th>Short Name</th>
                                        <th>No. of Years</th>
                                        <th>No. Semester</th>
                                        <th>Max Residency</th>
                                        <th>Academic Subject</th>
                                        <th>Credit Units</th>
                                        <th>Normal Load</th>
                                        <th>Gen. Edu. Units</th>
                                        <th>Major Units</th>
                                        <th>Elective Units</th>
                                        <th>Lecture Units</th>
                                        <th>Non-Lecture Units</th>
                                        <th>Program Class</th>
                                        <th>Remarks</th>
                                        <th>Active</th>
                                        <th style="width: 400px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div id="pagination" cellspacing="0"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

     <!-- Modal -->
    <div class="modal fade" id="myModalProgram" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Academic Program Information</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input type="hidden"id="SubjectuniqueID">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Campus:</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="chckCampus"></select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Subject Code:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtSubjCode" placeholder="Subject Code">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Subject Title:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtSubjTitle" placeholder="Subject Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtDesc" placeholder="Description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Units:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtUnits" placeholder="Units">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Credits per unit:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtCredperUnit" placeholder="Credits per unit">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lecture per hour:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtLecPerHour" placeholder="Lecture per hour">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Laboratory per hour:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtLabHrs" placeholder="Laboratory per hour">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Units per Laboratory:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtLabUnits" placeholder="Units per laboratory">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input id="chckinActive" type="checkbox">Active
                                    </label>
                                    <label>
                                         <input id="chckIsnonAcad" type="checkbox">Academic
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="javascript:saveSubject();" id="btn-save" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!--PROGRAM CLASS MODAL -->
    <div class="modal fade" id="myModalProgramClass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Program Class Information</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input type="hidden"id="ProgramClassuniqueID">
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Class Description:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtClassDesc" placeholder="Class Description">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Short Name:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtShortName" placeholder="Short Name">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="javascript:saveProgramClass();" id="btn-save" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../scripts/public/programs.js"></script>
    <script src="../../scripts/public/main.js"></script>

</body>

</html>
