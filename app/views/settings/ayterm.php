<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Academic Year & Terms - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Academic Year & Terms</div>
                        <div class="panel-body">
                            <div class="form-inline pull-right" role="form">
                                <!-- <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Show Deleted
                                        </label>
                                    </div> -->
                                <div class="form-group">
                                    <input id="txtValue" type="text" class="form-control" placeholder="Search">
                                </div>
                                <button onclick="javascript:searchTerm()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                <button onclick="javascript:clearTermFields()" class="btn btn-primary" data-toggle="modal" data-target="#myModalTerm"><i class="fa fa-plus"></i> Add</button>
                            </div>
                        </div>

                        <!-- Table -->
                        <table id="tbl_Term" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>School Year</th>
                                    <th>School Term</th>
                                    <th>Start of SY</th>
                                    <th>End of SY</th>
                                    <th style="width: 100px"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                         <div id="pagination" cellspacing="0"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModalTerm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Academic Year and Terms information</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input type="hidden" id="TermuniqueID">
                       <div class="form-group">
                            <label class="col-sm-3 control-label">School Year:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtSchoolYear" placeholder="School Year">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">School Term</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="chckSchoolTerm">
                                    <option id="1" value="1">School Year</option>
                                    <option id="2" value="2">1st Semester</option>
                                    <option id="3" value="3">2nd Semester</option>
                                    <option id="4" value="4">Summer</option>
                                    <option id="5" value="5">1st Trimester</option>
                                    <option id="6" value="6">2nd Trimester</option>
                                    <option id="7" value="7">3rd Trimester</option>
                                    <option id="8" value="8">1st Quarter</option>
                                    <option id="9" value="9">2nd Quarter</option>
                                    <option id="10" value="10">3rd Quarter</option>
                                    <option id="11" value="11">4th Quarter</option>
                                    <option id="12" value="12">6 Months [January-June]</option>
                                    <option id="13" value="13">6 Months [July-December]</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Start of SY:</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="txtStartofSY" placeholder="Start of SY">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">End of SY:</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="txtEndofSY" placeholder="End of SY">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input id="chckLocked" type="checkbox">Locked
                                    </label>
                                    <label>
                                         <input id="chckHidden" type="checkbox">Hidden
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="javascript:save();" id="btn-save" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../scripts/public/YearsandTerms.js"></script>
    <script src="../../scripts/public/main.js"></script>
</body>

</html>
