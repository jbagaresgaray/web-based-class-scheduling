<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../scripts/lib/select2/select2.css" rel="stylesheet">
    <link href="../../scripts/lib/select2/select2-bootstrap.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">User Accounts</div>
                        <div class="panel-body">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add</button>
                            <button onclick="refresh()" type="button" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</button>

                            <form class="form-inline pull-right" role="form">
                                <!-- <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Show Deleted
                                        </label>
                                    </div> -->
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            </form>
                        </div>

                        <!-- Table -->
                        <table class="table table-hover" id="tblusers">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Username</th>
                                    <th>User Group</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <div id="pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input id="authenticity_token" name="authenticity_token" type="hidden">
                        <div class="form-group">
                            <label for="cboFaculty" class="col-sm-3 control-label">Employee / Faculty</label>
                            <div class="col-sm-9">
                                <select class="form-control valid" data-rule-required="true" id="cboFaculty" name="cboFaculty">
                                </select>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="username" placeholder="Username">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" placeholder="Password">
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cboUserGroup" class="col-sm-3 control-label">User Group</label>
                            <div class="col-sm-9">
                                <select class="form-control valid" data-rule-required="true" id="cboUserGroup" name="cboUserGroup">
                                </select>
                                <span class="help-inline"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="save_user()" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../scripts/lib/select2/select2.min.js"></script>

    <script src="../../scripts/public/main.js"></script>
    <script src="../../scripts/public/users.js"></script>
</body>

</html>
