<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Campus - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../scripts/lib/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Campus Information</div>
                        <div class="panel-body">
                            <div class="col-lg-7 col-md-7">
                                <form role="form">
                                    <fieldset>
                                        <input type="hidden" id="uniqueID" name="uniqueID">
                                        <div class="form-group">
                                            <label for="campusName">Campus Name</label>
                                            <input class="form-control" placeholder="Campus Name" name="txtcampusName" id="txtcampusName" type="text" autofocus="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="campusCode">Campus Code</label>
                                            <input class="form-control" placeholder="Campus Code" name="txtcampusCode" id="txtcampusCode" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="acronym">Acronym</label>
                                            <input class="form-control" placeholder="Acronym" name="txtacronym" id="txtacronym" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="shortName">Short Name</label>
                                            <input class="form-control" placeholder="Short Name" name="txtshortName" id="txtshortName" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="campusHead">Campus Head</label>
                                            <input class="form-control" placeholder="Campus Head" name="txtcampusHead" id="txtcampusHead" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="barangay">Barangay</label>
                                            <input class="form-control" placeholder="Barangay" name="txtbarangay" id="txtbarangay" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="citytown">City / Town</label>
                                            <input class="form-control" placeholder="City / Town" name="txtcitytown" id="txtcitytown" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="district">District</label>
                                            <input class="form-control" placeholder="District" name="txtdistrict" id="txtdistrict" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="province">Province</label>
                                            <input class="form-control" placeholder="Province" name="txtprovince" id="txtprovince" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="zipcode">Zipcode</label>
                                            <input class="form-control" placeholder="Zipcode" name="txtzipcode" id="txtzipcode" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="mailaddress">Mailing Address</label>
                                            <input class="form-control" placeholder="Mailing Address" name="txtmailaddress" id="txtmailaddress" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input class="form-control" placeholder="Email" name="txtemail" id="txtemail" type="email" value="">
                                            <span class="help-inline"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="telno">Tel. No.</label>
                                            <input class="form-control" placeholder="Tel. No." name="txttelno" id="txttelno" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="faxno">Fax No.</label>
                                            <input class="form-control" placeholder="Fax No." name="txtfaxno" id="txtfaxno" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="website">Website</label>
                                            <input class="form-control" placeholder="Website" name="txtwebsite" id="txtwebsite" type="text" value="">
                                            <span class="help-inline"></span>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="box">
                                    <form id="form" enctype="multipart/form-data">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img id="upimage" class="img-responsive">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input id="reg_userfile" name="reg_userfile" type="file">
                                                </span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                           <a href="javascript:save()" id="btn-save" type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Save Information</a>
                            <a href="javascript:clear()" class="btn btn-danger"><i class="fa fa-trash"></i> Clear Information</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../scripts/lib/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
<<<<<<< HEAD:app/views/settings/campus.html
    <script type="text/javascript" src="../../scripts/public/campus.js"></script>
=======

    <script src="../../scripts/public/main.js"></script>
>>>>>>> 05bb1b7c92066ea471883c6f262f0b64ba2c4e2a:app/views/settings/campus.php
</body>

</html>
