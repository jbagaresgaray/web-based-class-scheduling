<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Building - Scheduling System</title>

    <link href="../../scripts/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../scripts/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="../../styles/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <?php include('../navbar.php');?>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group" id="list-group-items">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Buildings</div>
                        <div class="panel-body">
                            <div class="form-inline pull-right">
                                <!-- <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Show Deleted
                                        </label>
                                    </div> -->
                                <div class="form-group">
                                    <input id="txtValue" type="text" class="form-control" placeholder="Search">
                                </div>
                                <button onclick="javascript:searchBuilding()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalBuilding"><i class="fa fa-plus"></i> Add</button>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-th-list"></i> Rooms</button>
                                <button onclick="javascript:fecthBuilding('1')" type="button" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</button>
                            </div>
                        </div>

                        <!-- Table -->
                        <table id="tbl_building" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Campus Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div id="pagination" cellspacing="0"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModalBuilding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Building Information</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <input type="hidden"id="BuildinguniqueID">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Campus</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="chckCampus"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Building Name:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtBldgName" placeholder="Building Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Other Building Name:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtBldgOtherName" placeholder="Other Building Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Acronym:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtAcronym" placeholder="Acronym">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">No. of Floors:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtFloorsCount" placeholder="No. of Floors">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input id="chckIsLANReady" type="checkbox">LAN Ready
                                    </label>
                                    <label>
                                         <input id="chckElevator" type="checkbox">Elevator
                                    </label>
                                    <label>
                                        <input id="chckEscalator" type="checkbox">Escalator
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="javascript:saveSubjectMode();" id="btn-save" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="../../scripts/lib/jquery/jquery-2.0.0.min.js"></script>
    <script src="../../scripts/lib/bootstrap/js/bootstrap.min.js"></script>
     <script src="../../scripts/public/building.js"></script>

    <script src="../../scripts/public/main.js"></script>
</body>

</html>
