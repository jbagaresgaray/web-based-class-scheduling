-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 30, 2014 at 07:41 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scheduling`
--
CREATE DATABASE IF NOT EXISTS `scheduling` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `scheduling`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetClassScheduleConflicts`(
		ScheduleID INT,
		TermID  INT,
        SectionID  VARCHAR(15),
        sDay  VARCHAR(10),
        TimeStart VARCHAR(10),
        TimeEnd VARCHAR(10)
)
BEGIN
SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,fnFacultyName(CS.TeacherID) AS `Faculty`,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode
	FROM tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
		WHERE (CS.SubjectOfferingID <> ScheduleID) AND
				(CS.TermID = TermID) AND (CS.SectionID = SectionID)
			AND (
				( (CS.Days LIKE sDay) AND ((CS.SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941)))
					AND ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941))
				)
			OR
				( (CS.Days2 LIKE sDay) AND ((CS.SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941)))
					AND ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941))
				)
			OR
				( (CS.Days3 LIKE sDay) AND ((CS.SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941)))
					AND ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941))
				)
				);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetClassScheduleConflicts2`(
		TermID  INT,
        SectionID  VARCHAR(15),
        sDay  VARCHAR(10),
        TimeStart VARCHAR(10),
        TimeEnd VARCHAR(10)
)
BEGIN
SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode
	FROM tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.SectionID = SectionID) AND
			-- (CS.Days LIKE sDay) AND
			((SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941)))
			AND ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.1 TO SCHEDULE.2
	/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.SectionID = SectionID) AND
			-- (Days2 LIKE sDay) AND
			((SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941)))
			AND ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.2 TO SCHEDULE.3
		/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.SectionID = SectionID) AND
			-- (Days3 LIKE sDay) AND
			((SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941)))
			AND ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetFacultySchedule`(
        IN sTermID INTEGER,
        IN FacultyID VARCHAR(20)
    )
    DETERMINISTIC
BEGIN
SELECT SubjectOfferingID AS `SID`, SubjectCode, SubjectTitle,
	IFNULL(Sched1,'') AS Sched1, IFNULL(R1.RoomNo,'') AS `Room1`,
	IFNULL(Sched2,'') AS Sched2, IFNULL(R2.RoomNo,'') AS `Room2`,
	IFNULL(Sched3,'') AS Sched3, IFNULL(R3.RoomNo,'') AS `Room3`,
	fnFacultyName(CS.TeacherID) AS `Faculty`,
	Units, LabUnits, LecHrs, LabHrs,
    Days AS DAYS1,
    SchedTimeStart AS `Time_Start1`,
    SchedTimeEnd AS `Time_End1`,
    Days2 AS DAYS2,
    SchedTimeStart2 AS `Time_Start2`,
    SchedTimeEnd2 AS `Time_End2`,
    Days3 AS DAYS3,
    SchedTimeStart3 AS `Time_Start3`,
    SchedTimeEnd3 AS `Time_End3`,
    CS.IsSpecialClasses, CS.OverRideConflict,CS.IsDissolved,
	fnAcademicYearTerm(CS.TermID) AS AcademicYear,
	CSec.SectionTitle
FROM tblclassschedule AS CS
	LEFT JOIN tblsubject AS S ON S.SubjectID = CS.SubjectID
	LEFT JOIN tblsection AS CSec ON CS.SectionID = CSec.SectionID
	LEFT JOIN tblroom AS R1 ON CS.RoomID = R1.RoomID
	LEFT JOIN tblroom AS R2 ON CS.RoomID2 = R2.RoomID
	LEFT JOIN tblroom AS R3 on CS.RoomID3 = R3.RoomID
WHERE CS.TermID =sTermID AND CS.TeacherID = FacultyID And CS.Isdissolved = '0'
ORDER BY S.SubjectCode;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetFacultyScheduleConflicts`(
        TermID  INT,
        FacultyID  VARCHAR(15),
        sDay  VARCHAR(10),
        TimeStart VARCHAR(10),
        TimeEnd VARCHAR(10)
    )
BEGIN

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.TeacherID = FacultyID) AND
			-- (CS.Days LIKE sDay) AND
			((SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941)))
			AND ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.1 TO SCHEDULE.2
	/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.TeacherID2 = FacultyID) AND
			-- (Days2 LIKE sDay) AND
			((SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941)))
			AND ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.2 TO SCHEDULE.3
		/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.TeacherID3 = FacultyID) AND
			-- (Days3 LIKE sDay) AND
			((SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941)))
			AND ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetRoomSchedule`(
        IN sRoomID VARCHAR(45),
        IN sTermID VARCHAR(45)
    )
BEGIN
SELECT  SubjectOfferingID AS `SID`, SubjectCode, SubjectTitle,
	Sched1, R1.RoomNo AS `Room1`,
	Sched2, R2.RoomNo AS `Room2`,
	Sched3, R3.RoomNo AS `Room3`,
	Sched4, R4.RoomNo AS `Room4`,
	Sched5, R5.RoomNo AS `Room5`,
	fnFacultyName(CS.TeacherID) AS `Faculty`,
	fnEmployeeName(Sec.AdviserID) AS `ClassAdviser`,
	Units, LabUnits, LecHrs, LabHrs,
    fnClassSectionName(Sec.SectionID) AS Section,
    Days AS DAYS1,
    SchedTimeStart AS `Time_Start1`,
    SchedTimeEnd AS `Time_End1`,
    Days2 AS DAYS2,
    SchedTimeStart2 AS `Time_Start2`,
    SchedTimeEnd2 AS `Time_End2`,
    Days3 AS DAYS3,
    SchedTimeStart3 AS `Time_Start3`,
    SchedTimeEnd3 AS `Time_End3`,
    Days4 AS DAYS4,
    SchedTimeStart4 AS `Time_Start4`,
    SchedTimeEnd4 AS `Time_End4`,
    Days5 AS DAYS5,
    SchedTimeStart5 AS `Time_Start5`,
    SchedTimeEnd5 AS `Time_End5`,
    CS.IsSpecialClasses, CS.OverRideConflict,CS.IsDissolved

FROM tblclassschedule AS CS

	LEFT JOIN tblsection AS Sec ON Sec.SectionID = CS.SectionID
	LEFT JOIN tblsubject AS S ON S.SubjectID = CS.SubjectID
	LEFT JOIN tblRoom AS R1 ON R1.RoomID = CS.RoomID
	LEFT JOIN tblRoom AS R2 ON R2.RoomID = CS.RoomID2
	LEFT JOIN tblRoom AS R3 ON R3.RoomID = CS.RoomID3
	LEFT JOIN tblteacher AS E ON E.EmployeeID = CS.TeacherID
	LEFT JOIN tblteacher AS Adv ON Adv.EmployeeID = Sec.AdviserID
WHERE (CS.RoomID = sRoomID OR CS.RoomID2=sRoomID OR CS.RoomID3 =sRoomID)
AND CS.TermID=sTermID AND Sec.Isdissolved = '0' And CS.Isdissolved = '0'
ORDER BY SubjectCode;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetRoomScheduleConflicts`(
        TermID  INT,
        RoomID  VARCHAR(15),
        sDay  VARCHAR(10),
        TimeStart VARCHAR(10),
        TimeEnd VARCHAR(10)
    )
    DETERMINISTIC
BEGIN

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
            CS.Days AS Days,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.RoomID = RoomID) AND
			((SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941)))
			AND ((SchedTimeEnd > TimeStart AND SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941) OR (TimeEnd BETWEEN SchedTimeStart+1 AND SchedTimeEnd-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.1 TO SCHEDULE.2
	/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
            CS.Days2 AS Days,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.RoomID2 = RoomID) AND
			((SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941)))
			AND ((SchedTimeEnd2 > TimeStart AND SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941) OR (TimeEnd BETWEEN SchedTimeStart2+1 AND SchedTimeEnd2-9941))

		/*----------------------------------*/
	UNION -- SCHEDULE.2 TO SCHEDULE.3
		/*----------------------------------*/

	SELECT  SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits, CSec.SectionTitle,
			CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
			CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
			CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
            CS.Days3 AS Days,
			fnProgramCode(CSec.ProgramID) AS ProgramCode,
            fnFacultyName(CS.TeacherID) AS `Faculty`
	FROM    tblclassschedule AS CS LEFT JOIN
			tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
			tblsection AS CSec ON CSec.SectionID = CS.SectionID
	WHERE   (CS.TermID = TermID) AND
			(CS.RoomID3 = RoomID) AND
			((SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd)
			OR ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941)))
			AND ((SchedTimeEnd3 > TimeStart AND SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941) OR (TimeEnd BETWEEN SchedTimeStart3+1 AND SchedTimeEnd3-9941));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetSubjectOfferingSelfConflicts`(
        IN sTermID INT,
        IN ScheduleID INT,
        IN sDay VARCHAR(10),
        IN TimeStart VARCHAR(10),
        IN TimeEnd VARCHAR(10),
        IN CurrentSched INTEGER
    )
BEGIN

	IF CurrentSched = 1 THEN

		-- -------------------
		-- CHECK SCHEDULE 1
		-- -------------------
		BEGIN
		-- -------------------
			SELECT  CS.SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits,
					CS.SectionID, CSec.SectionTitle,fnFacultyName(CS.TeacherID) AS `Faculty`,
					CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
					CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
					CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
					CS.Days,CS.Days2,CS.Days3,
					fnProgramCode(CSec.ProgramID) AS ProgramCode
			FROM    tblclassschedule AS CS LEFT JOIN
					tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
					tblsection AS CSec ON CSec.SectionID = CS.SectionID
			WHERE (CS.SubjectOfferingID = ScheduleID) AND (CS.TermID = sTermID)
				AND (
					( (CS.Days LIKE sDay) AND ((CS.SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941)))
						AND ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941))
					)
				OR
					( (CS.Days2 LIKE sDay) AND ((CS.SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941)))
						AND ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941))
					)
				OR
					( (CS.Days3 LIKE sDay) AND ((CS.SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3<= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941)))
						AND ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941))
					)
					);
			END	;
		-- -----------------
		END IF;  -- schedule.1
		-- -----------------

	IF CurrentSched = 2 THEN
		-- -------------------
		-- CHECK SCHEDULE 2
		-- -------------------
		BEGIN
		-- -------------------
			SELECT  CS.SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits,
					CS.SectionID, CSec.SectionTitle,
					CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
					CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
					CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
					CS.Days,CS.Days2,CS.Days3,
					fnProgramCode(CSec.ProgramID) AS ProgramCode
			FROM    tblclassschedule AS CS LEFT JOIN
					tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
					tblsection AS CSec ON CSec.SectionID = CS.SectionID
			WHERE (CS.SubjectOfferingID = ScheduleID) AND (CS.TermID = sTermID)
				AND (
					( (CS.Days LIKE sDay) AND ((CS.SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941)))
						AND ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941))
					)
				OR
					( (CS.Days2 LIKE sDay) AND ((CS.SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941)))
						AND ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941))
					)
				OR
					( (CS.Days3 LIKE sDay) AND ((CS.SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3<= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941)))
						AND ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941))
					)
					);
			END;
		-- -----------------
		END IF; -- schedule.2
		-- -----------------

	IF CurrentSched = 3 THEN
		-- -------------------
		-- CHECK SCHEDULE 3
		-- -------------------
		BEGIN
		-- -------------------
			SELECT  CS.SubjectOfferingID, CS.TermID, CS.SubjectID, S.SubjectCode, S.SubjectTitle, S.CreditUnits,
					CS.SectionID, CSec.SectionTitle,
					CS.Sched1, fnRoomName2(CS.RoomID) AS Room_1,
					CS.Sched2, fnRoomName2(CS.RoomID2) AS Room_2,
					CS.Sched3, fnRoomName2(CS.RoomID3) AS Room_3,
					CS.Days,CS.Days2,CS.Days3,
					fnProgramCode(CSec.ProgramID) AS ProgramCode
			FROM    tblclassschedule AS CS LEFT JOIN
					tblsubject AS S ON S.SubjectID = CS.SubjectID LEFT JOIN
					tblsection AS CSec ON CSec.SectionID = CS.SectionID
			WHERE (CS.SubjectOfferingID = ScheduleID) AND (CS.TermID = sTermID)
				AND (
					( (CS.Days LIKE sDay) AND ((CS.SchedTimeStart BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941)))
						AND ((CS.SchedTimeEnd > TimeStart AND CS.SchedTimeEnd <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart+1 AND CS.SchedTimeEnd-9941))
					)
				OR
					( (CS.Days2 LIKE sDay) AND ((CS.SchedTimeStart2 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd2 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941)))
						AND ((CS.SchedTimeEnd2 > TimeStart AND CS.SchedTimeEnd2 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart2+1 AND CS.SchedTimeEnd2-9941))
					)
				OR
					( (CS.Days3 LIKE sDay) AND ((CS.SchedTimeStart3 BETWEEN TimeStart AND TimeEnd) OR (CS.SchedTimeEnd3 BETWEEN TimeStart AND TimeEnd) OR ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3<= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941)))
						AND ((CS.SchedTimeEnd3 > TimeStart AND CS.SchedTimeEnd3 <= TimeEnd) OR (TimeStart BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941) OR (TimeEnd BETWEEN CS.SchedTimeStart3+1 AND CS.SchedTimeEnd3-9941))
					)
					);
            END;
		-- -----------------
		END IF; -- schedule.3
		-- -----------------

END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fnAcademicYearTerm`(TermID INT) RETURNS varchar(100) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE AYTerm VARCHAR(100);

	SELECT CONCAT(AYT.SchoolYear , ' ' , UPper(AYT.SchoolTerm)) INTO AYTerm
	FROM tblayterm AYT
	WHERE AYT.TermID = TermID
  LIMIT 1;

	Return AYTerm;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnBuildingAcronym`(
        sBldgID INTEGER
    ) RETURNS varchar(200) CHARSET latin1
    DETERMINISTIC
BEGIN
        DECLARE BldgName VARCHAR(200);

        SELECT A.Acronym INTO BldgName
        FROM tblbuilding A
        WHERE  A.BldgID = sBldgID LIMIT 1;
        Return BldgName;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnBuildingName`(BldgID INT) RETURNS varchar(200) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE BldgName VARCHAR(200);

	SELECT BldgOtherName INTO BldgName FROM tblbuilding WHERE BldgID = BldgID LIMIT 1;
	SET BldgName  = (CASE WHEN  BldgName IS NULL THEN '' ELSE  BldgName   END);
	Return BldgName;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnCampusShortName`(CampusID INT) RETURNS varchar(60) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE CampusShortName VARCHAR(60);

	SELECT Campus.ShortName INTO CampusShortName FROM tblCampus AS Campus
	WHERE Campus.CampusID = CampusID
	Limit 1;

	Set CampusShortName = (case when CampusShortName = '' or CampusShortName Is null then 'All Campus/es' else CampusShortName End ) ;

	Return CampusShortName;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnClassLimit`(SchedID VARCHAR(45)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE Output  INT;

	SELECT `Limit` INTO Output FROM tblclassschedule WHERE SubjectOfferingID = SchedID;

	RETURN IFNULL(Output, 0);
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnClassScheduleFacultyID`(ScheduleID VARCHAR(45)) RETURNS varchar(15) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Faculty VARCHAR(15);

	SELECT CS.FacultyID INTO Faculty FROM  tblclassschedule CS WHERE CS.ScheduleID = ScheduleID LIMIT 1;

	Return Faculty;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnClassScheduleSectionID`(ScheduleID VARCHAR(45)) RETURNS varchar(45) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Section VARCHAR(45);

	SELECT CS.SectionID INTO Section  FROM tblclassschedule AS CS WHERE CS.SubjectOfferingID = ScheduleID LIMIT 1;

	Return Section;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnClassSectionName`(SectionID VARCHAR(45)) RETURNS varchar(50) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE SectionNames VARCHAR(50);

	SELECT CS.SectionTitle INTO SectionNames FROM tblsection AS CS WHERE CS.SectionID = SectionID LIMIT 1;

	Return SectionNames;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnEmployeeName`(
        sEmployeeID  VARCHAR(45)
    ) RETURNS varchar(60) CHARSET utf8
    DETERMINISTIC
BEGIN

	DECLARE Employee VARCHAR(100);

	IF sEmployeeID = 'admin' THEN
		SET Employee = 'Administrator';
	ELSE
		SELECT CONCAT(E.FirstName , ' ' , E.MiddleInitial , ' ' , E.LastName , ' ' , E.ExtName) INTO Employee FROM tblteacher AS E WHERE E.EmployeeID = sEmployeeID LIMIT 1;
	END IF;
	  Return IFNULL(Employee,'');
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnEmployeePosition`(PositionID INT) RETURNS varchar(50) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE Position VARCHAR(50);

    SELECT P.PositionDesc INTO Position FROM tblpositiontitles AS P
		WHERE P.PosnTitleID = PositionID;
	RETURN Position;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnFacultyName`(
        FacultyID VARCHAR(20)
    ) RETURNS varchar(200) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Faculty VARCHAR(100);

	SELECT fnEmployeeName(EmployeeID) INTO Faculty
   FROM  tblteacher WHERE TeacherID = FacultyID
	LIMIT 1;
   Return Faculty;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnProgramCode`(ProgID INT) RETURNS varchar(100) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE ProgramCode VARCHAR(100);

	SELECT Programs.ProgCode INTO ProgramCode FROM tblprograms AS Programs
	WHERE Programs.ProgID = ProgID
	LIMIT 1;

	Return ProgramCode;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnProgramName`(ProgID INT) RETURNS varchar(100) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE ProgramName VARCHAR(100);

	SELECT Programs.ProgName INTO ProgramName  FROM  tblprograms AS Programs WHERE Programs.ProgID = ProgID
	LIMIT 1;

	Return ifnull(ProgramName,'');
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnRoomName`(RoomID VARCHAR(45)) RETURNS varchar(45) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE RoomName VARCHAR(20);

	SELECT `RoomName` INTO RoomName FROM tblroom WHERE RoomID = RoomID LIMIT 1;
		SET RoomName = (CASE WHEN  RoomName  IS NULL THEN '' ELSE  RoomName  END);
	Return IFNULL(RoomName,'');
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnRoomName2`(sRoomID VARCHAR(45)) RETURNS varchar(60) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE RoomName VARCHAR(60);

	SELECT CONCAT(fnBuildingAcronym(R.BldgID) , '-' , R.RoomName) INTO RoomName FROM tblroom AS R
	WHERE R.RoomID = sRoomID LIMIT 1;

	Return IFNULL(RoomName,'');
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectCode`(SubjectID  VARCHAR(45)) RETURNS varchar(200) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Subjects VARCHAR(200);

	SELECT S.SubjectCode INTO Subjects  FROM tblsubject AS S WHERE S.SubjectID = SubjectID LIMIT 1;

	Return Subjects;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectCreditUnits`(SubjID VARCHAR(45)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE Units INT;

    SELECT CreditUnits INTO Units FROM tblsubject WHERE SubjectID= SubjID LIMIT 1;

    RETURN Units;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectDesc`(SubjectID VARCHAR(45)) RETURNS varchar(200) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Subjects VARCHAR(200);

	SELECT S.Description INTO Subjects FROM tblsubject AS S WHERE S.SubjectID = SubjectID LIMIT 1;

	Return Subjects;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectLabUnits`(SubjID VARCHAR(45)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE Lab INT;

    SELECT LabUnits INTO Lab FROM tblsubject WHERE SubjectID= SubjID LIMIT 1;

    RETURN Lab;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectLectUnits`(SubjID VARCHAR(45)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE LectUnits INT;

    SELECT Units INTO LectUnits FROM tblsubject WHERE SubjectID= SubjID LIMIT 1;

    RETURN LectUnits;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnSubjectTitle`(SubjectID VARCHAR(45)) RETURNS varchar(200) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE Subjects VARCHAR(200);

	SELECT S.SubjectTitle INTO Subjects FROM  tblsubject AS S WHERE S.SubjectID = SubjectID LIMIT 1;

	Return Subjects;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnYearLevel`(YrLevelID INT) RETURNS varchar(20) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE YearLevel VARCHAR(20);

	SELECT YearLevelTitle INTO YearLevel FROM tblyearlevel WHERE YearLevelID = YrLevelID
	LIMIT 1;

	Return YearLevel;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnYearLevelByLevelTerm`(sYearLevelID INT) RETURNS varchar(20) CHARSET utf8
    DETERMINISTIC
BEGIN
			DECLARE YearLevel VARCHAR(20);

			SELECT fnYearLevel(YLT.YearLevelID) INTO YearLevel FROM tblyearlevelterm AS YLT
			WHERE YLT.ID= sYearLevelID;

		  RETURN YearLevel;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `module_access_rights`
--

CREATE TABLE IF NOT EXISTS `module_access_rights` (
  `mar_id` int(11) NOT NULL AUTO_INCREMENT,
  `mug_id` int(11) DEFAULT NULL,
  `mmi_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mar_id`),
  KEY `mmi_idp_idx` (`mmi_id`),
  KEY `mug_idp_idx` (`mug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `module_access_rights`
--

INSERT INTO `module_access_rights` (`mar_id`, `mug_id`, `mmi_id`) VALUES
(15, 1, 1),
(16, 1, 2),
(17, 1, 3),
(18, 1, 4),
(19, 1, 5),
(20, 1, 6),
(21, 1, 7),
(22, 1, 8),
(23, 1, 9),
(24, 1, 10),
(25, 1, 11),
(26, 1, 12),
(27, 1, 13),
(28, 1, 14),
(29, 1, 15),
(30, 2, 1),
(31, 2, 2),
(32, 2, 3),
(33, 2, 4),
(34, 2, 5),
(35, 2, 6),
(36, 2, 7),
(37, 2, 8),
(38, 2, 10),
(39, 2, 11),
(40, 2, 12),
(41, 2, 13),
(42, 2, 14),
(43, 2, 15),
(44, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `module_menu_groups`
--

CREATE TABLE IF NOT EXISTS `module_menu_groups` (
  `mmg_id` int(11) NOT NULL AUTO_INCREMENT,
  `mmg_name` varchar(45) DEFAULT NULL,
  `mmg_order` varchar(45) DEFAULT NULL,
  `mmg_link` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mmg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `module_menu_groups`
--

INSERT INTO `module_menu_groups` (`mmg_id`, `mmg_name`, `mmg_order`, `mmg_link`) VALUES
(2, 'Manage', '2', 'manage'),
(3, 'Scheduler', '3', 'scheduler'),
(4, 'Faculty Center', '4', 'faculty'),
(5, 'Settings', '5', 'settings'),
(6, 'Reports', '6', 'reports');

-- --------------------------------------------------------

--
-- Table structure for table `module_menu_items`
--

CREATE TABLE IF NOT EXISTS `module_menu_items` (
  `mmi_id` int(11) NOT NULL AUTO_INCREMENT,
  `mmi_name` varchar(45) DEFAULT NULL,
  `mmi_link` varchar(45) DEFAULT NULL,
  `mmi_order` int(11) DEFAULT NULL,
  `mmi_description` text,
  `mmg_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mmi_id`),
  KEY `fk_module_menu_items_1_idx` (`mmg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `module_menu_items`
--

INSERT INTO `module_menu_items` (`mmi_id`, `mmi_name`, `mmi_link`, `mmi_order`, `mmi_description`, `mmg_id`) VALUES
(1, 'Academic Program', 'programs', 1, 'Manage Academic Programs.', 2),
(2, 'Subjects', 'subjects', 2, 'Manage Academic Subjects.', 2),
(3, 'Faculty ', 'faculty', 3, 'Manage Faculty Information.', 2),
(4, 'Class Section', 'section', 4, 'Manage Class Section.', 2),
(5, 'Scheduler', 'scheduler', 1, 'Manage and Generate Schedule.', 3),
(6, 'Faculty Schedule ', 'faculty_schedule', 2, 'Manage Academic Program Thesis', 3),
(7, 'Class Schedule ', 'class_schedule', 3, 'Manage Academic Year Level.', 3),
(8, 'Room Schedule ', 'room_schedule', 4, 'Manage Thesis Documents Information.', 3),
(9, 'Class Schedule', 'schedule', 1, 'Manage Faculty Class Schedule.', 4),
(10, 'Campus Information ', 'campus', 1, 'Manage Campus Information.', 5),
(11, 'Buildings and Rooms', 'buildings', 2, 'Manage Campus Buildings and Rooms', 5),
(12, 'Academic Year and Terms', 'ayterm', 3, 'Manage Academic Year and Terms.', 5),
(13, 'Year Level and Terms ', 'yearlevel', 4, 'Manage Campus Year Level and Terms.', 5),
(14, 'User Groups & Permissions', 'user_groups', 5, 'Manage User Groups and their assigned modules to access.', 5),
(15, 'User Accounts ', 'users', 6, 'Manage User Accounts.', 5);

-- --------------------------------------------------------

--
-- Table structure for table `module_user_groups`
--

CREATE TABLE IF NOT EXISTS `module_user_groups` (
  `mug_id` int(11) NOT NULL AUTO_INCREMENT,
  `mug_name` varchar(45) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`mug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `module_user_groups`
--

INSERT INTO `module_user_groups` (`mug_id`, `mug_name`, `is_deleted`) VALUES
(1, 'Administrator', 0),
(2, 'Registrar', 0),
(3, 'Teacher', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblayterm`
--

CREATE TABLE IF NOT EXISTS `tblayterm` (
  `TermID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SchoolYear` varchar(10) DEFAULT NULL,
  `Locked` tinyint(1) DEFAULT '0',
  `SchoolTerm` varchar(45) DEFAULT NULL,
  `StartofSY` datetime DEFAULT NULL,
  `EndofSY` datetime DEFAULT NULL,
  `Hidden` int(11) DEFAULT NULL,
  PRIMARY KEY (`TermID`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tblayterm`
--

INSERT INTO `tblayterm` (`TermID`, `SchoolYear`, `Locked`, `SchoolTerm`, `StartofSY`, `EndofSY`, `Hidden`) VALUES
(1, '2013-2014', 0, '1st Semester', '2013-06-10 00:00:00', '2014-03-28 00:00:00', 0),
(2, '2014-2015', 0, '1st Semester', '2014-06-10 00:00:00', '2015-03-28 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblbuilding`
--

CREATE TABLE IF NOT EXISTS `tblbuilding` (
  `BldgID` int(11) NOT NULL,
  `CampusID` int(11) DEFAULT NULL,
  `BldgName` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `BldgOtherName` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Acronym` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `FloorsCount` int(11) DEFAULT NULL,
  `BldgPic` longblob,
  `IsLANReady` tinyint(3) unsigned DEFAULT NULL,
  `Elevator` tinyint(3) unsigned DEFAULT NULL,
  `Escalator` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`BldgID`),
  KEY `Index_2` (`CampusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblbuilding`
--

INSERT INTO `tblbuilding` (`BldgID`, `CampusID`, `BldgName`, `BldgOtherName`, `Acronym`, `FloorsCount`, `BldgPic`, `IsLANReady`, `Elevator`, `Escalator`) VALUES
(1, 1, 'Main Building', 'Main Building', 'Main Building', 2, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblcampus`
--

CREATE TABLE IF NOT EXISTS `tblcampus` (
  `CampusID` int(11) NOT NULL AUTO_INCREMENT,
  `InstCode` int(11) DEFAULT NULL,
  `CampusName` varchar(100) DEFAULT NULL,
  `Acronym` varchar(200) DEFAULT NULL,
  `ShortName` varchar(50) DEFAULT NULL,
  `CampusHead` varchar(50) DEFAULT NULL,
  `Barangay` varchar(100) DEFAULT NULL,
  `TownCity` varchar(50) DEFAULT NULL,
  `District` varchar(100) DEFAULT NULL,
  `Province` varchar(100) DEFAULT NULL,
  `ZipCode` varchar(45) DEFAULT NULL,
  `MailingAddress` varchar(200) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `TelNo` varchar(45) DEFAULT NULL,
  `FaxNo` varchar(45) DEFAULT NULL,
  `SchoolWebsite` varchar(50) DEFAULT NULL,
  `CampusCode` varchar(45) DEFAULT NULL,
  `CampusLogo` longblob,
  PRIMARY KEY (`CampusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tblcampus`
--

INSERT INTO `tblcampus` (`CampusID`, `InstCode`, `CampusName`, `Acronym`, `ShortName`, `CampusHead`, `Barangay`, `TownCity`, `District`, `Province`, `ZipCode`, `MailingAddress`, `Email`, `TelNo`, `FaxNo`, `SchoolWebsite`, `CampusCode`, `CampusLogo`) VALUES
(1, 0, 'Mindanao State University at Naawan', 'MSU Naawan', 'MSU Naawan', NULL, 'Poblacion', 'Naawan', '1', 'Misamis Oriental', '9023', NULL, NULL, NULL, NULL, 'http://www.msunaawan.edu.ph', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblclassschedule`
--

CREATE TABLE IF NOT EXISTS `tblclassschedule` (
  `SubjectOfferingID` int(11) NOT NULL AUTO_INCREMENT,
  `TermID` int(20) NOT NULL,
  `SubjectID` varchar(10) DEFAULT NULL,
  `SectionID` int(11) DEFAULT NULL,
  `Limit` int(11) DEFAULT NULL,
  `IsSpecialClasses` tinyint(1) DEFAULT NULL,
  `SchedTimeStart` int(11) DEFAULT NULL,
  `SchedTimeEnd` int(11) DEFAULT NULL,
  `TeacherID` varchar(10) DEFAULT NULL,
  `RoomID` varchar(50) DEFAULT NULL,
  `Days` varchar(20) DEFAULT NULL,
  `EventID1` int(10) unsigned DEFAULT NULL,
  `Sched1` varchar(45) DEFAULT NULL,
  `SchedTimeStart2` int(11) DEFAULT NULL,
  `SchedTimeEnd2` int(11) DEFAULT NULL,
  `TeacherID2` varchar(45) DEFAULT NULL,
  `RoomID2` varchar(45) DEFAULT NULL,
  `Days2` varchar(20) DEFAULT NULL,
  `EventID2` int(10) unsigned DEFAULT NULL,
  `Sched2` varchar(45) DEFAULT NULL,
  `SchedTimeStart3` int(11) DEFAULT NULL,
  `SchedTimeEnd3` int(11) DEFAULT NULL,
  `TeacherID3` varchar(45) DEFAULT NULL,
  `RoomID3` varchar(45) DEFAULT NULL,
  `Days3` varchar(20) DEFAULT NULL,
  `EventID3` int(10) unsigned DEFAULT NULL,
  `Sched3` varchar(45) DEFAULT NULL,
  `OverRideConflict` tinyint(4) DEFAULT NULL,
  `IsDissolved` tinyint(4) DEFAULT NULL,
  `PostedBy` varchar(45) DEFAULT NULL,
  `DatePosted` varchar(45) DEFAULT NULL,
  `RoomPostedBy` varchar(45) DEFAULT NULL,
  `RoomDatePosted` varchar(20) DEFAULT NULL,
  `FacultyDatePosted` varchar(45) DEFAULT NULL,
  `FacultyPostedBy` varchar(45) DEFAULT NULL,
  `CreatedBy` varchar(70) DEFAULT NULL,
  `CreationDate` varchar(45) DEFAULT NULL,
  `ModifiedDate` varchar(45) DEFAULT NULL,
  `ModifiedBy` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`SubjectOfferingID`),
  KEY `SubjectID` (`SubjectID`),
  KEY `SectionID` (`SectionID`) USING BTREE,
  KEY `TermID` (`TermID`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblclassschedule`
--

INSERT INTO `tblclassschedule` (`SubjectOfferingID`, `TermID`, `SubjectID`, `SectionID`, `Limit`, `IsSpecialClasses`, `SchedTimeStart`, `SchedTimeEnd`, `TeacherID`, `RoomID`, `Days`, `EventID1`, `Sched1`, `SchedTimeStart2`, `SchedTimeEnd2`, `TeacherID2`, `RoomID2`, `Days2`, `EventID2`, `Sched2`, `SchedTimeStart3`, `SchedTimeEnd3`, `TeacherID3`, `RoomID3`, `Days3`, `EventID3`, `Sched3`, `OverRideConflict`, `IsDissolved`, `PostedBy`, `DatePosted`, `RoomPostedBy`, `RoomDatePosted`, `FacultyDatePosted`, `FacultyPostedBy`, `CreatedBy`, `CreationDate`, `ModifiedDate`, `ModifiedBy`) VALUES
(1, 1, '321', 1, 20, 0, 80000, 120000, '1', '872', '135', 1, 'MWF 08:00 AM-12:00 PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:24', NULL, NULL),
(2, 1, '389', 1, 20, 0, 60000, 80000, '1', '872', '13', 1, 'MW 06:00:00 AM-08:00:00 AM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:24', NULL, NULL),
(3, 1, '100', 1, 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:24', NULL, NULL),
(4, 1, '288', 1, 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:24', NULL, NULL),
(5, 1, '401', 1, 60, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:56', NULL, NULL),
(6, 1, '557', 1, 60, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2014-09-24 00:55:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblpositiontitles`
--

CREATE TABLE IF NOT EXISTS `tblpositiontitles` (
  `PosnTitleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PositionCode` varchar(45) NOT NULL,
  `PositionDesc` varchar(100) NOT NULL,
  `ShortName` varchar(45) NOT NULL,
  PRIMARY KEY (`PosnTitleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `tblpositiontitles`
--

INSERT INTO `tblpositiontitles` (`PosnTitleID`, `PositionCode`, `PositionDesc`, `ShortName`) VALUES
(1, 'PRESIDENT', 'President', ''),
(2, 'CHANCLR', 'Chancellor', ''),
(3, 'EXECDIR', 'Executive Director', ''),
(4, 'DEAN', 'Dean', ''),
(5, 'RECTOR - PRES.', 'Rector - President', ''),
(6, 'HEAD', 'Head', ''),
(7, 'ADMNTR', 'Administrator', ''),
(8, 'PRINCPL', 'Principal', ''),
(9, 'MNGDIR', 'Managing Director', ''),
(10, 'DIRECTOR', 'Director', ''),
(11, 'CHAIR', 'Chair', ''),
(12, 'SADMOFR', 'Supv. Adm. Officer', ''),
(13, 'VP', 'Vice-President', ''),
(14, 'ASSTPRNL', 'Assistant Principal', ''),
(15, 'PROFI', 'Professor I', ''),
(16, 'PROFII', 'Professor II', ''),
(17, 'PROFIII', 'Professor III', ''),
(18, 'PROFIV', 'Professor IV', ''),
(19, 'PROFV', 'Professor V', ''),
(20, 'PROFVI', 'Professor VI', ''),
(21, 'ASSOCPROFI', 'Associate Professor I', ''),
(22, 'ASSOCPROFII', 'Associate Professor II', ''),
(23, 'ASSOCPROFIII', 'Associate Professor III', ''),
(24, 'ASSOCPROFIV', 'Associate Professor IV', ''),
(25, 'ASSOCPROFV', 'Associate Professor V', ''),
(26, 'ASSTPROFI', 'Assistant Professor I', ''),
(27, 'ASSTPROFII', 'Assistant Professor II', ''),
(28, 'ASSTPROFIII', 'Assistant Professor III', ''),
(29, 'ASSTPROFIV', 'Assistant Professor IV', ''),
(30, 'ASSTPROFV', 'Assistant Professor V', ''),
(31, 'INSTI', 'Instructor I', ''),
(32, 'INSTII', 'Instructor II', ''),
(33, 'INSTIII', 'Instructor III', ''),
(34, 'CLERKI', 'Clerk I', ''),
(35, 'CLERKII', 'Clerk II', ''),
(36, 'REGI', 'Registrar I', ''),
(37, 'REGII', 'Registrar II', ''),
(38, 'REGIII', 'Registrar III', ''),
(39, 'CSHRI', 'Cashier I', ''),
(40, 'CSHRII', 'Cashier II', ''),
(41, 'LIBI', 'Librarian I', ''),
(42, 'LIBII', 'Librarian II', ''),
(43, 'LIBIII', 'Librarian III', ''),
(44, 'COORDNTR', 'Coordinator', ''),
(45, 'NURSEI', 'Nurse I', ''),
(46, 'PHNI', 'PHN I', ''),
(47, 'ACCTI', 'Accountant I', ''),
(48, 'ACCTII', 'Accountant II', ''),
(49, 'ACCTIII', 'Accountant III', ''),
(50, 'ACCTIV', 'Accountant IV', ''),
(51, 'ADMAIDEI', 'Adm. Aide I', ''),
(52, 'ADMAIDEII', 'Adm. Aide II', ''),
(53, 'ADMAIDEIII', 'Adm. Aide III', ''),
(54, 'ADMAIDEIV', 'Adm. Aide IV', ''),
(55, 'ADMAIDEV', 'Adm. Aide V', ''),
(56, 'ADMAIDEVI', 'Adm. Aide VI', ''),
(57, 'ADMASSTI', 'Adm. Asst I', ''),
(58, 'ADMASSTII', 'Adm. Asst II', ''),
(59, 'ADMASSTIII', 'Adm. Asst III', ''),
(60, 'ADMASSTIV', 'Adm. Asst IV', ''),
(61, 'ADMASSV', 'Adm. Asst V', ''),
(62, 'ADMOFCRI', 'Adm. Officer I', ''),
(63, 'ADMOFCRII', 'Adm. Officer II', ''),
(64, 'ADMOFCRIII', 'Adm. Officer III', ''),
(65, 'ADMOFCRIV', 'Adm. Officer IV', ''),
(66, 'ADMOFCRV', 'Adm. Officer V', ''),
(67, 'BOARDSECI', 'Board Secretary I', ''),
(68, 'BOARDSECII', 'Board Secretary II', ''),
(69, 'BOARDSECIII', 'Board Secretary III', ''),
(70, 'BOARDSECIV', 'Board Secretary IV', ''),
(71, 'BOARDSECV', 'Board Secretary V', ''),
(72, 'CHFADMOFCR', 'Chief Adm. Officer', ''),
(73, 'COLLIBI', 'College Librarian I', ''),
(74, 'COLLIBII', 'College Librarian II', ''),
(75, 'COLLIBIII', 'College Librarian III', ''),
(76, 'COLLIBIV', 'College Librarian IV', ''),
(77, 'COLLIBV', 'College Librarian V', ''),
(78, 'DENTISTI', 'Dentist I', ''),
(79, 'DENTISTII', 'Dentist II', ''),
(80, 'DENTISTIII', 'Dentist III', ''),
(81, 'MOTRPLSUPI', 'Motor Pool Supv I', ''),
(82, 'MOTRPLSUPII', 'Motor Pool Supv II', ''),
(83, 'MOTRPLSUPIII', 'Motor Pool Supv III', ''),
(84, 'GUARDI', 'Security Guard I', ''),
(85, 'GUARDII', 'Security Guard II', ''),
(86, 'GUARDIII', 'Security Guard III', ''),
(87, 'STUDRECEVALI', 'Student Record Eval. I', ''),
(88, 'STUDRECEVALII', 'Student Record Eval. II', ''),
(89, 'STUDRECEVALIII', 'Student Record Eval. III', ''),
(90, 'SVRCONTR', 'Service Contractor', ''),
(91, 'STUDASST', 'Student Assistance', ''),
(92, 'SUPPLYOFCRI', 'Supply Officer I', ''),
(93, 'SUPPLYOFCRII', 'Supply Officer II', ''),
(94, 'SUPPLYOFCRIII', 'Supply Officer III', ''),
(95, 'UTILWORKI', 'Utility Worker I', ''),
(96, 'UTILWORKII', 'Utility Worker II', ''),
(97, 'UTILWORKIII', 'Utility Worker III', ''),
(98, 'STOREKEEPI', 'Store Keeper I', ''),
(99, 'STOREKEEPII', 'Store Keeper II', ''),
(100, 'STOREKEEPIII', 'Store Keeper III', ''),
(101, 'STOREKEEPIV', 'Store Keeper IV', ''),
(102, 'CSHCLRKI', 'Cash Clerk I', ''),
(103, 'COMPROGMRI', 'Computer Programmer I', ''),
(104, 'PUBNURSEI', 'Public Health Nurse I', ''),
(105, 'ITDIRECTOR', 'IT Director', ''),
(106, 'ASSTITDIREC', 'Asst. IT Director', ''),
(107, 'DBADMIN', 'Database Administrator', ''),
(108, 'REGIV', 'Registrar IV', ''),
(109, 'VISII', 'Vocational Instruction Supervisor ', ''),
(111, 'PTINSTR', 'Part-Time Instructor', ''),
(112, 'AcctClerk', 'Accounting Clerk', 'Accounting Clerk'),
(113, 'Tech.Sup', 'Technical Support', 'Tech. Support'),
(114, 'CASUAL CLERK', 'CASUAL CLERK', 'CASUAL CLERK'),
(115, 'Reg.Casual Clerk', 'Reg.Casual Clerk', 'Reg.Casual Clerk'),
(116, 'INSTRUCTOR-JO', 'INSTRUCTOR-JO', 'INSTRUCTOR-JO');

-- --------------------------------------------------------

--
-- Table structure for table `tblprograms`
--

CREATE TABLE IF NOT EXISTS `tblprograms` (
  `ProgID` varchar(20) NOT NULL,
  `CampusID` int(11) DEFAULT NULL,
  `ProgCode` varchar(45) DEFAULT NULL,
  `ProgName` varchar(200) DEFAULT NULL,
  `ProgShortName` varchar(100) DEFAULT NULL,
  `ProgYears` float DEFAULT NULL,
  `ProgSems` float DEFAULT NULL,
  `MaxResidency` float DEFAULT NULL,
  `TotalAcadSubject` float DEFAULT NULL,
  `TotalAcadCreditUnits` float DEFAULT NULL,
  `TotalNormalLoad` float DEFAULT '0',
  `TotalGenEdUnits` float DEFAULT NULL,
  `TotalMajorUnits` float DEFAULT NULL,
  `TotalElectiveUnits` float DEFAULT NULL,
  `TotalLectureUnits` float DEFAULT NULL,
  `TotalNonLectUnits` float DEFAULT NULL,
  `ProgClass` varchar(45) DEFAULT NULL,
  `Remarks` text,
  `Active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ProgID`),
  KEY `CampusID` (`CampusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblprograms`
--

INSERT INTO `tblprograms` (`ProgID`, `CampusID`, `ProgCode`, `ProgName`, `ProgShortName`, `ProgYears`, `ProgSems`, `MaxResidency`, `TotalAcadSubject`, `TotalAcadCreditUnits`, `TotalNormalLoad`, `TotalGenEdUnits`, `TotalMajorUnits`, `TotalElectiveUnits`, `TotalLectureUnits`, `TotalNonLectUnits`, `ProgClass`, `Remarks`, `Active`) VALUES
('1', 1, 'AB ', 'Bachelor of Arts in English', 'Bachelor of Arts in English', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('10', 1, 'BSE ', 'Bachelor of Secondary Education', 'Bachelor of Secondary Education', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('12', 1, 'BSECE', 'BS Electronics & Communication Engineering', 'BSECE', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('13', 1, 'BSECE', 'BS Electronics & Communication Engineering', 'BS Electronics & Communication Engineering', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('14', 1, 'BSEE', 'Bachelor of Science in Electrical Engineering', 'Bachelor of Science in Electrical Engineering', 5, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('15', 1, 'BSEE', 'Bachelor Science in Electrical Engineering', 'Bachelor Science in Electrical Engineeri', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('16', 1, 'BSET', 'Bachelor of Science in Eco-Tourism', 'Bachelor of Science in Eco-Tourism', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('17', 1, 'BSIT', 'Bachelor of Science in Information Technology', 'Bachelor of Science in Information Technology', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('18', 1, 'BSN', 'Bachelor of Science in Nursing', 'Bachelor of Science in Nursing', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('19', 1, 'BT ', 'Bachelor of Technology', 'Bachelor of Technology', 4, 8, 4, 0, 0, 0, 0, 0, 0, 0, 0, '50', NULL, NULL),
('2', 1, 'ABBM', 'Bachelor of Arts in Business Management', 'Bachelor of Arts in Business Management', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('20', 1, 'Cert ', 'Certificate in Information Technology', 'Cert. in Information Technology', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('21', 1, 'CT ', 'Certificate of Technology', 'Certificate of Technology', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('22', 1, 'HighSchool', 'Secondary Laboratory School', 'Secondary Laboratory School', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '20', '', NULL),
('23', 1, 'MAEd', 'Master of Arts in Education', 'Master of Arts in Education', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('24', 1, 'MPA', 'Master in Public Administration', 'Master in Public Administration', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('25', 2, 'BSBA', 'Bachelor of Science in Business Administration', 'Bachelor of Science in Business Administration', 4, 8, 6, 45, 64, 0, 64, 32, 16, 12, 0, '50', NULL, NULL),
('26', 2, 'BSCE', 'Bachelor of Science in Civil Engineering', 'Bachelor of Science in Civil Engineering', 5, 10, 7, 36, 64, 0, 16, 12, 12, 12, 5, '50', NULL, NULL),
('27', 2, 'BSCS', 'Bachelor of Science in Computer Science', 'BS- Comp. Sci.', 4, 8, 6, 64, 32, 0, 2, 4, 6, 8, 9, '50', NULL, NULL),
('28', 1, 'BSIS', 'Bachelor of Science in Information Systems', 'BSIS', 4, 8, 7, 50, 10, 0, 86, 50, 10, 30, 20, '50', NULL, NULL),
('29', 1, 'BS DBM', 'Bachelor of Science in Database Management', 'BS in Database Management', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', NULL, NULL),
('3', 1, 'ABET', 'Bachelor of Arts in Eco Tourism', 'Bachelor of Arts in Eco Tourism', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('4', 1, 'ABPA', 'Bachelor of Arts in Public Administration', 'Bachelor of Arts in Public Administration', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('5', 1, 'ACS', 'Associate in Computer Science', 'Associate in Computer Science', 4, 8, 4, 0, 0, 0, 0, 0, 0, 0, 0, '50', NULL, NULL),
('6', 1, 'BEEd', 'Bachelor of Elementary Education', 'Bachelor of Elementary Education', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('7', 1, 'BS ', 'Bachelor of Science in Biology', 'Bachelor of Science in Biology', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('8', 1, 'BSA ', 'Bachelor of Science in Agriculture', 'Bachelor of Science in Agriculture', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL),
('9', 1, 'BSCS', 'Bachelor of Science in Computer Science', 'BS. Com Sci', 4, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, '50', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblprogram_class`
--

CREATE TABLE IF NOT EXISTS `tblprogram_class` (
  `ClassCode` int(10) unsigned NOT NULL,
  `ClassDesc` varchar(65) NOT NULL,
  `ShortName` varchar(45) NOT NULL,
  PRIMARY KEY (`ClassCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblprogram_class`
--

INSERT INTO `tblprogram_class` (`ClassCode`, `ClassDesc`, `ShortName`) VALUES
(5, 'Pre-School', ''),
(11, 'Elementary: Primary Level', ''),
(12, 'Elementary: Intermediate Level', ''),
(20, 'Secondary Level', ''),
(30, 'Technical / Vocational', ''),
(40, 'Pre-Baccalaureate Diploma, Certificate or Associate', ''),
(50, 'Baccalaureate Degree', ''),
(60, 'Post-Baccalaureate Degree Diploma or Certificate', ''),
(70, 'Bachelor of Laws, Doctor of Jurisprudence', ''),
(75, 'MD', ''),
(80, 'Masters', ''),
(90, 'PhD or Equivalent Level', ''),
(99, 'Not known or not indicated', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblroom`
--

CREATE TABLE IF NOT EXISTS `tblroom` (
  `RoomID` varchar(20) NOT NULL,
  `BldgID` int(11) DEFAULT NULL,
  `RoomName` varchar(100) DEFAULT NULL,
  `Capacity` int(11) DEFAULT NULL,
  `RoomNo` varchar(20) DEFAULT NULL,
  `RoomTypeID` int(11) DEFAULT NULL,
  `IsAirConditioned` tinyint(3) unsigned DEFAULT NULL,
  `IsUsable` tinyint(3) unsigned DEFAULT NULL,
  `IsLANReady` tinyint(3) unsigned DEFAULT NULL,
  `AllowNightClass` tinyint(3) unsigned DEFAULT NULL,
  `Shared` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`RoomID`),
  KEY `RoomTypeID` (`RoomTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblroom`
--

INSERT INTO `tblroom` (`RoomID`, `BldgID`, `RoomName`, `Capacity`, `RoomNo`, `RoomTypeID`, `IsAirConditioned`, `IsUsable`, `IsLANReady`, `AllowNightClass`, `Shared`) VALUES
('871', 1, '1', 62, '1', 1, 0, 1, 0, 0, 0),
('872', 1, '10', 65, '10', 1, 0, 1, 0, 0, 0),
('873', 1, '11', 61, '11', 1, 0, 1, 0, 0, 0),
('874', 1, '12', 60, '12', 1, 0, 1, 0, 0, 0),
('875', 1, '13', 60, '13', 1, 0, 1, 0, 0, 0),
('876', 1, '14', 60, '14', 1, 0, 1, 0, 0, 0),
('877', 1, '16', 60, '16', 1, 0, 1, 0, 0, 0),
('878', 1, '17', 60, '17', 1, 0, 1, 0, 0, 0),
('879', 1, '18', 60, '18', 1, 0, 1, 0, 0, 0),
('880', 1, '19', 60, '19', 1, 0, 1, 0, 0, 0),
('881', 1, '2', 60, '2', 1, 0, 1, 0, 0, 0),
('882', 1, '20', 60, '20', 1, 0, 1, 0, 0, 0),
('883', 1, '21', 60, '21', 1, 0, 1, 0, 0, 0),
('884', 1, '22', 65, '22', 1, 0, 1, 0, 0, 0),
('885', 1, '23', 60, '23', 1, 0, 1, 0, 0, 0),
('886', 1, '24', 60, '24', 1, 0, 1, 0, 0, 0),
('887', 1, '25-IEComLa', 60, '25-IEComLa', 1, 0, 1, 0, 0, 0),
('888', 1, '26-chemlab', 60, '26-chemlab', 1, 0, 1, 0, 0, 0),
('889', 1, '27-chemlab', 60, '27-chemlab', 1, 0, 1, 0, 0, 0),
('890', 1, '28- Eshop', 60, '28- Eshop', 1, 0, 1, 0, 0, 0),
('891', 1, '29', 61, '29', 1, 0, 1, 0, 0, 0),
('892', 1, '3', 60, '3', 1, 0, 1, 0, 0, 0),
('893', 1, '30', 60, '30', 1, 0, 1, 0, 0, 0),
('894', 1, '31', 80, '31', 1, 0, 1, 0, 0, 0),
('895', 1, '32', 70, '32', 1, 0, 1, 0, 0, 0),
('896', 1, '33-autosho', 80, '33-autosho', 1, 0, 1, 0, 0, 0),
('897', 1, '34-Eshop', 80, '34-Eshop', 1, 0, 1, 0, 0, 0),
('898', 1, '35-autosho', 70, '35-autosho', 1, 0, 1, 0, 0, 0),
('899', 1, '36', 60, '36', 1, 0, 1, 0, 0, 0),
('900', 1, '37', 60, '37', 1, 0, 1, 0, 0, 0),
('901', 1, '37-HSLAB1', 50, '37-HSLAB1', 1, 0, 1, 0, 0, 0),
('902', 1, '38-CompWrk', 60, '38-CompWrk', 1, 0, 1, 0, 0, 0),
('903', 1, '39-Lab', 60, '39-Lab', 1, 0, 1, 0, 0, 0),
('904', 1, '4', 61, '4', 1, 0, 1, 0, 0, 0),
('905', 1, '4-IA', 60, '4-IA', 1, 0, 1, 0, 0, 0),
('906', 1, '40-Lab', 60, '40-Lab', 1, 0, 1, 0, 0, 0),
('907', 1, '41', 50, '41', 1, 0, 1, 0, 0, 0),
('908', 1, '42', 50, '42', 1, 0, 1, 0, 0, 0),
('909', 1, '43', 50, '43', 1, 0, 1, 0, 0, 0),
('910', 1, '44', 50, '44', 1, 0, 1, 0, 0, 0),
('911', 1, '45', 50, '45', 1, 0, 1, 0, 0, 0),
('912', 1, '46', 61, '46', 1, 0, 1, 0, 0, 0),
('913', 1, '47', 50, '47', 1, 0, 1, 0, 0, 0),
('914', 1, '48', 60, '48', 1, 0, 1, 0, 0, 0),
('915', 1, '49', 61, '49', 1, 0, 1, 0, 0, 0),
('916', 1, '5', 65, '5', 1, 0, 1, 0, 0, 0),
('917', 1, '5-IA', 60, '5-IA', 1, 0, 1, 0, 0, 0),
('918', 1, '50', 60, '50', 1, 0, 1, 0, 0, 0),
('919', 1, '51', 60, '51', 1, 0, 1, 0, 0, 0),
('920', 1, '52', 60, '52', 1, 0, 1, 0, 0, 0),
('921', 1, '53-Civl ', 60, '53-Civl ', 1, 0, 1, 0, 0, 0),
('922', 1, '54-Draftin', 61, '54-Draftin', 1, 0, 1, 0, 0, 0),
('923', 1, '55', 60, '55', 1, 0, 1, 0, 0, 0),
('924', 1, '56', 60, '56', 1, 0, 1, 0, 0, 0),
('925', 1, '57', 60, '57', 1, 0, 1, 0, 0, 0),
('926', 1, '58', 60, '58', 1, 0, 1, 0, 0, 0),
('927', 1, '59-Garment', 60, '59-Garment', 1, 0, 1, 0, 0, 0),
('928', 1, '6-IA', 60, '6-IA', 1, 0, 1, 0, 0, 0),
('929', 1, '7', 60, '7', 1, 0, 1, 0, 0, 0),
('930', 1, '8', 60, '8', 1, 0, 1, 0, 0, 0),
('931', 1, '9', 60, '9', 1, 0, 1, 0, 0, 0),
('932', 1, 'AVR-IA', 50, 'AVR-IA', 1, 0, 1, 0, 0, 0),
('933', 1, 'CDO ', 60, 'CDO ', 1, 0, 1, 0, 0, 0),
('934', 1, 'CGH', 60, 'CGH', 1, 0, 1, 0, 0, 0),
('935', 1, 'Community', 60, 'Community', 1, 0, 1, 0, 0, 0),
('936', 1, 'Field', 1000, 'Field', 1, 0, 1, 0, 0, 0),
('937', 1, 'Grnd NSTP', 1000, 'Grnd NSTP', 1, 0, 1, 0, 0, 0),
('938', 1, 'Grnd ROTC', 1000, 'Grnd ROTC', 1, 0, 1, 0, 0, 0),
('939', 1, 'Grnd ROTC1', 1000, 'Grnd ROTC1', 1, 0, 1, 0, 0, 0),
('940', 1, 'Ground', 1000, 'Ground', 1, 0, 1, 0, 0, 0),
('941', 1, 'Guindn off', 60, 'Guindn off', 1, 0, 1, 0, 0, 0),
('942', 1, 'Gym', 1001, 'Gym', 1, 0, 1, 0, 0, 0),
('943', 1, 'Gym 1', 1001, 'Gym 1', 1, 0, 1, 0, 0, 0),
('944', 1, 'Gym 2', 1000, 'Gym 2', 1, 0, 1, 0, 0, 0),
('945', 1, 'HOH', 61, 'HOH', 1, 0, 1, 0, 0, 0),
('946', 1, 'In-campus', 60, 'In-campus', 1, 0, 1, 0, 0, 0),
('947', 1, 'kitchen', 60, 'kitchen', 1, 0, 1, 0, 0, 0),
('948', 1, 'MMC', 50, 'MMC', 1, 0, 1, 0, 0, 0),
('949', 1, 'NAL', 60, 'NAL', 1, 0, 1, 0, 0, 0),
('950', 1, 'NSTP office', 15, 'NSTP offic', 1, 0, 1, 0, 0, 0),
('951', 1, 'OJT', 100, 'OJT', 1, 0, 1, 0, 0, 0),
('952', 1, 'OJT 1', 100, 'OJT 1', 1, 0, 1, 0, 0, 0),
('953', 1, 'OJT 2', 100, 'OJT 2', 1, 0, 1, 0, 0, 0),
('954', 1, 'OJT 3', 100, 'OJT 3', 1, 0, 1, 0, 0, 0),
('955', 1, 'Prduction', 60, 'Prduction', 1, 0, 1, 0, 0, 0),
('956', 1, 'Pres-offic', 61, 'Pres-offic', 1, 0, 1, 0, 0, 0),
('957', 1, 'RHU', 61, 'RHU', 1, 0, 1, 0, 0, 0),
('958', 1, 'MESC 101', 50, 'MESC 101', 1, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblroomtypes`
--

CREATE TABLE IF NOT EXISTS `tblroomtypes` (
  `RoomTypeID` int(11) NOT NULL,
  `RoomType` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RoomTypeID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblroomtypes`
--

INSERT INTO `tblroomtypes` (`RoomTypeID`, `RoomType`) VALUES
(1, 'Generic Lecture Room'),
(2, 'Computer Laboratory'),
(3, 'Lab for Physical Sciences'),
(4, 'Lab for Biological Sciences'),
(5, 'Audio-Visual Room'),
(6, 'Workshop'),
(7, 'Library'),
(8, 'Sports Use'),
(9, 'Hall'),
(10, 'Engineering Laboratory'),
(20, 'Administrative Office'),
(30, 'Utility Room'),
(99, 'Not known');

-- --------------------------------------------------------

--
-- Table structure for table `tblsection`
--

CREATE TABLE IF NOT EXISTS `tblsection` (
  `SectionID` int(11) NOT NULL AUTO_INCREMENT,
  `SectionTitle` varchar(255) DEFAULT NULL,
  `YearLevelID` int(11) DEFAULT '0',
  `TermID` int(10) unsigned DEFAULT NULL,
  `CampusID` varchar(45) DEFAULT NULL,
  `ProgramID` varchar(45) DEFAULT NULL,
  `AdviserID` varchar(45) DEFAULT NULL,
  `IsBlock` tinyint(3) unsigned DEFAULT NULL,
  `RoomID` varchar(45) DEFAULT NULL,
  `Limit` int(10) unsigned DEFAULT NULL,
  `IsEvening` tinyint(3) unsigned DEFAULT NULL,
  `IsDissolved` tinyint(3) unsigned DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SectionID`),
  KEY `YearLevelID` (`YearLevelID`),
  KEY `TermID` (`TermID`) USING BTREE,
  KEY `CampusID` (`CampusID`) USING BTREE,
  KEY `ProgramID` (`ProgramID`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tblsection`
--

INSERT INTO `tblsection` (`SectionID`, `SectionTitle`, `YearLevelID`, `TermID`, `CampusID`, `ProgramID`, `AdviserID`, `IsBlock`, `RoomID`, `Limit`, `IsEvening`, `IsDissolved`, `CreationDate`, `CreatedBy`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'BSIT 1A', 1, 1, '1', '17', '', 1, '', 60, NULL, 0, '2013-04-18 20:56:17', 'Philip Cesar B. Garay', NULL, NULL),
(2, 'BSIT 1B', 1, 1, '1', '17', '', 1, '', 60, NULL, 0, '2013-04-18 20:56:30', 'Philip Cesar B. Garay', NULL, NULL),
(3, 'BSIT 1C', 1, 1, NULL, '17', NULL, 1, NULL, 60, NULL, 0, '2014-09-22 14:13:10', 'admin', 'admin', '2014-09-22 14:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `tblsubject`
--

CREATE TABLE IF NOT EXISTS `tblsubject` (
  `SubjectID` int(11) NOT NULL AUTO_INCREMENT,
  `SubjectCode` varchar(45) DEFAULT NULL,
  `SubjectTitle` varchar(50) DEFAULT NULL,
  `Description` text,
  `InActive` tinyint(4) unsigned DEFAULT '0',
  `Units` float DEFAULT '0',
  `CreditUnits` float unsigned DEFAULT '0',
  `LecHrs` float DEFAULT '0',
  `LabUnits` float unsigned DEFAULT '0',
  `LabHrs` float unsigned DEFAULT '0',
  `SubjectMode` varchar(45) DEFAULT NULL,
  `IsNonAcademic` tinyint(4) unsigned DEFAULT '0',
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  `LevelID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SubjectID`),
  KEY `Index_3` (`SubjectMode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1264 ;

--
-- Dumping data for table `tblsubject`
--

INSERT INTO `tblsubject` (`SubjectID`, `SubjectCode`, `SubjectTitle`, `Description`, `InActive`, `Units`, `CreditUnits`, `LecHrs`, `LabUnits`, `LabHrs`, `SubjectMode`, `IsNonAcademic`, `CreatedBy`, `CreationDate`, `ModifiedBy`, `ModifiedDate`, `LevelID`) VALUES
(1, 'Acctg 1&2', 'Fundamentals of Accounting 1', 'Fundamentals of Accounting 1', 1, 6, 6, 0, 1, 1, '7', 1, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(2, 'Acctg 1a', 'Introduction to Accounting and Bookkeeping', 'Introduction to Accounting and Bookkeeping', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(3, 'Acctg 1b', 'Principles of Accounting in Agriculture', 'Principles of Accounting in Agriculture', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(4, 'Acctg 1c', 'Introduction to Accounting', 'Introduction to Accounting', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(5, 'Acctg 3', 'Fundamentals of Accounting 2', 'Fundamentals of Accounting 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(6, 'Acctg 4', 'Financial Accounting', 'Financial Accounting', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(7, 'ACH 1', 'Camiguin Cultural Heritage with Camiguin Tour', 'Camiguin Cultural Heritage with Camiguin Tour', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(8, 'ACH 2', 'Philippine and Asia Cultural Heritage', 'Philippine and Asia Cultural Heritage', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(9, 'ACH 3', 'World Tourism with Geography', 'World Tourism with Geography', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(10, 'ACH 4', 'Cultural Enterprise and Product Development', 'Cultural Enterprise and Product Development', 1, 3, 3, 0, 0, 0, '4', 1, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(11, 'AE 1', 'Fundamentals of Agricultural Engineering 1', 'Fundamentals of Agricultural Engineering 1', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(12, 'AE 2', 'Fundamentals of Agricultural Engineering 2', 'Fundamentals of Agricultural Engineering 2', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(13, 'AgEcon 1', 'Farm Business Finance', 'Farm Business Finance', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(14, 'AgEcon 2', 'Introduction to Agricultural Marketing', 'Introduction to Agricultural Marketing', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(15, 'AgEd 100', 'Practice Teaching', 'Practice Teaching', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(16, 'AgEd 10A', 'Methods of Research and Field Study', 'Methods of Research and Field Study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(17, 'AgEd 10B', 'Research Paper Presentation', 'Research Paper Presentation', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(18, 'AgEd 20', 'Agricultural  Extension & Communication', 'Agricultural  Extension & Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(19, 'AgEd 30', 'Industrial and Practical Arts', 'Industrial and Practical Arts', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(20, 'AgEd 40', 'New Enterprise Planning', 'New Enterprise Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(21, 'AgEd 50', 'Inland Fish Culture', 'Inland Fish Culture', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(22, 'AgEd 60', 'Environmental Management & Sus Agr', 'Environmental Management & Sus Agr', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(23, 'AgEd 70', 'Lesson Planning VA', 'Lesson Planning VA', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(24, 'Agric1/AM3a', 'Meth Res AnSci/Feasibility Study', 'Meth Res AnSci/Feasibility Study', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(25, 'Agric1/AM3b', 'Meth Res CropSci/Feasibility Study', 'Meth Res CropSci/Feasibility Study', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(26, 'Agron 1', 'Intro to Agronomy', 'Intro to Agronomy', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(27, 'AM 3', 'Principles of Organization and Management', 'Principles of Organization and Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(28, 'AM 4', 'Agricultural Production Management', 'Agricultural Production Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(29, 'AM 5', 'Financial Mgt in Agric', 'Financial Mgt in Agric', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(30, 'AnSci 1', 'Principles of Animal Science', 'Principles of Animal Science', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(31, 'AnSci 10', 'Pasture & Forage', 'Pasture & Forage', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(32, 'AnSci 100', 'Thesis/Pract/Project Study', 'Thesis/Pract/Project Study', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(33, 'AnSci 20', 'Feeds and Feeding', 'Feeds and Feeding', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(34, 'AnSci 30', 'Poultry', 'Poultry', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(35, 'AnSci 40', 'Swine', 'Swine', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(36, 'AnSci 50', 'Ruminant', 'Ruminant', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(37, 'AnSci 60', 'Meat Technology', 'Meat Technology', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(38, 'AnSci 70', 'Animal Breeding & Genetics', 'Animal Breeding & Genetics', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(39, 'AnSci 80', 'Animal Diseases', 'Animal Diseases', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(40, 'Bio 1', 'General Biology', 'General Biology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(41, 'Bio 1h', 'Biological Science', 'Biological Science', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(42, 'Bio 2', 'Anatomy and Physiology', 'Anatomy and Physiology', 0, 3, 5, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(43, 'Bio 3', 'Microbiology', 'Microbiology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(44, 'Bio 3a', 'Microbiology and Parasitology', 'Microbiology and Parasitology', 0, 3, 4, 0, 1, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(45, 'Bio 4', 'Cell Biology', 'Cell Biology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(46, 'Bio 5', 'Genetics', 'Genetics', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(47, 'Bio 6', 'Terrestrial and Marine Biodiversity', 'Terrestrial and Marine Biodiversity', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(48, 'Bio 7', 'Ecology', 'Ecology', 0, 4, 6, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(49, 'Bio 7a', 'Ecology and Environmental Issues', 'Ecology and Environmental Issues', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(50, 'Bio 7b', 'Ecology and Field Studies', 'Ecology and Field Studies', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(51, 'Botn 1', 'Botany', 'Botany', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(52, 'Botn 2', 'Marine Botany', 'Marine Botany', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(53, 'Busn 11', 'Principles of Management', 'Principles of Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(54, 'Busn 22', 'Principles of Marketing', 'Principles of Marketing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(55, 'Busn 32B', 'Entrep, Case Studies', 'Entrep, Case Studies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(56, 'Chem 1a', 'General Chemistry', 'General Chemistry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(57, 'Chem 1b', 'General Chemistry', 'General Chemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(58, 'Chem 1c', 'General and Inorganic Chemistry', 'General and Inorganic Chemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(59, 'Chem 1d', 'General Chemistry', 'General Chemistry', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(60, 'Chem 2', 'Inorganic Chemistry', 'Inorganic Chemistry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(61, 'Chem 2a', 'Inorganic Chemistry', 'Inorganic Chemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(62, 'Chem 3', 'Organic Chemistry', 'Organic Chemistry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(63, 'Chem 3a', 'Organic Chemistry', 'Organic Chemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(64, 'Chem 4', 'Biochemistry', 'Biochemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(65, 'Chem 5', 'Analytical Chemistry', 'Analytical Chemistry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(66, 'Comp 1', 'Computer Typing', 'Computer Typing', 0, 0, 3, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(67, 'Comp 10', 'Computer Programming 2', 'Computer Programming 2', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(68, 'Comp 12', 'Computer Systems 2', 'Computer Systems 2', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(69, 'Comp 13', 'Computer Systems 3', 'Computer Systems 3', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(70, 'Comp 14', 'Database Management Systems', 'Database Management Systems', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(71, 'Comp 15', 'Computer Workshop', 'Computer Workshop', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(72, 'Comp 16', 'Object-Programming', 'Object-Programming', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(73, 'Comp 17', 'Desktop Publishing', 'Desktop Publishing', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(74, 'Comp 19', 'Systems Analysis and Design', 'Systems Analysis and Design', 0, 2, 5, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(75, 'Comp 2', 'Professional Ethics & Values Educ.', 'Professional Ethics & Values Educ.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(76, 'Comp 20', 'Design, Implementation & Analysis of Algorithms', 'Design, Implementation & Analysis of Algorithms', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(77, 'Comp 21', 'Network Installation & Administration', 'Network Installation & Administration', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(78, 'Comp 22', 'Practicum (270 Hrs)', 'Practicum (270 Hrs)', 0, 0, 5, 0, 5, 15, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(79, 'Comp 23', 'Software Design & Development', 'Software Design & Development', 0, 2, 5, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(80, 'Comp 24', 'Assembly Language', 'Assembly Language', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(81, 'Comp 25', 'Computer Graphics', 'Computer Graphics', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(82, 'Comp 26', 'Dynamic Web Page Design', 'Dynamic Web Page Design', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(83, 'Comp 27', 'Computer Audit & Sec. Control', 'Computer Audit & Sec. Control', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(84, 'Comp 28', 'Intro to Multimedia Development', 'Intro to Multimedia Development', 0, 1, 4, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(85, 'Comp 3', 'Computer Fundamentals', 'Computer Fundamentals', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(86, 'Comp 30', 'Seminar & Company Visit', 'Seminar & Company Visit', 0, 0, 3, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(87, 'Comp 31', 'Software Engineering', 'Software Engineering', 0, 2, 5, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(88, 'Comp 32', 'Multimedia Production & Development', 'Multimedia Production & Development', 0, 1, 4, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(89, 'Comp 33', 'Structure to Programming Languages', 'Structure to Programming Languages', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(90, 'Comp 35', 'Special Problem- Project', 'Special Problem- Project', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(91, 'Comp 3a', 'Computer Fundamentals', 'Computer Fundamentals', 0, 0, 2, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(92, 'Comp 4', 'Software Applications', 'Software Applications', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(93, 'Comp 5', 'Data Structures & Algorithms', 'Data Structures & Algorithms', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(94, 'Comp 5a', 'Computer Programming', 'Computer Programming', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(95, 'Comp 7', 'Computer Organization & Architecture', 'Computer Organization & Architecture', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(96, 'Comp 8', 'Operating Systems', 'Operating Systems', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(97, 'Comp 9', 'Computer Aided Design', 'Computer Aided Design', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(98, 'CoRe_a', 'Comprehensive Review', 'Comprehensive Review', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(99, 'CoRe_b', 'LET Comprehensive Review', 'LET Comprehensive Review', 0, 0, 0, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(100, 'CS 101', 'CS Fundamentals', 'CS Fundamentals', 0, 2, 3, 0, 1, 3, NULL, 0, NULL, '0001-01-01 00:00:00', NULL, '2013-04-27 00:00:00', NULL),
(101, 'CS 102', 'Computer Programming 1', 'Computer Programming 1', 0, 2, 3, 0, 1, 3, NULL, 0, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(102, 'CS 103', 'Computer Programming 2', 'Computer Programming 2', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(103, 'CS 104', 'Discrete Structures', 'Discrete Structures', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(104, 'CS 105', 'Computer Organization w/ Assembly Language', 'Computer Organization w/ Assembly Language', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', 'admin', '2012-05-11 10:22:33', NULL),
(105, 'CS 106', 'Professional Ethics', 'Professional Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(106, 'CS 11', 'Computer Systems 1', 'Computer Systems 1', 0, 2, 4, 0, 2, 4, NULL, 0, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(107, 'CS 201', 'Data Structures', 'Data Structures', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(108, 'CS 202', 'Design & Analysis of Algorithms', 'Design & Analysis of Algorithms', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(109, 'CS 203', 'Programming Languages', 'Programming Languages', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(110, 'CS 204', 'Automata and Language Theory', 'Automata and Language Theory', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(111, 'CS 205', 'Modeling and Simulation', 'Modeling and Simulation', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(112, 'CS 206', 'Digital Design', 'Digital Design', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(113, 'CS 207', 'Operating Systems', 'Operating Systems', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(114, 'CS 208', 'Network Principles & Programming', 'Network Principles & Programming', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(115, 'CS 302', 'CS Elective 2 (Network Installation & Mgt)', 'CS Elective 2 (Network Installation & Mgt)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(116, 'CS 211', 'Object Oriented Programming', 'Object Oriented Programming', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(117, 'CS 212', 'Database Systems', 'Database Systems', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(118, 'CS 213', 'Web Programming', 'Web Programming', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(119, 'CS 214', 'Software Engineering', 'Software Engineering', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(120, 'CS 301', 'CS Elect 1 (Visual Basic1 Sys Analysis & Design)', 'CS Elect 1 (Visual Basic1 Sys Analysis & Design)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(121, 'CS 303', 'CS Elective 3 (Visual Basic 2)', 'CS Elective 3 (Visual Basic 2)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(122, 'CS 304', 'CS Elective 4 (Software Project)', 'CS Elective 4 (Software Project)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(123, 'CS 401', 'Free Elective 1', 'Free Elective 1', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(124, 'CS 402', 'Free Elective 2 (Computer Hardware Servicing)', 'Free Elective 2 (Computer Hardware Servicing)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(125, 'CS 403', 'Free Elective 3 (Dynamic Web Application)', 'Free Elective 3 (Dynamic Web Application)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(126, 'CS 500', 'On-the-Job Training (160 Hrs)', 'On-the-Job Training (160 Hrs)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(127, 'CS 501', 'Thesis 1', 'Thesis 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(128, 'CS 502', 'Thesis 2', 'Thesis 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(129, 'Drwg 1', 'Intro to Design', 'Intro to Design', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(130, 'Drwg 2', 'Basic Project Design', 'Basic Project Design', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(131, 'ECE 1', 'Electronic Devices and Circuits', 'Electronic Devices and Circuits', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(132, 'ECE 10', 'Transmission Media and Antenna System', 'Transmission Media and Antenna System', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(133, 'ECE 11', 'Data Communications', 'Data Communications', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(134, 'ECE 12', 'ECE Laws, Contracts, and Ethics', 'ECE Laws, Contracts, and Ethics', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(135, 'ECE 14', 'Seminars and Field Trips (ECE)', 'Seminars and Field Trips (ECE)', 0, 0, 1, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(136, 'ECE 15', 'Industrial Electronics', 'Industrial Electronics', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(137, 'ECE 2', 'Electronic Circuit Analysis and Design', 'Electronic Circuit Analysis and Design', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(138, 'ECE 3', 'Pulse and Switching Electronics', 'Pulse and Switching Electronics', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(139, 'ECE 321', 'Transistor Circuits', 'Transistor Circuits', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(140, 'ECE 4', 'Signals, Spectra, and Signal Processing', 'Signals, Spectra, and Signal Processing', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(141, 'ECE 411', 'Integrated Circuits', 'Integrated Circuits', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(142, 'ECE 412', 'Electronic Amplifiers', 'Electronic Amplifiers', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(143, 'ECE 413', 'Power Electronics', 'Power Electronics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(144, 'ECE 422', 'Pulse Timing & Switching Circuits', 'Pulse Timing & Switching Circuits', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(145, 'ECE 423', 'Industrial Electronics', 'Industrial Electronics', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(146, 'ECE 424', 'Transmission Media', 'Transmission Media', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(147, 'ECE 511', 'Propagation & Antennas', 'Propagation & Antennas', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(148, 'ECE 512', 'Instrumentation & Controls', 'Instrumentation & Controls', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(149, 'ECE 513', 'Communication Electronics 1', 'Communication Electronics 1', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(150, 'ECE 515', 'Satellite Communication', 'Satellite Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(151, 'ECE 516', 'Feasibility Study (ECE)', 'Feasibility Study (ECE)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(152, 'ECE 517', 'Seminar & Field Trips', 'Seminar & Field Trips', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(153, 'ECE 521', 'Communication Electronics 2', 'Communication Electronics 2', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(154, 'ECE 523', 'Telephone System', 'Telephone System', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(155, 'ECE 524', 'Optical Communications', 'Optical Communications', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(156, 'ECE 6', 'Principles of Communications', 'Principles of Communications', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(157, 'ECE 7', 'Logic Circuits and Switching Theory', 'Logic Circuits and Switching Theory', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(158, 'ECE 8', 'Feedback and Control System', 'Feedback and Control System', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(159, 'ECE 9', 'Digital Communications', 'Digital Communications', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(160, 'Econ 1', 'Economics, Taxation and Land Reform', 'Economics, Taxation and Land Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(161, 'Econ 1a', 'Health Economics, Taxation, and Land Reform', 'Health Economics, Taxation, and Land Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(162, 'Econ 1b', 'Taxation and Land Reform', 'Taxation and Land Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(163, 'Econ 1c', 'Principles of Economics', 'Principles of Economics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(164, 'Econ 2', 'Microeconomics', 'Microeconomics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(165, 'Econ 2a', 'Micro/Macroeconomics', 'Micro/Macroeconomics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(166, 'Econ 3', 'Macroeconomics', 'Macroeconomics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(167, 'Educ 1', 'Fundamentals of Music', 'Fundamentals of Music', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(168, 'Educ 10', 'Assessment of Student Learning I', 'Assessment of Student Learning I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(169, 'Educ 10a', 'Assessment of Student Learning I w/ Field Study', 'Assessment of Student Learning I w/ Field Study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(170, 'Educ 11', 'Assessment of Student Learning II', 'Assessment of Student Learning II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(171, 'Educ 11a', 'Assessment of Student Learning II w/ Field study', 'Assessment of Student Learning II w/ Field study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(172, 'Educ 12', 'Social Dimensions of Education', 'Social Dimensions of Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(173, 'Educ 13', 'Curriculum Development', 'Curriculum Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(174, 'Educ 13a', 'Curriculum Development w/ Field Study', 'Curriculum Development w/ Field Study', 0, 3, 4, 0, 1, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(175, 'Educ 14', 'Educational Technology I', 'Educational Technology I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(176, 'Educ 15', 'Educational Technology II w/ Field study', 'Educational Technology II w/ Field study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(177, 'Educ 16', 'Principles of Teaching & Strategies I', 'Principles of Teaching & Strategies I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(178, 'Educ 16a', 'Principles of Teaching w/ Field Study', 'Principles of Teaching w/ Field Study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(179, 'Educ 17a', 'Principles of Teaching & Strategies II w/ FS', 'Principles of Teaching & Strategies II w/ FS', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(180, 'Educ 17b', 'Teaching Strat for Agriculture + Field Study', 'Teaching Strat for Agriculture + Field Study', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(181, 'Educ 18', 'Professional Ethics', 'Professional Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(182, 'Educ 19', 'Social Dimensions in Education', 'Social Dimensions in Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(183, 'Ed 122', 'Developmental Reading 1', 'Developmental Reading 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(184, 'Educ 20', 'The Teaching Profession w/ Field Study', 'The Teaching Profession w/ Field Study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(185, 'Educ 21', 'Student Teaching Incl. Observation & Participation', 'Student Teaching Incl. Observation & Participation', 0, 0, 6, 0, 6, 12, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(186, 'Educ 22', 'Problems Met', 'Problems Met', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(187, 'Ed 125', 'Developmental Reading 2', 'Developmental Reading 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(188, 'Ed 121', 'Child & Adolescent Development', 'Child & Adolescent Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(189, 'Educ 4', 'Principles of Education', 'Principles of Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(190, 'Educ 5', 'Foundation of Education', 'Foundation of Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(191, 'Educ 6', 'Facilitating Learning', 'Facilitating Learning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(192, 'Educ 6a', 'Facilitating Learning w/ Field Study', 'Facilitating Learning w/ Field Study', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(193, 'Educ 7', 'Guidance & Counselling', 'Guidance & Counselling', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(194, 'Educ 8', 'Intro to Educational Research', 'Intro to Educational Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(195, 'Educ 9', 'Test, Measurement & Evaluation', 'Test, Measurement & Evaluation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(196, 'EE 1', 'Circuits 1', 'Circuits 1', 0, 2, 4, 0, 2, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(197, 'EE 10', 'Electrical Transmission and Distribution System', 'Electrical Transmission and Distribution System', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(198, 'EE 11', 'Electrical Equipment Operation and Maintenance', 'Electrical Equipment Operation and Maintenance', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(199, 'EE 12', 'Electrical System Design', 'Electrical System Design', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(200, 'EE 13', 'Illumination Engineering Design', 'Illumination Engineering Design', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(201, 'EE 14', 'Seminars and Field Trips (EE)', 'Seminars and Field Trips (EE)', 0, 0, 1, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(202, 'EE 15', 'EE Laws, Codes, and Standards', 'EE Laws, Codes, and Standards', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(203, 'EE 16', 'Instrumentation and Controls', 'Instrumentation and Controls', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(204, 'EE 17', 'Power System Analysis and Design', 'Power System Analysis and Design', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(205, 'EE 18', 'Power Plant Engineering', 'Power Plant Engineering', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(206, 'EE 2', 'Circuits 2', 'Circuits 2', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(207, 'EE 20', 'Energy Conversion', 'Energy Conversion', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(208, 'EE 3', 'Electromagnetics', 'Electromagnetics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(209, 'EE 4', 'Circuits 3', 'Circuits 3', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(210, 'EE 411', 'Electromechanical Energy Conversion', 'Electromechanical Energy Conversion', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(211, 'EE 421', 'Electrical Machines I', 'Electrical Machines I', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(212, 'EE 5', 'Direct Current Machinery', 'Direct Current Machinery', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(213, 'EE 511', 'Electrical Machines II', 'Electrical Machines II', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(214, 'EE 512', 'Power System I', 'Power System I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(215, 'EE 514', 'Instrumentation & Management', 'Instrumentation & Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(216, 'EE 515', 'Feasibility Study (EE)', 'Feasibility Study (EE)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(217, 'EE 516', 'Field Trip & Seminars', 'Field Trip & Seminars', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(218, 'EE 521', 'Power System II', 'Power System II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(219, 'EE 523', 'EE Laws, Contract & Ethics', 'EE Laws, Contract & Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(220, 'EE 54', 'Electrical Equipment and Devices/ PEC I & II', 'Electrical Equipment and Devices/ PEC I & II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(221, 'EE 6', 'Electrical Engineering Safety', 'Electrical Engineering Safety', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(222, 'EE 7', 'Alternating Current Machinery', 'Alternating Current Machinery', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(223, 'EE 8', 'Alternating Current Apparatus', 'Alternating Current Apparatus', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(224, 'EE 9', 'Control Systems', 'Control Systems', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(225, 'Elec. 2', 'Industrial Design', 'Industrial Design', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(226, 'Elect 1', 'Applied Project Proto-Typing', 'Applied Project Proto-Typing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(227, 'Elective 1', 'Elective 1', 'Elective 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(228, 'Elective 2', 'Elective 2', 'Elective 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(229, 'Eltn I', 'Basic Electronics w/ Field of Study', 'Basic Electronics w/ Field of Study', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(230, 'Engl +', 'Remedial English', 'Remedial English', 0, 0, 0, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(231, 'Engl 1', 'Communication Arts 1', 'Communication Arts 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(232, 'Engl 10', 'Argumentation and Debate', 'Argumentation and Debate', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(233, 'Engl 11', 'Business Correspondence', 'Business Correspondence', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(234, 'Engl 12', 'Survey of American Literature', 'Survey of American Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(235, 'Engl 13', 'Survey of English Literature', 'Survey of English Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(236, 'Engl 14', 'Poetry', 'Poetry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(237, 'Engl 15', 'Afro-Asian Literature', 'Afro-Asian Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(238, 'Engl 16', 'Structure of English', 'Structure of English', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(239, 'Engl 17', 'Mythology and Short Story', 'Mythology and Short Story', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(240, 'Engl 18', 'The Novel', 'The Novel', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(241, 'Engl 19', 'The Drama', 'The Drama', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(242, 'Engl 2', 'Communication Arts 2', 'Communication Arts 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(243, 'Engl 20', 'Prep. and Eval. of Instructional Materials', 'Prep. and Eval. of Instructional Materials', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(244, 'Engl 21', 'Teaching English as a Second Language', 'Teaching English as a Second Language', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(245, 'Engl 22', 'Literary Criticism', 'Literary Criticism', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(246, 'Engl 23', 'Reading and Writing Essay', 'Reading and Writing Essay', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(247, 'Engl 24', 'Children''s Literature', 'Children''s Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(248, 'Engl 25', 'Introduction to Linguistics', 'Introduction to Linguistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(249, 'Engl 26', 'Remedial Instruction in English', 'Remedial Instruction in English', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(250, 'Engl 3c', 'Technical Writing and Reporting', 'Technical Writing and Reporting', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(251, 'Engl 3e', 'Effective Writing', 'Effective Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(252, 'Engl 3b', 'Creative Writing', 'Creative Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(253, 'Engl 4', 'Speech and Oral Communication', 'Speech and Oral Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(254, 'Engl 4a', 'Technical Communications', 'Technical Communications', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(255, 'Engl 5', 'Developmental Reading', 'Developmental Reading', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(256, 'Engl 6', 'Introduction to Literature', 'Introduction to Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(257, 'Engl 7', 'Philippine Literature', 'Philippine Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(258, 'Engl 8', 'Fundamentals of Journalism', 'Fundamentals of Journalism', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(259, 'Engl 9', 'Survey of World Literature', 'Survey of World Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(260, 'Entrep 1', 'Entrepreneurship and Business Planning', 'Entrepreneurship and Business Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(261, 'Envi 11', 'Environmental Problems & Intro. to Basic Resources', 'Environmental Problems & Intro. to Basic Resources', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(262, 'Envi 12', 'Principles of Resource Mgt.', 'Principles of Resource Mgt.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(263, 'Envi 13', 'Intro to Envitâ€™l & Resource Economics.', 'Intro to Envitâ€™l & Resource Economics.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(264, 'Envi 22', 'Policies & Laws in the Environment', 'Policies & Laws in the Environment', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(265, 'Envi 23', 'Coastal Resource Management', 'Coastal Resource Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(266, 'Envi 23a', 'Coastal Resource Mgt.', 'Coastal Resource Mgt.', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(267, 'Envi Sci', 'Environmental Science', 'Environmental Science', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(268, 'ES 1', 'Engineering Drawing', 'Engineering Drawing', 0, 0, 1, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(269, 'ES 10', 'Engineering Management', 'Engineering Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(270, 'ES 12', 'Fluid Mechanics', 'Fluid Mechanics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(271, 'ES 13', 'Microprocessor Systems', 'Microprocessor Systems', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(272, 'ES 2', 'Engineering Economy', 'Engineering Economy', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(273, 'ES 3', 'Computer Aided Drafting', 'Computer Aided Drafting', 0, 0, 1, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(274, 'ES 311', 'Engineering Mechanics', 'Engineering Mechanics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(275, 'ES 321', 'Strength of Materials', 'Strength of Materials', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(276, 'ES 4', 'Materials Engineering', 'Materials Engineering', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(278, 'ES 422', 'Engineering Materials', 'Engineering Materials', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(279, 'ES 5', 'Statics of Rigid Bodies', 'Statics of Rigid Bodies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(280, 'ES 6', 'Dynamics of Rigid Bodies', 'Dynamics of Rigid Bodies', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(281, 'ES 7', 'Mechanics of Deformable Bodies (strength of materi', 'Mechanics of Deformable Bodies (strength of materi', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(282, 'ES 8', 'Thermodynamics', 'Thermodynamics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(283, 'ES 9', 'Environmental and Safety Engineering', 'Environmental and Safety Engineering', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(284, 'FdNn 1', 'Principles of Food Prep. & Selection', 'Principles of Food Prep. & Selection', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(285, 'FdNn 12', 'Meal Management', 'Meal Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(286, 'FdNn 21', 'Hygiene, Sanitation & Environmental Concerns', 'Hygiene, Sanitation & Environmental Concerns', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(287, 'Fil 1a', 'Sining ng Pakikipagtalastasan', 'Sining ng Pakikipagtalastasan', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(288, 'Fil 101A', 'Komunikasyon sa Akademikong Filipino', 'Komunikasyon sa Akademikong Filipino', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(289, 'Fil 1c', 'Sining ng Komunikasyon', 'Sining ng Komunikasyon', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(290, 'Fil 2a', 'Panitikang Filipino', 'Panitikang Filipino', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(291, 'Fil 3', 'Pagbasa at Pagsulat', 'Pagbasa at Pagsulat', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(292, 'Fil 4', 'Mabisang Pananalita', 'Mabisang Pananalita', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(293, 'Fil 5', 'Retorika', 'Retorika', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(294, 'Fil 6', 'Wika at Panitikan', 'Wika at Panitikan', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(295, 'Fil 7', 'Introduksyon sa Pamamahayag', 'Introduksyon sa Pamamahayag', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(296, 'Fin 1a', 'Basic Finance', 'Basic Finance', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(297, 'Fin 1b', 'Principles of Money, Credit, and Banking', 'Principles of Money, Credit, and Banking', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(298, 'FN 1', 'Food and Nutrition', 'Food and Nutrition', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(299, 'FN 2', 'Hygiene and Sanitation', 'Hygiene and Sanitation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(300, 'FOLA', 'Foreign Language 1', 'Foreign Language 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(301, 'Fola 1', 'German Language', 'German Language', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(302, 'FOLA 12', 'Foreign Language 2', 'Foreign Language 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(303, 'HC 1 R', 'RLE (Clinics & Community = 153 Hrs)', 'RLE (Clinics & Community = 153 Hrs)', 0, 0, 3, 0, 3, 12, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(304, 'HC 1 (lec)', 'Health Care 1', 'Health Care 1', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(305, 'HC 2 R', 'RLE (Clinics & Community = 102 Hrs)', 'RLE (Clinics & Community = 102 Hrs)', 0, 0, 2, 0, 2, 16, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(306, 'HC 2 (lec)', 'Health Care 2', 'Health Care 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(307, 'Hist 1', 'Philippine History', 'Philippine History', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(308, 'Hist 1a', 'Philippine History and Culture', 'Philippine History and Culture', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(309, 'Hist 2', 'Life and Works of Rizal', 'Life and Works of Rizal', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(310, 'Hist 2a', 'Life and Works of Rizal and Other Heroes', 'Life and Works of Rizal and Other Heroes', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(311, 'Hist 3', 'World History', 'World History', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(312, 'Hist 4', 'Asian Civilization', 'Asian Civilization', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(313, 'Hist 5', 'Life and Works of Filipino Heroes', 'Life and Works of Filipino Heroes', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(314, 'HoOp 11', 'Intro to Hotel Operations', 'Intro to Hotel Operations', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(315, 'Hort 1', 'Introduction to Horticulture', 'Introduction to Horticulture', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(316, 'Hum 1', 'Introduction to Humanities', 'Introduction to Humanities', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(317, 'Hum 1a', 'Art Appreciation', 'Art Appreciation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(318, 'IPM 1', 'Principles of Crop Protection', 'Principles of Crop Protection', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(319, 'IPM 2', 'Crop Protection 1 (Economic Entomology)', 'Crop Protection 1 (Economic Entomology)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(320, 'IPM 3', 'Crop Protection 2 (Principles of Plant Pathology)', 'Crop Protection 2 (Principles of Plant Pathology)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(321, 'IT 101', 'IT Fundamentals', 'IT Fundamentals', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(322, 'IT 102', 'PC Operations 1', 'PC Operations 1', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(323, 'IT 2', 'Computer and Society', 'Computer and Society', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(324, 'IT 201', 'PC Operations 2', 'PC Operations 2', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(325, 'IT 205', 'Computer Hardware Servicing', 'Computer Hardware Servicing', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(326, 'IT 301', 'Database Management Systems 1', 'Database Management Systems 1', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(327, 'IT 303', 'Network Management', 'Network Management', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(328, 'IT 304', 'Web Development', 'Web Development', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(329, 'IT 305', 'Systems Analysis and Design', 'Systems Analysis and Design', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(330, 'IT 306', 'Database Management Systems 2', 'Database Management Systems 2', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(331, 'IT 401', 'Internship', 'Internship', 0, 0, 9, 0, 9, 27, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(332, 'IT 402', 'IT Project', 'IT Project', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(333, 'IT 403', 'Multimedia Systems', 'Multimedia Systems', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(334, 'LAW 1', 'Law on Obligation and Contract', 'Law on Obligation and Contract', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(335, 'LAW 2', 'Law on Sales Agency and Bailments', 'Law on Sales Agency and Bailments', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(336, 'LAW 3', 'Law on Partnership and Corporation', 'Law on Partnership and Corporation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(337, 'LAW 4', 'Law on Negotiable Instrument', 'Law on Negotiable Instrument', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL);
INSERT INTO `tblsubject` (`SubjectID`, `SubjectCode`, `SubjectTitle`, `Description`, `InActive`, `Units`, `CreditUnits`, `LecHrs`, `LabUnits`, `LabHrs`, `SubjectMode`, `IsNonAcademic`, `CreatedBy`, `CreationDate`, `ModifiedBy`, `ModifiedDate`, `LevelID`) VALUES
(338, 'Math 0', 'Remedial Math', 'Remedial Math', 0, 0, 0, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(339, 'Math 10', 'Discrete Mathematics', 'Discrete Mathematics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(340, 'Math 11', 'Probability and Statistics', 'Probability and Statistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(341, 'Math 12', 'Business Mathematics', 'Business Mathematics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(342, 'Math 13', 'Mathematics of Investment', 'Mathematics of Investment', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(343, 'Math 14', 'Advanced Engineering Mathematics', 'Advanced Engineering Mathematics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(344, 'Math 15', 'Numerical Analysis and Design', 'Numerical Analysis and Design', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(345, 'Math 15a', 'Numerical Analysis and Design', 'Numerical Analysis and Design', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(346, 'Math 1a', 'College Algebra', 'College Algebra', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(347, 'Math 1b', 'College Algebra', 'College Algebra', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(348, 'Math 1c', 'Advanced College Algebra', 'Advanced College Algebra', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(349, 'Math 102B', 'Contemporary Mathematics', 'Contemporary Mathematics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(350, 'Math 2', 'Plane and Solid Mensuration', 'Plane and Solid Mensuration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(351, 'Math 3', 'Trigonometry', 'Trigonometry', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(352, 'Math 3a', 'Plane Trigonometry', 'Plane Trigonometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(353, 'Math 3b', 'Plane and Spherical Trigonometry', 'Plane and Spherical Trigonometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(354, 'Math 4', 'Differential Calculus', 'Differential Calculus', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(355, 'Math 4a', 'Calculus', 'Calculus', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(356, 'Math 4b', 'Differential Calculus', 'Differential Calculus', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(357, 'Math 5', 'Integral Calculus', 'Integral Calculus', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(358, 'Math 5a', 'Integral Calculus', 'Integral Calculus', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(359, 'Math 6', 'Geometry', 'Geometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(360, 'Math 6a', 'Analytic Geometry', 'Analytic Geometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(361, 'Math 6b', 'Analytic Geometry and Solid Mensuration', 'Analytic Geometry and Solid Mensuration', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(362, 'Math 7', 'Differential Equations', 'Differential Equations', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(363, 'Math 8', 'Linear Algebra', 'Linear Algebra', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(364, 'Math 9', 'Vector Analysis', 'Vector Analysis', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(365, 'MGT 10', 'Industrial Management', 'Industrial Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(366, 'MGT 11', 'Executive Administration', 'Executive Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(367, 'MGT 12', 'Planning and Policy Formulation', 'Planning and Policy Formulation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(368, 'MGT 14', 'Practicum (200 hours)', 'Practicum (200 hours)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(369, 'MGT 14a', 'Human Behavior in Organization', 'Human Behavior in Organization', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(370, 'MGT 1a', 'Business Organization and Management', 'Business Organization and Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(371, 'Mgt 1b', 'Human Resource Management', 'Human Resource Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(372, 'Mgt 1c', 'Shop Layout & Management', 'Shop Layout & Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(373, 'MGT 2a', 'Personnel Management', 'Personnel Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(374, 'Mgt 2b', 'Transportation Management', 'Transportation Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(375, 'Mgt 3a', 'Hotel and Resort Management', 'Hotel and Resort Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(376, 'MGT 3b', 'Public Administration', 'Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(377, 'Mgt 4a', 'MICE Management', 'MICE Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(378, 'MGT 4b', 'Office Management and Automation', 'Office Management and Automation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(379, 'MGT 5', 'Wages and Salary Administration', 'Wages and Salary Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(380, 'MGT 6', 'Labor and Management Relations', 'Labor and Management Relations', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(381, 'MGT 7', 'Material Management', 'Material Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(382, 'MGT 8', 'Ethics and Public Relations', 'Ethics and Public Relations', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(383, 'MGT 9', 'Supervisory Techniques in Business', 'Supervisory Techniques in Business', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(384, 'MKTG 1', 'Basic Marketing', 'Basic Marketing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(385, 'NatSci 1a', 'Natural Science', 'Natural Science', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(386, 'NatSci 2', 'Earth Science', 'Earth Science', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(387, 'NatSci 2a', 'Earth Science w/ Field Study', 'Earth Science w/ Field Study', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(388, 'NCM 100', 'Foundations of Nursing', 'Foundations of Nursing', 0, 2, 2, 0, 0, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(389, 'NCM 101 RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(390, 'NCM 101(lec)', 'Promotive & Preventive Nursing Care Mgt.', 'Promotive & Preventive Nursing Care Mgt.', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(391, 'NCM 102 RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 6, 0, 6, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(392, 'NCM 103', 'RLE (Psyche Affiliation/Clncs/Hsptl & Comm=204Hrs)', 'RLE (Psyche Affiliation/Clncs/Hsptl & Comm=204Hrs)', 0, 0, 4, 0, 4, 12, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(393, 'NCM 104 RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(394, 'NCM 105 RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(395, 'NCM 105(lec)', 'Nursing Mgt. & Leadership', 'Nursing Mgt. & Leadership', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(396, 'NCM102(lec)', 'Curative & Rehabilitative Nursing Care Mgt', 'Curative & Rehabilitative Nursing Care Mgt', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(397, 'NCM104(lec)', 'Curative & Rehabilitative NCM II', 'Curative & Rehabilitative NCM II', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(398, 'NRM 1', 'Natural Resources Management', 'Natural Resources Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(399, 'NRM 2', 'Environmental Restoration and Rehabilitation', 'Environmental Restoration and Rehabilitation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(400, 'Nrsg 1', 'Community Health Development', 'Community Health Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(401, 'NSTP 1a', 'Reserve Officer Training Corps 1', 'Reserve Officer Training Corps 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(402, 'NSTP 2a', 'Reserve Officer Training Corps 2', 'Reserve Officer Training Corps 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(403, 'Nut', 'Basic Nutrition', 'Basic Nutrition', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(404, 'Tech 4', 'On-the-Job Training  (720 Hrs)', 'On-the-Job Training  (720 Hrs)', 0, 12, 12, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(405, 'OJT 1a', 'OJT (Practicum of 240 hours and Narrative report)', 'OJT (Practicum of 240 hours and Narrative report)', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(406, 'OJT 2', 'On-the-Job Training (270 Hrs)', 'On-the-Job Training (270 Hrs)', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(407, 'PA 10', 'Introduction to Public Administration', 'Introduction to Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(408, 'PA 11', 'Ecology of Public Administration', 'Ecology of Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(409, 'PA 12', 'Public Administration Thoughts and Institutions', 'Public Administration Thoughts and Institutions', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(410, 'PA 13', 'Local Government System', 'Local Government System', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(411, 'PA 14', 'Public Finance', 'Public Finance', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(412, 'PA 15', 'Development Planning', 'Development Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(413, 'PA 16', 'Public Policy and Program Administration', 'Public Policy and Program Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(414, 'PA 17', 'Filipino Behavior in Administration', 'Filipino Behavior in Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(415, 'PA 18', 'Ethics on Public Administration', 'Ethics on Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(416, 'PA 19', 'Introduction to Project Development', 'Introduction to Project Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(417, 'PA 20', 'Organization and Management', 'Organization and Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(418, 'PA 22', 'Personnel Administration', 'Personnel Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(419, 'PE 1', 'Gymnastics', 'Gymnastics', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(420, 'PE 2', 'Dance Fundamentals', 'Dance Fundamentals', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(421, 'PE 2a', 'Rhythmic Activities', 'Rhythmic Activities', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(422, 'PE 3', 'Individual and Dual Sports', 'Individual and Dual Sports', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(423, 'PE 4', 'Team Sports', 'Team Sports', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(424, 'Phar 1', 'Pharmacology', 'Pharmacology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(425, 'Philo 1', 'Introduction to Philosophy', 'Introduction to Philosophy', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(426, 'Philo 1a', 'Philosophy of Man', 'Philosophy of Man', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(427, 'Philo 2', 'Logic', 'Logic', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(428, 'Philo 2a', 'Logic and Ethics', 'Logic and Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(429, 'Philo 2b', 'Logic and Critical Thinking', 'Logic and Critical Thinking', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(430, 'Philo 3', 'Ethics', 'Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(431, 'Philo 4', 'Health Ethics', 'Health Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(432, 'Phys 1', 'Physics 1', 'Physics 1', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(433, 'Phys 1a', 'Mechanics and Heat', 'Mechanics and Heat', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(434, 'Phys 1b', 'Mechanics and Heat', 'Mechanics and Heat', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(435, 'Phys 1c', 'Mechanics and Heat', 'Mechanics and Heat', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(436, 'Phys 1d', 'Mechanics, Heat and Thermodynamics', 'Mechanics, Heat and Thermodynamics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(437, 'Phys 1e', 'Mechanics, Heat and Thermodynamics', 'Mechanics, Heat and Thermodynamics', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(438, 'Phys 2a', 'Electricity and Magnetism', 'Electricity and Magnetism', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(439, 'Phys 2b', 'Electricity and Magnetism', 'Electricity and Magnetism', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(440, 'Phys 2c', 'Electricity and Magnetism', 'Electricity and Magnetism', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(441, 'Phys 2d', 'Electricity, Magnetism, and Optics', 'Electricity, Magnetism, and Optics', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(442, 'Phys 2e', 'Optics and Sound', 'Optics and Sound', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(443, 'PolSci 1', 'Philippine Government and Constitution', 'Philippine Government and Constitution', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(444, 'Psycho 1', 'General Psychology', 'General Psychology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(445, 'Psycho 2', 'Educational Psychology', 'Educational Psychology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(446, 'Psycho 3', 'Industrial Psychology', 'Industrial Psychology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(447, 'Res 1', 'Research Methods', 'Research Methods', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(448, 'Res 1a', 'Introduction to Educational Research', 'Introduction to Educational Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(449, 'Res 1b', 'Introduction to Nursing Research', 'Introduction to Nursing Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(450, 'Res 2', 'Thesis Writing', 'Thesis Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(451, 'Res 2a', 'Thesis Writing', 'Thesis Writing', 0, 1, 4, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(452, 'Res 2b', 'Undergraduate Thesis', 'Undergraduate Thesis', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(453, 'Socio 1a', 'Sociology/Anthropology', 'Sociology/Anthropology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(454, 'Socio 1b', 'Sociology with Family Planning', 'Sociology with Family Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(455, 'Socio 1c', 'Family Planning and Pop. Ed.', 'Family Planning and Pop. Ed.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(456, 'Socio 2', 'Contemporary Issues', 'Contemporary Issues', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(457, 'Socio 3', 'Applied Sociology', 'Applied Sociology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(458, 'Soils 1', 'Principles of Soil Science', 'Principles of Soil Science', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(459, 'SpS 1', 'Livelihood & Entrepreneurship', 'Livelihood & Entrepreneurship', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(460, 'SpT 2', 'Integrative Teaching Approaches', 'Integrative Teaching Approaches', 0, 1, 1, 0, 0, 1, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(461, 'SpT 3', 'Guidance Counseling', 'Guidance Counseling', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(462, 'Stat 1', 'Elementary Statistics', 'Elementary Statistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(463, 'Stat 1a', 'Basic Statistics', 'Basic Statistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(464, 'Stat 1c', 'Statistics for Physical Science', 'Statistics for Physical Science', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(465, 'Stat 2', 'Ecostatistics', 'Ecostatistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(466, 'Stat 3', 'Statistics in Agricultural Education', 'Statistics in Agricultural Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(467, 'Stat 4', 'Experimental Statistics', 'Experimental Statistics', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(468, 'Stat 5', 'Educational Statistics', 'Educational Statistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(469, 'STS 1', 'Science, Technology and Society', 'Science, Technology and Society', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(470, 'TAX 1', 'Income Taxation', 'Income Taxation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(471, 'TAX 2', 'Business and Transfer Tax', 'Business and Transfer Tax', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(472, 'TE 1', 'Technical Elective 1 (Intro to Photoshop)', 'Technical Elective 1 (Intro to Photoshop)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(473, 'TE 1a', 'ECE Elective 1 (Wireless Communication)', 'ECE Elective 1 (Wireless Communication)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(474, 'TE 1b', 'EE Elective 1 (Tarck)', 'EE Elective 1 (Tarck)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(475, 'TE 2', 'Technical Elective 2 (Database Management)', 'Technical Elective 2 (Database Management)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(476, 'TE 2a', 'ECE Elective 2 (Power Supply Application)', 'ECE Elective 2 (Power Supply Application)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(477, 'TE 2b', 'EE Elective 2 (Track)', 'EE Elective 2 (Track)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(478, 'TE 3', 'Technical Elect 3 (Practical Business Application)', 'Technical Elect 3 (Practical Business Application)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(479, 'TE 3a', 'ECE Elective 3 (Broadcast Engineering)', 'ECE Elective 3 (Broadcast Engineering)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(480, 'TE 3b', 'EE Elective 3 (Track)', 'EE Elective 3 (Track)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(481, 'TE 4', 'Technical Elective 4 (Dynamic Web Application)', 'Technical Elective 4 (Dynamic Web Application)', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(482, 'TE 411a', 'Digital Electronics/Computer Hardware', 'Digital Electronics/Computer Hardware', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(483, 'TE 411b', 'Industrial Wiring Installation', 'Industrial Wiring Installation', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(484, 'TE 421a', 'Intro to Optics Communications', 'Intro to Optics Communications', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(485, 'TE 421b', 'Motor Repair & Rewinding', 'Motor Repair & Rewinding', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(486, 'TE 4a', 'ECE Elect 4 (Elect''x Power, Supply Design & App)', 'ECE Elect 4 (Elect''x Power, Supply Design & App)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(487, 'TE 4b', 'EE Elective 4 (Track)', 'EE Elective 4 (Track)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(488, 'TE 511a', 'Electrical Machine Design', 'Electrical Machine Design', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(489, 'TE 511b', 'Microwave/Satellite Communications', 'Microwave/Satellite Communications', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(490, 'TE 521a', 'Navigational Aids/Industrial Electronics Control', 'Navigational Aids/Industrial Electronics Control', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(491, 'TE 521b', 'Electrical Plant Design', 'Electrical Plant Design', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(492, 'TE 5a', 'ECE Elective 5 (Navigational Aids)', 'ECE Elective 5 (Navigational Aids)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(493, 'TE 6a', 'ECE Elective 6 (Computer System Architecture)', 'ECE Elective 6 (Computer System Architecture)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(494, 'Tech 1a', 'Automotive Fundamentals', 'Automotive Fundamentals', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(495, 'Tech 1b', 'Basic Electricity', 'Basic Electricity', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(496, 'Tech 1c', 'Basic Electronics & Test Instruments', 'Basic Electronics & Test Instruments', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(497, 'Tech 1d', 'Rough Carpentry', 'Rough Carpentry', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(498, 'Tech 1e', 'Architectural Design I', 'Architectural Design I', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(499, 'Tech 1f', 'Basic Garment', 'Basic Garment', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(500, 'Tech 2a', 'Electrical Installation', 'Electrical Installation', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(501, 'Tech 2b', 'Electronic Circuits & Applications', 'Electronic Circuits & Applications', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(502, 'Tech 2c', 'Engine System Acces. & Auto Electâ€™l System', 'Engine System Acces. & Auto Electâ€™l System', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(503, 'Tech 2d', 'Selectâ€™n Purchase & Care of Clothing', 'Selectâ€™n Purchase & Care of Clothing', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(504, 'Tech 2e', 'Architectural Design II', 'Architectural Design II', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(505, 'Tech 2f', 'Concrete Work', 'Concrete Work', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(506, 'Tech 3a', 'Carpentry & Masonry Finishes', 'Carpentry & Masonry Finishes', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(507, 'Tech 3b', 'Electromechanical Installation', 'Electromechanical Installation', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(508, 'Tech 3c', 'Engine Servicing & Tune-up', 'Engine Servicing & Tune-up', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(509, 'Tech 3d', 'Pattern Designing & Construction', 'Pattern Designing & Construction', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(510, 'Tech 3e', 'TV Basics & Troubleshooting', 'TV Basics & Troubleshooting', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(511, 'Tech 5a', 'Auto Body Bldg. & Repair', 'Auto Body Bldg. & Repair', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(512, 'Tech 5b', 'Dress Designing & Construction', 'Dress Designing & Construction', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(513, 'Tech 5c', 'Industrial Control', 'Industrial Control', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(514, 'Tech 5d', 'Plumbing & House Wiring', 'Plumbing & House Wiring', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(515, 'Tech 5e', 'Video Playback & Recorders', 'Video Playback & Recorders', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(516, 'Tech 6a', 'Fuel System & Injector Calibration', 'Fuel System & Injector Calibration', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(517, 'Tech 6c', 'Tailoring', 'Tailoring', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(518, 'Tech 6d', 'Transmission & Secondary Distribution System', 'Transmission & Secondary Distribution System', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(519, 'Tech 6e', 'Wire & Wireless Communication', 'Wire & Wireless Communication', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(520, 'Tech 6f', 'Specification & Estimates', 'Specification & Estimates', 0, 1, 2, 0, 1, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(521, 'Tech 6g', 'Highway & Pavements', 'Highway & Pavements', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(522, 'Tech 7a', 'Auto Air Conditioning', 'Auto Air Conditioning', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(523, 'Tech 7b', 'Digital Circuits', 'Digital Circuits', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(524, 'Tech 7c', 'Instrument Process & Control', 'Instrument Process & Control', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(525, 'Tech 7d', 'Mass Production Techniques', 'Mass Production Techniques', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(526, 'Tech 7e', 'Plane Surveying & Soil Mechanics', 'Plane Surveying & Soil Mechanics', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(527, 'THE I', 'Home Management Tech.', 'Home Management Tech.', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(528, 'Tour 1', 'Introduction to Ecotourism', 'Introduction to Ecotourism', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(529, 'Tour 10', 'Special Projects (Organize, coordinate and or  gui', 'Special Projects (Organize, coordinate and or  gui', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(530, 'Tour 11', 'Planning of Tourism I', 'Planning of Tourism I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(531, 'Tour 12', 'Principles of Tourism 2', 'Principles of Tourism 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(532, 'Tour 2', 'Tourism and Environmental Laws', 'Tourism and Environmental Laws', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(533, 'Tour 21', 'Tourism Planning & Devâ€™t.', 'Tourism Planning & Devâ€™t.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(534, 'Tour 22', 'Introduction to Travel & Tour', 'Introduction to Travel & Tour', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(535, 'Tour 3', 'Principles of Tourism and Tour Guiding', 'Principles of Tourism and Tour Guiding', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(536, 'Tour 31', 'Tour Guiding', 'Tour Guiding', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(537, 'Tour 32', 'Tour Guiding Practicum', 'Tour Guiding Practicum', 0, 0, 3, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(538, 'Tour 33', 'Ecotourism', 'Ecotourism', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(539, 'Tour 4', 'Domestic Tourism in Mindanao', 'Domestic Tourism in Mindanao', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(540, 'Tour 41', 'Mgt Tourism Establishment', 'Mgt Tourism Establishment', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(541, 'Tour 42', 'Trends & Issues of Tourism Mgt.', 'Trends & Issues of Tourism Mgt.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(542, 'Tour 43', 'Travel & Tour Agency Mgt.', 'Travel & Tour Agency Mgt.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(543, 'Tour 44', 'Field Trips & Seminar Training', 'Field Trips & Seminar Training', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(544, 'Tour 5', 'Travel and Tour Operations In Luzon', 'Travel and Tour Operations In Luzon', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(545, 'Tour 6', 'Travel Agency Management In Visayas', 'Travel Agency Management In Visayas', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(546, 'Tour 7', 'Tour Promotions and Marketing', 'Tour Promotions and Marketing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(547, 'Tour 8', 'Tourism Planning Micro-Project Development', 'Tourism Planning Micro-Project Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(548, 'Tour 9', 'International Tourism and Operations', 'International Tourism and Operations', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(549, 'Tour Elect', 'Local Language', 'Local Language', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(550, 'WE 1', 'Social & Moral Values', 'Social & Moral Values', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(551, 'Zool 1', 'Zoology', 'Zoology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(552, 'Zool 2', 'Embryology', 'Embryology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(553, 'Zool 3', 'Invertebrate Zoology', 'Invertebrate Zoology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(554, 'Zool 4', 'Comparative Anatomy & Physiology', 'Comparative Anatomy & Physiology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(555, 'Zool 5', 'Ichthyology', 'Ichthyology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(556, 'NSTP 1b', 'Civic Welfare Training Service 1', 'Civic Welfare Training Service 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(557, 'NSTP 2b', 'Civic Welfare Training Service 2', 'Civic Welfare Training Service 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(558, 'ROTC 31', 'Military Science & Tactics', 'Military Science & Tactics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(559, 'ROTC 32', 'Military Science & Tactics', 'Military Science & Tactics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(560, 'ROTC 41', 'Military Science & Tactics', 'Military Science & Tactics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(561, 'ROTC 42', 'Military Science & Tactics', 'Military Science & Tactics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(562, 'NCM 100 RLE', 'RLE (Clinics & Community = 51 Hrs)', 'RLE (Clinics & Community = 51 Hrs)', 0, 0, 1, 0, 1, 16, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(563, 'Stat 1b', 'Basic Statistics (w/ Biostatistics)', 'Basic Statistics (w/ Biostatistics)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(565, 'Educ 16b', 'Strategies of Health Education', 'Strategies of Health Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(566, 'NatSci 1', 'Natural Science', 'Natural Science', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(567, 'OJT 1b', 'On-the-Job Training (240 Hrs)', 'On-the-Job Training (240 Hrs)', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(568, 'PE 3a', 'Swimming & Skin Diving', 'Swimming & Skin Diving', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(569, 'CoRe_c', 'Guided Review', 'Guided Review', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(570, 'ECE 522', 'Data Communications', 'Data Communications', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(571, 'Hist 1b', 'Philippine History & Constitution', 'Philippine History & Constitution', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(572, 'Engl 3', 'Technical Writing', 'Technical Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(573, 'ECE 414', 'Principles of Communications', 'Principles of Communications', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(574, 'Mgt 3c', 'Production Management & Quality Control', 'Production Management & Quality Control', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(576, 'ECE 514', 'Digital Communication', 'Digital Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(577, 'ECE 525', 'ECE Laws, Contracts & Ethics', 'ECE Laws, Contracts & Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(578, 'Res 1c', 'Research Methodology & Proposal Making', 'Research Methodology & Proposal Making', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(579, 'Tech 8', 'On-the-Job Training (720 Hrs)', 'On-the-Job Training (720 Hrs)', 0, 12, 12, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(580, 'Ext 1', 'Community Integration & Immersion', 'Community Integration & Immersion', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(581, 'Bio 1b', 'General Biology w/ Cell Bio', 'General Biology w/ Cell Bio', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(582, 'Bio 6a', 'Intro to Marine Biology', 'Intro to Marine Biology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(583, 'Bio 8', 'General Taxonomy', 'General Taxonomy', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(584, 'Stat 6', 'Biostatistics', 'Biostatistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(585, 'Bio 9', 'Marine Plankton', 'Marine Plankton', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(586, 'Bio 7c', 'Ecology', 'Ecology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(587, 'Bio 3b', 'Microbiology', 'Microbiology', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(588, 'Bio 1c', 'General Biology w/ Cell Bio', 'General Biology w/ Cell Bio', 0, 3, 3, 0, 0, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(589, 'Educ 200', 'Methods of Research', 'Methods of Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(590, 'Educ 201', 'Philosophy of Education', 'Philosophy of Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(591, 'Educ 202', 'Statistics & Measurement in Education', 'Statistics & Measurement in Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(592, 'Educ 203', 'Curriculum Development & Implementation', 'Curriculum Development & Implementation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(593, 'Educ 204', 'Educational Management & Introduction', 'Educational Management & Introduction', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(594, 'Educ 205', 'Dynamics of Educational Leadership', 'Dynamics of Educational Leadership', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(595, 'Educ 206', 'Personnel Management', 'Personnel Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(596, 'Educ 207', 'Legal Bases of Education & Current School Legislat', 'Legal Bases of Education & Current School Legislat', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(597, 'Educ 208', 'Educational Administration & Supervision', 'Educational Administration & Supervision', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(598, 'Educ 209', 'Financial Management of Phil. Educational Institut', 'Financial Management of Phil. Educational Institut', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(599, 'Educ 210', 'Teacher Education & Professional Development', 'Teacher Education & Professional Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(600, 'Educ 211', 'Supervision of Instruction and the Analysis of Tea', 'Supervision of Instruction and the Analysis of Tea', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(601, 'Educ 212', 'Comprehensive Overview of Educational Management', 'Comprehensive Overview of Educational Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(602, 'Educ 213', 'Historical Foundation of Education', 'Historical Foundation of Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(603, 'Educ 214', 'Human Relations in Education', 'Human Relations in Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(604, 'Educ 215', 'Technical Writing in Education', 'Technical Writing in Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(605, 'Educ 299', 'Seminar in Thesis Writing', 'Seminar in Thesis Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(606, 'Educ 300', 'Thesis Writing', 'Thesis Writing', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(612, 'Bio 1d', 'General Biology', 'General Biology', 0, 3, 5, 0, 2, 5, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(613, 'SpT 1a', 'Peace Education', 'Peace Education', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(614, 'CropSci 30', 'Cereal Production & Management', 'Cereal Production & Management', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(615, 'CropSci 40', 'Weeds & Their Control', 'Weeds & Their Control', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(616, 'CropSci 50', 'Legumes & Rootcrops Production', 'Legumes & Rootcrops Production', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(617, 'CropSci 60', 'Pomology', 'Pomology', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(618, 'CropSci 70', 'Vegetable Production & Management', 'Vegetable Production & Management', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(619, 'CropSci 80', 'Cropping System', 'Cropping System', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(620, 'CropSci 100', 'Thesis/Pract/Project Study', 'Thesis/Pract/Project Study', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(622, 'CS 203a', 'Programming Languages', 'Programming Languages', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(623, 'Envi 11a', 'Basic Resources', 'Basic Resources', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(625, 'FdNn 21a', 'Concerns', 'Concerns', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(626, 'Comp 4a', 'Software Applications', 'Software Applications', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(629, 'Engl 1(Sec)', 'English I', 'English I', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(630, 'Fil 1(Sec)', 'Filipino I', 'Filipino I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(631, 'Math 1(Sec)', 'Mathematics I', 'Mathematics I', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(632, 'Sci 1(Sec)', 'Science & Technology I', 'Science & Technology I', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(633, 'Aral Pan 1(Sec)', 'Araling Panlipunan I', 'Araling Panlipunan I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(634, 'THE 1(Sec)', 'THE I', 'THE I', 0, 2, 2, 0, 0, 0, NULL, 0, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(635, 'Values 1(Sec)', 'Edukasyon sa Pagpapahalaga (Values Education) I', 'Edukasyon sa Pagpapahalaga (Values Education) I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(636, 'PEHM 1(Sec)', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(637, 'Elect Math1', 'Elective Math I', 'Elective Math I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(639, 'Comp 1(Sec)', 'Computer I', 'Computer I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(640, 'RHGP(Sec)', 'RHGP I', 'RHGP I', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(641, 'Engl 11(Sec)', 'English II', 'English II', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(642, 'Fil 11(Sec)', 'Filipino II', 'Filipino II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(643, 'Math 11(Sec)', 'Mathematics II', 'Mathematics II', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(644, 'Sci 11(Sec)', 'Science & Technology II', 'Science & Technology II', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(645, 'Aral Pan 11(Sec', 'Araling Panlipunan II', 'Araling Panlipunan II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(646, 'THE 11(Sec)', 'THE II', 'THE II', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(647, 'Values 11(Sec)', 'Edukasyon sa Pagpapahalaga(Values Education) II', 'Edukasyon sa Pagpapahalaga(Values Education) II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(648, 'PEHM 11(Sec)', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(649, 'Elect Math11', 'Elective Math II', 'Elective Math II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(650, 'Comp 11(Sec)', 'Computer II', 'Computer II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(651, 'RHGP 11(Sec)', 'RHGP II', 'RHGP II', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(652, 'Engl 111(Sec)', 'English III', 'English III', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(653, 'Fil 111(Sec)', 'Filipino III', 'Filipino III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(654, 'Math 111(Sec)', 'Mathematics III', 'Mathematics III', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(655, 'Sci 111(Sec)', 'Science  & Technology III', 'Science  & Technology III', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(656, 'AralPan111(Sec)', 'Araling Panlipunan III', 'Araling Panlipunan III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(657, 'Values 111(Sec)', 'Edukasyon sa Pagpapahalaga (Values Education) III', 'Edukasyon sa Pagpapahalaga (Values Education) III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(658, 'THE 111(Sec)', 'THE III', 'THE III', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(659, 'PEHM 111(Sec)', 'Edukasyong Pangkalusugan, Pangkatawan at Musika(PE', 'Edukasyong Pangkalusugan, Pangkatawan at Musika(PE', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(660, 'ElectMath 111', 'Elective Math III', 'Elective Math III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(661, 'Comp 111(Sec)', 'Computer III', 'Computer III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(662, 'RHGP 111(Sec)', 'RHGP III', 'RHGP III', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(663, 'Engl 4(Sec)', 'English IV', 'English IV', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(664, 'Math 4(Sec)', 'Mathematics IV', 'Mathematics IV', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(665, 'Sci 4(Sec)', 'Science & Technology IV', 'Science & Technology IV', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(666, 'Fil 4(Sec)', 'Filiino IV', 'Filiino IV', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(667, 'AralPan 4(Sec)', 'Araling Panlipunan IV', 'Araling Panlipunan IV', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(668, 'THE 4(Sec)', 'THE IV', 'THE IV', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(669, 'Values 4(Sec)', 'Edukasyon sa Pagpapahalaga (Values Edukasyon) IV', 'Edukasyon sa Pagpapahalaga (Values Edukasyon) IV', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(670, 'ElectMath4(Sec)', 'Elective Math IV', 'Elective Math IV', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(671, 'Comp 4 (Sec)', 'Computer IV', 'Computer IV', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(672, 'RHGP 4(Sec)', 'RHGP IV', 'RHGP IV', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(673, 'PEHM 4(Sec)', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 'Edukasyong Pangkalusugan, Pangkatawan at Musika (P', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(674, 'PA 200', 'Statistical Methods', 'Statistical Methods', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(675, 'PA 201', 'Methods of Research in Public Administration', 'Methods of Research in Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(676, 'PA 202', 'Theory & Practice in Public Administration', 'Theory & Practice in Public Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(677, 'PA 203', 'Quantitative Methods in Decision Making', 'Quantitative Methods in Decision Making', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(678, 'PA 204', 'Management Communication', 'Management Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(679, 'PA 205', 'Government Planning, Budgeting & Control', 'Government Planning, Budgeting & Control', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL);
INSERT INTO `tblsubject` (`SubjectID`, `SubjectCode`, `SubjectTitle`, `Description`, `InActive`, `Units`, `CreditUnits`, `LecHrs`, `LabUnits`, `LabHrs`, `SubjectMode`, `IsNonAcademic`, `CreatedBy`, `CreationDate`, `ModifiedBy`, `ModifiedDate`, `LevelID`) VALUES
(680, 'PA 206', 'Applied Economics', 'Applied Economics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(681, 'PA 207', 'Human Resource Management', 'Human Resource Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(682, 'PA 208', 'Human Behavior in Organization', 'Human Behavior in Organization', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(683, 'PA 209', 'Fiscal Administration', 'Fiscal Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(684, 'PA 210', 'Policy Studies', 'Policy Studies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(685, 'PA 211', 'Seminar on Local Government Administration', 'Seminar on Local Government Administration', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(686, 'PA 212', 'Seminar on Government Services', 'Seminar on Government Services', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(687, 'PA 213', 'Seminar on Productivity & Total Quality Management', 'Seminar on Productivity & Total Quality Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(688, 'PA 299', 'Seminar in Thesis Writing', 'Seminar in Thesis Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(689, 'PA 300', 'Thesis Writing', 'Thesis Writing', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(696, 'IT 2a', 'Computer and Society II', 'Computer and Society II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(699, 'Bio 1e', 'General Biology', 'General Biology', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(701, 'Chem 1e', 'General Chemistry', 'General Chemistry', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(702, 'Phys 1f', 'Mechanics & Heat', 'Mechanics & Heat', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(703, 'Comp 3b', 'Computer Fundamentals', 'Computer Fundamentals', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(704, 'Bio 5a', 'Genetics', 'Genetics', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(705, 'Chem 3b', 'Organic Chemistry', 'Organic Chemistry', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(707, 'AM 2', 'Entrep and Cooperatives', 'Entrep and Cooperatives', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(708, 'AnSci 90', 'Undergraduate Seminar', 'Undergraduate Seminar', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(709, 'CropSci 10', 'Seed Technology', 'Seed Technology', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(710, 'CropSci 20', 'Post Harvest Technology', 'Post Harvest Technology', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(711, 'CropSci 90', 'Undergraduate Seminar', 'Undergraduate Seminar', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(712, 'Bio 1f', 'Biological Science', 'Biological Science', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(714, 'Math 101A', 'Fundamentals of Math', 'Fundamentals of Math', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(715, 'Educ 23', 'Lesson Planning & Educational Tech', 'Lesson Planning & Educational Tech', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(716, 'Educ 15a', 'Educational Technology II', 'Educational Technology II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(717, 'AgEd 80', 'Indust''l, Pract & Fishery + Field Study', 'Indust''l, Pract & Fishery + Field Study', 0, 2, 4, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(719, 'AgEd 80a', 'Earth Science & Envt''l Mgt w/ SA + FS', 'Earth Science & Envt''l Mgt w/ SA + FS', 0, 2, 4, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(722, 'AgEd 90', 'Undergraduate Seminar', 'Undergraduate Seminar', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(724, 'Res 2c', 'Thesis Writing', 'Thesis Writing', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(725, 'TFN 1', 'Theoretical Foundations in Nursing', 'Theoretical Foundations in Nursing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(726, 'NCM 100a', 'Fundamentals of Nursing Practice', 'Fundamentals of Nursing Practice', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(727, 'NCM 100a RLE', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 102 Hrs)', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 102 Hrs)', 0, 0, 2, 0, 2, 16, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(728, 'NCM 101a (lec)', 'Care of Individuals & Family w/ Mat & Child Health', 'Care of Individuals & Family w/ Mat & Child Health', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(732, 'NCM 101a RLE', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 0, 0, 6, 0, 6, 16, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(733, 'HA 1', 'Health Assessment', 'Health Assessment', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(734, 'HA 1R', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 51 Hrs)', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 51 Hrs)', 0, 0, 1, 0, 1, 8.5, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(735, 'CHN 1', 'Community Health Nursing', 'Community Health Nursing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(736, 'CHN 1R', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 102 Hrs)', 'RLE (Nrsg Arts Lab/Clinics/Hosp & Com = 102 Hrs)', 0, 0, 2, 0, 2, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(737, 'Educ 16c', 'Teaching Strategies in Health Education', 'Teaching Strategies in Health Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(738, 'Bioethics', 'Bioethics', 'Bioethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(740, 'NuDiet', 'Nutrition & Diet Therapy', 'Nutrition & Diet Therapy', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(741, 'NCM 102a', 'Care of Clients Across the Lifespan w/ Mother ', 'Care of Clients Across the Lifespan w/ Mother ', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(743, 'NCM 102a RLE', 'RLE (Clinics/Hosp & Com = 306 Hrs)', 'RLE (Clinics/Hosp & Com = 306 Hrs)', 0, 0, 6, 0, 6, 16, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(744, 'NI', 'Nursing Informatics', 'Nursing Informatics', 0, 2, 3, 0, 1, 54, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(745, 'NCM 103a', 'Across the Lifespan w/ Probs in Oxygenation', 'Across the Lifespan w/ Probs in Oxygenation', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(747, 'NCM 103a RLE', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 0, 0, 6, 0, 6, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(748, 'Hum 2', 'World Civilization & Literature', 'World Civilization & Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(750, 'NCM 104a', 'Across the Lifespan w/ Probs in Perception', 'Across the Lifespan w/ Probs in Perception', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(751, 'NCM 104a RLE', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 'RLE (Clinics/Hospital & Community = 306 Hrs)', 0, 0, 6, 0, 6, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(752, 'NRes 1Lec', 'Nursing Research 1', 'Nursing Research 1', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(753, 'NCM 105', 'RLE (Clinics/Hospital & Community = 204 Hrs)', 'RLE (Clinics/Hospital & Community = 204 Hrs)', 0, 0, 4, 0, 4, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(754, 'NCM 106a', 'Across the Lifespan & Population Group', 'Across the Lifespan & Population Group', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(755, 'NCM 106a RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(756, 'NRes 2RLE', 'Nursing Research 2', 'Nursing Research 2', 0, 0, 2, 0, 2, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(757, 'CA 1', 'Competency Appraisal 1', 'Competency Appraisal 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(758, 'NCM 107', 'Nursing Leadership & Management', 'Nursing Leadership & Management', 0, 8, 8, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(759, 'NCM 107 RLE', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 'RLE (Clinics/Hospital & Community = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(760, 'CA 2', 'Competency Appraisal 2', 'Competency Appraisal 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(761, 'Phys 1g', 'Physics', 'Physics', 0, 2, 3, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(762, 'Rizal', 'Life, Works and Writings of Rizal', 'Life, Works and Writings of Rizal', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(763, 'Engl 1a', 'Communication Skills I', 'Communication Skills I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(764, 'Engl 2a', 'Communication Skills II', 'Communication Skills II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(765, 'OJT 3', 'On-the-Job Training (720 Hrs)', 'On-the-Job Training (720 Hrs)', 0, 12, 12, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(766, 'EE 9a', 'Control System', 'Control System', 0, 3, 4, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(767, 'TLE 1', 'Food Selection Preparation w/ Nutrition', 'Food Selection Preparation w/ Nutrition', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(768, 'TLE 2', 'Agriculture & Fishery Arts', 'Agriculture & Fishery Arts', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(769, 'TLE 3', 'Food Preservation & Baking', 'Food Preservation & Baking', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(770, 'TLE 4', 'Graphics Science & Drafting', 'Graphics Science & Drafting', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(771, 'TLE 5', 'Basic Automotive', 'Basic Automotive', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(772, 'TLE 6', 'Clothing Selection, Care & Accessories', 'Clothing Selection, Care & Accessories', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(773, 'TLE 7', 'Garment Pattern Making & Construction', 'Garment Pattern Making & Construction', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(774, 'TLE 8', 'Handicraft', 'Handicraft', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(776, 'TLE 9', 'Basic Carpentry', 'Basic Carpentry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(777, 'TLE 10', 'Basic Electricity', 'Basic Electricity', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(778, 'TLE 11', 'Basic Electronics', 'Basic Electronics', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(779, 'TLE 12', 'Entrepreneurship', 'Entrepreneurship', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(783, 'Educ 24', 'Personal & Community Hygiene', 'Personal & Community Hygiene', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(784, 'Sci 211', 'College Physics 1', 'College Physics 1', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(785, 'Sci 221', 'College Physics II', 'College Physics II', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(786, 'Math 101', 'Technical Math', 'Technical Math', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(787, 'Tech 101', 'Basic Electronics & Test Instruments', 'Basic Electronics & Test Instruments', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(788, 'ES 11', 'Fluid Mechanics', 'Fluid Mechanics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(789, 'EE 9b', 'Control System', 'Control System', 0, 3, 5, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(790, 'Chem 1f', 'General and Inorganic Chemistry II', 'General and Inorganic Chemistry II', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(791, 'Tech 101d', 'Rough Carpentry', 'Rough Carpentry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(792, 'Tech 102d', 'Concrete Work & Masonry Finishes', 'Concrete Work & Masonry Finishes', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(793, 'Tech 201d', 'Finishing Carpentry', 'Finishing Carpentry', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(794, 'Drwg 101', 'Introduction to Design', 'Introduction to Design', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(795, 'Drwg 102', 'Basic Project Design', 'Basic Project Design', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(796, 'Sci 21', 'Advanced Applied Chemistry', 'Advanced Applied Chemistry', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(797, 'Hum 11', 'Culture & Arts Appreciation', 'Culture & Arts Appreciation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(798, 'Envi 21', 'Intro to Environmental Management', 'Intro to Environmental Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(799, 'Phys 3', 'Fluids', 'Fluids', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(800, 'Fil 22', 'Filipino sa Tanging Gamit', 'Filipino sa Tanging Gamit', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(801, 'SocSci 102', 'Philippine History, Roots & Development', 'Philippine History, Roots & Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(802, 'Chem 2c', 'General Chemistry II', 'General Chemistry II', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(803, 'ES 111', 'Engineering Drawing 1', 'Engineering Drawing 1', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(809, 'ES 112', 'Engineering Drawing 2', 'Engineering Drawing 2', 0, 1, 2, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(810, 'SocSci 311', 'Phil. Socio-Economic Life', 'Phil. Socio-Economic Life', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(811, 'SocSci 33', 'Social Philosophy', 'Social Philosophy', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(812, 'Bio 2a', 'Anatomy and Physiology w/ Field Study', 'Anatomy and Physiology w/ Field Study', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(814, 'Tech 101a', 'Automotive Fundamentals', 'Automotive Fundamentals', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(816, 'Tech 102a', 'Engine System Acce. & Auto Elect''l System', 'Engine System Acce. & Auto Elect''l System', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(817, 'Tech 201a', 'Engine Servicing & Tune-up', 'Engine Servicing & Tune-up', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(818, 'Entrep 301', 'Operating Small Business', 'Operating Small Business', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(819, 'Tech 301a', 'Auto Body Repair', 'Auto Body Repair', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(820, 'Tech 302a', 'Fuel System: Injection & Calibration', 'Fuel System: Injection & Calibration', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(821, 'Tech 401a', 'Auto Air Conditioning', 'Auto Air Conditioning', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(822, 'Sci 301', 'Advanced Applied Physics I', 'Advanced Applied Physics I', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(823, 'Sci 302', 'Advanced Applied Physics II', 'Advanced Applied Physics II', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(824, 'Entrep 302', 'Case Studies', 'Case Studies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(825, 'SocSci 302', 'Labor Code & Cooperative Code', 'Labor Code & Cooperative Code', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(826, 'Comp 3c', 'Computer Fundamentals II', 'Computer Fundamentals II', 0, 1, 2, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(827, 'Elect 302', 'Technical Research', 'Technical Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(828, 'Elect 401', 'Project Study/Applied Research Writing', 'Project Study/Applied Research Writing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(829, 'SocSci 401', 'Constitution, Taxation & Land Reform', 'Constitution, Taxation & Land Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(830, 'Tech 1g', 'Rough Carpentry & Architectural Design', 'Rough Carpentry & Architectural Design', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(832, 'Tech 2g', 'Concrete Work & Architectural Design', 'Concrete Work & Architectural Design', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(833, 'Tech 102', 'Electronics Circuit & Application', 'Electronics Circuit & Application', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(834, 'Tech 201', 'Wire & Wireless Communication', 'Wire & Wireless Communication', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(835, 'Tech 301', 'TV Basics & Troubleshooting', 'TV Basics & Troubleshooting', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(837, 'ROTC 11', 'Military Science & Tactics', 'Military Science & Tactics', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(838, 'ROTC 12', 'Military Science & Tactics', 'Military Science & Tactics', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(839, 'Tech 101b', 'Basic Electricity', 'Basic Electricity', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(841, 'Tech 102b', 'Electrical Installation', 'Electrical Installation', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(843, 'Tech 201b', 'Electromechanical Installation', 'Electromechanical Installation', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(845, 'W.E. 102', 'Industrial Values', 'Industrial Values', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(846, 'Eng. 201', 'Effective Speech', 'Effective Speech', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(847, 'Psycho 201', 'Fundamentals of Psychology', 'Fundamentals of Psychology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(848, 'ROTC 21', 'Military Science and Tactics', 'Military Science and Tactics', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(849, 'ROTC 22', 'Military Science and Tactics', 'Military Science and Tactics', 0, 1.5, 1.5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(850, 'Tech 202', 'Electromechanical Construction', 'Electromechanical Construction', 0, 3, 5, 0, 2, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(851, 'Sci. 202', 'Basic Applied Physics', 'Basic Applied Physics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(854, 'Comp 14a', 'Database Management & Algorithm', 'Database Management & Algorithm', 0, 2, 4, 0, 2, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(855, 'Math 1f', 'Mathematics for Agriculture', 'Mathematics for Agriculture', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(856, 'NatSci 1b', 'Agricultural Biology', 'Agricultural Biology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(857, 'APT 1', 'Poultry', 'Poultry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(858, 'CPT 1', 'Field Crops Production & Management', 'Field Crops Production & Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(859, 'FMT 1', 'Agricultural Mechanics 1', 'Agricultural Mechanics 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(860, 'CPT 2', 'Soil Conservation & Management', 'Soil Conservation & Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(861, 'CPT 3', 'Horticultural Crops Production & Management', 'Horticultural Crops Production & Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(862, 'PHT 1', 'Basic Principles in Post Harvest Technology', 'Basic Principles in Post Harvest Technology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(863, 'FMT 2', 'Agricultural Mechanics 2', 'Agricultural Mechanics 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(864, 'APT 2', 'Swine', 'Swine', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(865, 'Pract 1', 'Practicum in Agriculture', 'Practicum in Agriculture', 0, 16, 16, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(866, 'SocSci 1', 'Rural Sociology', 'Rural Sociology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(867, 'APT 3', 'Ruminant', 'Ruminant', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(868, 'NatSci 3', 'Farm Physics', 'Farm Physics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(869, 'AM 1', 'Farm Business Management', 'Farm Business Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(871, 'FMT 3', 'Irrigation', 'Irrigation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(872, 'PHT 2', 'Post Harvest Handling System', 'Post Harvest Handling System', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(873, 'AgEcon 31', 'Farm Management', 'Farm Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(876, 'Tour 11a', 'Principles of Tourism 1', 'Principles of Tourism 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(877, 'CS 121', 'Intro to Information Technology', 'Intro to Information Technology', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(878, 'Eng 221', 'Literature 1', 'Literature 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(880, 'Tech102', 'Auto Power Train, Chassis & Suspension', 'Auto Power Train, Chassis & Suspension', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(883, 'Math 121', 'Analytic Geometry', 'Analytic Geometry', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(884, 'SocSci 211', 'Philippine History and New Constitution', 'Philippine History and New Constitution', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(886, 'OJT', 'On-the-Jon Training (160 Hrs)', 'On-the-Jon Training (160 Hrs)', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(887, 'IT 202', 'Accounting Principles', 'Accounting Principles', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(891, 'Music 1(Sec)', 'Music I', 'Music I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(892, 'Music 2 (Sec)', 'Music II', 'Music II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(893, 'Music 3 (Sec)', 'Music III', 'Music III', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(894, 'Music 4 (Sec)', 'Music 4', 'Music 4', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(895, 'Health 1 (Sec)', 'Health 1', 'Health 1', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(896, 'Health 2', 'Health 2', 'Health 2', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(897, 'Health 3 (Sec)', 'Health 3', 'Health 3', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(898, 'Health 2 (Sec)', 'Health 2', 'Health 2', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(899, 'Health 4 (Sec)', 'Health 4', 'Health 4', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(900, 'PE 1 (Sec)', 'PE 1', 'PE 1', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(901, 'PE 2 (Sec)', 'PE 2', 'PE 2', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(902, 'PE 3 (Sec)', 'PE 3', 'PE 3', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(903, 'PMT/PE 4 (Sec)', 'PMT/PE 4', 'PMT/PE 4', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(913, 'EE 411a', 'Electromechanical Conversion', 'Electromechanical Conversion', 0, 2, 4, 0, 2, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(914, 'ECE 411a', 'Integrated Circuit', 'Integrated Circuit', 0, 2, 4, 0, 2, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(920, 'ES 422a', 'Material Science', 'Material Science', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(923, 'ECE 511a', 'Propagation and Antennas', 'Propagation and Antennas', 0, 3, 4, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(926, 'Bio 1g', 'Natural Science', 'Natural Science', 0, 3, 4, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(927, 'EE 522', 'Electrical System Design', 'Electrical System Design', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(928, 'CS 111', 'Computer Typing', 'Computer Typing', 0, 0, 1, 0, 1, 3, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(929, 'CS 112', 'Introduction to Information Technology', 'Introduction to Information Technology', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(930, 'CS 122', 'Software Applications I', 'Software Applications I', 0, 0, 2, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(935, 'CS 212a', 'Software Application II', 'Software Application II', 0, 0, 2, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(936, 'Math 122', 'Differential Calculus with Analytic Geometry', 'Differential Calculus with Analytic Geometry', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(937, 'CS 221', 'Database Management System & File Organization', 'Database Management System & File Organization', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(938, 'CS 312', 'Operating System', 'Operating System', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(939, 'CS 321', 'Software Design & Development', 'Software Design & Development', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(943, 'CS 322', 'Computer Organization & Architecture', 'Computer Organization & Architecture', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(944, 'CS 324', 'Computer Workshop', 'Computer Workshop', 0, 0, 2, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(945, 'Math 322', 'Discrete Mathematics II', 'Discrete Mathematics II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(946, 'CS 412', 'Assembly Language Programming', 'Assembly Language Programming', 0, 1, 3, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(947, 'CS 415', 'Data Communication Networking', 'Data Communication Networking', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(950, 'CS 421', 'Seminar & Field Trip', 'Seminar & Field Trip', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(951, 'CS 424', 'Structure to Programming Languages', 'Structure to Programming Languages', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(952, 'CS 425', 'Research / Project', 'Research / Project', 0, 3, 6, 0, 3, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(953, 'Hum 311', 'Logic & Philosophy of Man', 'Logic & Philosophy of Man', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(954, 'Math 321', 'Numerical Methods & Analysis', 'Numerical Methods & Analysis', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(955, 'Engl 221', 'Oral Communication', 'Oral Communication', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(961, 'Tech 301c', 'Plumbing & House Wiring', 'Plumbing & House Wiring', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(962, 'Fil 11', 'Gamiting Filipino', 'Gamiting Filipino', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(963, 'PE 1a', 'Health & Self Test Activities', 'Health & Self Test Activities', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(965, 'Engl 1b', 'Essential of Writen & Oral Communication', 'Essential of Writen & Oral Communication', 0, 3, 3, 0, 0, 0, NULL, 0, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(966, 'Hele 1', 'Home Economics & Livelihood Education', 'Home Economics & Livelihood Education', 0, 2, 5, 0, 3, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(967, 'Fil 102A', 'Pagbasa at Pagsulat Tungo sa Pananaliksik', 'Pagbasa at Pagsulat Tungo sa Pananaliksik', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(968, 'NCM 101b (lec)', 'Care of Mother, Child & Fam', 'Care of Mother, Child & Fam', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(971, 'NCM 101b RLE', 'RLE (Nrsg Arts lab/Clinics&Hospital&Clinics)', 'RLE (Nrsg Arts lab/Clinics&Hospital&Clinics)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(972, 'NCM 102b (lec)', 'Care of Mother Chld Fam.&Pop. Grp at Risk', 'Care of Mother Chld Fam.&Pop. Grp at Risk', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(973, 'NCM 102b RLE', 'RLE (Nrsg. Arts Lab/Clinic/Comm.& Hsptl Exposure)', 'RLE (Nrsg. Arts Lab/Clinic/Comm.& Hsptl Exposure)', 0, 0, 6, 0, 6, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(975, 'Tech 301d', 'Industrial Control', 'Industrial Control', 0, 3, 5, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(976, 'SocSci 301', 'Phil. Industrial & Socio Economic Life', 'Phil. Industrial & Socio Economic Life', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(978, 'NCM 103b (lec)', 'Care of Clients: Prob in Oxygenation, Fluid&Elect', 'Care of Clients: Prob in Oxygenation, Fluid&Elect', 0, 8, 8, 0, 0, 144, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(980, 'NCM 103b RLE', 'RLE (Clinics/Hospital&Community)=306 hrs)', 'RLE (Clinics/Hospital&Community)=306 hrs)', 0, 0, 6, 0, 6, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(981, 'NCM 104b (lec)', 'Care of Clients /Prob. in Inflammatory&Immu. Res.', 'Care of Clients /Prob. in Inflammatory&Immu. Res.', 0, 5, 5, 0, 0, 90, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(982, 'NCM 104b RLE', 'RLE (Clinics/Hospital & Community = 204 Hrs)', 'RLE (Clinics/Hospital & Community = 204 Hrs)', 0, 0, 4, 0, 4, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(983, 'NCM 105a (lec)', 'Care of Clients w/Maladaptive Pat. of Behavior', 'Care of Clients w/Maladaptive Pat. of Behavior', 0, 4, 4, 0, 0, 72, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(984, 'NCM 105a RLE', 'RLE (Clinics/Hospital & Community = 102 hrs)', 'RLE (Clinics/Hospital & Community = 102 hrs)', 0, 0, 4, 0, 4, 102, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(985, 'PhilHist', 'Philippine History, Government & Constitution', 'Philippine History, Government & Constitution', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(986, 'NCM 106b (lec)', 'Care of Clients:Prob in Cellular Aber., Acute Bio', 'Care of Clients:Prob in Cellular Aber., Acute Bio', 0, 6, 6, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(987, 'NCM 106b RLE', 'RLE (Clinics/Hospital & Community = 255 hrs)', 'RLE (Clinics/Hospital & Community = 255 hrs)', 0, 0, 5, 0, 5, 14, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(988, 'NCM 107-A(lec)', 'Nursing Leadership & Management', 'Nursing Leadership & Management', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(989, 'NCM 107-A RLE', 'Nursing Leadership & Management (153 hrs)', 'Nursing Leadership & Management (153 hrs)', 0, 0, 3, 0, 3, 153, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(990, 'Elective 1a', 'Elective Course 1(Parent Child Nursing)', 'Elective Course 1(Parent Child Nursing)', 0, 2, 2, 0, 0, 36, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(991, 'Elective 2a', 'Elective Course 2(Quality Health Care & Nursing)', 'Elective Course 2(Quality Health Care & Nursing)', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(993, 'INP', 'Intensive Nursing Practicum (RLE = 408 Hrs)', 'Intensive Nursing Practicum (RLE = 408 Hrs)', 0, 0, 8, 0, 8, 32, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(994, 'HealthEd', 'Health Education', 'Health Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(998, 'NCM 107-B RLE', 'RLE (Clinics/Hospital & Community=153hrs)', 'RLE (Clinics/Hospital & Community=153hrs)', 0, 0, 3, 0, 3, 8, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(999, 'IT 200', 'On-the-Job Training (160 Hrs)', 'On-the-Job Training (160 Hrs)', 0, 3, 3, 0, 0, 0, NULL, 1, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(1000, 'CS 200', 'On-the-Job Training (160 Hrs)', 'On-the-Job Training (160 Hrs)', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1001, 'Tech 302b', 'Specifications & Est. Cons. Methods', 'Specifications & Est. Cons. Methods', 0, 3, 5, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1002, 'Educ 53', 'Human Growth, Learning and Development', 'Human Growth, Learning and Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1004, 'Educ 55', 'Test,Measurement and Evaluation', 'Test,Measurement and Evaluation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1005, 'Educ 51', 'Educ'' Psycho/Socio/Anthro', 'Educ'' Psycho/Socio/Anthro', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1006, 'Educ 56', 'Values Formation and Professional Ethics', 'Values Formation and Professional Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1008, 'LibSi', 'Elem. Ref.Works and Bibliography', 'Elem. Ref.Works and Bibliography', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1009, 'CS 111a', 'Computer Typing', 'Computer Typing', 0, 1, 2, 0, 1, 3, NULL, 0, NULL, '0001-01-01 00:00:00', '', NULL, NULL),
(1012, 'CS 212b', 'Software Applications II', 'Software Applications II', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1016, 'CS 312a', 'Computer Information System (Networking)', 'Computer Information System (Networking)', 0, 1, 2, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1017, 'CS 313', 'File Organization & Processing', 'File Organization & Processing', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1018, 'CS 422', 'Software Engineering', 'Software Engineering', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1020, 'CS 412a', 'Assembly Language Programming', 'Assembly Language Programming', 0, 2, 4, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1021, 'CS 413', 'Data Research & Report', 'Data Research & Report', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1022, 'CS 423', 'Management Information System', 'Management Information System', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1023, 'CS 426', 'Research / Project', 'Research / Project', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1024, 'Val 1', 'Peace and Global Education', 'Peace and Global Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1025, 'SpT 1b', 'Multi Grade Teaching', 'Multi Grade Teaching', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1026, 'MAPE 1', 'Personal, Community & Envi Health', 'Personal, Community & Envi Health', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1027, 'SpT 2a', 'Special Education', 'Special Education', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1028, 'SocSci 2', 'Trends & Issues in Social Studies', 'Trends & Issues in Social Studies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1029, 'PA 31', 'Home Economics / Livelihood Education 1', 'Home Economics / Livelihood Education 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1030, 'PA 32', 'Home Economics / Livelihood Education 2', 'Home Economics / Livelihood Education 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1033, 'Educ 23a', 'Foundation of Education 2', 'Foundation of Education 2', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1034, 'Bio 31', 'General Genetics, Evolution & Diversity', 'General Genetics, Evolution & Diversity', 0, 3, 5, 0, 2, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1035, 'Math 1g', 'College Mathematics', 'College Mathematics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1040, 'Engl 3d', 'Communication Arts 3', 'Communication Arts 3', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1041, 'Econ 1d', 'Introduction to Economics', 'Introduction to Economics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1043, 'Busn 21', 'Accounting Appreciation', 'Accounting Appreciation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1045, 'Comp 2a', 'Computer Application for the Hospitality Industry', 'Computer Application for the Hospitality Industry', 0, 3, 5, 0, 2, 1, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1046, 'Educ 139', 'Seminar in General Education', 'Seminar in General Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1047, 'Educ 140', 'Seminar in Professional Education', 'Seminar in Professional Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1048, 'NCM 101lec', 'Care of Mother, Child & Family', 'Care of Mother, Child & Family', 0, 4, 4, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1049, 'NCM 101RLE', 'RLE(Nrsg Arts Lab/Hospital)', 'RLE(Nrsg Arts Lab/Hospital)', 0, 0, 6, 0, 6, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1051, 'NCM 102Lec', 'Care of Mother, Child & Family', 'Care of Mother, Child & Family', 0, 5, 5, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1052, 'NCM 102RLE', 'RLE (Nrsg Arts Lab & Hospital Community)', 'RLE (Nrsg Arts Lab & Hospital Community)', 0, 0, 6, 0, 6, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1053, 'Agri 1', 'Introduction to Agriculture', 'Introduction to Agriculture', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1054, 'Engl 1c', 'Study, Thinking and Communication Skills', 'Study, Thinking and Communication Skills', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1055, 'SocSci 1a', 'Society & Culture with Familly Planning', 'Society & Culture with Familly Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1063, 'Econ 1e', 'Gen Econ with Taxation and Agrarian Reform', 'Gen Econ with Taxation and Agrarian Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1065, 'SocSci 2a', 'Philippine History, Government, Politics', 'Philippine History, Government, Politics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1066, 'AgEcon 1a', 'Agricultural Economics and Marketing', 'Agricultural Economics and Marketing', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1067, 'AnSci 1a', 'Introduction to Animal Science', 'Introduction to Animal Science', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1068, 'CropSci 1', 'Crop Growth and Development', 'Crop Growth and Development', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1069, 'Eng 2', 'Writing in the Discipline', 'Writing in the Discipline', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1070, 'Stat 1d', 'Agricultural Statistics', 'Agricultural Statistics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1071, 'AnSci 2', 'Intro to Livestock & Poultry Production Practices', 'Intro to Livestock & Poultry Production Practices', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1072, 'Comp 1a', 'Basic Computer Concept & Application', 'Basic Computer Concept & Application', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1073, 'CropSci 2', 'Principles & Practices of Crop Production', 'Principles & Practices of Crop Production', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1074, 'Phys 1h', 'General Physics', 'General Physics', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1075, 'AgExt 1', 'Extension Teaching & Community', 'Extension Teaching & Community', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1076, 'Agri 3', 'Introduction to Ecological Agriculture', 'Introduction to Ecological Agriculture', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1077, 'AnSci 3', 'Slaughter of Animals and Processing of Products', 'Slaughter of Animals and Processing of Products', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1078, 'CropProt 2', 'Approaches and Practices in Pest Management', 'Approaches and Practices in Pest Management', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1079, 'CropSci 3', 'Post Harvest Handling of Durables & Perishables', 'Post Harvest Handling of Durables & Perishables', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1080, 'Entrep 1a', 'Introduction to Enterprise and Entrepreneurship', 'Introduction to Enterprise and Entrepreneurship', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1082, 'Fil 2b', 'Pagbasa at Pagsulat sa Iba''t ibang Disiplina', 'Pagbasa at Pagsulat sa Iba''t ibang Disiplina', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1084, 'Chem 4a', 'Biochemistry', 'Biochemistry', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1085, 'AgriPract', 'Practicum in Agriculture (2nd summer)', 'Practicum in Agriculture (2nd summer)', 0, 0, 6, 0, 6, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1086, 'Agri 4', 'Methods of Agricultural Research', 'Methods of Agricultural Research', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1087, 'Agri 5', 'Beneficial Micro Org. & Arthropods', 'Beneficial Micro Org. & Arthropods', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1088, 'Soils 2', 'Soil Survey, Classification & Land Use', 'Soil Survey, Classification & Land Use', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1090, 'AM 1a', 'Basics of Project Study & Development', 'Basics of Project Study & Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1096, 'Philo 2c', 'Philosophy and Ethics', 'Philosophy and Ethics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1097, 'Maj.1 AnSci 6', 'Anatomy and Physiology of Farm Animals', 'Anatomy and Physiology of Farm Animals', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1098, 'Angri 6', 'Biotech and Society', 'Biotech and Society', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1099, 'AnSci 5', 'Animal Nutrition & Feeding', 'Animal Nutrition & Feeding', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1101, 'CropSci 4', 'Crop Physiology and Taxicology', 'Crop Physiology and Taxicology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1103, 'AM 2a', 'Agribusiness Commodity Systems', 'Agribusiness Commodity Systems', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1104, 'CropSci 5', 'Principles & Practices in Plant Breeding', 'Principles & Practices in Plant Breeding', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1105, 'Seminar A1', 'Special Problem I', 'Special Problem I', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1106, 'Soils 3', 'Soil Fert./Coservation Management', 'Soil Fert./Coservation Management', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1107, 'Maj.2 AnSci 7', 'Animal Diseases and Control', 'Animal Diseases and Control', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1108, 'Maj.2 AnSci 8', 'Poultry Production', 'Poultry Production', 0, 2, 3, 0, 1, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1110, 'Agri 7Pract', 'Practicum in Agriculture (3rd summer)', 'Practicum in Agriculture (3rd summer)', 0, 0, 6, 0, 6, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1111, 'AgEcon 3', 'Agricultural Policy Development', 'Agricultural Policy Development', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1112, 'AgEcon 6', 'Basic Farm Accounting', 'Basic Farm Accounting', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1113, 'AgEng''g 1', 'Basic Agricultural Engineering', 'Basic Agricultural Engineering', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1114, 'Sp 1a', 'Special Topics Related to Thesis 1', 'Special Topics Related to Thesis 1', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1118, 'Elective 1b', 'Animal Breeding', 'Animal Breeding', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1119, 'Major 9', 'Swine Production', 'Swine Production', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1120, 'AgEcon 4', 'Financial Management & Agri Based Enterprise', 'Financial Management & Agri Based Enterprise', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1121, 'Agri 8', 'Guided Review', 'Guided Review', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1123, 'AM 5a', 'Colloquium', 'Colloquium', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1124, 'AgEng 2', 'Agromereorology', 'Agromereorology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1127, 'CropSci 5a', 'Plant Propagarion/Nursery Management', 'Plant Propagarion/Nursery Management', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1129, 'Seminar A2', 'Special Problem II', 'Special Problem II', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1131, 'Sp 2a', 'Special Topics Related to Thesis 2', 'Special Topics Related to Thesis 2', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1137, 'Elective 2b', 'Small Ruminant Production', 'Small Ruminant Production', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1138, 'Major 11', 'Dairy Production', 'Dairy Production', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1139, 'Major 12', 'Beef and Carabeef Production', 'Beef and Carabeef Production', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1140, 'Eng 1', 'Basic Communication Skills', 'Basic Communication Skills', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL);
INSERT INTO `tblsubject` (`SubjectID`, `SubjectCode`, `SubjectTitle`, `Description`, `InActive`, `Units`, `CreditUnits`, `LecHrs`, `LabUnits`, `LabHrs`, `SubjectMode`, `IsNonAcademic`, `CreatedBy`, `CreationDate`, `ModifiedBy`, `ModifiedDate`, `LevelID`) VALUES
(1142, 'SocSci 1b', 'Sociology and Culture with Family Planning', 'Sociology and Culture with Family Planning', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1143, 'Econ 1f', 'Principles of Economics w/ Land Reform & Taxation', 'Principles of Economics w/ Land Reform & Taxation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1145, 'Math 2b', 'Trigonometry', 'Trigonometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1146, 'Lit 1', 'Philippine Literature in English', 'Philippine Literature in English', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1147, 'AgEcon 1b', 'Farm Business Management', 'Farm Business Management', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1148, 'Bio 7d', 'General Ecology', 'General Ecology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1149, 'Lit 2', 'World Literature', 'World Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1152, 'AE 1a', 'Engineering Applied to Agriculture', 'Engineering Applied to Agriculture', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1153, 'IPM 1a', 'Economic Entomology', 'Economic Entomology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1154, 'AF 1', 'Introduction to Agroforestry', 'Introduction to Agroforestry', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1155, 'AF 2', 'Agroforestry Farming System', 'Agroforestry Farming System', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1156, 'AE 2a', 'Agricultural Metereology', 'Agricultural Metereology', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1157, 'IPM 2a', 'Plant Pathology', 'Plant Pathology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1158, 'AF 3', 'Principles of Watershed Management', 'Principles of Watershed Management', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1159, 'AF 4', 'General Dendrology', 'General Dendrology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1160, 'AF 5', 'Seed Technology & Nursery Management', 'Seed Technology & Nursery Management', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1161, 'SF 1', 'Rural Sociology/Principles of Social Forestry', 'Rural Sociology/Principles of Social Forestry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1162, 'AFE 1', 'Agro Forestry Economics', 'Agro Forestry Economics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1163, 'AF 98', 'Methods of Research in Agroforestry', 'Methods of Research in Agroforestry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1164, 'AF 7a', 'Elective (Coastal Resource Management)', 'Elective (Coastal Resource Management)', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1165, 'AF 7b', 'Agroforestry Laws and Policies', 'Agroforestry Laws and Policies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1166, 'AF 8', 'Timber Management', 'Timber Management', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1167, 'AF 9', 'Range Management', 'Range Management', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1168, 'AF 10', 'General Silviculture', 'General Silviculture', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1169, 'SS 2', 'Soil Conservation', 'Soil Conservation', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1170, 'AF 15', 'Wood Processing and Technology', 'Wood Processing and Technology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1171, 'AF 20', 'Wood Science and Technology', 'Wood Science and Technology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1172, 'AF 100', 'Undergrad Thesis/Practicum', 'Undergrad Thesis/Practicum', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1173, 'AF', 'Elective (Surveying)', 'Elective (Surveying)', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1174, 'AF 99', 'Undergraduate Seminar', 'Undergraduate Seminar', 0, 1, 1, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1175, 'Eng 101', 'Thinking and Study Skills', 'Thinking and Study Skills', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1176, 'Bio 101', 'Introduction to Biological Sciences', 'Introduction to Biological Sciences', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1177, 'Pol Scie 101', 'Politics & Government w/ Philippine Constitution', 'Politics & Government w/ Philippine Constitution', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1178, 'Hum 101', 'Music & Arts', 'Music & Arts', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1180, 'Engl 102', 'Writing for Acdemic Purposes', 'Writing for Acdemic Purposes', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1181, 'Phy Scie 100', 'Introduction to Physical Sciences', 'Introduction to Physical Sciences', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1182, 'Ed 138', 'Peace Education', 'Peace Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1183, 'Engl 3a', 'Interactive English Listening, Speaking & Grammar', 'Interactive English Listening, Speaking & Grammar', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1184, 'Fil 3a', 'Masining na Pagpahayag', 'Masining na Pagpahayag', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1185, 'Fil 4a', 'Ang Panitikan ng Pilipinas', 'Ang Panitikan ng Pilipinas', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1186, 'PE 3b', 'Recreational Outdoor Activities', 'Recreational Outdoor Activities', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1187, 'Math 2a', 'Advanced Algebra ', 'Advanced Algebra ', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1190, 'Educ 16d', 'Principles of Teaching 1', 'Principles of Teaching 1', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1191, 'ICT 1', 'Basic ICT Applications', 'Basic ICT Applications', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1193, 'Educ 5a', 'The Teaching of Children''s Literature', 'The Teaching of Children''s Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1194, 'Chem 1', 'General and Inorganic Chemistry', 'General and Inorganic Chemistry', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1195, 'PE 4a', 'Fundamental Skills in Games & Sports', 'Fundamental Skills in Games & Sports', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1197, 'Educ 7a', 'Non Formal Education and Livelihood Education ', 'Non Formal Education and Livelihood Education ', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1198, 'Lit 2a', 'Masterpieces of World Literature', 'Masterpieces of World Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1199, 'Educ 31', 'The Eight-Week Curriculum', 'The Eight-Week Curriculum', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1200, 'Fil 7a', 'Mga Anyo ng Kontemporaryong Panitikang Filipino', 'Mga Anyo ng Kontemporaryong Panitikang Filipino', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1203, 'Math 4c', 'Plane and Solid Geometry', 'Plane and Solid Geometry', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1204, 'Phys 1i', 'Physics for Health Sciences', 'Physics for Health Sciences', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1211, 'Educ 7b', 'Guidance and Counselling Incl. spec. ed.', 'Guidance and Counselling Incl. spec. ed.', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1212, 'MAPE 1a', 'Foundation of MAPE', 'Foundation of MAPE', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1213, 'Engl 6a', 'The Structure of English', 'The Structure of English', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1214, 'Fil 5a', 'Pagpapahalagang Pampanitikan', 'Pagpapahalagang Pampanitikan', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1215, 'Math 10a', 'Analytic Geometry & Introduction Calculus', 'Analytic Geometry & Introduction Calculus', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1216, 'Bio 7e', 'Ecology', 'Ecology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1217, 'SocStud 1', 'Basic Geography', 'Basic Geography', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1218, 'Engl 4b', 'English for Specific Purposes', 'English for Specific Purposes', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1219, 'Math 8a', 'Problem Solving', 'Problem Solving', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1220, 'Sci 4', 'Astronomy', 'Astronomy', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1221, 'SocStud 2', 'Geography & Natural Resources of the Philippines', 'Geography & Natural Resources of the Philippines', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1222, 'Educ 35', 'Teaching Multigrade Classes', 'Teaching Multigrade Classes', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1223, 'Educ 37', 'Values Education', 'Values Education', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1224, 'Educ 19a', 'On-Campus Student Teaching', 'On-Campus Student Teaching', 0, 3, 6, 0, 3, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1225, 'Engl 100', 'English Grammar & Language Skills', 'English Grammar & Language Skills', 0, 0, 0, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1226, 'PE 101', 'Self-Testing Activities', 'Self-Testing Activities', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1231, 'Bio 3c', 'General Botany', 'General Botany', 0, 3, 5, 0, 2, 5, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1232, 'Bio 2b', 'General Zoology', 'General Zoology', 0, 3, 5, 0, 2, 5, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1233, 'Sci 1', 'History & Philosophy of Sciences', 'History & Philosophy of Sciences', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1235, 'Educ 36', 'Interactive Teaching Strategies', 'Interactive Teaching Strategies', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1236, 'Sci 2', 'Statistics for Sciences', 'Statistics for Sciences', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1238, 'Bio 7f', 'Ecology with Laboratory', 'Ecology with Laboratory', 0, 3, 5, 0, 2, 6, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1239, 'SciRes 1', 'Research in Science', 'Research in Science', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1240, 'Bio 4a', 'Cell Biology', 'Cell Biology', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1241, 'Bio 10', 'Biotechniques', 'Biotechniques', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1242, 'Phys 11', 'Optics', 'Optics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1243, 'Phys 12', 'Modern Physics', 'Modern Physics', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1245, 'Educ 20a', 'Off-Campus Student Teaching', 'Off-Campus Student Teaching', 0, 3, 6, 0, 3, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1247, 'PE 102', 'Fundamentals of Rhythm', 'Fundamentals of Rhythm', 0, 2, 2, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1248, 'Ed 111', 'Art Appreciation', 'Art Appreciation', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1249, 'Lit 101', 'Philippine Literature', 'Philippine Literature', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1250, 'Soc Sci 12', 'Basic Economics, Taxation & Agrarian Reform', 'Basic Economics, Taxation & Agrarian Reform', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1252, 'NRes 1RLE', 'Nursing Research 1', 'Nursing Research 1', 0, 0, 1, 0, 1, 51, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1254, 'CS 401a', 'Computer Aided Design (CAD)', 'Computer Aided Design (CAD)', 0, 0, 2, 0, 2, 4, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1255, 'Educ 21a', 'Practice Teaching', 'Practice Teaching', 0, 0, 6, 0, 6, 12, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1257, 'Educ 17', 'Principles of Teaching & Strategies w/ FS', 'Principles of Teaching & Strategies w/ FS', 0, 3, 4, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1259, 'Sci 11', 'Basic Applied Chamistry', 'Basic Applied Chamistry', 0, 2, 3, 0, 1, 2, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1260, 'Socio 4', 'Educational Sociology', 'Educational Sociology', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1261, 'Eng 13', 'Modern Communication I', 'Modern Communication I', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL),
(1263, 'Eng 14', 'Modern Communication II', 'Modern Communication II', 0, 3, 3, 0, 0, 0, NULL, 0, 'Admin', '2012-05-09 15:00:13', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblsubjectmode`
--

CREATE TABLE IF NOT EXISTS `tblsubjectmode` (
  `SubjectMode` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text CHARACTER SET latin1 NOT NULL,
  `ShortName` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`SubjectMode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `tblsubjectmode`
--

INSERT INTO `tblsubjectmode` (`SubjectMode`, `Description`, `ShortName`) VALUES
(1, 'Lecture  Only', ''),
(2, 'Laboratory Only', ''),
(3, 'Seminar Mode', ''),
(4, 'Independent Reading or Independent Study', ''),
(5, 'Clinical Internship or Medical Residency', ''),
(6, 'Apprenticeship or on the Job Training', ''),
(7, 'Lecture and Field Work', ''),
(8, 'Fieldwork Only', ''),
(12, 'Lecture and Laboratory', ''),
(67, 'Test3', 'Test2'),
(80, 'Distance Mode', ''),
(88, 'Test', 'Test'),
(89, 'Test2', 'Test2'),
(90, 'Thesis or Dissertation Writing', ''),
(99, 'No Information on the Matter', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblteacher`
--

CREATE TABLE IF NOT EXISTS `tblteacher` (
  `TeacherID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeID` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `LastName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `FirstName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `MiddleName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `MiddleInitial` varchar(6) CHARACTER SET latin1 DEFAULT NULL,
  `ExtName` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
  `Prefix` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
  `PosnTitleID` int(12) DEFAULT NULL,
  `CampusID` int(12) DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `PlaceOfBirth` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `Gender` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `TelNo` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `MobileNo` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `AddressNo` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Street` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `Barangay` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `TownCity` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `Province` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `ZipCode` int(12) DEFAULT NULL,
  `Photo` longblob,
  `Signature` longblob,
  `Inactive` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`TeacherID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=232 ;

--
-- Dumping data for table `tblteacher`
--

INSERT INTO `tblteacher` (`TeacherID`, `EmployeeID`, `LastName`, `FirstName`, `MiddleName`, `MiddleInitial`, `ExtName`, `Prefix`, `PosnTitleID`, `CampusID`, `DateOfBirth`, `PlaceOfBirth`, `Gender`, `TelNo`, `MobileNo`, `Email`, `AddressNo`, `Street`, `Barangay`, `TownCity`, `Province`, `ZipCode`, `Photo`, `Signature`, `Inactive`) VALUES
(1, '1234', 'Lammawin ', 'Venus', '', 'I.', '', 'Dr.', 1, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(2, 'a', 'Tagotongan', 'Queenie', 'E.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(3, 'A101', 'Salugsugan', 'Rogelio', NULL, '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(4, 'A4', 'Abbu', 'Perpetua', 'A', '', '', '', 116, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(5, 'A5', 'Agbayani', 'John', 'G.', '', '', 'Engr.', 31, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(6, 'A6', 'Remigoso', 'Elizabeth', 'H.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(7, 'A7', 'Palad', 'Salie', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(8, 'A8', 'Dela Victoria', 'Marcelino', 'V.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(9, 'AA', 'Agol', 'Adelfa', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(10, 'AAAA', 'Sugcay', 'Sammy', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(11, 'aaaaa', 'Mindoro', 'Fatima', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(12, 'AAQ061877', 'Quibedo', 'Arsenio Jr.', 'Abian', 'A.', '', 'Mr.', 116, 1, NULL, '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(13, 'AAS', 'Sagocsoc', 'Alfredo', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(14, 'AB', 'Bonsato', 'Analisa', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(15, 'AC1', 'Carillo', 'A.', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(16, 'ACB', 'Bonggot', 'Alfredo', 'C.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(17, 'ACL', 'Limbaco', 'Angelica', 'C.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(18, 'Admin', NULL, NULL, NULL, '', '', '', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(19, 'AF', 'Abanil', 'Felipe', 'G.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(20, 'AGA', 'Acot', 'Alona', 'G.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(21, 'AMB', 'Baclayo', 'Adenda', 'M.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(22, 'AMCP', 'Pajo', 'Ann Marie', 'C.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(23, 'amqs092478', 'Salvio', 'Ana Michele', 'Quibedo', 'Q.', '', 'Mrs.', 54, 1, '1978-09-24 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(24, 'AMT', 'MathTeacher', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(25, 'AMU', 'Usngan', 'Acmad', 'M.', '', '', '', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(26, 'ANM', 'Morados', 'Alexander', 'N.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(27, 'AO', 'Ocampo', 'Anna', 'P.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(28, 'ARC', 'Carrillo', 'Angelina', 'R.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(29, 'ASO', 'Ong', 'Aida', 'S.', '', '', 'Dr.', 26, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(30, 'AT', 'Tabid', 'Alfredo', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(31, 'ATTY', 'Tadlip', 'Luisito', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(32, 'AX', 'Estoye', 'Ryan', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(33, 'AZ', 'Acebes', 'Adones', '', '', '', '', 116, 1, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(34, 'B', 'Ebarle', 'Josefina', 'A', '', '', 'Dr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(35, 'BBA', 'Abucejo', 'Betulio', 'B.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(36, 'BBT', 'Odchigue', 'Conrado', 'F.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(37, 'BD', 'Domoloan ', 'Bernadette', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(38, 'BOG', 'Gaputan', 'Glenah Marie', 'M.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(39, 'BPA', 'Palarca', 'Bernarda', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(40, 'BTA', 'Teacher', 'B', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(41, 'C', 'Basco', 'Althemar', 'O.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(42, 'CA', 'Abbu', 'Cheryl', '', '', '', '', NULL, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(43, 'CBG', 'Galagar', 'Cerenia', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(44, 'CCC', 'Conzon', 'Claudie', 'C.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(45, 'CFA', 'Abregana', 'Crisusa', 'F.', '', '', '', 31, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(46, 'CGP', 'Palis', 'Conchita', 'G.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(47, 'CLA', 'Lerion', 'Carmela', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(48, 'CMM', 'Magto', 'Christi', 'M.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(49, 'COG', 'Gaputan', 'Consuelo', 'O.', '', '', '', 6, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(50, 'COR', 'Reyes', 'Consuelo', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(51, 'COT', 'ROTC1', 'E', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(52, 'CQ', 'Quilaton', 'Careen', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(53, 'CR', 'Cordero', 'R', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(54, 'CT', 'Teacher', 'C', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(55, 'CTC', 'Catugal', 'Catalino', 'T.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(56, 'CVIL', 'Lammawin', 'Crystal Vemil', 'I.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(57, 'D', 'Chan', 'Marieta', 'L.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(58, 'DGT', 'Tirariray', 'Dulce', 'G.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(59, 'DGT1', 'Tirariray1', 'Dulce', 'Galito-', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(60, 'DJAE', 'Ebarle', 'Josefina', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(61, 'DJD', 'Dosdos', 'Ditas', 'J.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(62, 'DK', 'Kionisala', 'Danilo', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(63, 'DP', 'Pabalate', 'Doris', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(64, 'DT', 'Teacher', 'D', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(65, 'DVLN', 'Noguera', 'Dhandi Vee', 'L.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(66, 'E', 'Salas', 'Gil', 'B.', '', '', 'Engr.', 31, 1, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(67, 'EA', 'AniÃ±on', 'Ermelinda', '', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(68, 'EBP', 'Panaligan', 'Eres', 'S.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(69, 'EBS', 'Sabuero', 'Edmund', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(70, 'Econ', 'Econ Teacher', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(71, 'ELG', 'Genelsa', 'Eustracio', 'L.', '', '', 'Dr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(72, 'Eng', 'Teacher', 'I', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(73, 'EOP', 'Popera', 'Edith', 'O.', '', '', 'Dr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(74, 'ETC', 'Cabigon', 'Erlinda', 'T.', '', '', 'Dr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(75, 'ETC1', 'Cabigon1', 'Erlinda', 'T', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(76, 'ETN', 'Nudalo', 'Evangeline', 'T.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(77, 'EZ', 'Zaballero', 'Eunice', '', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(78, 'F', 'Tabalba', 'Daisy', 'Y', 'Y.', '', '', NULL, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(79, 'F*R', 'SolaÃ±a', 'Frederick', '', '', '', 'Mr.', 116, NULL, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(80, 'Fil', 'Filipino', 'T', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(81, 'FLB', 'Bolo', 'Felisa', 'L.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(82, 'FLBM', 'Bolo, MAEd', 'Felisa', 'B.', '', '', '', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(83, 'FLC', 'Cabeguin', 'Felicisimo', 'L.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(84, 'FN', 'Teacher', 'J', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(85, 'G', 'NSTP', ' ', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(86, 'GA', 'Agol', 'Gliceria', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(87, 'GB', 'Baal', 'Genalyn', 'B.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(88, 'GBF', 'Fabe', 'Gilda', 'B.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(89, 'GBJ', 'Jabines', 'Gerry', NULL, '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(90, 'GC', 'Guidance ', 'Teacher', 'L.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(91, 'GCO', 'Olavides', 'Gail', 'C.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(92, 'GDC', 'Cutab', 'Gerlie', 'D.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(93, 'GE', 'Entrada', 'Grace', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(94, 'GLL020272', 'Lopoy', 'Glenda', 'Ladesma', 'L.', '', 'Mrs.', 114, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(95, 'H', 'Mantiza', 'Joseph', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(96, 'HLPL', 'Labadan', 'Holly Louella', 'P.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(97, 'IE', 'No Teacher', 'IT', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(98, 'IEd A', 'Teacher', 'A', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(99, 'IML012158', 'Labadan', 'Inecito', 'Mabale', 'M.', '', 'Mr.', 38, 1, NULL, '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(100, 'IVS', 'Soliven,', 'Ireneo Jr.', 'V.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(101, 'J', 'Agbayani', 'Juvy', 'M.', '', '', '', 32, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(102, 'JAB', 'Bonggot', 'Jofrey', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(103, 'JAP', 'Peras', 'Jasmine', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(104, 'JAS', 'Sampurna', 'Jaharra', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(105, 'JB', 'Baldelovar', 'Jessie', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(106, 'JBT', 'Tenefrancia', 'Julita', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(107, 'JC', 'Cabigon', 'Jaimi', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(108, 'JD', 'Ganas', 'Janice', 'Dagoc', 'D.', '', '', NULL, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(109, 'JDP', 'Pacuribot', 'Jesus', 'D.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(110, 'JF', 'Fabe', 'Jarbie', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(111, 'JFM', 'Mongaya', 'Jasper', 'F.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(112, 'JFM051182', 'Mongaya', 'Jasper', 'FabiaÃ±a', 'F.', '', 'Mr.', 58, 1, NULL, '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(113, 'JGS', 'Sabellina', 'Jenelyn', 'G.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(114, 'JJ', 'Javillonar', 'Jexter', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(115, 'JL', 'Lohmeyer', 'Jude', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(116, 'JLG', 'Dagoc', 'Roy', ' ', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(117, 'Jo', 'Gorial', 'Joefel', 'L.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(118, 'JOD', 'Daroy', 'Joselito', 'O.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(119, 'JOL', 'Limbaco', 'Jimmy', 'O.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(120, 'JR', 'Jurial', 'Roselyn', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(121, 'JRAH032887', 'Hora', 'Joann Rowela', 'Andalan', 'A.', '', 'Ms.', 115, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(122, 'JRJ', 'Jael', 'Jayson Ray', 'P.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(123, 'JST031779', 'Tabamo', 'Jean', 'Somobay', 'S.', '', 'Mrs.', 56, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(124, 'jyc', 'Canatoy', 'Joyce', 'Y.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(125, 'KBR', 'Romualdo', 'Kurt Bryan', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(126, 'KQ', 'Teacher', 'H', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(127, 'L', 'Math 2', ' ', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(128, 'LBC', 'Cabel', 'Lendel', 'B', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(129, 'LET', 'LET Review', 'Teacher', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(130, 'LHA', 'Abucejo', 'Lucia', 'H.', '', '', '', 31, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(131, 'LIL', 'Lopena', 'Lailane', 'Ilago', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(132, 'LLD062660', 'Diaz', 'Letty', 'Limbaco', 'L.', '', 'Mrs.', 66, 1, '1960-06-26 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(133, 'LLT', 'Tadle', 'Liezel', 'L.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(134, 'LRU', 'Uayan', 'Lorna', 'Ranara', 'R.', '', '', 31, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(135, 'LS', 'Sumalinog', 'Leandro', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(136, 'M', 'ElbiÃ±a', 'Janette', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(137, 'MBO041065', 'Oclarit', 'Macario', 'Bacor', 'B.', '', 'Dr.', 72, 1, NULL, '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(138, 'MBS', 'Soliven', 'Milagrosa', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(139, 'MC', 'Cagatin', 'Marymill', '', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(140, 'MCT', 'Tamisan', 'Marlowe', 'C.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(141, 'MDA', 'Alianza', 'Maria  Milagros', 'D.', '', '', '', 4, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(142, 'MDGT', 'Tirariray', 'Mae Dil', 'G.', '', '', '', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(143, 'MDR', 'Delos Reyes', 'Melchora', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(144, 'MDS', 'Salcedo', 'Ma Louella', 'D.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(145, 'MNL050879', 'Lacierda', 'Michelle', 'Noguera', 'N.', '', 'Mrs.', 49, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(146, 'MP', 'Palmere', 'Marilou', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(147, 'MPA', 'Palad', 'Mark', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(148, 'MRM', 'Modina', 'Menoel', 'R.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(149, 'N', 'Cabilogan', 'Cecilio', 'I.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(150, 'NON', '-', '-', '-', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(151, 'not', 'NO TEACHER', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(152, 'not1', 'NO TEACHER 1', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(153, 'NS', 'Siaboc', 'Nena', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(154, 'NSTP', 'CWTS', 'IN', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(155, 'NU', 'Uba', 'Noreen', 'A.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(156, 'O', 'Galas', 'Vergie', 'Golez', 'G.', '', 'Mrs.', 111, 1, '1954-09-13 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(157, 'OES', 'Sabuga', 'Orson', 'E.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(158, 'Off', 'Conzon1', 'Claudie', 'Cimacio-', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(159, 'Off1', 'Tenefrancia1', 'Beltran', 'Bacong-', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(160, 'Off2', 'Labadan1', 'Rey', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(161, 'Off3', 'Fabe1', 'Jarb', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(162, 'OJT', 'OJT', 'T', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(163, 'OJT 2', 'OJT1', 'T', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(164, 'OJT 3', 'OJT2', 'E', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(165, 'OJT1', 'O', 'JT', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(166, 'ORTP', 'Paderanga', 'Oliver Ratunil', 'T.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(167, 'P', 'Socio 1b', ' ', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(168, 'PE 1', 'PE Teacher', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(169, 'pl', 'NO  Teacher', 'ICS', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(170, 'PT', 'Practice1', 'Teacher', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(171, 'Q', 'Teacher Philo', 'a', 'b', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(172, 'QQ', 'Teacher', 'G', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(173, 'R', 'Genelsa', 'Mauricia', 'G.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(174, 'RAL', 'Ladesma', 'Remedios', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(175, 'RAN', 'Nakila', 'Rudylito', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(176, 'RAT', 'Tongco', 'Romeo', 'A.', '', '', 'Engr.', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(177, 'RBA', 'Ampatin', 'Rogelio', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(178, 'RBG', 'Gomez', 'Rachelle', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(179, 'RBP', 'Pescadero', 'Ronald', 'B.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(180, 'RBR', 'Redondo', 'Ricky', 'B.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(181, 'RC', 'Capricho', 'Rebecca', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(182, 'RCP', 'Perez', 'Rolando', 'C.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(183, 'RCT', 'CastaÃ±os', 'Ruby', '', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(184, 'RD', 'Dagoc', 'Rene', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(185, 'RFC', 'Calope', 'Rafael', NULL, '', '', 'Dr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(186, 'RFIA', 'Flordelis', 'Reggie', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(187, 'RFM', 'Margelino Jr.', 'Reynaldo', 'F.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(188, 'RJD', 'Dadang', 'Raquelyn', 'J.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(189, 'RLM', 'Galinada', 'Mirra', 'N.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(190, 'RLS121073', 'Sagocsoc', 'Rechie', 'Lagunay', 'L.', '', 'Mrs.', 114, 1, NULL, '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(191, 'RLV012479', 'Vios', 'Roland', 'Ladio', 'L.', '', 'Mr.', 31, 1, '1979-01-24 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(192, 'RN', 'Narita', 'Rovelito', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(193, 'ROTC', 'IT', 'R', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(194, 'RP', 'Pabua', 'Ralph', NULL, '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(195, 'RR', 'Remigoso', 'Raul', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(196, 'RRGL052878', 'Limbaco', 'Roque Rendelle', 'Gabe', 'G.', '', 'Mr.', 53, 1, NULL, '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(197, 'RRR000000', 'Roa', 'Rolando', '', '', '', 'Mr.', 116, NULL, NULL, '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(198, 'RSP', 'Apugan', 'Roberto', 'S.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(199, 'RTD', 'Macaranas', 'Wilfie', 'B', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(200, 'RVL', 'Labadan', 'Rey Vincent', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(201, 'RZ', 'Zerrudo', 'Rosalie', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(202, 'S', 'Amor', 'Pepito', 'T.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(203, 'SAW', 'Wong', 'Sally', 'A.', '', '', '', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(204, 'SEO', 'Sabuga1', 'Orson', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(205, 'SG', 'Gempero', 'Sarah', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(206, 'SGB', 'Salas', 'Gil', 'Bulawin', 'B.', '', 'Engr.', 31, 1, '1900-01-01 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(207, 'SJB', 'Babarin', 'Jono', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(208, 'SMB', 'Bautista', 'Salvi', 'M.', '', '', 'Engr.', 116, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(209, 'ST', 'Practice', 'Teacher', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(210, 'T', 'SpT1', '', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(211, 'tc', 'ROTC ', 'E', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(212, 'tham', 'Math Teacher', 'X', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(213, 'TJO', 'JT', 'O', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(214, 'TMN', 'Navarro', 'Thea Mae', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(215, 'U', 'NatSci', 'P', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(216, 'V', 'Realista', 'Maya', 'N.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(217, 'VC', 'Villagracia', 'C', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(218, 'VGT', 'Tamisan', 'Vilma', 'G.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(219, 'VIL', 'Lammawin', 'Venus', 'I.', '', '', 'Dr.', 4, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(220, 'VIL 1', 'Lammawin1', 'Venus ', 'Irving-', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(221, 'VVL', 'Lumacang', 'Vidie  Victor', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(222, 'W', 'Ladera', 'Rulyn', 'R.', '', '', '', 26, 1, '1900-01-01 00:00:00', '', 'F', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(223, 'WBB', 'Badoy', 'Wilfredo ', 'B.', '', '', 'Engr.', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(224, 'WX', 'X', 'C.I.', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(225, 'X', 'Anatomy', 'P', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(226, 'Y', 'Quibedo,', 'Arsenio Jr.', 'A.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(227, 'YD', 'Kionisala', 'Yolanda', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(228, 'YT', 'Teacher', 'Y', NULL, '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(229, 'YVN', 'Yamba', 'Victor NiÃ±o', 'Pendinito', 'P.', '', 'Mr.', 31, NULL, '1989-01-19 00:00:00', '', 'M', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(230, 'YYY', 'Tinoy', 'Benjie', 'Dinapo', '', '', '', NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0),
(231, 'ZSD', 'Dailo', 'Zorahayda', 'S.', '', '', NULL, NULL, 1, '1900-01-01 00:00:00', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE IF NOT EXISTS `tblusers` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_username` varchar(50) DEFAULT NULL,
  `e_password` varchar(32) DEFAULT NULL,
  `mug_id` int(11) DEFAULT NULL,
  `e_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  KEY `fk_employeeid_idx` (`e_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`u_id`, `u_username`, `e_password`, `mug_id`, `e_id`) VALUES
(9, 'vil', 'vil', 3, 1),
(10, 'QT', 'QT', 3, 2),
(11, 'admin', 'admin', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblyearlevel`
--

CREATE TABLE IF NOT EXISTS `tblyearlevel` (
  `YearLevelID` int(11) NOT NULL AUTO_INCREMENT,
  `YearLevelTitle` varchar(20) DEFAULT NULL,
  `YearLevelDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`YearLevelID`),
  UNIQUE KEY `YearLevelTitle_UNIQUE` (`YearLevelTitle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tblyearlevel`
--

INSERT INTO `tblyearlevel` (`YearLevelID`, `YearLevelTitle`, `YearLevelDesc`) VALUES
(1, '1st Year', '1st Year standing'),
(2, '2nd Year', '2nd Year standing'),
(3, '3rd Year', '3rd Year standing'),
(4, '4th Year', '4th Year standing'),
(5, '5th Year', '5th Year standing'),
(6, '6th Year', '6th Year standing'),
(7, '7th Year', '7th Year standing'),
(8, '8th Year', '8th Year standig'),
(9, '9th Year', '9th Year standing'),
(10, 'Graduate', 'Graduate Studies');

-- --------------------------------------------------------

--
-- Table structure for table `tblyearlevelterm`
--

CREATE TABLE IF NOT EXISTS `tblyearlevelterm` (
  `ID` int(10) unsigned NOT NULL,
  `TermCode` int(10) unsigned NOT NULL,
  `YearLevelID` int(10) unsigned NOT NULL,
  `YearTermDesc` varchar(45) DEFAULT NULL,
  `Remarks` varchar(45) DEFAULT NULL,
  `YearStatus` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblyearlevelterm`
--

INSERT INTO `tblyearlevelterm` (`ID`, `TermCode`, `YearLevelID`, `YearTermDesc`, `Remarks`, `YearStatus`) VALUES
(1, 1, 1, '1st Year - 1st Semester', 'Higher Education', 11),
(2, 1, 1, '1st Year - 2nd Semester', 'Higher Education', 12),
(3, 1, 1, '1st Year - Summer', 'Higher Education', 13),
(4, 1, 2, '2nd Year - 1st Semester', 'Higher Education', 21),
(5, 1, 2, '2nd Year - 2nd Semester', 'Higher Education', 22),
(6, 1, 2, '2nd Year - Summer', 'Higher Education', 23),
(7, 1, 3, '3rd Year - 1st Semester', 'Higher Education', 31),
(8, 1, 3, '3rd Year - 2nd Semester', 'Higher Education', 32),
(9, 1, 3, '3rd Year - Summer', 'Higher Education', 33),
(10, 1, 4, '4th Year - 1st Semester', 'Higher Education', 41),
(11, 1, 4, '4th Year - 2nd Semester', 'Higher Education', 42),
(12, 1, 4, '4th Year - Summer', 'Higher Education', 43),
(13, 1, 5, '5th Year - 1st Semester', 'Higher Education', 51),
(14, 1, 5, '5th Year - 2nd Semester', 'Higher Education', 52),
(15, 1, 5, '5th Year - Summer', 'Higher Education', 53),
(16, 1, 6, '6th Year - 1st Semester', 'Higher Education', 61),
(17, 1, 6, '6th Year - 2nd Semester', 'Higher Education', 62),
(18, 1, 6, '6th Year - Summer', 'Higher Education', 63),
(19, 1, 7, '7th Year - 1st Semester', 'Higher Education', 61),
(20, 1, 7, '7th Year - 2nd Semester', 'Higher Education', 62),
(21, 1, 7, 'Graduate Programs', 'Higher Education', 0),
(22, 1, 1, 'Elective Courses', '', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `module_access_rights`
--
ALTER TABLE `module_access_rights`
  ADD CONSTRAINT `mmi_idp` FOREIGN KEY (`mmi_id`) REFERENCES `module_menu_items` (`mmi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mug_idp` FOREIGN KEY (`mug_id`) REFERENCES `module_user_groups` (`mug_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `module_menu_items`
--
ALTER TABLE `module_menu_items`
  ADD CONSTRAINT `fk_module_menu_items_1` FOREIGN KEY (`mmg_id`) REFERENCES `module_menu_groups` (`mmg_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tblbuilding`
--
ALTER TABLE `tblbuilding`
  ADD CONSTRAINT `FK_CampusID` FOREIGN KEY (`CampusID`) REFERENCES `tblcampus` (`CampusID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblroom`
--
ALTER TABLE `tblroom`
  ADD CONSTRAINT `FK_RoomTypeID` FOREIGN KEY (`RoomTypeID`) REFERENCES `tblroomtypes` (`RoomTypeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD CONSTRAINT `fk_employeeid` FOREIGN KEY (`e_id`) REFERENCES `tblteacher` (`TeacherID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
